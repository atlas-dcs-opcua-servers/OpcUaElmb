#Create OpcUaServer msi
param (
	[string]$config="Release",
	[string]$tag,
	[string]$type="Win64"
)

Write-output "Windows build script for OpcUaCanOpen project"

if (Test-Path -Path CMakeCache.txt) {Remove-item CMakeCache.txt }
if (Test-Path -Path CMakeFiles) { Remove-item CMakeFiles -Recurse }

#if ($tag -eq [string]::Empty) {
#	$verr="2"
#}
#else {
#	$v=$tag.replace("cc7","")
#	$verr=$v.replace("-",".")
#}

& ".\opcuacanopentag.ps1" -tag $tag

Write-output "Version is $env:OpcUaCanOpen_tag Config is $config"

$ver = '#define VERSION_STR "'+$env:OpcUaCanOpen_tag+'"'
$ver | Out-File Server/src/Version.h -encoding ascii

$tagchange = 'CPACK_PACKAGE_VERSION "'+$env:OpcUaCanOpen_tag+'"'

(get-content bin/ServerConfig.xml.windows) -replace "<SoftwareVersion>([0-9.]+)</SoftwareVersion>","<SoftwareVersion>$env:OpcUaCanOpen_tag</SoftwareVersion>" | Set-content bin/ServerConfig.xml
$Command ="cmake"
$buildtype='-DCMAKE_BUILD_TYPE="'+$config+'"'+' -DCPACK_PACKAGE_VERSION="'+$env:OpcUaCanOpen_tag+'"'
#Get-ChildItem -Path 'C:/Program Files/UaSdk/lib' -Recurse

#Get-ChildItem -Path  'D:/3rdPartySoftware/unified-automation/unified-automation-3rd-party/win64/vs2017/openssl/inc32' -Recurse -Include *.dll
#Get-ChildItem -Path  'D:/3rdPartySoftware/unified-automation/unified-automation-3rd-party/win64/vs2017/openssl/out32dll' -Recurse -Include *.dll
#Get-ChildItem -Path  'C:/Program Files/UaSdk' -Recurse -Include *.lib | % { $_.FullName }
#Get-ChildItem -Path  'C:/Program Files/UaSdk' -Recurse -Include *.dll | % { $_.FullName }
#Get-ChildItem -Path  'D:/3rdPartySoftware/openssl/openssl-1.0.2r' -Recurse -Include *.lib | % { $_.FullName }
#(Get-Item D:/3rdPartySoftware/unified-automation/unified-automation-3rd-party/win64/vs2017/openssl/out32dll/libeay32.dll).Target
if ($type -eq "Win32") {
	$codeGenerator='-G "Visual Studio 15 2017"'
}
else {
	$codeGenerator='-G "Visual Studio 15 2017 $type"'
}
$par1=@(".",$codeGenerator,$buildtype,"-Wno-dev")
iex "& $Command $par1"
$par2=@("--build",".","--config $config")
iex "& $Command $par2"
if ($config -eq "Release") {
	cpack --verbose -G WIX -C $config
}
