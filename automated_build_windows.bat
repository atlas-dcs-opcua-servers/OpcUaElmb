@ECHO OFF

rem ===============================================================================================

@echo "Windows build script for OpcUaCanOpen project"
@ECHO %1
SET "DEBUG_OR_RELEASE=%1"

if "%1" == ""	SET "DEBUG_OR_RELEASE=Release"

@echo "Building configuration: %DEBUG_OR_RELEASE%     (If you want the other, pass debug or release as an argument)"

if EXIST "CMakeCache.txt" del CMakeCache.txt
if EXIST "CMakeFiles" del /F /Q .\CMakeFiles

if NOT "%2" == "" (
	VERSION_STR=%2
	echo "#define VERSION_STR \"VERSION_STR\"" > Server/src/Version.h
	copy bin/ServerConfig.xml.windows bin/ServerConfig.xml
	get-content bin/ServerConfig.xml | %{$_ --repace "<SoftwareVersion>[0-9.a-zA-Z]*<\/SoftwareVersion>","<SoftwareVersion>%VERSION_STR%<\/SoftwareVersion>"}
	get-content CMakeLists.txt | %{$_ --repace "CPACK_PACKAGE_VERSION \"[0-9.a-zA-Z]*\" ","CPACK_PACKAGE_VERSION %VERSION_STR%"}

)
cmake . -G "Visual Studio 15 2017" -DCMAKE_BUILD_TYPE="%DEBUG_OR_RELEASE%" -DBOOST_ROOT="%2"

cmake --build . --config "%DEBUG_OR_RELEASE%" 
rem #cpack --verbose -G WIX



