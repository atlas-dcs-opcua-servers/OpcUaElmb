/**********************************************************
 * Ids.hgen -- do not modify
 **********************************************************
 * Generated from .\Server\opcuacanopenserver.csv with script .\generate_nodeids.py
 * on host PCATICSTEST09 by user vfilimon at 2016-09-19 03:54:04
 **********************************************************/
 
#ifndef IDS_H_
#define IDS_H_

#define BA_CANOBJECT 1002 // ObjectType
#define BA_CANBUS 1003 // ObjectType
#define BA_CANNODETYPE 1004 // ObjectType
#define BA_EMERGENCY 1005 // ObjectType
#define BA_PDOOBJECT 1006 // ObjectType
#define BA_SDOOBJECT 1007 // ObjectType
#define BA_SEQUENCEPROGRAM 1009 // ObjectType
#define BA_SEQMETHOD 1003 // ObjectType
#define BA_SDOITEMTYPE 2003 // VariableType
#define BA_SDOITEMTYPE1 2004 // VariableType
#define BA_SDOITEMTYPE2 2005 // VariableType
#define BA_SDOITEMTYPE3 2006 // VariableType
#define BA_PDOITEM 2007 // VariableType
#define BA_HASADDRESS 4002 // ReferenceType
#define BA_HASVALUE 4003 // ReferenceType
#define BA_CANOBJECT_NMT 6001 // Variable
#define BA_CANBUS_SYNCHINTERVAL 6002 // Variable
#define BA_CANBUS_NODEGUARDINGINTERVAL 6003 // Variable
#define BA_CANBUS_PORTERROR 6004 // Variable
#define BA_CANBUS_SYNCHCOMMAND 6005 // Variable
#define BA_CANBUS_PORT 6006 // Variable
#define BA_CANBUS_TYPE 6007 // Variable
#define BA_CANBUS_SPEED 6008 // Variable
#define BA_CANBUS_INITNMT 6009 // Variable
#define BA_CANNODETYPE_STATE 6010 // Variable
#define BA_CANNODETYPE_BOOTUP 6011 // Variable
#define BA_EMERGENCY_EMRGENCYERRORCODE 6012 // Variable
#define BA_EMERGENCY_ERROR 6013 // Variable
#define BA_EMERGENCY_SPECIFICERRORCODEBYTE1 6014 // Variable
#define BA_EMERGENCY_SPECIFICERRORCODEBYTE2 6015 // Variable
#define BA_EMERGENCY_SPECIFICERRORCODEBYTE3 6016 // Variable
#define BA_EMERGENCY_SPECIFICERRORCODEBYTE5 6017 // Variable
#define BA_EMERGENCY_SPECIFICERRORCODEBYTE4 6018 // Variable
#define BA_CANNODETYPE_TYPE 6019 // Variable
#define BA_CANNODETYPE_NODEID 6020 // Variable
#define BA_CANNODETYPE_INITNMT 6021 // Variable
#define BA_PDOOBJECT_COBID 6022 // Variable
#define BA_PDOOBJECT_ACCESS 6023 // Variable
#define BA_PDOOBJECT_NUMCH 6024 // Variable
#define BA_PDOOBJECT_RTR_COMMAND 6025 // Variable
#define BA_SDOOBJECT_INDEX 6026 // Variable
#define BA_SDOITEMTYPE_TYPE 6030 // Variable
#define BA_SDOITEMTYPE_ACCESS 6031 // Variable
#define BA_SDOITEMTYPE_TIMEOUT 6032 // Variable
#define BA_SDOITEMTYPE1_BASEDATAVARIABLE 6033 // Variable
#define BA_SDOITEMTYPE2_INDEX 6034 // Variable
#define BA_SDOITEMTYPE2_SUBINDEX 6035 // Variable
#define BA_SDOITEMTYPE3_NUMBER 6036 // Variable
#define BA_PDOOBJECT_COBID 6037 // Variable
#define BA_PDOOBJECT_ACCESS 6038 // Variable
#define BA_PDOOBJECT_NUMCH 6039 // Variable
#define BA_PDOITEM_BYTEINDEX 6040 // Variable
#define BA_PDOITEM_BIT 6041 // Variable
#define BA_SEQMETHOD_COMMAND 6042 // Variable
#define BA_SEQMETHOD_ADDRESS 6043 // Variable
#define BA_SEQMETHOD_VALUE 6044 // Variable
#define BA_CANOBJECT_START 7001 // Method
#define BA_CANOBJECT_STOP 7002 // Method
#define BA_CANOBJECT_RESET 7003 // Method
#define BA_CANOBJECT_PREOPERATION 7004 // Method
#define BA_CANBUS_SYNCH 7005 // Method
#define BA_PDOOBJECT_RTR 7006 // Method
#define BA_SEQUENCEPROGRAM_SEQMETHODS 7007 // Method

#endif /* IDS_H_ */
