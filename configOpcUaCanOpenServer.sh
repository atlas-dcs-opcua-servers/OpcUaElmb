echo "Setting CAP_NET_ADMIN on Server's binary..."
/usr/sbin/setcap cap_net_admin=ep /opt/OpcUaCanOpenServer/bin/OpcUaCanOpenServer
/usr/sbin/setcap cap_net_admin=ep /opt/OpcUaCanOpenServer/bin/libsockcan.so
if [ -f "/opt/OpcUaCanOpenServer/bin/libancan.so" ]; then
	/usr/sbin/setcap cap_net_admin=ep /opt/OpcUaCanOpenServer/bin/libancan.so
fi
echo "Creating ld.so entry..."
/bin/cp -a /opt/OpcUaCanOpenServer/bin/OpcUaCanOpen.conf /etc/ld.so.conf.d
echo "Running ldconfig..."
/sbin/ldconfig
echo "Generating OPC UA Server Certificate..."
/opt/OpcUaCanOpenServer/bin/OpcUaCanOpenServer -cs