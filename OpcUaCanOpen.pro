#
# Qmake 'pro' file for all OpcUaCanOpen subprojects
#
# For Windows:
# to generate the Visual Studio solution file and project files:
#   qmake -tp vc OpcUaCanOpen.pro -r
# or,
# using the Qt Visual Studio Add-In, select "Open Qt Project File..."
#
# For Linux:
#   qmake OpcUaCanOpen.pro -r

CONFIG -= qt core gui 
CONFIG += debug_and_release

TEMPLATE = subdirs

# The various subprojects
SUBDIRS += Can/CanBusAccess/CanBusAccess.pro
SUBDIRS += Configuration/Configuration.pro
SUBDIRS += Server/Server.pro
win32 {
	!exists("C:/Program Files/Kvaser/CANLIB/Lib/MS/canlib32.lib") {
		exists("C:/Program Files (x86)/Kvaser/CANLIB/Lib/MS/canlib32.lib") {
			SUBDIRS += CanInterface/kvcan/kvcan.pro
		}
		!exists("C:/Program Files (x86)/Kvaser/CANLIB/Lib/MS/canlib32.lib") {
				message("The Kvser driver has not been installed")
		}
	}
	exists("C:/Program Files/Kvaser/CANLIB/Lib/MS/canlib32.lib") {
		SUBDIRS += CanInterface/kvcan/kvcan.pro
	}

	!exists("C:/Program Files/SYSTEC-electronic/USB-CANmodul Utility Disk/Lib/usbcan64.lib") {
		exists("C:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Lib/usbcan64.lib") {
			SUBDIRS += CanInterface/stcan/stcan.pro
		}
		!exists("C:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Lib/usbcan32.lib") {
				message("The Systec driver has not been installed")
		}
	}
	exists("C:/Program Files/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Lib/usbcan32.lib") {
		SUBDIRS += CanInterface/stcan/stcan.pro
	}

	exists("C:/Program Files/PCAN/PCAN-Basic API/Win32/VC_LIB/PCANBasic.lib") {
		SUBDIRS += CanInterface/pkcan/pkcan.pro
	}
	!exists("C:/Program Files/PCAN/PCAN-Basic API/Win32/VC_LIB/PCANBasic.lib") {
		message("The PKCAN driver has not been installed")
	}
	!exists($$(BOOST_ROOT)) {
		message("Environment variable BOOST_ROOT must point at a valid BOOST installation, currently points at: $$(BOOST_ROOT)")
	}	
}
unix {
	SUBDIRS += CanInterface/sockcan/sockcan.pro
}
