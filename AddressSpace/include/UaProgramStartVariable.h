#ifndef __UAPROGRAMSTARTVARIABLE_H__
#define __UAPROGRAMSTARTVARIABLE_H__

#include "uaobjecttypes.h"
#include "NmBuildingAutomation.h"
#include "uaobjecttypes.h"
#include "UaControlDeviceItem.h"
#include "UaProgramItem.h"
#include "CANOpenServerConfig.h"
#include "Ids.h"

namespace AddressSpace
{
	class UaProgramStartVariable:
		public UaControlVariable
	{
	public:
		/**
		* @brief Constructor
		* @param name the name of ua node
		* @param newNodeId item ua node id
		* @param pNodeManager pointer to node manager
		* @param dv initial value of item (usually empty)
		* @param pSharedMutex  pointer to shared mutex (using toolkit)
		*/
		UaProgramStartVariable(
			const UaString& name,
			const UaNodeId& newNodeId,
			NmBuildingAutomation* pNodeManager,
			UaVariant &dv,
			UaProgramItem *prog,
			UaMutexRefCounted* pSharedMutex
			) : UaControlVariable(name, newNodeId, pNodeManager, dv, pSharedMutex) 
		{
			setTypeDefinition(UaNodeId(BA_STARTPROGRAM, pNodeManager->getNameSpaceIndex()));
			m_pProg = prog;
			setUserData(NULL);
		}

		/**
		* @brief Redefine setValue function to sent data change signal;
		* @param pSession pointer to current session
		* @param dataValue new value of item
		* @param checkAccessLevel whether check access
		* @return status
		*/
		virtual UaStatus setValue(Session *pSession, const UaDataValue& dataValue, OpcUa_Boolean checkAccessLevel)
		{
			UaStatus ret;
			ret = OpcUa::BaseDataVariableType::setValue(pSession, dataValue, checkAccessLevel);
			//			TRACE1_DATA(SERVER_CORE, UA_T"<-- UaCalcItem::callCalculation %s", this->browseName().toString().toUtf8());
//			UaNodeId id = nodeId();
			valueChange(pSession, dataValue, checkAccessLevel); // send signal
			m_pProg->executeProgram();
			return ret;
		}
		/**
		* @brief Redefine setValue function to sent data change signal;
		* @param pSession pointer to current session
		* @param dataValue new value of item
		* @param checkAccessLevel whether check access
		* @return status
		*/
		virtual UaStatus setValueOnChange(Session *pSession, const UaDataValue& dataValue, OpcUa_Boolean checkAccessLevel)
		{
			UaStatus ret;
			ret = OpcUa::BaseDataVariableType::setValue(0, dataValue, false);
			//			TRACE1_DATA(SERVER_CORE, UA_T"<-- UaCalcItem::callCalculation %s", this->browseName().toString().toUtf8());
//			UaNodeId id = nodeId();
			valueChange(pSession,dataValue, checkAccessLevel); // send signal
			m_pProg->executeProgram();
			return ret;
		}
	private:
		UaProgramItem *m_pProg;

	};

}

#endif