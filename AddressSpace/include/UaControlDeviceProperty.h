#ifndef __UACANTROLDEVICEPROPERTY_H__
#define __UACANTROLDEVICEPROPERTY_H__

#include "uaobjecttypes.h"
#include "uabasenodes.h"
#include <opcua_dataitemtype.h>
#include <opcua_basedatavariabletype.h>
#include <opcua_propertytype.h>
#include <map>
#include "ControlInterface.h"
#include "InterfaceUserDataBase.h"
#include "CANOpenServerConfig.h"

namespace AddressSpace
{
	class 	NmBuildingAutomation;
	/**
	* @brief This class represent the generic variable created based on XML definitions.
	* It comprises the function using all variable in OpcUaCanOpenServer. (read,write, TypeDefinition)
	*/
	class UaControlDeviceProperty : public OpcUa::PropertyType
	{
		UA_DISABLE_COPY(UaControlDeviceProperty);
	public:

		/**
		* @brief Constructor
		* @param name the name of ua node
		* @param newNodeId item ua node id
		* @param pNodeManager pointer to node manager
		* @param dv initial value of item (usually empty)
		* @param baTypeId identifier of a device (using to connect to hardware device)
		* @param conf XML configuration entry for this node
		* @param interf pointer to the object representing hardware device
		* @param pSharedMutex  pointer to shared mutex (using toolkit)
		*/
		UaControlDeviceProperty (
			UaNode *cur,
			NmBuildingAutomation* pNodeManager,
			UaVariable *instance,
			::xml_schema::type *conf,
			InterfaceUserDataBase* interf,
			UaMutexRefCounted* pSharedMutex
		);
		
		virtual ~UaControlDeviceProperty();

	};
}
#endif
  
