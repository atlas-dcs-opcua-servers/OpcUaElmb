#ifndef _tmath
#define _tmath

//#include "CompileMathExpression.h"
#include <cmath>
#include <string>
#include <vector>

enum tokenids {
	IDANY,
	SPLUS,
	SMINUS,
	SMIN,
	SMUL,
	SDIV,
	SEXP,
	SIF,
	SNOT,
	SOR,
	SAND,
	SNE,
	SGT,
	SGE,
	SEQ,
	SLE,
	SLT,
	SFUNC
};

union ptr_func {
	double (*ptr0Func)();
	double (*ptr1Func)(double);
	double (*ptr2Func)(double,double);
	double (*ptr3Func)(double,double,double);
	double(*ptr3IFFunc)(bool, double, double);
	bool (*ptr0LFunc)();
	bool (*ptr1LDFunc)(double);
	bool (*ptr2LDFunc)(double,double);
	bool (*ptr1LFunc)(bool);
	bool (*ptr2LFunc)(bool,bool);

};

struct funcTable {
	unsigned int iden;
	std::string name;
	int num_param;
	ptr_func p_func;

    funcTable() { iden = IDANY; name = ""; num_param = 0; p_func.ptr0Func = NULL; }
	funcTable ( const struct funcTable &ft)
	{
		name = ft.name; iden = ft.iden; num_param = ft.num_param; p_func = ft.p_func;
	};
	funcTable(unsigned int id,	std::string n,	int n_p,	double (p_f)())
	{
		iden = id; name = n; num_param = n_p; p_func.ptr0Func = p_f;
	};
	funcTable(unsigned int id,	std::string n,	int n_p,	double (p_f)(double) )
	{
		iden = id; name = n; num_param = n_p; p_func.ptr1Func = p_f;
	};
	funcTable(unsigned int id, std::string n,	int n_p,	double (p_f)(double,double) )
	{
		iden = id; name = n; num_param = n_p; p_func.ptr2Func = p_f;
	};
	funcTable(unsigned int id,	std::string n,	int n_p,	double (p_f)(double,double,double))
	{
		iden = id; name = n; num_param = n_p; p_func.ptr3Func = p_f;
	};
	funcTable(unsigned int id, std::string n, int n_p, double (p_f)(bool, double, double))
	{
		iden = id; name = n; num_param = n_p; p_func.ptr3IFFunc = p_f;
	};
	funcTable(unsigned int id, std::string n, int n_p, bool (p_f)())
	{
		iden = id; name = n; num_param = n_p; p_func.ptr0LFunc = p_f;
	};
	funcTable(unsigned int id,	std::string n,	int n_p,	bool (p_f)(double) )
	{
		iden = id; name = n; num_param = n_p; p_func.ptr1LDFunc = p_f;
	};
	funcTable(unsigned int id, std::string n,	int n_p,	bool (p_f)(double,double) )
	{
		iden = id; name = n; num_param = n_p; p_func.ptr2LDFunc = p_f;
	};
	funcTable(unsigned int id,	std::string n,	int n_p,	bool (p_f)(bool) )
	{
		iden = id; name = n; num_param = n_p; p_func.ptr1LFunc = p_f;
	};
	funcTable(unsigned int id, std::string n,	int n_p,	bool (p_f)(bool,bool) )
	{
		iden = id; name = n; num_param = n_p; p_func.ptr2LFunc = p_f;
	};
};

class TMath
{
public:
	class isName {
		public:
		 isName(std::string sn): n(sn) {}
		 inline bool operator()(const struct funcTable &in) { return in.name == n; }
		private:
		 std::string n;
	};

	TMath() {}

	static std::vector<funcTable> functionSet;

	inline double static Pi() { return M_PI; }
	inline double static E() { return M_E; }
	inline double static PLUS(double d1,double d2) { return d1+d2; }
	inline double static MINUS(double d1,double d2 ) { return d1-d2; }
	inline double static MINUS2(double d1 ) { return -d1; }
	inline double static MUL(double d1,double d2) { return d1*d2; }
	inline double static DIV(double d1,double d2) { return d1/d2; }
	inline double static EXP2(double d1,double d2) { return pow(d1,d2); }
	inline bool static ETRE(bool d1) { return d1; }
	inline bool static ETRE(double d1) { (void)(d1); return true; }
	inline double static IFF(bool d1, double d2, double d3) { return ETRE(d1) ? d2 : d3; }
	inline bool static NOT(bool d1) { return !d1; }
	inline bool static NE(double d1,double d2) { return d1 != d2; }
	inline bool static OR(bool d1, bool d2) { return d1 || d2; }
	inline bool static AND(bool d1, bool d2) { return d1 && d2; }
	inline bool static GT(double d1,double d2) { return d1 > d2; }
	inline bool static EQ(double d1,double d2) { return d1 == d2; }
	inline bool static GE(double d1,double d2) { return d1 >= d2; }
	inline bool static LE(double d1,double d2) { return d1 <= d2; }
	inline bool static LT(double d1,double d2) { return d1 < d2; }

	static void init()
    {
		functionSet[SPLUS] = funcTable(SPLUS,"+",2,PLUS);
		functionSet[SMINUS] = funcTable(SMINUS,"-",2,MINUS);
		functionSet[SMIN] = funcTable(SMIN,"-",1,MINUS2);
		functionSet[SMUL] = funcTable(SMUL,"*",2,MUL);
		functionSet[SDIV] = funcTable(SDIV,"/",2,DIV);
		functionSet[SEXP] = funcTable(SEXP,"^",2,EXP2);
		functionSet[SNOT] = funcTable(SNOT,"!",1,NOT);
		functionSet[SNE] = funcTable(SNE,"!=",2,NE);
		functionSet[SOR] = funcTable(SOR,"||",2,OR);
		functionSet[SAND] = funcTable(SAND,"&&",2,AND);
		functionSet[SGT] = funcTable(SGT,">=",2,GT);
		functionSet[SGE] = funcTable(SGE,">",2,GE);
		functionSet[SEQ] = funcTable(SEQ,"=",2,EQ);
		functionSet[SLE] = funcTable(SLE,"<",2,LE);
		functionSet[SLT] = funcTable(SLT,"<=",2,LT);
		functionSet[SIF] = funcTable(SIF, "?", 3, IFF);
		functionSet[SFUNC+1] = funcTable(SFUNC+1,"pi(",0,Pi);
		functionSet[SFUNC+2] = funcTable(SFUNC+2,"e(",0,E);
		functionSet[SFUNC+3] = funcTable(SFUNC+3,"sin(",1,sin);
		functionSet[SFUNC+4] = funcTable(SFUNC+4,"cos(",1,cos);
		functionSet[SFUNC+5] = funcTable(SFUNC+5,"tan(",1,tan);
		functionSet[SFUNC+6] = funcTable(SFUNC+6,"asin(",1,asin);
		functionSet[SFUNC+7] = funcTable(SFUNC+7,"acos(",1,acos);
		functionSet[SFUNC+8] = funcTable(SFUNC+8,"atan(",1,atan);
		functionSet[SFUNC+9] = funcTable(SFUNC+9,"atan2(",2,atan2);
		functionSet[SFUNC+10] = funcTable(SFUNC+10,"sinh(",1,sinh);
		functionSet[SFUNC+11] = funcTable(SFUNC+11,"cosh(",1,cosh);
		functionSet[SFUNC+12] = funcTable(SFUNC+12,"tanh(",1,tanh);
		functionSet[SFUNC+13] = funcTable(SFUNC+13,"exp(",1,exp);
		functionSet[SFUNC+14] = funcTable(SFUNC+14,"log(",1,log);
		functionSet[SFUNC+15] = funcTable(SFUNC+15,"log10(",1,log10);	
		functionSet[SFUNC+16] = funcTable(SFUNC+16,"pow(",2,pow);
		functionSet[SFUNC+17] = funcTable(SFUNC+17,"sqrt(",1,sqrt);
		functionSet[SFUNC+18] = funcTable(SFUNC+18,"ceil(",1,ceil);
		functionSet[SFUNC+19] = funcTable(SFUNC+19,"fabs(",1,fabs);
		functionSet[SFUNC+20] = funcTable(SFUNC+20,"floor(",1,floor);
		functionSet[SFUNC+21] = funcTable(SFUNC+21,"fmod(",1,fmod);

  };
  
	static unsigned int getFunctionId(std::string nFunc)
	{
		std::vector<struct funcTable>::iterator ift = find_if(functionSet.begin(), functionSet.end(), isName(nFunc));
		if (ift == functionSet.end()) {
			std::cout << "The function " << nFunc << "did not implement" << std::endl;
			exit(-1);
		}

		return	ift->iden;
	};	
	static int getNumParam(unsigned int nFunc)
	{
 		return functionSet[nFunc].num_param;
	};	
	static const struct funcTable &getFuncTable(unsigned int nFunc)
	{
		return functionSet[nFunc];
	};
	static struct funcTable &getFuncTableStr(std::string nFunc)
	{
		return functionSet[getFunctionId(nFunc)];
	};

	
}; 
#endif
