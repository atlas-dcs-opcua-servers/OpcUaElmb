#ifndef __UACONTROLNODE_H__
#define __UACONTROLNODE_H__

#include "uaobjecttypes.h"
#include "nodemanagerbase.h"
#include "CANOpenServerConfig.h"
#include "UaControlDeviceGeneric.h"
#include "opcua_propertytype.h"
#include "UaDeviceMethod.h"
#include "UaControlDeviceItem.h"

//#include "boost/shared_ptr.hpp"

namespace AddressSpace
{
	/** @brief Generic class type to create UaNode based on type information and create connection
	* to hardware objects.
	* @details
	* The type information of system object can be represented as a UaNode graph.<br>
	* Each node connects to another node via reference. In OPC Ua several reference type is defined.<br>
	*	- OpcUaId_HasSubtype describes an inheritance.
	*	- OpcUaId_HasComponent - aggregation
	*
	* The type information are created by node manager method afterStartUp.
	* Actually when creates the object based on type information the tree of objects is created.
	*/
	class UaControlNode

	{
		//		UA_DISABLE_COPY(UaControlNode);

	public:
		UaControlNode() {};
		virtual ~UaControlNode(void) {};

		/** @brief this function creates recursively the object tree based on type object
		* @param cur parent ua node
		* @param instance pointer to type node
		* @param pNodeManager node manager object
		* @param conf XML description of object created
		* @param interf pointer to parent hardware object
		* @return the object describe the result (good or not)
		*/
		template<class T>
		static UaStatus UaControlNodeCreateByType(T *cur, UaNode* instance,
			NmBuildingAutomation* pNodeManager,
			::xml_schema::type *conf,
			pUserDeviceStruct* interf,
			UaMutexRefCounted* m)
		{
			UaControlDeviceGeneric*	pDeclarationObject = NULL;
			OpcUa::BaseMethod*           pMethodDeclaration = NULL;
			OpcUa::BaseDataVariableType* pDataVariable = NULL;
			OpcUa::PropertyType *pProperty;
			UaStatus                    addStatus;
			UaNodeId					nTypeId;   // ID of node type
			UaNodeId					uaId;		//new id
			UaVariant                   defaultValue;
			UaNode *ptNode;
			UaString newName;
			defaultValue.clear();


			/**************************************************************
			* Create the Device components
			**************************************************************/

			UaReferenceLists *uaList = instance->getUaReferenceLists();			// take the list children

			for (UaReference *pRef = (UaReference *)uaList->pSourceNodes(); pRef != 0; pRef = pRef->pNextInverseReference()) {
				OpcUa_UInt32 refType = pRef->referenceTypeId().identifierNumeric();

				ptNode = pRef->pSourceNode(); 					// take child node
																						//			nTypeId  = ptNode->nodeId();
				newName = ptNode->browseName().toString();
				uaId = pNodeManager->getNewNodeId(cur, newName);
				if (refType == OpcUaId_HasSubtype)
				{
					OpcUa_Int32 nodeIdType = ptNode->nodeId().identifierNumeric();
					if (nodeIdType >= 1000) {
						addStatus = UaControlNodeCreateByType(cur, ptNode, pNodeManager, conf, interf, m);
						UA_ASSERT(addStatus.isGood());
					}
				}
			}

			for (UaReference *pRef = (UaReference *)uaList->pTargetNodes(); pRef != 0; pRef = pRef->pNextForwardReference()) {
				OpcUa_UInt32 refType = pRef->referenceTypeId().identifierNumeric();

				ptNode = pRef->pTargetNode(); 					// take child node
																						//			nTypeId  = ptNode->nodeId();
				newName = ptNode->browseName().toString();
				uaId = pNodeManager->getNewNodeId(cur, newName);
				//				cout << "New Name " << newName.toUtf8() << " new key " << uaId.toString().toUtf8() << endl;
				//				printf("Node Class %d %s %d %s\n", ptNode->nodeClass(), ptNode->browseName().toString().toUtf8(),refType, cur->displayName(0).toString().toUtf8());
				switch (refType) {
				case OpcUaId_HasComponent:
					switch (ptNode->nodeClass()) {
					case OpcUa_NodeClass_VariableType:
						pDataVariable = new UaControlDeviceItem(newName, uaId, pNodeManager, defaultValue, static_cast<UaVariableType*>(ptNode), conf, interf, m);
						addStatus = pNodeManager->addNodeAndReference(cur, pDataVariable, OpcUaId_HasComponent);
						UA_ASSERT(addStatus.isGood());
						break;
					case OpcUa_NodeClass_Variable:
						pDataVariable = new UaControlDeviceItem(cur, pNodeManager, static_cast<UaVariable*>(ptNode), conf, interf, m);
						addStatus = pNodeManager->addNodeAndReference(cur, pDataVariable, OpcUaId_HasComponent);
						UA_ASSERT(addStatus.isGood());
						break;
					case OpcUa_NodeClass_ObjectType:
						pDeclarationObject = new UaControlDeviceGeneric(newName, uaId, pNodeManager, static_cast<UaObjectType *>(ptNode), conf, interf);
						addStatus = pNodeManager->addNodeAndReference(cur, pDeclarationObject, OpcUaId_HasComponent);
						UA_ASSERT(addStatus.isGood());
						break;
					case OpcUa_NodeClass_Object:
						pDeclarationObject = new UaControlDeviceGeneric(newName, uaId, pNodeManager, static_cast<UaObject *>(ptNode), conf, interf);
						addStatus = pNodeManager->addNodeAndReference(cur, pDeclarationObject, OpcUaId_HasComponent);
						UA_ASSERT(addStatus.isGood());
						break;
					case OpcUa_NodeClass_Method:
						pMethodDeclaration = new UaDeviceMethod<T>(cur, pNodeManager, static_cast<UaMethod *>(ptNode), m);
						addStatus = pNodeManager->addNodeAndReference(cur, pMethodDeclaration, OpcUaId_HasComponent);
						UA_ASSERT(addStatus.isGood());
						cur->addMethod(pMethodDeclaration);
						break;
					}

					break;
					//case OpcUaId_HasSubtype:
					//	addStatus = UaControlNodeCreateByType(cur, ptNode, pNodeManager, conf, interf, m);
					//	UA_ASSERT(addStatus.isGood());
					//	break;
				case OpcUaId_HasProperty:
				{
					UaDataValue udt;
					UaDateTime sdt = UaDateTime::now();
					pUserDeviceStruct* dev = static_cast<pUserDeviceStruct*>(cur->getUserData());
					defaultValue = static_cast<InterfaceUserDataBase*>(dev->getDevice())->getPropertyValue(ptNode->nodeId().identifierNumeric(), conf);
					udt.setDataValue(defaultValue, OpcUa_False, OpcUa_Good, sdt, sdt);

					pProperty = new OpcUa::PropertyType(uaId, newName, pNodeManager->getNameSpaceIndex(), defaultValue, Ua_AccessLevel_CurrentRead, pNodeManager, m);
					pProperty->setValue(0, udt, OpcUa_False);
					addStatus = pNodeManager->addNodeAndReference(cur, pProperty, OpcUaId_HasProperty);
					if (!addStatus.isGood())
					{
						UA_ASSERT(addStatus.isGood());
						return addStatus;
					}
				}
				break;
				}
			}
			return addStatus;
		}
	};

}
#endif
