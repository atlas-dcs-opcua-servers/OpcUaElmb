#ifndef __UACANMETHOD_H__
#define __UACANMETHOD_H__

#include "uaobjecttypes.h"
#include "uabasenodes.h"
#include <opcua_dataitemtype.h>
#include <opcua_basedatavariabletype.h>
#include "InterfaceUserDataBase.h"
#include "NmBuildingAutomation.h"

namespace AddressSpace
{
	/** @brief the class to execute method on a can buss
	* Inherits ua BaseMethod and contains hardware interface connection.
	*/
	using namespace UserDevice;

	template<class T>
	class UaDeviceMethod : public OpcUa::BaseMethod
	{
		UA_DISABLE_COPY(UaDeviceMethod);
	public:
		/** @brief Constructor
		* @param cur parent ua node
		* @param pNodeManager pointer to node manager
		* @param instance pointer to method type
		* @param pSharedMutex
		*/
		UaDeviceMethod(T* cur , NmBuildingAutomation* pNodeManager,UaMethod* instance,UaMutexRefCounted *pSharedMutex)
			 :OpcUa::BaseMethod(cur,instance,pSharedMutex) 
		{

			m_iCode = instance->nodeId().identifierNumeric();

			pUserDeviceStruct *udb = pNodeManager->m_pControlInterface->createInterfaceEntry(static_cast<pUserDeviceStruct*>(cur->getUserData()), m_iCode, 0, this);
			setUserData(udb);		// Open Device hardware channel
			//if (udb)
			//	udb->addReference();
		}

		UaDeviceMethod(T* cur, const UaNodeId& nodeId, const UaString& name, NmBuildingAutomation* pNodeManager, OpcUa_UInt32 icode, UaMutexRefCounted *pSharedMutex)
			:OpcUa::BaseMethod(nodeId, name, pNodeManager->getNameSpaceIndex(), pSharedMutex)
		{
			m_iCode = icode;
			pUserDeviceStruct *udb = pNodeManager->m_pControlInterface->createInterfaceEntry(static_cast<pUserDeviceStruct*>(cur->getUserData()), m_iCode, 0, this);
			setUserData(udb);		// Open Device hardware channel
			//if (udb)
			//	udb->addReference();
		}

		//! execute the method
		virtual UaStatus execute() 
		{
			UaStatus ret = OpcUa_Bad;
			pUserDeviceStruct* udInterf = static_cast<pUserDeviceStruct *>(getUserData());
			if (udInterf) {
				InterfaceUserDataBase* uInterf = static_cast<InterfaceUserDataBase *>(udInterf->getDevice());
				ret = uInterf->sendDeviceMessage(m_iCode);
			}
			return ret;
		} 
		
		//! Destructor
		virtual ~UaDeviceMethod(void)
		{
		};

		OpcUa_UInt32 getFunctionCode() { return m_iCode; }

	protected:
		OpcUa_UInt32 m_iCode; //! function code define the operation
	};
}
#endif
  
