#ifndef __NMBUILDINGAUTOMATION_H__
#define __NMBUILDINGAUTOMATION_H__

#include "nodemanagerbase.h"
#include "variablehandle.h"
#include "opcua_baseobjecttype.h"
#include "iomanager.h"
#include "CANOpenServerConfig.h"
#include "ControlInterface.h"


namespace AddressSpace
{
	class UaControlVariable;

	typedef  std::vector<UaControlVariable *> UaControlVariableSet ;
	typedef  std::map<string, UaControlVariable *> UaControlVariableMap;

	/**
	* @namespace AddressSpace
	* @brief Classes and methods to create OpcUa address space and to send/receive commands and data
	*
	* The Ua C++ toolkit has one class which user must customize in order to create the address space<>
	* NmBuildingAutomation is a main building block to develop Opc Ua server. it has several function which defines the address space and ensure interaction between nodes and devices. <br>
	* 
	*/
//	class UaCanBusObject;
//	class UaCanNodeObject;
	class UaCalcCompiler;
	
	/** @brief NmBuildingAutomation is a class defining all basic features of OpcUaCanOpenServer<br>
	*
	* This class inherits the abstract NodeManagerBase. <br>
	* the afterStartUp() is to create address space of the server and .
	* Opc Ua toolkit calls it after startup. All nodes (types, objects,variables, references) are created by it.
	* Also user device object and connection with address space are defined here <br>
	* The function beforeShutDown executes when the server shutdown to stop server softly.<br>
	* The Opc Ua toolkit defines two modes of operations:
	*	-# <b>UaVariable_Value_CacheIsSource</b> is used for readable nodes, where data are stored in value attribute by user callback function.<br>
			In OpcUaCanOpenServer this mode is used for taken pdo item, Emergence messages and bootup messages.
	*	-# <b>UaVariable_Value_Cache</b> is used to read and write value via input/output manager.<br>
	*		- The NodeManagerBase has a default IOManager. In order to use them the virtual readValue and writeValue function should be redefined.<br>
	*		OpcUaCanOpenServer uses the default readValue function. The writeValue function is redefined to send nmt and rtr message and also to set sync and nodeguarding interval.<br>
	*		- In order to ensure the parallel execution pdo writing and sdo operation on can bus OpcUaCanOpenServer is using the special io manager.<br>
	*			- IOManagerPDO is to write pdo manager. Each canbus object includes this manager<>br
	*			- IOManagerSDO is to read and write sdo and segmented sdo message. Each can node object has this manager.
	*		OpcUa tool kit calls getVariableHandler and getIOManager functions to determine which io manager should be used for variable. 
	*
	* Also the 	NmBuildingAutomation has several function to facilitate of manipulating of ua nodes.
	*		
	*/
	class NmBuildingAutomation : public NodeManagerBase
	{
		UA_DISABLE_COPY(NmBuildingAutomation);
	public:

		/** 
		* The constructor NmBuildingAutomation
		* @param conInterf pointer to the device interface
		* @param nameOfServer - server name
		*/	
		NmBuildingAutomation(shared_ptr<UserDevice::ControlInterface> conInterf, const UaString nameOfSerevr);

		virtual ~NmBuildingAutomation();

		virtual UaStatus   afterStartUp();   //! the place where the nodes should be created
		virtual UaStatus   beforeShutDown();  //! to executed need action to to the server softly 

		/**		IOManagerUaNode implementation to read */
		virtual UaStatus readValues(const UaVariableArray &arrUaVariables, UaDataValueArray &arrDataValues);

		/**		IOManagerUaNode implementation to write */
		virtual UaStatus writeValues(const UaVariableArray &arrUaVariables, const PDataValueArray &arrpDataValues, UaStatusCodeArray &arrStatusCodes);

		/** @brief function for determining on iomanager for attribute of node 
		* @param pUaNode node asking the io manager
		* @param attributeId attribute id for different attribute can be used different io manager
		* @return point to IOManager
		*/
		virtual IOManager* getIOManager(UaNode* pUaNode,  OpcUa_Int32 attributeId) const;

		/** @brief get handler using OpcUa toolkit for io operations
		* @param session point to session structure
		* @param serviceType type of operation (read, write and monitoring)
		* @param nodeid node identifier of variable
		* @param attributeId identifier of attribute of variable
		* @return variableHandler used core to make an operation. this handler contains pointer on IOManager
		*/
		virtual VariableHandle* getVariableHandler(Session* session, VariableHandle::ServiceType serviceType, OpcUa_NodeId *nodeId, OpcUa_Int32 attributeId);

		UaNodeId getNewNodeId(const UaNode *,const UaString );			//! produce new node id based on parent id and string
		UaNodeId getTopNodeId(const UaNode *pNode) const;				//! get top node id (canbus)
//		UaNodeId getCanNodeNodeId(const UaNode *pNode) const;			//! get node id of can node 
		UaNode*  getUpNode(const UaNode *pNode) const; //! get parent node
		UaString getDefaultLocale() const { return m_defaultLocaleId; }


		/** @brief find set of variables in address space
		* @param pParObj pointer to type node 
		* @param pattern searching pattern
		* @param storage return found variables
		* @return number of finding variables
		*/
		OpcUa_Int32 findUaVariables(const OpcUa::BaseObjectType *pParObj,string& pattern, UaControlVariableSet& storage);  //! get set of variable of type 

		/** @brief find set of variables starting from node
		* @param pNode pointer to starting node 
		* @param pattern searching pattern
		* @param storage return found variables
		* @return number of finding variables
		*/

		OpcUa_Int32 findUaNodeVariables(UaNode *pNode,string& pattern,UaControlVariableSet &storage);

		shared_ptr<UserDevice::ControlInterface> m_pControlInterface;			//!  Connection to control interface
//		UaStatusCode createNodeManagerAliasNode(const UaNodeId &sourceNodeId);
		UaObject* getInstanceDeclarationObject(OpcUa_UInt32 numericIdentifier,NodeManagerUaNode *nm)		//! get an object based on numerical id ( if not return null)
		{
			return getInstanceDeclaration<UaObject>(numericIdentifier,nm);
		}
		//		UaObject* getInstanceDeclarationObjectType(OpcUa_UInt32 numericIdentifier); //! get an object type based on numerical id ( if not return null)
		//		UaMethod* getInstanceDeclarationMethod(OpcUa_UInt32 numericIdentifier);		//! get a method based on numerical id ( if not return null)
		UaVariable* getInstanceDeclarationVariable(const UaNodeId &n)		//! get a variable based on node id ( if not return null)
		{
			return getInstanceDeclaration<UaVariable>(n);
		}
		//		UaVariable* getInstanceDeclarationVariable(const UaString );		//! get a variable based on string ( if not return null)

		UaObjectType* getInstanceDeclarationObjectType(OpcUa_UInt32 numericIdentifier) //! get an object type based on numerical id ( if not return null)
		{
			
			return getInstanceDeclaration<UaObjectType>(numericIdentifier, m_pControlInterface->getNodeTypeManager());
			// Try to find the instance declaration node with the numeric identifier 
			// and the namespace index of this node manager
			//UaNode* pNode = m_pTypeNodeManager->getNode(UaNodeId(numericIdentifier, m_pTypeNodeManager->getNameSpaceIndex()));

			//if (pNode != NULL) {
			//	int t = pNode->nodeClass();
			//	if (t == OpcUa_NodeClass_ObjectType)
			//	{
			//		// Return the node if valid and a variable
			//		return (UaObject*)pNode;
			//	}
			//}
			return NULL;
		}

		UaVariableType* getInstanceDeclarationVariableType(OpcUa_UInt32 numericIdentifier) //! get an object type based on numerical id ( if not return null)
		{
			return getInstanceDeclaration<UaVariableType>(numericIdentifier, m_pControlInterface->getNodeTypeManager());
			
			// Try to find the instance declaration node with the numeric identifier 
			// and the namespace index of this node manager
			//UaNode* pNode = m_pTypeNodeManager->getNode(UaNodeId(numericIdentifier, m_pTypeNodeManager->getNameSpaceIndex()));

			//if (pNode != NULL) {
			//	int t = pNode->nodeClass();
			//	if (t == OpcUa_NodeClass_VariableType)
			//	{
			//		// Return the node if valid and a variable
			//		return (UaVariableType*)pNode;
			//	}
			//}
			//return NULL;
		}

		UaNode*  findNode(const UaNodeId &NodeId) const { return NodeManagerUaNode::findNode(NodeId); } //! get parent node

	private:
		template<typename X>
		X* getInstanceDeclaration(OpcUa_UInt32 numericIdentifier, NodeManagerUaNode *nm)
		{
			/// Try to find the instance declaration node with the numeric identifier 
			/// and the namespace index of this node manager
			X* pNode = dynamic_cast<X *>(nm->getNode(UaNodeId(numericIdentifier, nm->getNameSpaceIndex())));
			//X* pNode = dynamic_cast<X *>(findNode(UaNodeId(numericIdentifier, nm->getNameSpaceIndex())));
			return pNode;
		};

		template<typename X>
		X* getInstanceDeclaration(const UaNodeId &n)
		{
			/// Try to find the instance declaration node with the numeric identifier 
			/// and the namespace index of this node manager
			X* pNode = dynamic_cast<X *>(getNode(n));
			//			X* pNode = dynamic_cast<X *>(findNode(n));
			return pNode;
		};



		UaStatus createTypeNodes();  //! create of ua node types
		UaStatus createNodeTypes();  //! create of cannode types
		UaStatus createCanbuses();   //! create of canbuses
//		UaStatus createItems();	     //! create calculated items

		UaString	m_sNameNodeId;
		OpcUa_Int32 m_iNumNameNodeId;

		/** XML configuration structure */
//		CanOpenOpcServerConfig::NODETYPE_sequence	m_nodetypes;	//! list of nodes
//		CanOpenOpcServerConfig::CANBUS_sequence		m_canbuses;		//! list of canbuses
//		CanOpenOpcServerConfig::ITEM_sequence		m_items;		//! list of calculated items 
//		CanOpenOpcServerConfig::REGEXPR_sequence	m_regexprisson;	//! list of regular expression 

	};
}
#endif // __NMBUILDINGAUTOMATION_H__
