#ifndef __UAPROGRAMTYPE_H__
#define __UAPROGRAMTYPE_H__

#include "uaobjecttypes.h"
#include "NmBuildingAutomation.h"
#include "uaobjecttypes.h"
#include "UaControlDeviceGeneric.h"
#include "CANOpenServerConfig.h"

namespace AddressSpace
{
	class UaProgramItem :
		public  UaControlDeviceGeneric
	{
	public:
		/** Constructor
		* @param pt XML description of Program in the system
		* @param newNodeId ua node id
		* @param pNodeManager pointer to node manager
		*/
//		UaProgramItem(PROGRAM *pt, const UaNodeId& newNodeId, NmBuildingAutomation* pNodeManager, UaControlDeviceGeneric* parent, UserDataBase* puds);
		UaProgramItem(PROGRAM *pt, UaObjectType *pNodeType, const UaNodeId& newNodeId, NmBuildingAutomation *pNodeManager, UaControlDeviceGeneric* parent, UserDevice::pUserDeviceStruct * puds);
//		UaProgramItem(PROGRAM *pt, UaObject *pNodeType, const UaString name, NmBuildingAutomation *pNodeManager, UaControlDeviceGeneric* parent, UserDataBase * puds);

		//! destructor 
		virtual ~UaProgramItem(void)
		{
		};

		void executeProgram();
	private:
		UaControlVariable *m_pErrorItem;
	};

}

#endif