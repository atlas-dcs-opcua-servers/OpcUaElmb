#ifndef __UACONTROLVARIABLE_H__
#define __UACONTROLVARIABLE_H__

#include "uaobjecttypes.h"
#include "uabasenodes.h"
#include <opcua_dataitemtype.h>
#include <opcua_basedatavariabletype.h>
#include "CanOpenData.h"
#include "boost/bind.hpp"
#include "boost/signals2.hpp"
#include <map>
#include "CANOpenServerConfig.h"
#include "InterfaceUserDataBase.h"
#include "srvtrace.h"
#include <opcua_baseobjecttype.h>

namespace AddressSpace
{
	class NmBuildingAutomation;
	class UaControlNode;
	/** @brief Generic class has a signal attribute to send signal when value is changed 
	* All variable in OpcUaCanopen Server inherits this class
	*/
	class UaControlVariable : 
		public OpcUa::BaseDataVariableType
	{
		UA_DISABLE_COPY(UaControlVariable);
	public:
		/**
		* @brief Constructor
		* @param name the name of ua node
		* @param newNodeId item ua node id
		* @param pNodeManager pointer to node manager
		* @param dv initial value of item (usually empty)
		* @param pSharedMutex  pointer to shared mutex (using toolkit)
		*/
		UaControlVariable(
			UaNode *cur,
			NmBuildingAutomation* pNodeManager,
			UaVariable *instance,
			UaMutexRefCounted* pSharedMutex
		);

		UaControlVariable (
			const UaString& name, 
			const UaNodeId& newNodeId,
			NmBuildingAutomation* pNodeManager,
			UaVariant &dv,
			UaMutexRefCounted* pSharedMutex
			);

		virtual ~UaControlVariable()
		{
			//UserDevice::InterfaceUserDataBase * iudb = (UserDevice::InterfaceUserDataBase*)getUserData();

			//if (iudb) {
			//	iudb->releaseMutex();
			//	if (iudb->countMutex() == 0)
			//		delete iudb;
			//	setUserData(0);

			//}
//			OpcUa::BaseDataVariableType::~BaseDataVariableType();
		}

		virtual UaStatus write(UaDataValue &udv) {
			return setValue(0, udv, false);
		}

		virtual UaStatus read() {
			return OpcUa_Good;
		}


		/**
		* @brief Redefine setValue function to sent data change signal; 
		* @param pSession pointer to current session
		* @param dataValue new value of item
		* @param checkAccessLevel whether check access
		* @return status
		*/
		virtual UaStatus setValue(Session *pSession,const UaDataValue& dataValue,OpcUa_Boolean checkAccessLevel)
		{
			UaStatus ret;

			convertToDouble(dataValue);
			ret = OpcUa::BaseDataVariableType::setValue(pSession, dataValue, checkAccessLevel);

//			TRACE1_DATA(SERVER_CORE, UA_T"<-- UaCalcItem::callCalculation %s", this->browseName().toString().toUtf8());
			valueChange(pSession, dataValue, checkAccessLevel); // send signal
			return ret;
		}
		/**
		* @brief Redefine setValue function to sent data change signal;
		* @param pSession pointer to current session
		* @param dataValue new value of item
		* @param checkAccessLevel whether check access
		* @return status
		*/
		virtual UaStatus setValueOnChange(Session *pSession, const UaDataValue& dataValue, OpcUa_Boolean checkAccessLevel)
		{
			UaStatus ret;

			convertToDouble(dataValue);

			ret = OpcUa::BaseDataVariableType::setValue(pSession, dataValue, checkAccessLevel);

			//			TRACE1_DATA(SERVER_CORE, UA_T"<-- UaControlDevicetem::setItem %s", browseName().toString().toUtf8());
			valueChange(pSession, dataValue, checkAccessLevel); // send signal
			return ret;
		}

		virtual void connectItem(UaControlVariable *item)
		{
			boost::arg<1> _1;
			boost::arg<2> _2;
			boost::arg<3> _3;
//			boost::arg<4> _4;
//			TRACE2_DATA(SERVER_CORE, UA_T"<-- UaControlDevicetem::connectItem %s %s", browseName().toString().toUtf8(), item->browseName().toString().toUtf8());
			item->valueChange.connect(boost::bind(&UaControlVariable::setValueOnChange, this, _1,_2,_3));
//			item->valueChange.connect(&UaControlVariable::setValueOnChange);

		}
		//! @return node id of type of variable
		virtual UaNodeId typeDefinitionId() const { return m_TypeDefinition; }

		/** @brief  set node id of type of variable
		* @param uni ua node id of type
		*/
		virtual void setTypeDefinition( UaNodeId uni) { m_TypeDefinition = uni; }

		/** @brief write date to hardware
		* @param udv value of variable
		*/
		boost::signals2::signal<void(Session *pSession, const UaDataValue& dataValue, OpcUa_Boolean checkAccessLevel) > valueChange; //! Signal 
		
		void convertToDouble(const UaDataValue &dataValue)
		//void convertToDouble(void)
		{
//			double res = 0.0;
			const OpcUa_Variant *val = dataValue.value();

			switch (val->Datatype) {
			case    OpcUaType_Boolean:
				curValue = val->Value.Boolean;
				break;
			case OpcUaType_SByte:
				curValue = val->Value.SByte;
				break;
			case OpcUaType_Byte:
				curValue = val->Value.Byte;
				break;
			case OpcUaType_Int16:
				curValue = val->Value.Int16;
				break;
			case OpcUaType_UInt16:
				curValue = val->Value.UInt16;
				break;
			case OpcUaType_Int32:
				curValue = val->Value.Int32;
				break;
			case OpcUaType_UInt32:
				curValue = val->Value.UInt32;
				break;
			case OpcUaType_Int64:
				curValue = (double)val->Value.Int64;
				break;
			case OpcUaType_UInt64:
				curValue = (double)val->Value.UInt64;
				break;
			case OpcUaType_Float:
				curValue = val->Value.Float;
				break;
			case OpcUaType_Double:
				curValue = val->Value.Double;
				break;
			default:
//				throw 1;
				curValue = 0.0;
			};
//			curValue = res;
		}
		OpcUa_Double	curValue;

	private:
		UaNodeId		m_TypeDefinition; //! Type of value of node
	
	};

	
	/**
	* @brief This class represent the generic variable created based on XML definitions.
	* It comprises the function using all variable in OpcUaCanOpenServer. (read,write, TypeDefinition)
	*/
	class UaControlDeviceItem : 
		public UaControlVariable
	{
		UA_DISABLE_COPY(UaControlDeviceItem);
	public:

		/**
		* @brief Constructor
		* @param name the name of ua node
		* @param newNodeId item ua node id
		* @param pNodeManager pointer to node manager
		* @param dv initial value of item (usually empty)
		* @param baTypeId identifier of a device (using to connect to hardware device)
		* @param conf XML configuration entry for this node
		* @param interf pointer to the object representing hardware device
		* @param pSharedMutex  pointer to shared mutex (using toolkit)
		*/
		
		UaControlDeviceItem (
			UaNode *cur,
			NmBuildingAutomation* pNodeManager,
			UaVariable *instance,
			::xml_schema::type *conf,
			UserDevice::pUserDeviceStruct* interf,
			UaMutexRefCounted* pSharedMutex
		);
		
		/**
		* @brief Constructor
		* @param name the name of ua node
		* @param newNodeId item ua node id
		* @param pNodeManager pointer to node manager
		* @param dv initial value of item (usually empty)
		* @param instance  pointer to variable type defining common parameters
		* @param conf xml configuration entry for this node
		* @param interf pointer to the object representing hardware device
		* @param pSharedMutex  pointer to shared mutex (using toolkit)
		*/
		UaControlDeviceItem (
			const UaString& name, 
			const UaNodeId& newNodeId,
			NmBuildingAutomation* pNodeManager,
			UaVariant &dv,
			UaVariableType *instance,
			::xml_schema::type *conf,
			UserDevice::pUserDeviceStruct* interf,
			UaMutexRefCounted* pSharedMutex
		);

		virtual UaStatus write(UaDataValue &udv);

		//! read data from hardware
		virtual UaStatus read();

		/** @brief read data from hardware via callback function
		* this function are using for subscribe item
		* @param iom callback function to inform toolkit about value changing
		*/
		virtual UaStatus read(IOVariableCallback *iom); 

		//! Set Access function where ac can be CAN_IN,CAN_OUT,CAN_IO
		void setAccess(DirectionType ac) {
			switch(ac) {
			case CAN_IN:
				setValueHandling(UaVariable_Value_CacheIsSource);
				setAccessLevel(Ua_AccessLevel_CurrentRead );
				break;
			case CAN_OUT:
				setValueHandling(UaVariable_Value_Cache);
				setAccessLevel(Ua_AccessLevel_CurrentWrite);
				break;
			case CAN_IO:
				setValueHandling(UaVariable_Value_Cache);
				setAccessLevel(Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite);
				break;
			}
		}

		virtual void addMethod(OpcUa::BaseMethod* meth) { m_pMethod.push_back(meth); }
	protected:
		std::vector<OpcUa::BaseMethod*>	m_pMethod;  //! list of methods belonging to node
//		UaControlNode m_cFunction;
	}; 
}
#endif
  
