#ifndef _MathExpression
#define _MathExpression

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/lex_lexertl.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/bind/bind.hpp>
#include "TMath.h"

#include <iostream>
#include <ostream>
#include <fstream>
#include <string>
#include <vector>

using namespace boost::spirit;
using boost::phoenix::val;

	typedef std::string::iterator base_iterator_type;
	typedef lex::lexertl::token<base_iterator_type, boost::mpl::vector<int,double,std::string> > lex_token_type;
	typedef lex::lexertl::lexer<lex_token_type> lexer_type;

	class variableSet 
	{
	public:
		variableSet(std::string name_input) {
			item = name_input;
			std::cout << item << "  Enter->";
			std::cin >> value;
		};
		variableSet(): item() {}
		virtual double getValue() {
			return value;
		};				

		std::string item;
		double value;
	};


	inline 	std::ostream& operator << ( std::ostream &out,  const funcTable &op)
	{
		out << " [ " << op.name << " " << op.iden << " " << op.num_param << " ] ";
		return out;
	};

	BOOST_FUSION_ADAPT_STRUCT(
		funcTable,
		(unsigned int,iden)
		(std::string, name)
		(int,num_param)
		(ptr_func,p_func)
		);

	typedef boost::variant<struct funcTable,int,double, std::string> operand_type;

	typedef std::vector<operand_type> operands_type_set;

	template <typename Lexer>
	struct OpcUa_Formula_Tokens : lex::lexer<Lexer>
	{
		OpcUa_Formula_Tokens()
		{
			item = "[a-zA-Z_][a-zA-Z0-9_]*(\\.[a-zA-Z0-9_]+)*";
			func = "[a-zA-Z_][a-zA-Z0-9_]*\\(";
			regvar = "\\$[a-zA-Z_][a-zA-Z0-9_]*";
			constant_float = "[0-9]*\\.[0-9]*([eE][-\\+]?[0-9]+)?";
			constant_int = "[0-9]+";
			plus = "\\+";
			mul = "\\*";
			div = "\\/";
			minus = "\\-";
			exp = "\\^";
			lor = "\\|";
			land = "&";
			gt = ">";
			lt = "<";
			lnot = "!";
			leq = "\\=";
			lif = "\\?";
			lthen = "\\:";
			lbr = "\\(";
			rbr = "\\)";
			comma = "\\,";
			ws = "\\s+";

			this->self = mul | div | exp | minus | plus | lif;
			this->self += func | item | constant_int | constant_float | regvar ;
			this->self += lbr | rbr | comma | lthen ;
			this->self += leq | lnot | lt |  gt | land | lor;

			// define the whitespace to ignore (spaces, tabs, newlines 
			// and add those to another lexer state (here: "WS")

			this->self("WS") = ws;
		}

		lex::token_def<lex::omit>   ws;
		lex::token_def<std::string> item , func, regvar;
		lex::token_def<>plus,minus,div,exp,mul,lbr,rbr,comma;
		lex::token_def<>land,lor,lnot,leq,gt,lt,lif,lthen;
		lex::token_def<int>constant_int;
		lex::token_def<double>constant_float;

	};

	template <typename Iterator,typename Skipper>
	struct OpcUa_Formula_Grammar_type : qi::grammar<Iterator,Skipper > 
	{
		template <typename TokenDef>
		OpcUa_Formula_Grammar_type(TokenDef const& tok, operands_type_set &opts ,bool logical) : 
			OpcUa_Formula_Grammar_type::base_type(logical ? expressionL : expressionA)
		{
			using boost::phoenix::ref;
			using boost::phoenix::push_back;
			using boost::spirit::qi::_1;
			std::string tmpstr;

//			start = expressionA  | expressionL;

			expressionA = 
//				expressionL >> tok.lif >> expressionA >> tok.lthen >> expressionA 
//				|
			term >> *(term_minus | term_plus);
			
			factor_expression = tok.lbr >> expressionA >> tok.rbr;

			term_minus =	tok.minus >> term
				[	
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SMINUS) )
				]  ;

			term_plus = 	tok.plus >> term 
				[
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SPLUS ))
				] ;

//			term =  factor_exp >> *( factor_mul | factor_div  );

			term = factor_p_m >> *(factor_mul | factor_div);

			factor_mul = tok.mul >> factor_exp
				[ 
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SMUL)) 
				] ;

			factor_div = tok.div  >> factor_exp
				[
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SDIV)) 
				] ;

			factor_exp = factor_p_m >> *(factor_exp2 );

			factor_exp2 = tok.exp >> factor_p_m 
				[
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SEXP ) )
				] ;

			factor = factor_expression | factor_int | factor_float | factor_func | factor_item | factor_regvar;

			factor_p_m = factor_minus | factor_plus | factor;

			factor_int = tok.constant_int
				[ 
					push_back(boost::phoenix::ref(opts), _1) 
				];
			factor_float =	tok.constant_float
				[ 
					push_back(boost::phoenix::ref(opts), _1)
				];

			factor_func =  tok.func [_a = _1] >> factor_arg
				[
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTableStr,_a) ) 
				]  ;

			factor_arg = (expressionA % tok.comma ) >> tok.rbr;

			factor_item =  tok.item  
				[ 
					push_back(boost::phoenix::ref(opts), _1) 
				];

	

			factor_minus =  tok.minus >> factor
				[
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SMIN) )
				] ;

			factor_plus = tok.plus >> factor ;

			factor_regvar =  tok.regvar  
				[ 
					push_back(boost::phoenix::ref(opts), _1)
				];


			expressionL = lterm >> *(logic_or);

			lfactor_expression = tok.lbr >> expressionL >> tok.rbr;
			
			lterm = lfactor >> *(logic_and);

			logic_or = tok.lor >> tok.lor >> lterm
					[
						push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SOR) )
					] ;

			logic_and = tok.land >> tok.land >> lfactor
					[
						push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SAND) )
					] ;

//			logic_exp = lfactor | *(logic_exp2);

			lfactor = logic_exp | lfactor_expression | lfactor2;

			logic_exp = tok.lnot >> lfactor
					[
						push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SNOT) )
					] ;


			lfactor2 =  expressionA >> -(ltermlt | ltermgt | ltermle | ltermge | ltermlne | ltermleq) ;

			ltermleq = tok.leq >> expressionA
				[
					push_back(boost::phoenix::ref(opts), boost::phoenix::bind(TMath::getFuncTable, SEQ))
				];
			ltermge = tok.gt >> tok.leq >> expressionA
				[
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SGE) )
				] ;

			ltermlne = tok.lnot >> tok.leq >> expressionA
				[
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SNE) )
				] ;

			ltermgt = tok.gt >> expressionA
				[
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SGT) )
				] ;

			ltermlt = tok.lt >> expressionA
				[
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SLT) )
				] ;

			ltermle = tok.lt >> tok.leq >> expressionA
				[
					push_back(boost::phoenix::ref(opts),boost::phoenix::bind(TMath::getFuncTable,SLE) )
				] ;




#ifdef BOOST_SPIRIT_DEBUG
			
			expressionL.name("expressionL");
			qi::debug(expressionL);
			expressionA.name("expressionA");
			qi::debug(expressionA);
			term.name("term");
			qi::debug(term);
			term_minus.name("term_minus");
			qi::debug(term_minus);
			term_plus.name("term_plus");
			qi::debug(term_plus);
			lterm.name("lterm");
			qi::debug(lterm);
			lfactor.name("lfactor");
			qi::debug(lfactor);
			logic_or.name("logic_or");
			qi::debug(logic_or);
			logic_and.name("logic_and");
			qi::debug(logic_and);
			ltermge.name("ltermge");
			qi::debug(ltermge);
			ltermgt.name("ltermgt");
			qi::debug(ltermgt);
			ltermlt.name("ltermlt");
			qi::debug(ltermlt);
			ltermle.name("ltermle");
			qi::debug(ltermle);
			ltermlne.name("ltermlne");
			qi::debug(ltermlne);
			ltermleq.name("ltermleq");
			qi::debug(ltermleq);
			lfactor_expression.name("lfactor_expression");
			qi::debug(lfactor_expression);
			factor.name("factor");
			qi::debug(factor);
			factor_p_m.name("factor_p_m");
			qi::debug(factor_p_m);
			factor_div.name("factor_div");
			qi::debug(factor_div);
			factor_mul.name("factor_mul");
			qi::debug(factor_mul);
			factor_exp.name("factor_exp");
			qi::debug(factor_exp);
			factor_exp2.name("factor_exp");
			qi::debug(factor_exp2);
			factor_item.name("factor_item");
			qi::debug(factor_item);
			factor_minus.name("factor_minus");
			qi::debug(factor_minus);
			factor_plus.name("factor_plus");
			qi::debug(factor_plus);
			factor_int.name("factor_int");
			qi::debug(factor_int);
			factor_float.name("factor_float");
			qi::debug(factor_float);
			factor_arg.name("factor_arg");
			qi::debug(factor_arg);
			factor_func.name("factor_func");
			qi::debug(factor_func);
			factor_regvar.name("factor_regvar");
			qi::debug(factor_regvar);


#endif


		}

		qi::rule< Iterator,qi::locals<std::string>,Skipper > factor_func;
		qi::rule< Iterator,Skipper > term,factor_exp,factor_exp2,factor_mul,factor_div, term_minus,term_plus,factor_regvar,factor_p_m;
		qi::rule< Iterator,Skipper > factor_int,factor_float,factor_item, factor_minus,factor_expression,factor_plus,factor_arg;
		qi::rule< Iterator, Skipper > logic_or, logic_and, lfactor, ltermgt, ltermlt, lterm, ltermge, ltermle, ltermleq, ltermlne, lfactor_expression;
		qi::rule< Iterator,Skipper > logic_exp,logic_exp2,lfactor2;
		qi::rule< Iterator,Skipper > expressionL,expressionA,start;
		qi::rule< Iterator,Skipper > factor;



	};

	struct OpcUaFormula {

		operands_type_set opt;

		std::vector<std::string> sVariables;

		OpcUaFormula() : opt(), sVariables() { TMath::init(); }
		bool Compile(std::string formul, bool logical);
		bool hasVariables() { return !sVariables.empty(); } 
		double GetValue(std::string ind) {
			std::cout << ind << "Enter->";
			double ret;
			std::cin >> ret;
			return ret;
		};				
		//	boost::function<double(int)> getValueFromItem;
		boost::function<double(std::string)> getValueFromItem;
//		template <typename T> void eval(T&);
		double eval();
		bool evalCondition();

	private:
		bool isCmdFunction(unsigned int idn) {
			return (idn == SGE) | (idn == SGT) | (idn == SLT) | (idn == SLE) | (idn == SNE) | (idn == SEQ);
		}

	    double _eval( operands_type_set::reverse_iterator &in);
		bool _evalCondition(operands_type_set::reverse_iterator &in);

		int m_iIndex;
	};

#endif
