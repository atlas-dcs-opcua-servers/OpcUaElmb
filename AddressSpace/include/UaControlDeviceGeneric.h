#ifndef __UACANOPENGENERIC_H__
#define __UACANOPENGENERIC_H__


#include "uaobjecttypes.h"
#include "nodemanagerbase.h"
#include "CANOpenServerConfig.h"
#include "InterfaceUserDataBase.h"

namespace AddressSpace
{
	class NmBuildingAutomation;
	class UaMethodGeneric;

//	class UaControlNode;
	/** @brief Generic class type to create UaObject based on type information and create connection
	* to hardware objects. 
	* @details
	* The type information of system object can be represented as a UaNode graph.<br>
	* Each node connects to another node via reference. In OPC Ua several reference type is defined.<br>
	*	- OpcUaId_HasSubtype describes an inheritance.
	*	- OpcUaId_HasComponent - aggregation
	*
	* The type information are created by node manager method afterStartUp. 
	* Actually when creates the object based on type information the tree of objects is created.
	*/
	class UaControlDeviceGeneric :
		public OpcUa::BaseObjectTypeGeneric
	{																	
		UA_DISABLE_COPY(UaControlDeviceGeneric);

	public:
//		class UaControlNode<UaControlDeviceGeneric>;
		/** @brief Constructor
		* @param name name of ua object
		* @param newNodeId ua node id of created object
		* @param pNodeManager pointer to node manager 
		* @param baTypeId this is function code which is used to make a link to hardware object
		* @param conf XML description of object created
		* @param interf pointer of parent hardware object. This pointer passed to interface object to create internal structure
		*/
		UaControlDeviceGeneric (
			const UaString& name, 
			const UaNodeId& newNodeId,
			NmBuildingAutomation* pNodeManager,
			OpcUa_Int32 baTypeId,					// object type Id		
			::xml_schema::type *conf,
			UserDevice::pUserDeviceStruct * interf
			);
			
		/** @brief Constructor
		* @param name name of ua object
		* @param newNodeId ua node id of created object
		* @param pNodeManager pointer to node manager 
		* @param instance pointer to object type
		* @param conf XML description of object created
		* @param interf pointer of parent hardware object. This pointer passed to interface object to create internal structure
		*/
		UaControlDeviceGeneric (
			const UaString &name,
			const UaNodeId& newNodeId,
			NmBuildingAutomation* pNodeManager,
			UaObjectType *instance,
			::xml_schema::type *conf,
			UserDevice::pUserDeviceStruct* interf
			);
		/** @brief Constructor
		* @param name name of ua object
		* @param newNodeId ua node id of created object
		* @param pNodeManager pointer to node manager
		* @param instance pointer to object 
		* @param conf XML description of object created
		* @param interf pointer of parent hardware object. This pointer passed to interface object to create internal structure
		*/
		UaControlDeviceGeneric(
			const UaString &name,
			const UaNodeId& newNodeId,
			NmBuildingAutomation* pNodeManager,
			UaObject *instance,
			::xml_schema::type *conf,
			UserDevice::pUserDeviceStruct* interf
		);
		virtual ~UaControlDeviceGeneric(void);

		/** @brief this function creates recursively the object tree based on type object
		* @param cur parent ua node
		* @param instance pointer to type object
		* @param pNodeManager node manager object
		* @param conf XML description of object created
		* @param interf pointer to parent hardware object
		* @return the object describe the result (good or not)
		UaStatus UaObjectCreateByType(UaControlDeviceGeneric* cur ,UaObject* instance,
			NmBuildingAutomation* pNodeManager,
			::xml_schema::type *conf,
			UserDataBase* interf);
			*/

		//! Implement UaObject interface
		OpcUa_Byte eventNotifier() const;

		/** @brief Override UaObject method implementation
		* @param pMethod Ua method
		*/
//		MethodManager* getMethodManager(UaMethod* pMethod) const;

		//! Get Type Number from type nodeID
		OpcUa_UInt32 getDeviceTypeNumber() const;

		/** @brief Implement MethodManager interface
		* this function calls by ua toolkit object when client ask to execute method
		* @param pCallback callback to return an result
		* @param serviceContext internal structure 
		* @param callbackHandle handle of callback function
		* @param pMethodHandle method handler
		* @param inputArguments parameters for method
		* @return status - result of operation
		*/
		virtual UaStatus beginCall(
			MethodManagerCallback* pCallback,
			const ServiceContext&  serviceContext,
			OpcUa_UInt32           callbackHandle,
			MethodHandle*          pMethodHandle,
			const UaVariantArray&  inputArguments);

		//! Own synchronous call implementation the can be overridden in subclasses (not use)
		virtual UaStatus call( 
			UaMethod*             /*pMethod*/,
			const UaVariantArray& /*inputArguments*/,
			UaVariantArray&       /*outputArguments*/,
			UaStatusCodeArray&    /*inputArgumentResults*/,
			UaDiagnosticInfos&    /*inputArgumentDiag*/) 
		{ return OpcUa_BadMethodInvalid; }

		//! get common mutex
//		UaMutexRefCounted* getObjectMutex() const { return m_pSharedMutex; }

		virtual void addMethod(OpcUa::BaseMethod* meth) { m_pMethod.push_back(meth); }

	protected:

		std::vector<OpcUa::BaseMethod*>	m_pMethod;  //! list of methods belonging to node
//		UaMutexRefCounted* m_pSharedMutex;   //! shared mutex (uses be toolkit objects

	};
}
#endif
