#ifndef __UACALCITEM_H__
#define __UACALCITEM_H__

#include "exprtk.hpp"

#include <opcua_basedatavariabletype.h>
#include "NmBuildingAutomation.h"
#include "UaControlDeviceItem.h"
#include "UaControlDeviceGeneric.h"
#include "uadatetime.h"
#include "uadatavalue.h"
#include <cmath>
#include <string>
#include "srvtrace.h"
#include "CANOpenServerConfig.h"

#ifndef isnan
#define isnan(x) std::isnan(x)
#endif

#ifndef isinf
#define isinf(x) std::isinf(x)
#endif

namespace AddressSpace
{
	typedef  std::map<string,vector<UaControlVariable *> > mapDataVariable ;  
	
	typedef ::ITEM ITEM_type;
	typedef ::xsd::cxx::tree::sequence< ITEM_type > ITEM_sequence;
	typedef ITEM_sequence::iterator ITEM_iterator;
	typedef ITEM_sequence::const_iterator ITEM_const_iterator;
		
	typedef ::REGEXPR REGEXPR_type;
	typedef ::xsd::cxx::tree::sequence< REGEXPR_type > REGEXPR_sequence;
	typedef REGEXPR_sequence::iterator REGEXPR_iterator;
	typedef REGEXPR_sequence::const_iterator REGEXPR_const_iterator;
	
	typedef exprtk::symbol_table<double> symbol_table_t;
	typedef exprtk::expression<double>     expression_t;
	typedef exprtk::parser<double>             parser_t;

	struct fmod : public exprtk::ifunction<double>
	{
		//			using exprtk::ifunction<double>::operator();

		fmod() : exprtk::ifunction<double>(2)
		{
			//				static_cast<double>(0.0);
		}

		inline double operator()(const double& a1, const double& a2)
		{
			return std::fmod(a1, a2);
		}
	};

	static fmod f1;
	/** @brief This class ensure a value which calculates from input variable based on analytic formula.
	* @details
	* The calculation item takes value when the logical condition formula becomes true.<br>
	* When the value of control variable is changed it send the signal to this class.<br>
	* The callback function "calculateOnChange" first of all checks th "when condition" and if it is true calculate the value.
	*/
	class UaCalcItem: public UaControlVariable
	{
		UA_DISABLE_COPY(UaCalcItem);
	public:

		typedef map<std::string, UaControlVariable*> variableSet; //! type of set of variable for formula

		/**
		* The callback function to calculate the value.
		* @param dValue the value of changed control variable
		*/
		virtual void calculateOnChange(Session *pSession, const UaDataValue& dataValue, OpcUa_Boolean checkAccessLevel);

		//! initial calculation after startup
		virtual void initCalculation();
		/**
		* Constructor
		* @param name the name of ua node
		* @param uaId item ua node id
		* @param pNodeConfig pointer to node manager
		* @param dv initial value of item (usually empty)
		* @param formul pointer to formula object
		* @param whenCondition pointer to conditional formulas
		* @param statusCondition formula status conditions
		* @param pSharedMutex  pointer to shared mutex (using toolkit)
		*/
//		UaCalcItem( const UaString& name,UaNodeId& uaId, 
//			NmBuildingAutomation * pNodeConfig, UaVariant &dv,string &formul, string &whenCondition, string &statusCondition,UaMutexRefCounted* pSharedMutex );

		UaCalcItem(const UaString& name, UaNodeId& uaId, const UaNode *parent,
			NmBuildingAutomation * pNodeConfig, UaVariant &dv, ITEM &citem, UaControlVariableMap &cvs, UaMutexRefCounted* pSharedMutex);

		bool compile();

		//! destructor
		virtual ~UaCalcItem(void) {}

		/**
		* Connect to the input object. Set callback function
		* @param name name of control variable
		* @param obd pointer to the input control variable
		*/
		bool pushDataVariable(const string& name,UaControlVariable *obd)
			{ 
				bool res;

				boost::arg<1> _1;
				boost::arg<2> _2;
				boost::arg<3> _3;
				m_var.insert(pair<string, UaControlVariable *>(name, obd));
				res = m_formul_table.add_variable(name,obd->curValue);

				if (!m_cExists) 
					obd->valueChange.connect(boost::bind(&UaCalcItem::calculateOnChange,this,_1,_2,_3));
				return res;
		}

		/**
		* Connect to the object using in conditional formula. Set callback function
		* @param name name of control variable
		* @param obd pointer to the input control variable
		*/
		bool pushConditionVariable(const string& name, UaControlVariable *obd)
			{
				bool res =  true;
	
				boost::arg<1> _1;
				boost::arg<2> _2;
				boost::arg<3> _3;
				m_whenVar.insert(pair<string, UaControlVariable *>(name, obd));
//				res = m_whenCondition_table.add_variable(name, obd->curValue);
				res = m_formul_table.add_variable(name, obd->curValue);

				obd->valueChange.connect(boost::bind(&UaCalcItem::calculateOnChange,this,_1,_2,_3));
				return res;
		}
		/**
		* Connect to the object using in conditional formula. Set callback function
		* @param name name of control variable
		* @param obd pointer to the input control variable
		*/
		bool pushStatusVariable(const string& name, UaControlVariable *obd)
			{ 
				bool res;

				m_statusVar.insert(pair<string, UaControlVariable *>(name, obd));
//				res = m_statusFormula_table.add_variable(name, obd->curValue);
				res = m_formul_table.add_variable(name, obd->curValue);

				return res;
			}

		bool fExists() { return m_fExists; }
		
		void replaceAll(std::string& str, const std::string& from, const std::string& to) {
    		if(from.empty())
        		return;
    		size_t start_pos = 0;
    		while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        		str.replace(start_pos, from.length(), to);
        		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
    	}
}
		bool checkDouble(const string& str, double &res)
		{
			size_t indx = 0;
			m_fExists = true;
			try {
				res = stod(str,&indx);
			}
			catch (...)
			{
				return false;
			}
			if (str.size() > indx)
				return false;
			else {
				m_fExists = false;
				return true;
			}
	
		}

/*
		void readData(Session *pSession, variableSet &vs)
		{
			for (auto &it: vs )
			{

				if (!(it.second->active)) {
					UaDataValue udv = it.second->ucv->value(pSession);
					it.second->value = convertToDouble(udv);
				}
			}
		}
*/

		OpcUa_StatusCode checkStatus(variableSet& vs)
		{
			OpcUa_StatusCode ret = OpcUa_Good;
			for (auto const &vit : vs)
			{
				UaControlVariable *uac = vit.second;
				ret = uac->value(0).statusCode();
//				cout << "Status Code " << uac->browseName().toString().toUtf8() << " "<< hex << ret << " value "<< uac->curValue << endl << flush;
				if (ret != OpcUa_Good)
					break;
			}
			return ret;
		}

/*		double convertToDouble(const UaDataValue &dataValue)
		{
			double res = 0.0;
			const OpcUa_Variant *val = dataValue.value();
			switch (val->Datatype) {
			case    OpcUaType_Boolean:
				res = val->Value.Boolean;
				break;
			case OpcUaType_SByte:
				res = val->Value.SByte;
				break;
			case OpcUaType_Byte:
				res = val->Value.Byte;
				break;
			case OpcUaType_Int16:
				res = val->Value.Int16;
				break;
			case OpcUaType_UInt16:
				res = val->Value.UInt16;
				break;
			case OpcUaType_Int32:
				res = val->Value.Int32;
				break;
			case OpcUaType_UInt32:
				res = val->Value.UInt32;
				break;
			case OpcUaType_Int64:
				res = (double)val->Value.Int64;
				break;
			case OpcUaType_UInt64:
				res = (double)val->Value.UInt64;
				break;
			case OpcUaType_Float:
				res = val->Value.Float;
				break;
			case OpcUaType_Double:
				res = val->Value.Double;
				break;
			default:
				throw 1;
				//			res = 0.0;
			};
			return res;
		}
		*/
		const OpcUa::BaseObjectType *m_pParent;
		NmBuildingAutomation * m_pNodeManager;

		/**
		* bind function which calculator uses to take data from OpcUa variable

		double getValueFromItem(std::string);
		double getValueFromWhenItem(std::string);
		double getValueFromStatusItem(std::string);
		*/
	private:

		expression_t	m_formul;
		expression_t	m_whenCondition;
		expression_t	m_statusFormula;

		symbol_table_t m_formul_table;
//		symbol_table_t m_whenCondition_table;
//		symbol_table_t m_statusFormula_table;

//		parser_t   parser_d;
//		parser_t   parser_b;
//		parser_t   parser_i;

		string m_sFormul;					//! formula in text form
		string m_sWhenCondition;			//! condition formula
		string m_sStatusCondition;			//! status logical formula

		bool m_fExists;
		bool m_cExists;
		bool m_sExists;

		variableSet m_whenVar;
		variableSet m_var;
		variableSet m_statusVar;

	};

}
#endif
  
