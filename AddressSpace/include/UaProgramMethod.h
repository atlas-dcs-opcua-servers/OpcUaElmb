#ifndef __UAPROGRAMMETHOD_H__
#define __UAPROGRAMMETHOD_H__

#include "Ids.h"
#include "UaDeviceMethod.h"

namespace AddressSpace
{
	/** @brief the class to execute method on a can buss
	* Inherits ua BaseMethod and contains hardware interface connection.
	*/

	template<class T>
	class UaProgramMethod : public UaDeviceMethod<T>
	{
		UA_DISABLE_COPY(UaProgramMethod);
	public:
		/** @brief Constructor
		* @param cur parent ua node
		* @param pNodeManager pointer to node manager
		* @param instance pointer to method type
		* @param pSharedMutex
		*/
		UaProgramMethod(T* cur , NmBuildingAutomation* pNodeManager,UaMethod* instance,UaMutexRefCounted *pSharedMutex)
			 :UaDeviceMethod<T>(cur,pNodeManager,instance,pSharedMutex) 
		{
//			m_iCode = instance->nodeId().identifierNumeric();
		}

		UaProgramMethod(T* cur, const UaNodeId& nodeId, const UaString& name, NmBuildingAutomation* pNodeManager, OpcUa_UInt32 icode, UaMutexRefCounted *pSharedMutex)
			:UaDeviceMethod<T>(cur, nodeId, name, pNodeManager, icode, pSharedMutex)
		{
//			m_iCode = icode;
		}

		//! execute the method
		virtual UaStatus execute() 
		{
			UaStatus ret,rets;
			UaControlDeviceItem *ptNode;
			UaControlVariable *pValue;
			OpcUa_UInt32 refType;

			UaReferenceLists *uaList = this->getUaReferenceLists();			// take the list children
			for (UaReference *pRef = (UaReference *)uaList->pTargetNodes(); pRef != 0; pRef = pRef->pNextForwardReference()) {
				refType = pRef->referenceTypeId().identifierNumeric();

				switch (refType) {
				case BA_HASADDRESS:
					ptNode = (UaControlDeviceItem *)pRef->pTargetNode(); 	// take address of operation
					break;
				case BA_HASVALUE:
					pValue = (UaControlVariable *)pRef->pTargetNode();
					break;

				default:

					break;
				}
			}

			OpcUa_UInt32 fCode = this->getFunctionCode();

			if (fCode == BA_SDOREAD)
			{
				ret = ptNode->read();
			}
			else
				if (fCode == BA_SDOWRITE)
				{
					UaDataValue dataValue = pValue->value(0);
					UaDateTime udt, sdt;
					ret = ptNode->write(dataValue);
					if (ret.isBad()) {
						UaVariant val = *pValue->value(0).value();
						udt = pValue->value(0).sourceTimestamp();
						sdt = UaDateTime::now();
						dataValue.setDataValue(val, OpcUa_False, ret.statusCode(), udt, sdt);
						rets = pValue->setValue(0, dataValue, OpcUa_False);
					}
				}


			return ret;
		} 
		
		//! Destructor
		virtual ~UaProgramMethod(void)
		{
		};


	protected:
	};
}
#endif
  
