#ifndef __UADEVICENODETYPE_H__
#define __UADEVICENODETYPE_H__

#include "uavariant.h"
#include "NmBuildingAutomation.h"
#include "CANOpenServerConfig.h"
#include "uaobjecttypes.h"

namespace AddressSpace
{
	/** @brief Generic class including the pointer to hardware object
	*/
	class UaDeviceNodeType:
		public UaObjectTypeSimple
	{
	public:
		/** @brief Constructor inherits UaObjectTypeSimple parameters
		* @param name of Ua Node
		* @param nodeId ua node id
		* @param defaultLocaleId locale id for objects of this class
		* @param isAbstract whether this class abstract (cannot be an instant of object)
		*/
		UaDeviceNodeType(  const UaString& name, 
						const UaNodeId& nodeId, 
						const UaString& defaultLocaleId, 
						OpcUa_Boolean   isAbstract) :
				UaObjectTypeSimple(name,nodeId,defaultLocaleId,isAbstract), m_NodeTypeXsd(0)	
		{
			setUserData(0);
		} ;
		//! Destructor
		virtual ~UaDeviceNodeType(void) {};
		//! get hardware interface object
		virtual NODETYPE *getXsdData() { return m_NodeTypeXsd; }
		//! to set hardware interface object
		virtual void setXsdData(NODETYPE  *userData) { m_NodeTypeXsd = userData; }
	private:
		NODETYPE   *m_NodeTypeXsd; //! pointer to hardware interface object
	};
}
#endif
