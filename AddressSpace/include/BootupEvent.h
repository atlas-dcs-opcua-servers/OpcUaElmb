#ifndef BOOTUPEVENT_H
#define BOOTUPEVENT_H

#include "uaeventdata.h"
#include "BuildingAutomationTypeIds.h"
#include "eventmanageruanode.h"

/**
* Bootup event arrases when the boot up message is come
*/
class BootupEvent: public BaseEventTypeData
{
    UA_DISABLE_COPY(BootupEvent);
public:
    BootupEvent(OpcUa_Int16 nsIdx)
	{
		m_nsIdx = nsIdx;
		m_EventTypeId.setNodeId(Ba_BootupEventType, m_nsIdx);
	}
	virtual ~BootupEvent() {}

    void registerEventFields()
	{
		// Register event type
		EventManagerUaNode::registerEventType(OpcUaId_BaseEventType, m_EventTypeId);
	}

	virtual void getFieldData(OpcUa_UInt32 index, Session* pSession, OpcUa_Variant& data)
	{
		BaseEventTypeData::getFieldData(index, pSession, data);
	}

    OpcUa_Int16 m_nsIdx;

private:
//    static map<OpcUa_UInt32, OpcUa_UInt32> s_BootupEventFields;
};

#endif // BOOTUPEVENT_H