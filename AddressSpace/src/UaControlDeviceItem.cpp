#include "UaControlDeviceItem.h"
#include "CanPDOItem.h"
#include "NmBuildingAutomation.h"
#include "UaControlNode.h"

using namespace UserDevice;
using namespace CanOpen;

namespace AddressSpace
{
	UaControlVariable::UaControlVariable (
			const UaString& name, 
			const UaNodeId& newNodeId,
			NmBuildingAutomation* pNodeManager,
			UaVariant &dv,
			UaMutexRefCounted* pSharedMutex 
			) : OpcUa::BaseDataVariableType(newNodeId,name,pNodeManager->getNameSpaceIndex(),
				dv, Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite,
				pNodeManager, pSharedMutex) {		if (pSharedMutex) pSharedMutex->addReference();	}

	UaControlVariable::UaControlVariable(
		UaNode *cur,
		NmBuildingAutomation* pNodeManager,
		UaVariable *instance,
		UaMutexRefCounted* pSharedMutex
	) : OpcUa::BaseDataVariableType(cur, instance, pNodeManager, pSharedMutex) { if (pSharedMutex) pSharedMutex->addReference(); }

	UaControlDeviceItem::UaControlDeviceItem(
		UaNode *cur,
		NmBuildingAutomation* pNodeManager,
		UaVariable *instance,
		::xml_schema::type *conf,
		pUserDeviceStruct* interf,
		UaMutexRefCounted* pSharedMutex
	) : UaControlVariable(cur,  pNodeManager, instance, pSharedMutex)
	{
		UaStatus addStatus;
		UaNodeId nodeTypeId = instance->nodeId();
		pUserDeviceStruct* pCanInter = pNodeManager->m_pControlInterface->createInterfaceEntry(interf, nodeTypeId.identifierNumeric(), conf, this);
		//if (pCanInter == interf) {
		//	UserDevice::AbstractHardware *newd = new DEVICE<CanOpen::CanOpenItem, ::xml_schema::type>();
		//	pCanInter = newd->createHardwareEntry(interf, conf, this, nodeTypeId.identifierNumeric());
		//	delete newd;
		//}
		//if (pCanInter)
		//	pCanInter->addReference();
		setUserData(pCanInter);							// Set interface for this Item 				

		addStatus = UaControlNode::UaControlNodeCreateByType<UaControlDeviceItem>(this, instance, pNodeManager, conf, pCanInter, pSharedMutex);  // create object structure base on type
		UA_ASSERT(addStatus.isGood());
	}

	UaControlDeviceItem::		
		UaControlDeviceItem (
		const UaString& name, 
		const UaNodeId& newNodeId,
		NmBuildingAutomation* pNodeManager,
		UaVariant &dv,
		UaVariableType *instance,
		::xml_schema::type *conf,
		pUserDeviceStruct* interf,
		UaMutexRefCounted* pSharedMutex
		) : UaControlVariable (name,newNodeId,pNodeManager, dv,pSharedMutex)
	{
		UaStatus addStatus;
		UaNodeId nodeTypeId = instance->nodeId();
		setTypeDefinition(nodeTypeId);
		pUserDeviceStruct* pCanInter = pNodeManager->m_pControlInterface->createInterfaceEntry(interf,nodeTypeId.identifierNumeric() ,conf,this);
		setUserData(pCanInter);							// Set interface for this Item 				

		//		if (pCanInter)
		//			pCanInter->addReference();

		addStatus =UaControlNode::UaControlNodeCreateByType<UaControlDeviceItem>(this, instance, pNodeManager, conf, pCanInter,pSharedMutex);  // create object structure base on type
		UA_ASSERT(addStatus.isGood());
	}

/*
	UaControlDeviceItem::
		UaControlDeviceItem (
		const UaString& name, 
		const UaNodeId& newNodeId,
		NmBuildingAutomation* pNodeManager,
		UaVariant &dv,
		OpcUa_Int32 baTypeId,	
		::xml_schema::type *conf,
		UserDataBase* interf,
		UaMutexRefCounted* pSharedMutex
		) : UaControlVariable (name,newNodeId,pNodeManager, dv,pSharedMutex)
	{

		UserDataBase* pCanInter = pNodeManager->m_pControlInterface->createInterfaceEntry(interf,baTypeId,conf,this);		
		setUserData(pCanInter);							// Set interface for this Item 				
		setTypeDefinition(baTypeId);
	}
*/	

	UaStatus UaControlDeviceItem::write(UaDataValue &udv)
	{
		pUserDeviceStruct* iubd = static_cast<pUserDeviceStruct *>(getUserData());
		if (iubd) {
//			cout << this->browseName().toString().toUtf8() << endl;
			CanOpenItem * cpdoi = static_cast<CanOpenItem *>(iubd->getDevice());
			UaStatus ret = cpdoi->write(udv);
			return ret;
		}
		return OpcUa_False;
	}
/*
	UaNodeId UaControlDeviceItem::typeDefinitionId() const
	{
		UserDataBase * udb = getUserData();
//		InterfaceUserDataBase* iubd = ((UserDataBase_ptr<InterfaceUserDataBase> *)udb)->getDevice();
		InterfaceUserDataBase* iubd = (InterfaceUserDataBase *)udb;
		UaNodeId ret(iubd->getCodeFunction(), browseName().namespaceIndex());
		return ret;
	}
*/
	UaStatus UaControlDeviceItem::read() {
		UaStatus Status = OpcUa_Good;

		pUserDeviceStruct* iubd = static_cast<pUserDeviceStruct *>(getUserData());
		if (iubd) {
			UserDeviceItem * cpdoi = static_cast<UserDeviceItem *>(iubd->getDevice());
			UaStatus ret = cpdoi->read();
			return ret;

		}
		return OpcUa_False;

	}
	UaStatus UaControlDeviceItem::read(IOVariableCallback *iom) { 
		UaStatus Status;
		UaDataValue dataValue;

		pUserDeviceStruct* iubd = static_cast<pUserDeviceStruct *>(getUserData());
		Status = static_cast<UserDeviceItem*>(iubd->getDevice())->read();
		if (Status.isGood()) {
			dataValue = this->value(0);
			iom->dataChange(dataValue);
		}
		return Status;
	}
}
