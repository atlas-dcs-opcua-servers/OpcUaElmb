#include "UaControlDeviceGeneric.h"
#include "UaControlDeviceItem.h"
#include "NmBuildingAutomation.h"
#include "UaControlDeviceProperty.h"
#include "methodhandleuanode.h"
#include "UaDeviceMethod.h"

namespace AddressSpace
{
	template<class T>
	UaStatus UaControlNode<T>::UaControlNodeCreateByType(T *cur, UaNode* instance, NmBuildingAutomation* pNodeManager,::xml_schema::type *conf,	UserDataBase* interf)
	{
		OpcUa::BaseObjectTypeGeneric*	pDeclarationObject = NULL;
		OpcUa::BaseMethod*           pMethodDeclaration = NULL;
		OpcUa::BaseDataVariableType* pDataVariable        = NULL;
		UaControlDeviceProperty * pProperty;
		UaStatus                    addStatus;
		UaNodeId					nTypeId;   // ID of node type
		UaNodeId					uaId;		//new id
		UaVariant                   defaultValue;
		defaultValue.clear();
		/**************************************************************
		* Create the Device components
		**************************************************************/

		UaReferenceLists *uaList  = instance->getUaReferenceLists();			// take the list children
		for ( UaReference *pRef = (UaReference *)uaList->pTargetNodes(); pRef != 0; pRef = pRef->pNextForwardReference() ) {
			OpcUa_UInt32 refType = pRef->referenceTypeId().identifierNumeric();

			UaNode *ptNode = pRef->pTargetNode(); 					// take child node

//			nTypeId  = ptNode->nodeId();
			UaString newName(ptNode->browseName().name());
			uaId = pNodeManager->getNewNodeId(cur,newName);
//			printf("Node Class %d %s %d %s\n", ptNode->nodeClass(), ptNode->displayName(0).toString().toUtf8(),refType, cur->displayName(0).toString().toUtf8());
			switch (refType) {
			case OpcUaId_HasComponent:
				switch (ptNode->nodeClass()) {
				case OpcUa_NodeClass_Variable:
					pDataVariable = new UaControlDeviceItem(newName,uaId,pNodeManager,defaultValue,(UaVariableType*)ptNode,conf,interf,getObjectMutex());
					addStatus = pNodeManager->addNodeAndReference(cur,pDataVariable,OpcUaId_HasComponent);
					UA_ASSERT(addStatus.isGood());
					break;
				case OpcUa_NodeClass_ObjectType:
					pDeclarationObject = new UaControlDeviceGeneric(newName,uaId,pNodeManager,(UaObject *)ptNode,conf,interf);
					addStatus = pNodeManager->addNodeAndReference(cur,pDeclarationObject,OpcUaId_HasComponent);
					UA_ASSERT(addStatus.isGood());
					break;
				case OpcUa_NodeClass_Method:
					pMethodDeclaration = 	new UaDeviceMethod(cur,pNodeManager,(UaMethod *)ptNode,getObjectMutex());
					addStatus = pNodeManager->addNodeAndReference(cur,pMethodDeclaration,OpcUaId_HasComponent);
					UA_ASSERT(addStatus.isGood());
					m_pMethod.push_back(pMethodDeclaration);
					break;
				}
				break;
			case OpcUaId_HasSubtype:
//				printf("Node Class subtype %d %s\n", ptNode->nodeClass(), ptNode->displayName(0).toString().toUtf8());
				addStatus = UaControlNodeCreateByType(cur,ptNode,pNodeManager,conf,interf);
				UA_ASSERT(addStatus.isGood());
				break;
			case OpcUaId_HasProperty:
				pProperty = new UaControlDeviceProperty(cur, pNodeManager, (UaVariable*)ptNode, conf, interf, getObjectMutex());
				addStatus = pNodeManager->addNodeAndReference(cur, pProperty, OpcUaId_HasProperty);
//				addStatus = pNodeManager->addNodeAndReference(cur->getUaReferenceLists(), pProperty, OpcUaId_HasProperty);
				if (!addStatus.isGood())
				{
					std::cout << "While creating a property: " << pProperty->browseName().toString().toUtf8() << endl;
					UA_ASSERT(addStatus.isGood());
					return addStatus;
				}
				break;
			}
		}
		return addStatus;
	}
}