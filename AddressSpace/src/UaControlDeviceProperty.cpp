#include "UaControlDeviceProperty.h"
#include "NmBuildingAutomation.h"
using namespace UserDevice;

namespace AddressSpace
{
	UaControlDeviceProperty::
		UaControlDeviceProperty(
		UaNode *cur,
		NmBuildingAutomation* pNodeManager,
		UaVariable *instance,
		::xml_schema::type *conf,
		InterfaceUserDataBase* interf,
		UaMutexRefCounted* pSharedMutex
		) : OpcUa::PropertyType(cur, instance, pNodeManager, pSharedMutex)
	{
		UaNodeId nodeTypeId = instance->dataType();
		UaNodeId nodeId = instance->nodeId();
		setDataType(nodeTypeId);

		//InterfaceUserDataBase* pCanInter = pNodeManager->m_pControlInterface->createInterfaceEntry(interf, nodeId.identifierNumeric(), conf, this);
		//setUserData(pCanInter);
		//if (pCanInter)
		//	pCanInter->addReference();
		
	}

	UaControlDeviceProperty::~UaControlDeviceProperty()
	{
	//	InterfaceUserDataBase * iudb = static_cast<InterfaceUserDataBase*>(getUserData());

	//	if (iudb) {
	//		iudb->releaseMutex();
	//		if (iudb->countMutex() == 0)
	//			delete iudb;
	//		setUserData(0);
	//	}
	}
}
