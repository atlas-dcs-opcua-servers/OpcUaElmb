#include "CompileMathExpression.h"
#include "TMath.h"
#include "UaCanTrace.h"

std::vector<struct funcTable> TMath::functionSet(100);

/*
double OpcUaFormula::_eval(operands_type_set::reverse_iterator &in , double (*getValueFromItem)(std::string))
{
	int nType = in->which();
	double tmp;
	switch (nType) {
	case 0:
		{
			funcTable ft = boost::get<funcTable>(*in);
			switch (ft.num_param)
			{
			case 0: tmp = ft.p_func.ptr0Func();
					break;
			case 1: tmp = ft.p_func.ptr1Func(_eval(++in,getValueFromItem));
					break;
			case 2: tmp = ft.p_func.ptr2Func(_eval(++in,getValueFromItem),_eval(++in,getValueFromItem));
					break;
			case 3: tmp = ft.p_func.ptr3Func(_eval(++in,getValueFromItem),_eval(++in,getValueFromItem),_eval(++in,getValueFromItem));
					break;
			};
			return tmp;

		}
	case 1: return boost::get<int>((*in));
	case 2: return boost::get<double>((*in));
	case 3: 
		{
			return (*getValueFromItem)(boost::get<std::string>((*in)));
		}
	}
	return 0.0;
};
*/

double OpcUaFormula::_eval(operands_type_set::reverse_iterator &in )
{
	int nType = in->which();
	double tmp = 0.0, par1 = 0.0, par2 = 0.0, par3 = 0.0;

	switch (nType) {
	case 0:
		{
			funcTable ft = boost::get<funcTable>(*in);

			switch (ft.num_param)
			{
			case 0: 
				tmp = ft.p_func.ptr0Func();
				break; 
			case 1: 
				in++;
				par1 = _eval(in);
				tmp = ft.p_func.ptr1Func(par1);
				break;
			case 2:
				in++;
				par1 = _eval(in);
				in++; 
				par2 = _eval(in);
				tmp = ft.p_func.ptr2Func(par2,par1);
				break;
			case 3: 
				in++;
				par1 = _eval(in);
				in++;
				par2 = _eval(in);
				in++;
				par3 = _eval(in);
				tmp = ft.p_func.ptr3Func(par3,par2,par1);
				break;
			};
			break;

		}
	case 1: 
		tmp = boost::get<int>((*in));
		break;
	case 2: 
		tmp =  boost::get<double>((*in));
		break;
	case 3:
		tmp = getValueFromItem(boost::get<std::string>((*in)));
		break;
	}
	return  tmp;
};

bool OpcUaFormula::_evalCondition(operands_type_set::reverse_iterator &in )
{
	int nType = in->which();
	unsigned int idn;
	bool tmpbool = true,parl1 = true,parl2 = true ;
	
	if (nType == 0) {
		funcTable ft = boost::get<funcTable>(*in);
		idn = ft.iden;
		if ( isCmdFunction(idn) ) {
			in++; 
			double par1 = 0.0,par2 = 0.0;
			par1 = _eval(in);
			in++;
			par2 = _eval(in); 
			tmpbool = ft.p_func.ptr2LDFunc(par2,par1);
		}
		else
			switch (ft.num_param)
			{
			case 0: 
				tmpbool = ft.p_func.ptr0LFunc();
				break;
			case 1: 
				in++;
				parl1 = _evalCondition(in); 
				tmpbool = ft.p_func.ptr1LFunc(parl1);
				break;
			case 2:
				in++;
				parl1 = _evalCondition(in); 
				in++;
				parl2 = _evalCondition(in);
				tmpbool = ft.p_func.ptr2LFunc(parl2,parl1);
				break;
			}
	}
	return tmpbool;
};

double OpcUaFormula::eval()
{
	double val = 0.0;
	operands_type_set::reverse_iterator op = opt.rbegin();
	
	val = _eval(op); 

	return val;
};

bool OpcUaFormula::evalCondition()
{
	operands_type_set::reverse_iterator op = opt.rbegin();

	bool val = true;
	try {
		val = _evalCondition(op); 
	}
	catch (...)
	{
		LOG(Log::INF) << "Evaluation fault!";
		std::cout << "Evaluation fault!" << std::endl;
		return  false;
	}
	return val;
};

bool OpcUaFormula::Compile(std::string formul,bool logical) 
{
//	typedef std::string::iterator Iterator;

//	typedef lex::lexertl::token<Iterator, lex::omit, boost::mpl::true_> token_type;

//	typedef lex::lexertl::lexer<token_type> lexer_type;

	typedef OpcUa_Formula_Tokens<lexer_type> OpcUaFormulaTokens;
	typedef qi::in_state_skipper<OpcUaFormulaTokens::lexer_def> skipper_type;
	typedef OpcUaFormulaTokens::iterator_type iterator_type;
	

	typedef OpcUa_Formula_Grammar_type< iterator_type,skipper_type> OpcUa_Formula_Grammar;

	OpcUaFormulaTokens  OpcUa_Formula_lexer;

	OpcUa_Formula_Grammar OpcUa_Formula_parser( OpcUa_Formula_lexer, opt,logical);
	// At this point we generate the iterator pair used to expose the
    // tokenized input stream.
    std::string::iterator iter = formul.begin();
	std::string::iterator end = formul.end();

    // Parsing is done based on the the token stream, not the character 
    // stream read from the input.
    // Note how we use the lexer defined above as the skip parser. It must
    // be explicitly wrapped inside a state directive, switching the lexer 
    // state for the duration of skipping whitespace.
    std::string ws("WS");
    bool r = tokenize_and_phrase_parse(iter, end, OpcUa_Formula_lexer,OpcUa_Formula_parser, qi::in_state(ws)[OpcUa_Formula_lexer.self]);
//	bool r = qi::phrase_parse(iter, end, OpcUa_Formula_parser, qi::in_state(ws)[OpcUa_Formula_lexer.self]);
    
	if (r  && (iter == end ) )
    {
        /*
		std::cout << "Exit " << r << std::endl;
		for (std::string::iterator itt = iter; itt != end; itt++)
		{
			std::cout << *itt << std::endl;
		}
*/
		for ( operands_type_set::iterator itt = opt.begin(); itt != opt.end(); itt++)
		{
			if (itt->which() == 3) {
				std::string a =  boost::get<std::string>((*itt));
				if (a[0] == '$')
					sVariables.push_back(a);
			}
//			std::cout << *itt << std::endl;
		}

		return true;
    }
	LOG(Log::INF) << "Parsing failed " << formul;

	std::cout << "-------------------------" << std::endl;
	std::cout << "Parsing failed " << formul << std::endl;
	std::cout << "-------------------------" << std::endl;

	for ( std::string::iterator itt = iter; itt != end; itt++)
    {
		LOG(Log::INF) << *itt;
    }

    return false;
};
