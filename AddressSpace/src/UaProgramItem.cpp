//#include "BuildingAutomationTypeIds.h"
#include "UaProgramStartVariable.h"
#include "UaProgramItem.h"
#include "UaProgramMethod.h"
#include "CANOpenServerConfig.h"
#include "UaControlDeviceItem.h"

using namespace UserDevice;

namespace AddressSpace
{
	/**
	* Constructor 
	* @param ca canbus XML description
	* @param newNodeId  CanBus Node ID
	* @param pNodeManager reference to node Manager for this node
	**/
//	UaProgramItem::UaProgramItem(PROGRAM *pt, const UaNodeId& newNodeId, NmBuildingAutomation* pNodeManager, UaControlDeviceGeneric* parent,UserDataBase* puds)
//		: UaControlDeviceGeneric(UaString(pt->name().c_str()), newNodeId, pNodeManager, BA_PROGRAM, pt, puds)
//		: OpcUa::BaseObjectTypeGeneric(newNodeId, UaString(pt->name().c_str()), pNodeManager->getNameSpaceIndex(), pNodeManager)
//	UaProgramItem::UaProgramItem(PROGRAM *pt, UaObject *pNodeType, const UaNodeId& newNodeId, NmBuildingAutomation *pNodeManager, UaControlDeviceGeneric* parent, UserDataBase* puds)
//	: UaControlDeviceGeneric(newNodeId, pNodeManager, pNodeType, pt, puds)

	UaProgramItem::UaProgramItem(PROGRAM *pt, UaObjectType *pNodeType, const UaNodeId& newNodeId, NmBuildingAutomation *pNodeManager, UaControlDeviceGeneric* parent, pUserDeviceStruct * puds)
		: UaControlDeviceGeneric(UaString(pt->name().c_str()),newNodeId, pNodeManager, pNodeType, pt, puds)

	{
//		OpcUa_UInt16 indx = pNodeManager->m_pControlInterface-> ->getNameSpaceIndex();
		UaStatus ret = OpcUa_Good;
		UaProgramStartVariable *pStartProgramItem;

		UaNodeId	uaId, uaMethId;
		UaVariant	defaultValue;
		
		defaultValue.setBool(0);
		uaId = pNodeManager->getNewNodeId(this, "StartProgram");

		pStartProgramItem = new UaProgramStartVariable("StartProgram", uaId, pNodeManager, defaultValue, this, parent->getSharedMutex());

		pStartProgramItem->setValueHandling(UaVariable_Value_Cache);

		ret = pNodeManager->addNodeAndReference(this, pStartProgramItem, OpcUaId_HasComponent);

		UA_ASSERT(ret.isGood());

		defaultValue.setStatusCode(OpcUa_Good);
		uaId = pNodeManager->getNewNodeId(this, "Error");

		m_pErrorItem = new UaControlVariable("Error", uaId, pNodeManager, defaultValue, parent->getSharedMutex());

		m_pErrorItem->setValueHandling(UaVariable_Value_Cache);

		ret = pNodeManager->addNodeAndReference(this, m_pErrorItem, OpcUaId_HasComponent);

		UA_ASSERT(ret.isGood());

		PROGRAM::METHOD_sequence mitem1 = pt->METHOD();

		for (PROGRAM::METHOD_iterator mitem = mitem1.begin(); mitem != mitem1.end(); mitem++)
		{
			OpcUa_UInt32 numericIdentifier;
			string mname = mitem->command().get();
			if (mname == "read")
				numericIdentifier = BA_SDOREAD;
			else if (mname == "write")
				numericIdentifier = BA_SDOWRITE;
			else {
				UaNodeId uaMethId = pNodeManager->getNewNodeId(parent, mname.c_str());
				UaDeviceMethod<UaControlDeviceGeneric> * methNode = (UaDeviceMethod<UaControlDeviceGeneric> *)(pNodeManager->findNode(uaMethId));
				ret = pNodeManager->addUaReference(this, methNode, OpcUaId_HasComponent);
				UA_ASSERT(ret.isGood());
				continue;
			}
//			UaMethod* instance = pNodeManager->getInstanceDeclarationMethod(numericIdentifier);		//! get a method based on numerical id ( if not return null)

			UaString address = mitem->address().get().c_str();

			UaNodeId adrNodeId = pNodeManager->getNewNodeId(parent, address);
			UaControlDeviceItem * adrNode = (UaControlDeviceItem*)(pNodeManager->findNode(adrNodeId));

			UaString name = mname.c_str();
			UaNodeId uaId = pNodeManager->getNewNodeId(this, name+"_"+address);
			UaProgramMethod<UaControlDeviceGeneric> *pUaMethod = new UaProgramMethod<UaControlDeviceGeneric>(this, uaId, name + "_" + address, pNodeManager, numericIdentifier, parent->getSharedMutex());

//			UaDeviceMethod *pUaMethod = new UaDeviceMethod(this, pNodeManager, instance, NULL);

			ret = pNodeManager->addNodeAndReference(this, pUaMethod, OpcUaId_HasComponent);
			addMethod(pUaMethod);
			UA_ASSERT(ret.isGood());

			ret = pNodeManager->addUaReference(pUaMethod, adrNode, pNodeManager->m_pControlInterface->getTypeNodeId(BA_HASADDRESS)); //UaNodeId(BA_HASADDRESS, indx));
			UA_ASSERT(ret.isGood());

			UaString valueMethod = mitem->value().get().c_str();
			UaNodeId vNodeId = pNodeManager->getNewNodeId(this, valueMethod);
			defaultValue = *adrNode->value(0).value();
//			UaControlVariable *ucv = new UaControlVariable(valueMethod, vNodeId, pNodeManager, UaVariant(*adrNode->value(0).value()), NULL);
			UaControlVariable *ucv = new UaControlVariable(valueMethod, vNodeId, pNodeManager, defaultValue, NULL);

			ret = pNodeManager->addNodeAndReference(this, ucv, OpcUaId_HasComponent);
			UA_ASSERT(ret.isGood());

			ret = pNodeManager->addUaReference(pUaMethod, ucv, pNodeManager->m_pControlInterface->getTypeNodeId(BA_HASVALUE)); //UaNodeId(BA_HASVALUE, indx));
			UA_ASSERT(ret.isGood());
			if (numericIdentifier == BA_SDOREAD)
				ucv->connectItem(adrNode);
		}

	}

/*
	UaProgramItem::UaProgramItem(PROGRAM *pt, UaObject *pNodeType, const UaNodeId& newNodeId, NmBuildingAutomation *pNodeManager, UaControlDeviceGeneric* parent, UserDataBase* puds)

		: UaControlDeviceGeneric(newNodeId, pNodeManager, pNodeType, pt, puds)

	{
		UaStatus ret = OpcUa_Good;
		PROGRAM::METHOD_sequence mitem1 = pt->METHOD();

		for (PROGRAM::METHOD_iterator mitem = mitem1.begin(); mitem != mitem1.end(); mitem++)
		{
			OpcUa_UInt32 numericIdentifier;
			string mname = mitem->command().get();
			if (mname == "read")
				numericIdentifier = BA_SDOREAD;
			else if (mname == "write")
				numericIdentifier = BA_SDOWRITE;
			else {
				return;
			}
			UaMethod* instance = pNodeManager->getInstanceDeclarationMethod(numericIdentifier);		//! get a method based on numerical id ( if not return null)

			UaString address = mitem->address().get().c_str();
			UaNode * nNode = pNodeManager->getUpNode(this);
			UaNodeId adrNodeId = pNodeManager->getNewNodeId(nNode, address);
			UaControlDeviceItem * adrNode = (UaControlDeviceItem*)(pNodeManager->findNode(adrNodeId));

			UaString name = mname.c_str();
			UaDeviceMethod *pUaMethod = new UaDeviceMethod(puds, pNodeManager, instance, NULL);

			ret = pNodeManager->addNodeAndReference(this, pUaMethod, OpcUaId_HasComponent);
			UA_ASSERT(ret.isGood());

			ret = pNodeManager->addNodeAndReference(pUaMethod, adrNode, UaNodeId(Ba_ReferenceAddress, pNodeManager->getNameSpaceIndex()));
			UA_ASSERT(ret.isGood());

			UaString valueMethod = mitem->value().get().c_str();
			UaNodeId vNodeId = pNodeManager->getNewNodeId(nNode, valueMethod);
			UaControlVariable *ucv = new UaControlVariable(valueMethod, vNodeId, pNodeManager, UaVariant(*adrNode->value(0).value()), NULL);

			ret = pNodeManager->addNodeAndReference(pUaMethod, adrNode, (const UaNodeId)UaNodeId(Ba_ReferenceValue, pNodeManager->getNameSpaceIndex()));
			UA_ASSERT(ret.isGood());
			if (numericIdentifier == BA_SDOREAD)
				ucv->connectItem(adrNode);
		}
	
	}
	*/
	
	void UaProgramItem::executeProgram()
	{
		UaStatus ret = OpcUa_Good;

		UaReferenceLists *uaList = getUaReferenceLists();			// take the list children
		for (UaReference *pRef = (UaReference *)uaList->pTargetNodes(); pRef != 0; pRef = pRef->pNextForwardReference()) {
			UaNode *ptNode = pRef->pTargetNode();
			if (ptNode->nodeClass() == OpcUa_NodeClass_Method) {
				ret = ((UaDeviceMethod<UaControlDeviceGeneric> *)ptNode)->execute();

				UaDataValue dataValue;
				UaDateTime udt, sdt;
				udt = UaDateTime::now();
				sdt = UaDateTime::now();
				UaVariant val = ret.statusCode();
				dataValue.setDataValue(val, OpcUa_False, ret.statusCode(), udt, sdt);
				m_pErrorItem->setValue(0, dataValue, OpcUa_False);
				if (ret.isBad())
					return;
			
			}
		}
	}
}
