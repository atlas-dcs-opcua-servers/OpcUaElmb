//#include "BuildingAutomationTypeIds.h"
#include "NmBuildingAutomation.h"
#include "variablehandleuanode.h"
#include <opcua_dataitemtype.h>
#include "UaCanBusObject.h"
#include "UaControlDeviceGeneric.h"
#include "UaControlDeviceItem.h"
#include "UaDeviceNodeType.h"
#include "UaCanNodeObject.h"
//#include "CanOpenConfigXML.h"
#include "srvtrace.h"
#include "UaDeviceNodeType.h"
#include "uadatetime.h"
#include "ControlInterface.h"
#include "opcua_builtintypes.h"
#include "CanOpenItem.h"

#include <map>
#include <boost/xpressive/xpressive.hpp>

using namespace std;
using namespace UserDevice;

namespace AddressSpace
{
	
/**
 * The constructor NmBuildingAutomation
 * @param conInterf pointer to the device interface
 * @param nameOfServer - server name
 */
	NmBuildingAutomation::NmBuildingAutomation(shared_ptr<UserDevice::ControlInterface> conInterf,const UaString nameOfServer): NodeManagerBase(nameOfServer)
	{
		m_pControlInterface = conInterf;
	}
	
	///
	/// function to create the new node id based on name
	///
	UaNodeId NmBuildingAutomation::getNewNodeId(const UaNode *pNode, const UaString name)
	{
		UaNodeId uNodeId;
		UaString nTmp;

		if ((pNode != NULL) && (pNode->nodeId().identifierType() == OpcUa_IdentifierType_String)) {
			nTmp = UaString("%1.%2").arg(pNode->nodeId().toString()).arg(name);
		}	
		else
			nTmp = name;
		OpcUa_Int16 nidx = getNameSpaceIndex();
		uNodeId = UaNodeId(nTmp,nidx); 
		return uNodeId;
	}

	///
	/// Get NodeId of top node
	///
	UaNodeId NmBuildingAutomation::getTopNodeId(const UaNode *pNode) const
	{
		UaNodeId uNodeId;
		char buffer[1024];
		string nTmp = pNode->nodeId().toString().toUtf8();
		int nbus = nTmp.copy(buffer, nTmp.find_first_of('.'),0);
		buffer[nbus] = '\0';
		UaString bus = UaString(buffer);
		uNodeId = UaNodeId(bus,getNameSpaceIndex()); 
		return uNodeId;
	}

	UaNode* NmBuildingAutomation::getUpNode(const UaNode *pNode) const
	{
		UaNodeId uNodeId;
		string buffer;
		string nTmp = pNode->nodeId().toString().toUtf8();
		int lastpos =  nTmp.find_last_of('.');
		buffer = nTmp.substr(0,lastpos);
		UaString sNode = UaString(buffer.c_str());

		uNodeId = UaNodeId(sNode,getNameSpaceIndex()); 
		return  findNode(uNodeId);
	}

	///
	/// destructor
	///
	NmBuildingAutomation::~NmBuildingAutomation()
	{
	}

	/**
	* function create the address space for the bus based on XML parser information
	*/
	UaStatus NmBuildingAutomation::afterStartUp()
	{
		UaStatus ret = OpcUa_Good;
/*	
		ret = m_pControlInterface->CreateAddressSpace(this);
		UA_ASSERT(ret.isGood());
		if (ret.isGood())
			ret  = m_pControlInterface->initialize();
*/
			return ret;
	
	}

	//UaVariable* NmBuildingAutomation::getInstanceDeclarationVariable(const UaString name)
	//{
	//	// Try to find the instance declaration node with the numeric identifier 
	//	// and the namespace index of this node manager
	//	UaNode* pNode = findNode(UaNodeId(name, getNameSpaceIndex()));
	//	if (pNode != NULL) {
	//		int t = pNode->nodeClass();
	//		if (t == OpcUa_NodeClass_Variable) 
	//		{
	//			// Return the node if valid and a variable
	//			return static_cast<UaVariable*>(pNode);
	//		}
	//	}

	//	return NULL;
	//}

	//UaVariable* NmBuildingAutomation::getInstanceDeclarationVariable(const UaNodeId &uanid)
	//{
	//	// Try to find the instance declaration node with the numeric identifier 
	//	// and the namespace index of this node manager
	//	UaNode* pNode = findNode(uanid);
	//	if  (pNode != NULL) {
	//		int t = pNode->nodeClass();
	//		if	 (t == OpcUa_NodeClass_Variable)
	//		{
	//			// Return the node if valid and a variable
	//		}
	//		return static_cast<UaVariable*>(pNode);
	//	}
	//	return NULL;
	//}

	
	//UaVariable* NmBuildingAutomation::getInstanceDeclarationVariable(OpcUa_UInt32 numericIdentifier)
	//{
	//	// Try to find the instance declaration node with the numeric identifier 
	//	// and the namespace index of this node manager
	//	UaNode* pNode = findNode(UaNodeId(numericIdentifier, getNameSpaceIndex()));
	//	if (pNode != NULL) {
	//		int t = pNode->nodeClass();
	//		if (t == OpcUa_NodeClass_Variable) 
	//		{
	//			// Return the node if valid and a variable
	//			return static_cast<UaVariable*>(pNode);
	//		}
	//	}
	//	return NULL;
	//}
/*
	UaObject* NmBuildingAutomation::getInstanceDeclarationObjectType(OpcUa_UInt32 numericIdentifier)
	{
		// Try to find the instance declaration node with the numeric identifier 
		// and the namespace index of this node manager
		UaNode* pNode = findNode(UaNodeId(numericIdentifier, getNameSpaceIndex()));

		if (pNode != NULL) {
			int t = pNode->nodeClass();
			if  (t == OpcUa_NodeClass_ObjectType) 
			{
				// Return the node if valid and a variable
				return (UaObject*)pNode;
			}
		}
		return NULL;
	}
*/	
//	template<typename X>
//	X* NmBuildingAutomation::getInstanceDeclaration(OpcUa_UInt32 numericIdentifier)
	//{
	//	// Try to find the instance declaration node with the numeric identifier 
	//	// and the namespace index of this node manager
	//	X* pNode = dynamic_cast<X *>(indNode(UaNodeId(numericIdentifier, getNameSpaceIndex())));
	//	return X;
		//if (pNode != nullptr) {
		//	retu
		//	int t = pNode->nodeClass();
		//	switch (t)
		//	if (t == OpcUa_NodeClass_ObjectType)
		//	{
		//	case OpcUa_NodeClass_ObjectType:
		//			// Return the node if valid and a variable
		//		return dynamic_cast<UaObjectType*>(pNode);
		//	case OpcUa_NodeClass_Variable:
		//		// Return the node if valid and a variable
		//		return dynamic_cast<UaVariable*>(pNode);
		//	case OpcUa_NodeClass_Object:
		//		// Return the node if valid and a variable
		//		return dynamic_cast<UaObject*>(pNode);
		//	case OpcUa_NodeClass_Method:
		//		// Return the node if valid and a variable
		//		return dynamic_cast<UaMethod*>(pNode);

		//	}
		//}
		//return nullptr;
	//}

	UaStatus NmBuildingAutomation::beforeShutDown()
	{
		UaStatus ret;
//		delete m_pControlInterface;
		return ret;
	}

	//UaObject* NmBuildingAutomation::getInstanceDeclarationObject(OpcUa_UInt32 numericIdentifier)
	//{
	//	// Try to find the instance declaration node with the numeric identifier 
	//	// and the namespace index of this node manager
	//	UaNode* pNode = findNode(UaNodeId(numericIdentifier, getNameSpaceIndex()));
	//	if (pNode != NULL) {
	//		int t = pNode->nodeClass();
	//		if  (t == OpcUa_NodeClass_Object) 
	//		{
	//			// Return the node if valid and a variable
	//			return (UaObject*)pNode;
	//		}
	//	}
	//	return NULL;
	//}


	//UaMethod* NmBuildingAutomation::getInstanceDeclarationMethod(OpcUa_UInt32 numericIdentifier)
	//{
	//	// Try to find the instance declaration node with the numeric identifier 
	//	// and the namespace index of this node manager
	//	UaNode* pNode = findNode(UaNodeId(numericIdentifier, getNameSpaceIndex()));
	//	if ( (pNode != NULL) && (pNode->nodeClass() == OpcUa_NodeClass_Method) )
	//	{
	//		// Return the node if valid and a variable
	//		return (UaMethod*)pNode;
	//	}
	//	else
	//	{
	//		return NULL;
	//	}
	//}

	UaStatus NmBuildingAutomation::readValues(const UaVariableArray &arrUaVariables, UaDataValueArray &arrDataValues)
	{
		UaStatus ret = OpcUa_Good;
		OpcUa_UInt32 i;
		OpcUa_UInt32 count = arrUaVariables.length();
		UaDateTime timeStamp = UaDateTime::now();

		// Create result array
		arrDataValues.create(count);

		for (i=0; i<count; i++)
		{
			// Set time stamps
			arrDataValues[i].setServerTimestamp(timeStamp);
			UaVariable* pVariable = arrUaVariables[i];
			if (pVariable)
			{
				UaNodeId uu = pVariable->typeDefinitionId();

				if (m_pControlInterface->isTraceVariable(pVariable->nodeId()) || !m_pControlInterface->isDeviceVariable(uu))
				{
					arrDataValues[i] = pVariable->value(0);
				}
				else 
				{
					ret = ((UaControlDeviceItem *)pVariable)->read();
					arrDataValues[i] = pVariable->value(0);
					arrDataValues[i].setStatusCode(ret.statusCode());
				}
			}
		}
		return ret;
	}
	
	UaStatus NmBuildingAutomation::writeValues(const UaVariableArray &arrUaVariables, const PDataValueArray &arrpDataValues, UaStatusCodeArray &arrStatusCodes)
	{
		UaStatus gret = OpcUa_Good,ret = OpcUa_Good;
		OpcUa_UInt32 i;

		OpcUa_UInt32 count = arrUaVariables.length();

		// Create result array
		arrStatusCodes.create(count);

		for (i=0; i<count; i++)
		{
			UaVariable* pVariable = arrUaVariables[i];
			if (pVariable)
			{
				///
				/// Trace level settings
				///
				
				if (m_pControlInterface->isTraceVariable(pVariable->nodeId())) {
					pVariable->setValue(0,*arrpDataValues[i] ,false);
					m_pControlInterface->putTraceFunctions(arrpDataValues[i]);
				}
				else {
				
					///
					/// Send data or execute the command = type id numeric value
					///
					OpcUa_BuiltInType cvtype = (OpcUa_BuiltInType)pVariable->dataType().identifierNumeric();
					OpcUa_Variant tData,sData;
					
					OpcUa_DataValue dValue = *arrpDataValues[i];
					sData = dValue.Value;
			
					if (CanOpen::CanOpenItem::convertOpcUaType(&sData, &tData, cvtype).isGood()) {
						UaDataValue udttmp;
						dValue.Value = tData;
						udttmp = dValue;
						pVariable->setValue(0, udttmp, false);
						udttmp = pVariable->value(0);
						((UaControlVariable *)pVariable)->write(udttmp);
					}
					else {
						arrStatusCodes[i] = OpcUa_BadDataEncodingInvalid;


					}
	
				}
			}
		}

		return gret;
	}
	

 /**
 * This function returns IOManager for mode and attribute  
 * @param pUaNode pointer to UaN asking the IO manager
 * @param attributeId id of attribute of node 
 */
	IOManager* NmBuildingAutomation::getIOManager(UaNode* pUaNode,  OpcUa_Int32 attributeId) const
	{
		IOManager *iom = m_pControlInterface->getIOManager(pUaNode,attributeId);
		if (iom == NULL)
			iom =  NodeManagerBase::getIOManager(pUaNode,attributeId);
		return iom;

	}
	
	/** Get the variable handle for the passed node Id.
		* @param session point to session structure
		* @param serviceType type of operation (read, write and monitoring)
		* @param nodeid node identifier of variable
		* @param attributeId identifier of attribute of variable
		* @return variableHandler used core to make an operation. this handler contains pointer on IOManager
	 */
	VariableHandle* NmBuildingAutomation::getVariableHandler(Session* session, VariableHandle::ServiceType serviceType, OpcUa_NodeId *nodeid, OpcUa_Int32 attributeId)
	{
		UaNodeId	t_uanodeid;

		if ( (nodeid == NULL) || (nodeid->NamespaceIndex != m_nNamespaceIndex) )
		{
			return NULL;
		}
		VariableHandle* pVariableHandle = NULL;
		pVariableHandle = m_pControlInterface->getVariableHandler(session, serviceType, nodeid, attributeId);
		if (pVariableHandle == NULL)
			pVariableHandle = NodeManagerBase::getVariableHandle(session,serviceType,nodeid,attributeId);
/*
		VariableHandle* pVariableHandle = NULL;
		if ( attributeId == OpcUa_Attributes_Value )
		{
			OpcUa_UInt32 idt;
			// The value attribute is requested - we are using the IOManager
			UaNode *pUaNode = findNode(*nodeid);	
			UaNodeId id = pUaNode->typeDefinitionId();
			if (id.identifierType() == OpcUa_IdentifierType_Numeric) {
				idt = id.identifierNumeric();
				if (idt == Ba_SDOType_Item || 
					idt == Ba_SDOType1_Item ||
					idt == Ba_SDOType2_Item ||
					idt == Ba_SDOType3_Item 
					)
				{
					pVariableHandle = new VariableHandleUaNode;
					t_uanodeid = getCanNodeNodeId(pUaNode);
					UaNode *pParNode = findNode(t_uanodeid);	
					pVariableHandle->m_pIOManager = ((UaCanNodeObject *)pParNode)->getSDOIOManager();
					pVariableHandle->m_AttributeID = attributeId;
					((VariableHandleUaNode *)pVariableHandle)->setUaNode(pUaNode);
				}
				else {
					if (idt == Ba_PDOType_Item)
					{
						if (((UaVariable *)pUaNode)->accessLevel() == Ua_AccessLevel_CurrentWrite) {
							pVariableHandle = new VariableHandleUaNode;
							t_uanodeid = getTopNodeId(pUaNode);
							UaNode *pTopNode = findNode(t_uanodeid);
							pVariableHandle->m_pIOManager = ((UaCanBusObject *)pTopNode)->getPDOIOManager();
							pVariableHandle->m_AttributeID = attributeId;
							((VariableHandleUaNode *)pVariableHandle)->setUaNode(pUaNode);
						}
						else
							pVariableHandle = NodeManagerBase::getVariableHandle(session,serviceType,nodeid,attributeId);
					}
				}
			}
		}
		else 
		
			pVariableHandle = NodeManagerBase::getVariableHandle(session,serviceType,nodeid,attributeId);
			*/
		return pVariableHandle;
	}

	/** @brief find set of variables
	* @param pParObj pointer to type node 
	* @param pattern searching pattern
	* @param storage return found variables
	* @return number of finding variables
	*/
	OpcUa_Int32  NmBuildingAutomation::findUaVariables(const OpcUa::BaseObjectType *pParObj,string& pattern,UaControlVariableSet& storage)
	{
		OpcUa_Int32 nMatched =  0;
		UaReference *pRefList = (UaReference *)pParObj->pTargetNodes();
		UaString fPatten = UaString("%1.%2").arg(pParObj->getKey().toString()).arg(pattern.c_str());
		string fullpatten = fPatten.toUtf8();
		while (pRefList) {
			nMatched = nMatched + findUaNodeVariables(pRefList->pTargetNode(),fullpatten,storage);
			pRefList = pRefList->pNextForwardReference();
		}
		return nMatched;
	}

	/** @brief find set of variables starting from node
	* @param pNode pointer to starting node 
	* @param pattern searching pattern
	* @param storage return found variables
	* @return number of finding variables
	*/
	OpcUa_Int32  NmBuildingAutomation::findUaNodeVariables(UaNode *pNode,string & pattern,UaControlVariableSet& storage)
	{
		OpcUa_Int32 nMatched =  0;
		boost::xpressive::sregex expression = boost::xpressive::sregex::compile(pattern);

		if (pNode->nodeClass() == OpcUa_NodeClass_Variable) {
			UaNodeId nodeid = pNode->getKey(); 
			if (nodeid.identifierType()  == OpcUa_IdentifierType_String) {
				boost::xpressive::smatch what;
				string sName = nodeid.toString().toUtf8();
				if (boost::xpressive::regex_match(sName,what,expression)) {
					nMatched = 1;
					storage.push_back((UaControlVariable *)pNode);
				}
			}
		}
		else { 
			if (pNode->nodeClass() ==  OpcUa_NodeClass_Object) {
				UaReference *pRefList = (UaReference *)pNode->getUaReferenceLists()->pTargetNodes();
				while (pRefList) {
					nMatched = nMatched + findUaNodeVariables(pRefList->pTargetNode(),pattern,storage);
					pRefList = pRefList->pNextForwardReference();
				}
			}
		}
		return nMatched;
	}

}
