#include "UaCalcItem.h"
#include "boost/bind/bind.hpp"
#include "UaCanTrace.h"

#include <math.h>

#ifdef WIN32
#define isnan(x) _isnan(x)
#define isinf(x) (!_finite(x))
#define fpu_error(x) (isinf(x) || isnan(x))
#endif

namespace AddressSpace
{
	using namespace std::placeholders;
//	UaCalcItem::UaCalcItem(const UaString &name,UaNodeId& uaId, NmBuildingAutomation * pNodeConfig, UaVariant &dv, ITEM &citem, UaControlVariableMap &cvs, UaMutexRefCounted* pSharedMutex):
//		UaControlVariable(name,uaId,pNodeConfig,dv,pSharedMutex)
	

	struct ad_usr : public parser_t::unknown_symbol_resolver
	{
		typedef parser_t::unknown_symbol_resolver usr_t;

//		ad_usr(symbol_table_t &sb, UaCalcItem& par, void(*pushDV)(const string&, UaControlVariable *) )
		ad_usr( UaCalcItem *par)// , std::function<void(const string&, UaControlVariable *)> pushVD)
//		ad_usr()
			: usr_t(usr_t::e_usrmode_extended), p(par)//, pDV(pushVD)
		{}

		void setFunc(std::function<bool(const string&, UaControlVariable *)> pushVD) { pDV = pushVD;  }

		virtual bool process(const std::string& unknown_symbol,
			symbol_table_t&      symbol_table,
			std::string&        error_message)
		{
			bool result = false;
			UaNodeId nodeId;
			UaNode *node;

			nodeId = p->m_pNodeManager->getNewNodeId(p->m_pParent, UaString(unknown_symbol.c_str()));
			node = p->m_pNodeManager->findNode(nodeId);
			if (node == NULL) {
				nodeId = p->m_pNodeManager->getNewNodeId(NULL, UaString(unknown_symbol.c_str()));
				node = p->m_pNodeManager->findNode(nodeId);
			}


			if (node != NULL)
			{
//	Default value of zero
//	result = m_sb.create_variable(unknown_symbol,static_cast<UaControlVariable*>(node)->curValue);
				result = pDV(unknown_symbol, static_cast<UaControlVariable*>(node));
				if (!result)
				{
					error_message = "Failed to create variable...";
				}
			}
			else
				error_message = "Indeterminable symbol type.";
			return result;
		}
		//			symbol_table_t m_sb;
		UaCalcItem *p;
		std::function<bool(const string&, UaControlVariable *)> pDV;
	};

	UaCalcItem::UaCalcItem(const UaString& name, UaNodeId& uaId, const UaNode *parent,
		NmBuildingAutomation * pNodeConfig, UaVariant &dv, ITEM &citem, UaControlVariableMap &cvs, UaMutexRefCounted* pSharedMutex) :
		UaControlVariable(name, uaId, pNodeConfig, dv, pSharedMutex), m_pNodeManager(pNodeConfig)
	{
		int i = 0;
		double res;
		UaDateTime sdt;
		UaDataValue dataValue;
		bool result = false;
		//		fmod f1;

		m_pParent = static_cast<const OpcUa::BaseObjectType*>(parent);
		//		string nname = citem.name();
		m_cExists = citem.when().present();
		m_sExists = citem.status().present();
		string ssFormul = citem.value();
		replaceAll(ssFormul, " && ", " and ");
		replaceAll(ssFormul, " || ", " or ");

		m_sFormul = ssFormul;
		if (checkDouble(m_sFormul, res)) {

			sdt = UaDateTime::now();

			UaVariant val = res;
			if (isnan(res) || isinf(res)) {
				dataValue.setDataValue(val, OpcUa_False, OpcUa_BadUnexpectedError, sdt, sdt);
			}
			else
				dataValue.setDataValue(val, OpcUa_False, OpcUa_True, sdt, sdt);

			setValue(0, dataValue, false);
			m_fExists = false;

			//			return;
		}
		else {
			m_fExists = true;

			//		cout << "Create " << this->browseName().toString().toUtf8() << endl << flush;

			for (UaControlVariableMap::const_iterator vit = cvs.begin(); vit != cvs.end(); vit++)
			{
				string nn = (*vit).first;
				UaControlVariable *uac = (*vit).second;
				//			cout << "Add Variable " << nn << " " << this->browseName().toString().toUtf8() << endl << flush;

				if (m_sFormul.find(nn) != string::npos)
					result = pushDataVariable(nn, uac);
			}

		}
		if (m_cExists) {
			ssFormul = citem.when().get();
			replaceAll(ssFormul, " && ", " and ");
			replaceAll(ssFormul, " || ", " or ");
			m_sWhenCondition = ssFormul;

			for (UaControlVariableMap::const_iterator vit = cvs.begin(); vit != cvs.end(); vit++)
			{
				string nn = (*vit).first;
				UaControlVariable *uac = (*vit).second;
				if (m_sWhenCondition.find(nn) != string::npos)
					result = pushConditionVariable(nn, uac);
			}

		}

		if (m_sExists) {
			ssFormul = citem.status().get();
			replaceAll(ssFormul, " && ", " and ");
			replaceAll(ssFormul, " || ", " or ");
			m_sStatusCondition = ssFormul;

			for (UaControlVariableMap::const_iterator vit = cvs.begin(); vit != cvs.end(); vit++)
			{
				string nn = (*vit).first;
				UaControlVariable *uac = (*vit).second;
				if (m_sStatusCondition.find(nn) != string::npos)
					result = pushStatusVariable(nn, uac);
			}
		}
		//		res = m_whenCondition_table.add_function("fmod", f1);
		//		res = m_statusFormula_table.add_function("fmod", f1);
		res = m_formul_table.add_function("fmod", f1);
		//		m_formul_table.add_constants();
		//		bool l = compile();
		setValueHandling(UaVariable_Value_CacheIsSource);	//!< set change date by event
		setUserData(NULL);
	}

	bool UaCalcItem::compile()
	{

		bool ret;
		std::vector<std::string> variable_list;
		//			OpcUa_Int32 nVar;
		//			UaControlVariableSet ucVariables;
		if (m_cExists) {
		
			m_whenCondition.register_symbol_table(m_formul_table);
			ad_usr wusr(this);
			auto fp = std::bind(&UaCalcItem::pushConditionVariable, this, std::placeholders::_1, std::placeholders::_2);
			wusr.setFunc(fp);
			parser_t parser_b;
			parser_b.enable_unknown_symbol_resolver(&wusr);
			m_sWhenCondition.erase(std::remove(m_sWhenCondition.begin(), m_sWhenCondition.end(), '$'), m_sWhenCondition.end());
			ret = parser_b.compile(m_sWhenCondition, m_whenCondition);

//			m_whenCondition.value();


			if (!ret)
			{
				cout << "Error Condition: " << parser_b.error().c_str() << " " << this->browseName().toString().toUtf8() << " " << m_sWhenCondition << endl;
				exit(-1);
			}
			//			return ret;
						/*
										unknown_var_whenCondition_table.get_variable_list(variable_list);

										for (auto& var_name : variable_list)
										{
											double& v = unknown_var_whenCondition_table.variable_ref(var_name);
											nodeId = m_pNodeManager->getNewNodeId(m_pParent, UaString(var_name.c_str()));
											node = m_pNodeManager->findNode(nodeId);
											v = static_cast<UaControlVariable*>(node)->curValue;
										}
						*/

		}

		m_formul.register_symbol_table(m_formul_table);

		m_sFormul.erase(std::remove(m_sFormul.begin(), m_sFormul.end(), '$'), m_sFormul.end());

		ad_usr musr(this);
		auto fp = std::bind(&UaCalcItem::pushDataVariable, this,std::placeholders::_1,std::placeholders::_2);
		musr.setFunc(fp);

		parser_t parser_d;
		parser_d.enable_unknown_symbol_resolver(&musr); //.enable_unknown_symbol_resolver(&musr);

		ret = parser_d.compile(m_sFormul, m_formul);
		if (!ret) {
			cout << "Error value: " << parser_d.error().c_str()<< " " << this->browseName().toString().toUtf8() << " " << m_sFormul << endl;
			exit(-1);
		}
		/*
					unknown_var_formul_table.get_variable_list(variable_list);

					for (auto& var_name : variable_list)
					{
						double& v = unknown_var_formul_table.variable_ref(var_name);
						nodeId = m_pNodeManager->getNewNodeId(m_pParent, UaString(var_name.c_str()));
						node = m_pNodeManager->findNode(nodeId);
						v = static_cast<UaControlVariable*>(node)->curValue;
					}
		*/

		if (m_sExists) {
			m_statusFormula.register_symbol_table(m_formul_table);
			m_sStatusCondition.erase(std::remove(m_sStatusCondition.begin(), m_sStatusCondition.end(), '$'), m_sStatusCondition.end());
			auto fp = std::bind(&UaCalcItem::pushStatusVariable, this, std::placeholders::_1, std::placeholders::_2);
			ad_usr susr(this);
			susr.setFunc(fp);
			parser_t parser_i;
			parser_i.enable_unknown_symbol_resolver(&susr);
			ret = parser_i.compile(m_sStatusCondition, m_statusFormula);
			if (!ret)
			{
				cout << "Error Status: " << parser_i.error().c_str() << " " << this->browseName().toString().toUtf8() << " " << m_sStatusCondition << endl;
				exit(-1);
			}
			/*
							unknown_var_statusFormula_table.get_variable_list(variable_list);

							for (auto& var_name : variable_list)
							{
								double& v = unknown_var_statusFormula_table.variable_ref(var_name);
								nodeId = m_pNodeManager->getNewNodeId(m_pParent, UaString(var_name.c_str()));
								node = m_pNodeManager->findNode(nodeId);
								v = static_cast<UaControlVariable*>(node)->curValue;
							}
							*/
		}

		return ret;
	}

	void UaCalcItem::calculateOnChange(Session *pSession, const UaDataValue& dValue, OpcUa_Boolean checkAccessLevel)
	{
		//boost::arg<1> _1;		//!< boost placeholder for binding argument
		UaDataValue dataValue;
		UaDateTime udt,sdt;
		UaStatus Status = OpcUa_Good;
		double res = curValue;		//current value
		bool cond = false;
		UaString sDiag;
		OpcUa_StatusCode val_status;
		OpcUa_StatusCode status_status;

		/**
		* bind function which calculator uses to take data from OpcUa variable
		*/

		udt = dValue.sourceTimestamp();
		sdt = UaDateTime::now();
		UaVariant val;

		try {
			val_status = checkStatus(m_var);
//			cout << "Main Block " << hex << val_status << endl << flush;
			if (val_status != OpcUa_Good) {
				sDiag = "Sensors are bad";
				Status.setStatus(val_status, sDiag);
			}
			else {
				res = m_formul.value(); // calculation
//				cout << "Result " << hex << res << endl;
				if (m_sExists) {
					status_status = checkStatus(m_statusVar);
//					cout << "Status Status " << hex << status_status << endl << flush;
					if (status_status != OpcUa_Good) {
						sDiag = "Status sensor is bad";
						Status.setStatus(status_status, sDiag);
					}
					else {
						cond = m_statusFormula.value();
//						cout << "Condition value " << cond << endl << flush;
						if (!cond) {
							sDiag = "Condition is false";
							Status.setStatus(OpcUa_Bad, sDiag);
						}
						else {
							sDiag = "OK";
							Status.setStatus(OpcUa_Good, sDiag);
						}
					}
				}
				else {
					sDiag = "OK";
					Status.setStatus(OpcUa_Good, sDiag);
				}
				if (isnan(res) || isinf(res)) {
					sDiag = "Bad Data";
					Status.setStatus(OpcUa_BadUnexpectedError, sDiag);
				}
				val = res;
				dataValue.setDataValue(val, OpcUa_False, Status.code(), udt, sdt);
				Status = setValue(pSession, dataValue, checkAccessLevel);
				return;
			}
		}
		catch (...)
		{
			sDiag = "Calculation Error";
			Status.setStatus(OpcUa_BadUnexpectedError,sDiag);
		}

//		if (Status.isNotGood())
		dataValue = value(pSession);
		val = *dataValue.value();
//		else {
			//if (isnan(res) || isinf(res)) {
			//	sDiag = "Bad Data";
			//	Status.setStatus(OpcUa_BadUnexpectedError, sDiag);
			//	//dataValue.setStatusCode(OpcUa_BadUnexpectedError);
			//	//dataValue.setSourceTimestamp(udt);
			//	//dataValue.setServerTimestamp(sdt);
			//	val = *dataValue.value();
			//}
			//else
		//		val = res;
		//}
		//else
		//	if (Status.isNotGood()) {
		//		dataValue.setStatusCode(Status.code());
		//		dataValue.setSourceTimestamp(udt);
		//		dataValue.setServerTimestamp(sdt);
		//	}

		dataValue.setDataValue(val, OpcUa_False, Status.code(), udt, sdt);
		Status = setValue(pSession, dataValue, checkAccessLevel);
	}

	void UaCalcItem::initCalculation()
	{
		boost::arg<1> _1;		//! boost placeholder for binding argument
		UaDataValue dataValue;
		UaDateTime udt,sdt;
		UaStatus Status = OpcUa_Good;
//		double res;
		UaVariant val;
		
		/**
		* bind function which calculator uses to take data from OpcUa variable
		*/
	
//		m_formul->getValueFromItem =  boost::bind(&AddressSpace::UaCalcItem::getValueFromItem,this,_1);
		calculateOnChange(0, dataValue, OpcUa_False);
		
		//try {
		//	res = m_formul.Eval(); // calculation
		//	val = res;
		//	udt = UaDateTime::now();
		//	sdt = UaDateTime::now();

		//	if (isnan(res) || isinf(res)) {
		//		UaString sDiag = "Calculation Error";
		//		Status.setStatus(OpcUa_BadUnexpectedError, sDiag);
		//	}

		//	dataValue.setDataValue(val, OpcUa_False, Status.statusCode(), udt, sdt);

		//	Status = setValue(0, dataValue, OpcUa_False);
		//}
		//catch  (...)
		//{
		//	UaString sDiag = "No Initial Data";
		//	Status.setStatus(OpcUa_BadWaitingForInitialData, sDiag);

		//}

	}
	

	/**
	* this function take data from OpcUa variable.
	* the pointer saved in vector m_var
	* @param ind index of vector of pointer to variable
	*/
	/*
	double UaCalcItem::getValueFromItem(std::string ind)
	{
		double res = 0.0;
		variableSet::iterator vit = m_var.find(ind);
		if (vit != m_var.end() ) {
			res = convertToDouble(vit);
		}
		return res;
	}

	double UaCalcItem::getValueFromWhenItem(std::string ind)
	{
		double res = 0.0;
		variableSet::iterator vit = m_whenVar.find(ind);
		if (vit != m_whenVar.end() ) {
			res = convertToDouble(vit);
		}
		return res;
	}

	double UaCalcItem::getValueFromStatusItem(std::string ind)
	{
		double res = 0.0;
		variableSet::iterator vit = m_statusVar.find(ind);
		if (vit != m_statusVar.end() ) {
			res = convertToDouble(vit);
		}
		return res;
	}
	
	double UaCalcItem::convertToDouble(variableSet::iterator &vit)
	{
		UaDataValue dataValue;
		double res = 0.0;
		dataValue = (*vit).second->value(0);
		const OpcUa_Variant *val = dataValue.value();
		switch (val->Datatype) {
		case    OpcUaType_Boolean:
			res = val->Value.Boolean;
			break;
		case OpcUaType_SByte:
			res = val->Value.SByte;
			break;
		case OpcUaType_Byte:
			res = val->Value.Byte;
			break;
		case OpcUaType_Int16:
			res = val->Value.Int16;
			break;
		case OpcUaType_UInt16:
			res = val->Value.UInt16;
			break;
		case OpcUaType_Int32:
			res = val->Value.Int32;
			break;
		case OpcUaType_UInt32:
			res = val->Value.UInt32;
			break;
		case OpcUaType_Int64:
			res = (double)val->Value.Int64;
			break;
		case OpcUaType_UInt64:
			res = (double)val->Value.UInt64;
			break;
		case OpcUaType_Float:
			res = val->Value.Float;
			break;
		case OpcUaType_Double:
			res = val->Value.Double;
			break;
		default: 
			throw 1;
//			res = 0.0;
		};
		return res;
	}
	*/
	/**
	* this function take timeStamp from OpcUa variable the latest.
	*/
	/*
	UaDateTime UaCalcItem::UaGetSourceTimeStamp()
	{
		UaDateTime udt=0,tudt;
		UaDataValue dataValue;
		for(variableSet::iterator vit = m_var.begin(); 	vit != m_var.end(); vit++ ) {
			dataValue = (*vit).second->value(0);
			tudt = dataValue.sourceTimestamp();
			if (tudt > udt ) udt = tudt;
		}
		return udt;
	}
	*/
	//UaStatus UaCalcCompilerItem::findLinkToUaVariable(UaCalcItem *pcn,NmBuildingAutomation *pNodeManager,OpcUaFormula *formul,OpcUa_Int index,pushUaVariable puv)
	//{

	//	UaStatus ret = OpcUa_Good;
	//	if (formul) {
	//		for (operands_type_set::iterator it = formul->opt.begin(); it !=  formul->opt.end(); it++)
	//		{
	//			UaVariable* t_var;
	//			string stdName;
	//			UaString sName;
	//			UaNodeId uaId;
	//			if ((*it).which() == 3) {
	//				stdName = boost::get<std::string>((*it));
	//				sName = stdName.c_str();
	//				if (sName.at(0) == UaChar('$')) {
	//					if (isVariableMap(stdName))
	//					{
	//						UaControlVariableSet setOfVariable = getMapDataVariable().at(stdName);
	//						(pcn->*puv)(stdName, setOfVariable[index]);
	//					}
	//					else
	//					{
	//						ret = OpcUa_Bad;
	//						LOG(Log::INF) << "The variable is not defined " << stdName.c_str();
	//						UA_ASSERT(ret.isGood());
	//						return ret;
	//					}

	//				}
	//				else {
	//					uaId = pNodeManager->getNewNodeId(getParentCalc(), sName);
	//					t_var = pNodeManager->getInstanceDeclarationVariable( uaId);
	//					if (t_var) {
	//						(pcn->*puv)(stdName,(UaControlVariable *)t_var);
	//					}
	//					else { 
	//						uaId = pNodeManager->getNewNodeId(NULL, sName);
	//						t_var = pNodeManager->getInstanceDeclarationVariable( uaId);
	//						if (t_var) {
	//							(pcn->*puv)(stdName,(UaControlVariable *)t_var);
	//						}
	//						else {
	//							ret = OpcUa_Bad;
	//							LOG(Log::INF) << "Could not find the item " << stdName.c_str();
	//							UA_ASSERT(ret.isGood());
	//							return ret;
	//						}
	//					}
	//				}
	//			}
	//		}
	//	}
	//	return ret;
	//}

	//UaStatus UaCalcCompilerItem::CreateLinks(NmBuildingAutomation *pNodeManager)
	//{
	//	UaStatus ret;
	//	for (unsigned int i = 0; i < m_vCalcItem.size(); i++)
	//	{
	//		UaCalcItem *uci = m_vCalcItem[i];
	//		OpcUaFormula *formul = getFormula();
	//		OpcUaFormula *whenCondition = getWhenCondition();
	//		OpcUaFormula *statusCondition = getStatusCondition();

	//		pushUaVariable puv = &UaCalcItem::pushDataVariable;
	//		ret = findLinkToUaVariable(uci,pNodeManager,formul,i,puv);
	//		UA_ASSERT(ret.isGood());
	//		if (ret.isBad())
	//			exit(-1);

	//		puv = &UaCalcItem::pushConditionVariable;
	//		ret = findLinkToUaVariable(uci,pNodeManager,whenCondition,i,puv);
	//		UA_ASSERT(ret.isGood());
	//		if (ret.isBad())
	//			exit(-1);
	//		
	//		puv = &UaCalcItem::pushStatusVariable;
	//		ret = findLinkToUaVariable(uci,pNodeManager,statusCondition,i,puv);
	//		UA_ASSERT(ret.isGood());
	//		if (ret.isBad())
	//			exit(-1);

	//	}
	//	return ret;
	//}
	//
	//void UaCalcCompilerItem::initCalculation()
	//{
	//	for (unsigned int i = 0; i < m_vCalcItem.size(); i++)
	//	{
	//		UaCalcItem *uci = m_vCalcItem[i];
	//		uci->initCalculation();
	//	}
	//}

	//UaStatus UaCalcCompiler::addCalcCompilerItem(UaCalcCompilerItem *ucci)
	//{
	//	UaStatus ret = OpcUa_Good;

	//	m_vCCI.push_back(ucci);
	//	return ret;
	//}

}

