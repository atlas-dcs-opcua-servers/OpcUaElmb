#include "UaControlDeviceGeneric.h"
#include "NmBuildingAutomation.h"

#include "methodhandleuanode.h"
#include "UaDeviceMethod.h"
#include "UaControlNode.h"

namespace AddressSpace
{
	/**
	* Basic class represents the common features of can bus and can node
	*/

	UaControlDeviceGeneric::UaControlDeviceGeneric(
		const UaString& name,
		const UaNodeId& newNodeId,
		NmBuildingAutomation* pNodeManager,
		OpcUa_Int32	baTypeId,
		::xml_schema::type *conf,
		pUserDeviceStruct* interf
		)
		: OpcUa::BaseObjectTypeGeneric(newNodeId,name, pNodeManager->getNameSpaceIndex(), pNodeManager )
	{
		UaObjectType*         pInstanceDeclarationObject = NULL;
		UaStatus              addStatus;

		m_pSharedMutex->addReference();
		
		pUserDeviceStruct * pCanInter = pNodeManager->m_pControlInterface->createInterfaceEntry(interf,baTypeId,conf,this);			// Open Device hardware channel
		setUserData(pCanInter);							// Set interface for this node 				
		setTypeDefinitionId(baTypeId);
		
		pInstanceDeclarationObject = pNodeManager->getInstanceDeclarationObjectType(baTypeId); // take the type

//		cout << this->browseName().toString().toUtf8() << " " << pInstanceDeclarationObject->browseName().toString().toUtf8() << endl;

		addStatus = UaControlNode::UaControlNodeCreateByType<UaControlDeviceGeneric>(this,pInstanceDeclarationObject,pNodeManager,conf,pCanInter,getSharedMutex()); // create object structure base on type
		UA_ASSERT(addStatus.isGood());
	
	}

	/**
	* Basic class represents the common features of can bus and can node
	*/
	UaControlDeviceGeneric::UaControlDeviceGeneric(
		const UaString &name,
		const UaNodeId& newNodeId,
		NmBuildingAutomation* pNodeManager,
		UaObjectType *instance,
		::xml_schema::type *conf,
		pUserDeviceStruct* interf
		)
//		: OpcUa::BaseObjectTypeGeneric(UaNodeId(name,pNodeManager->getNameSpaceIndex()),instance, pNodeManager )
		: OpcUa::BaseObjectTypeGeneric( newNodeId, name, pNodeManager->getNameSpaceIndex(), pNodeManager)
	{
		UaNodeId	uaId;

		UaStatus     addStatus;
		m_pSharedMutex->addReference();

		setDisplayName(UaLocalizedText(pNodeManager->getDefaultLocale(),name));
		uaId = instance->nodeId();
		pUserDeviceStruct* pCanInter = pNodeManager->m_pControlInterface->createInterfaceEntry(interf,uaId.identifierNumeric(),conf,this);			// Open Device hardware chennal
		setUserData(pCanInter);							// Set interface for this node 		
		//if (pCanInter)
		//	pCanInter->addReference();

		setTypeDefinitionId(uaId);
		/**************************************************************
		* Create the Address Space components
		**************************************************************/
		addStatus = UaControlNode::UaControlNodeCreateByType<UaControlDeviceGeneric>(this,instance,pNodeManager,conf,pCanInter,getSharedMutex());  // create object structure base on type
		UA_ASSERT(addStatus.isGood());
	}

	/**
	* Basic class represents the common features of can bus and can node
	*/
	UaControlDeviceGeneric::UaControlDeviceGeneric(
		const UaString &name,
		const UaNodeId& newNodeId,
		NmBuildingAutomation* pNodeManager,
		UaObject *instance,
		::xml_schema::type *conf,
		pUserDeviceStruct* interf
	)
		: OpcUa::BaseObjectTypeGeneric(newNodeId,instance, pNodeManager )
	{
		UaNodeId	uaId;

		UaStatus     addStatus;
		m_pSharedMutex->addReference();

		setDisplayName(UaLocalizedText(pNodeManager->getDefaultLocale(), name));
		uaId = instance->nodeId();
		pUserDeviceStruct* pCanInter = pNodeManager->m_pControlInterface->createInterfaceEntry(interf, uaId.identifierNumeric(), conf, this);			// Open Device hardware chennal
		setUserData(pCanInter);							// Set interface for this node 		

		setTypeDefinitionId(uaId);
		/**************************************************************
		* Create the Address Space components
		**************************************************************/
		addStatus = UaControlNode::UaControlNodeCreateByType<UaControlDeviceGeneric>(this, instance, pNodeManager, conf, pCanInter, getSharedMutex());  // create object structure base on type
		UA_ASSERT(addStatus.isGood());
	}

	UaControlDeviceGeneric::~UaControlDeviceGeneric(void)
	{
//		pUserDeviceStruct * iudb = (pUserDeviceStruct*)getUserData();
//		cout << browseName().toString().toUtf8() << endl;
//		if (iudb) {
//			delete iudb;
	//		setUserData(0);

//		}
//		OpcUa::BaseObjectTypeGeneric::~BaseObjectTypeGeneric();
	}

	OpcUa_Byte UaControlDeviceGeneric::eventNotifier() const
	{
		return Ua_EventNotifier_None;
	}

	OpcUa_UInt32 UaControlDeviceGeneric::getDeviceTypeNumber() const
	{
		return this->typeDefinitionId().identifierNumeric();
	}

	UaStatus UaControlDeviceGeneric::beginCall(
		MethodManagerCallback* pCallback,
		const ServiceContext&  serviceContext,
		OpcUa_UInt32           callbackHandle,
		MethodHandle*          pMethodHandle,
		const UaVariantArray&  inputArguments)
	{
		UaStatus            ret;
		UaVariantArray      outputArguments;
		UaStatusCodeArray   inputArgumentResults;
		UaDiagnosticInfos   inputArgumentDiag;
		MethodHandleUaNode* pMethodHandleUaNode = static_cast<MethodHandleUaNode*>(pMethodHandle);
		UaDeviceMethod<UaControlDeviceGeneric>*           pMethod             = NULL;

		OpcUa_ReferenceParameter(serviceContext);
		
		if(pMethodHandleUaNode)
		{
			pMethod = (UaDeviceMethod<UaControlDeviceGeneric> *)pMethodHandleUaNode->pUaMethod();

			if(pMethod)
			{
				// Check if we have the calling method
				for (OpcUa_UInt32 i = 0; i < m_pMethod.size(); i++) {
					if ( pMethod->nodeId() == m_pMethod[i]->nodeId())
					{

					if ( inputArguments.length() > 0 )
					{
						ret = OpcUa_BadInvalidArgument;
					}
					else
					{
						pMethod->execute();	
						break; //include call method
					}
					}
				}

				pCallback->finishCall(
					callbackHandle,
					inputArgumentResults,
					inputArgumentDiag,
					outputArguments,
					ret);

				ret = OpcUa_Good;
			}
			else
			{
				assert(false);
				ret = OpcUa_BadInvalidArgument;
			}
		}
		else
		{
			assert(false);
			ret = OpcUa_BadInvalidArgument;
		}

		return ret;
	}
}