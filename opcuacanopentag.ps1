﻿#Create OpcUaCanOpenServer windows tag fro linux one
param (
	[string]$tag
)


if ($tag -eq [string]::Empty) {
	$verr="20"
}
else {
	$v=$tag.replace("cc7","")
	$verr=$v.replace("-",".")
}
$env:OpcUaCanOpen_tag = $verr
Invoke-RestMethod -uri "https://gitlab.cern.ch/api/v4/projects/8887/variables/OpcUaCanOpen_tag" -Method "put" -Body @{"value" = $verr } -Headers @{ 'PRIVATE-TOKEN'='5rfvKt84rtYQNBJi-bgB' } -ContentType "application/x-www-form-urlencoded"
