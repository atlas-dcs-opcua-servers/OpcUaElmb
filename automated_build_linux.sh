#!/bin/bash
echo "linux build script for OpcUaCanOpen project"

DEBUG_OR_RELEASE=$1
tag=$2
out=$3

if [ -f "CMakeCache.txt" ]; then  
	rm "CMakeCache.txt" 
fi

if [ -d "CMakeFiles" ]; 
	then rm -R "./CMakeFiles" 
fi

if [ -z "$DEBUG_OR_RELEASE" ]
then
	DEBUG_OR_RELEASE="Release"
	cmake3 -Wno-dev -DCMAKE_BUILD_TYPE=$DEBUG_OR_RELEASE -DNOT_BUILD_RPM="YES" .
	make -j8
else
	if [ -z "$tag" ]; then
		cmake3 -Wno-dev -DCMAKE_BUILD_TYPE=$DEBUG_OR_RELEASE -DNOT_BUILD_RPM="YES" .
		make -j8
	else
		if [ -z "$out" ]; then
			outdir="RPMS"
		else 
			outdir="-DCPACK_OUTPUT_FILE_PREFIX=$out"
		fi

		echo "Building configuration: $DEBUG_OR_RELEASE     (If you want the other, pass debug or release as an argument)"
		TAG_ARRAY=(${tag/-/ })
		RPM_VERSION=${TAG_ARRAY[0]}
		RPM_RELEASE=${TAG_ARRAY[1]}
		#cmake3 -GNinja -DCMAKE_BUILD_TYPE=$DEBUG_OR_RELEASE -DCPACK_PACKAGE_VERSION=$VERSION_NUMBER -DCPACK_PACKAGE_RELEASE=$RELEASE_NUMBER .
		echo "#define VERSION_STR \"$tag\"" > Server/src/Version.h
		sed "s/<SoftwareVersion>.*<\/SoftwareVersion>/<SoftwareVersion>$tag<\/SoftwareVersion>/ "  bin/ServerConfig.xml.linux > bin/ServerConfig.xml.linux.new
#		rm bin/ServerConfig.xml.linux
		mv bin/ServerConfig.xml.linux.new bin/ServerConfig.xml
		cmake3 -Wno-dev -DCMAKE_BUILD_TYPE=$DEBUG_OR_RELEASE -DCPACK_PACKAGE_VERSION=$RPM_VERSION -DCPACK_PACKAGE_RELEASE=$RPM_RELEASE $outdir .
		make -j8
		#ninja-build
		if [ "$DEBUG_OR_RELEASE" = "Release" ]; then
			cpack3 --verbose -G RPM
		fi
	fi
fi
