#!/bin/bash

DEST=/opt/OpcUaCanOpenServer/bin
echo "Installing in $DEST"
sudo mkdir -p $DEST
echo "Copying needed files"
sudo cp OpcUaCanOpenServer AddressSpace.xml ../Configuration/CANOpenServerConfig.xsd $DEST 
sudo cp ServerConfig.xml.linux $DEST/ServerConfig.xml
sudo cp ../lib/libsockcan.so $DEST
#sudo cp ../lib/libancan.so $DEST
echo "Now will set sticky (SUID) bit and cap for the server"
sudo chmod u+s $DEST/OpcUaCanOpenServer
sudo setcap cap_net_raw,cap_net_admin,cap_sys_rawio,cap_sys_admin=ep $DEST/OpcUaCanOpenServer
sudo setcap cap_net_admin=ep $DEST/libsockcan.so
#sudo setcap cap_net_admin=ep $DEST/libancan.so
echo "Done"

echo "To run the server:"
echo "cd $DEST"
echo "./OpcUaCanOpenServer <path-to-your-OPCUA-config-file>"


