
#!/bin/bash

DEST=/localdisk/winccoa/OpcUaCanOpenServer
echo "Installing in $DEST"
mkdir  -p $DEST
echo "Copying needed files"
cd /det/dcs/Development/OpcUaElmb/bin/
cp OpcUaCanOpenServer AddressSpace.xml ../Configuration/CANOpenServerConfig.xsd $DEST 
cp ServerConfig.xml.linux $DEST/ServerConfig.xml
sudo cp ../lib/libsockcan.so $DEST
#sudo cp ../lib/libancan.so $DEST
#ln -s $DEST/libsockcan.so.1.0.2 $DEST/libsockcan.sols 
#cp /det/dcs/Development/libsocketcan/lib/libsocketcan.so.2.2.0 $DEST
#ln -s $DEST/libsocketcan.so.2.2.0 $DEST/libsocketcan.so.2
#ln -s $DEST/libsocketcan.so.2.2.0 $DEST/libsocketcan.so
echo "Now will set sticky (SUID) bit and cap for the server"
sudo chmod u+s $DEST/OpcUaCanOpenServer
sudo setcap cap_net_raw,cap_net_admin,cap_sys_rawio,cap_sys_admin=ep $DEST/OpcUaCanOpenServer
sudo setcap cap_net_admin=ep $DEST/libsockcan.so
#sudo setcap cap_net_admin=ep libancan.so

echo "Done"

echo "To run the server:"
echo "cd $DEST"
echo "./OpcUaCanOpenServer <path-to-your-OPCUA-config-file>"


