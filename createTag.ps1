﻿#Create OpcUaCanOpenServer windows tag from linux one
param (
	[Parameter(Mandatory=$true)]
	[string]$tag,
	[Parameter(Mandatory=$true)]
	[string]$desc
)

$v=$tag.replace("cc7","")
$verr=$v.replace("-",".")

Invoke-RestMethod -uri "https://gitlab.cern.ch/api/v4/projects/8887/variables/OpcUaCanOpen_tag" -Method "put" -Body @{"value" = $verr } -Headers @{ 'PRIVATE-TOKEN'='5rfvKt84rtYQNBJi-bgB' } -ContentType "application/x-www-form-urlencoded"
git pull
git tag -a $tag -m $desc
git push origin $tag