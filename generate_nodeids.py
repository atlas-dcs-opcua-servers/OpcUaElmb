from __future__ import print_function
import sys
import platform
import getpass
import time
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('nodeids', help='path/to/NodeIds.csv')
parser.add_argument('outfile', help='outfile w/o extension')
args = parser.parse_args()

f = open(args.nodeids)
input_str = f.read()
f.close()
input_str = input_str.replace('\r','')
rows = map(lambda x:tuple(x.split(',')), input_str.split('\n'))

fh = open(args.outfile + ".h",'w')
def printh(string):
    print(string)
    print(string, end='\n', file=fh)

printh('''/**********************************************************
 * '''+args.outfile+'''.hgen -- do not modify
 **********************************************************
 * Generated from '''+args.nodeids+''' with script '''+sys.argv[0]+'''
 * on host '''+platform.uname()[1]+''' by user '''+getpass.getuser()+''' at '''+
       time.strftime("%Y-%m-%d %I:%M:%S")+'''
 **********************************************************/\n 
#ifndef ''' + args.outfile.upper().split("/")[-1] + '''_H_
#define ''' + args.outfile.upper().split("/")[-1] + '''_H_
''')

for row in rows:

    printh("#define BA_%s %s // %s" % (row[0].upper(), row[1], row[2]))

printh('\n#endif /* ' + args.outfile.upper().split("/")[-1] + '_H_ */')

fh.close()
