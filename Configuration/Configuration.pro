TEMPLATE = lib
TARGET   = opcuacoconfig
CONFIG  += staticlib thread debug_and_release
CONFIG  -= qt
win32 {
  # On a 64-bit PC get the tools from the 32-bit folder
  UAPROGFILES = "C:/Program Files"
  exists( "C:/Program Files (x86)" ) {
    UAPROGFILES = "C:/Program Files (x86)"
  }
  message( "Defined UAPROGFILES=$$UAPROGFILES" )

#  BoostDir    = $$(BOOST_ROOT)
  XsdTools    = "$$UAPROGFILES/CodeSynthesis XSD 3.3"
  XsdToolsLib = "$$XsdTools/lib/vc-10.0"
  INCLUDEPATH += $$XsdTools/include

}
unix {
  exists($$(UNIFIED_AUTOMATION_OPCUA_SDK)) {
    message("using environment variable UNIFIED_AUTOMATION_OPCUA_SDK")
    UaOpcSdk = $$(UNIFIED_AUTOMATION_OPCUA_SDK)
  }
  !exists($$(UNIFIED_AUTOMATION_OPCUA_SDK)) {
    message("environment variable UNIFIED_AUTOMATION_OPCUA_SDK is empty, using hardcoded path")
    UaOpcSdk = /winccoa/Development/Common/automation/1.3.2/linux-src/sdk
  }
  message("using unified automation SDK located at: $$UaOpcSdk")
}


CONFIG(debug, debug|release) {
  DESTDIR = ./debug
  OBJECTS_DIR = ./debug
  unix:QMAKE_CXXFLAGS += -O0 -g3 -Wall -c -fmessage-length=0 -fPIC
}
CONFIG(release, debug|release) {
  DESTDIR = ./release
  OBJECTS_DIR = ./release
  unix:QMAKE_CXXFLAGS += -O3 -Wall -c -fmessage-length=0 -fPIC
}

QT -= core gui

unix {
  LIBS += -ldl
  QMAKE_LIB_FLAG +=  -Wl,--export-dynamic
}

win32 {
  DEFINES += WIN32_LEAN_AND_MEAN
  INCLUDEPATH += $$(BOOST_ROOT)
  QMAKE_CXXFLAGS += /Zc:wchar_t
}

# Sources and headers
include( Configuration.pri )

# XSD tool code generation
# (NB: does not get included in Visual Studio project?)

PRE_TARGETDEPS += CANOpenServerConfig.h
xsdcommand = cxx-tree --output-dir ./ \
		--hxx-suffix .h --cxx-suffix .cpp 

xsdfiles.target = CANOpenServerConfig.h CANOpenServerConfig.cpp 
win:  xsdfiles.commands = xsd.exe $$xsdcommand CANOpenServerConfig.xsd
unix: xsdfiles.commands = xsdcxx  $$xsdcommand CANOpenServerConfig.xsd
xsdfiles.depends = CANOpenServerConfig.xsd 
QMAKE_EXTRA_TARGETS += xsdfiles
