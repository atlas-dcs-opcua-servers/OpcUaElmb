#!/bin/bash

#log file of the building
LOG=OpcUaRpm.log 
if [ -f $LOG ];  then rm $LOG;  echo "The OpcUa rpm creation log file" > $LOG; fi
echo '>>> OpcUaCanOpenServer development log file created: '$LOG

#usefull varialbes
CURDIR="`pwd`"
#
SERVERSOURCEZIP=$CURDIR/RPMS/rpm_opcua/SOURCES/OpcUaCanOpenServer.tar.gz
SERVERSOURCEDIR=$CURDIR/RPMS/rpm_opcua/SOURCES/OpcUaCanOpenServer
SERVERSPECFILE=$CURDIR/RPMS/rpm_opcua/SPECS/OpcUaCanOpenServer.spec
#
SPECVERSLINE="define version"

#================== CREATE THE SOURCE FILE OF SERVER ==================================================== 
# 1) remove old ziped source file
if [ -f $SERVERSOURCEZIP ];
then rm $SERVERSOURCEZIP; echo  'server source zip file deleted... ';
else echo  'server old  source zip file not exists, nothing to delete... ';
fi

# 2) remove old source folder  
if [ -d $SERVERSOURCEDIRl ];
then rm -rf $SERVERSOURCEDIR; echo  'server source dir deleted... ';
fi

mkdir -p $SERVERSOURCEDIR

# 3) copy server files and its zip under rpm SOURCES
if [ -n "$UNIFIED_AUTOMATION_OPCUA_SDK" ];
then UASDK=$UNIFIED_AUTOMATION_OPCUA_SDK;
else UASDK=/winccoa/Development/Common/automation/1.3.3/linux-src/sdk;
fi

cp $CURDIR/bin/OpcUaCanOpenServer $SERVERSOURCEDIR/OpcUaCanOpenServer
cp $CURDIR/bin/canhost $SERVERSOURCEDIR/canhost
cp $CURDIR/bin/ServerConfig.xml.linux $SERVERSOURCEDIR/ServerConfig.xml
cp $UASDK/lib/libuastack.so $SERVERSOURCEDIR/libuastack.so
cp $CURDIR/OPCUACANOpenServer.xml.exam $SERVERSOURCEDIR/OPCUACANOpenServer.xml.exam
cp $CURDIR/server_watchdog.sh $SERVERSOURCEDIR/server_watchdog.sh
cp $CURDIR/Configuration/CANOpenServerConfig.xsd $SERVERSOURCEDIR/CANOpenServerConfig.xsd
cp $CURDIR/CanInterface/release/libsockcan.so.1.0.2 $SERVERSOURCEDIR/libsockcan.so.1.0.2
cp $CURDIR/init-dev.sh $SERVERSOURCEDIR/init-dev.sh
cp $CURDIR/OpcUaCanOpen.conf $SERVERSOURCEDIR/OpcUaCanOpen.conf
cp $CURDIR/opcuacrontab.txt $SERVERSOURCEDIR/opcuacrontab.txt


cd $CURDIR/RPMS/rpm_opcua/SOURCES
tar -pczf OpcUaCanOpenServer.tar.gz OpcUaCanOpenServer
echo  '>>> Server new RPM source files created... ';

#================== BUILD RPMS ==============================================================================
cd $CURDIR/RPMS/rpm_opcua
rpmbuild -bb SPECS/OpcUaCanOpenServer.spec >> $LOG
cd ../..

echo '>>> New server RPM done: OpcUaCanOpenServer-2.1.7-5.x64_86.rpm'
echo '>>> Congratulations! Creation of new OpcUaCanOpenServer RPM done, please find them under: /RPMS/rpm_opcua/RPMS/x64_86/' 

