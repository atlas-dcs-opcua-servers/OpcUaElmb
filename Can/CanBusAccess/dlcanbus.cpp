// This is the main DLL file.

#include "dlcanbus.h"
#include "UaCanTrace.h"

/*
 * dlcanbus.cpp
 *
 *  Created on: Apr 4, 2011
 *      Author: vfilimon
 */

#ifndef WIN32
#include "dlfcn.h"
//#include <windows.h> //I've ommited this line.
#endif

dlcanbus::dlcanbus() {

	p_libComponent = NULL;
	maker_CanAccessObj = NULL;
//	if (!openInterface(name) )
//		p_canObj = NULL;
}

dlcanbus::~dlcanbus() {

//	if (p_canObj)
//		delete p_canObj;
#ifdef WIN32
	FreeLibrary(p_libComponent);
#else
	dlclose(p_libComponent);
#endif
}

bool dlcanbus::openInterface(char *ncomponent)
{
	char name[BUF_SIZE];

	componentName = ncomponent;
#ifdef WIN32
	wchar_t wname[2*BUF_SIZE];
	sprintf_s(name,BUF_SIZE,"%scan.dll",ncomponent);
	size_t len = strlen(name)+1;
	//	wchar_t *wname = new wchar_t[len];
	memset(wname,0,2*len);
	::MultiByteToWideChar(  CP_ACP, NULL,name, -1, wname,(int)len );

	p_libComponent = LoadLibrary(wname);
	//	delete []wname;

	if (p_libComponent == 0) {
		long err = GetLastError();
		LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << "Cannot create component " << name << " " << err;
		cout << "Cannot create component " << name << " " << err << endl;
		return false;
	}
	maker_CanAccessObj = (create_canObj *)GetProcAddress(p_libComponent,"getCanbusAccess");
	if (!maker_CanAccessObj)
	{
		long err = GetLastError();
		LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << "Cannot create component " << name << " " << err;
		FreeLibrary(p_libComponent);
	}
#endif
#ifndef WIN32
	const char * err;
    sprintf(name,"lib%scan.so",ncomponent);
    p_libComponent = dlopen(name, RTLD_NOW);
		
	if (p_libComponent == 0) {
		char *err = dlerror();
		if (err) {
			LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << "dlopen " <<  err ;

			cout << "dlopen error" <<  err << endl;
			return false;
		}
	}

	maker_CanAccessObj = (create_canObj *)dlsym(p_libComponent,"getCanbusAccess");

	err = dlerror();
	if (err) {
		LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << "dlopen " <<  err ;
		cout << "dlsym " << err << endl;
		return false;
	}
	if (!maker_CanAccessObj)
	{
		dlclose(p_libComponent);
		LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << "Process load FAILED!";
		cout << "Process load FAILED!" << endl;
		return false;
	}
	
#endif
	return true;
}
