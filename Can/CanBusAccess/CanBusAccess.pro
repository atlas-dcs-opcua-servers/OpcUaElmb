TEMPLATE = lib
TARGET   = CanBusAccess
CONFIG  += staticlib thread debug_and_release
CONFIG  -= qt

CONFIG(debug, debug|release) {
  DESTDIR = ./debug
  OBJECTS_DIR = ./debug
  unix:QMAKE_CXXFLAGS=
  unix:QMAKE_CXXFLAGS_DEBUG = -O0 -g3 -Wall -c -fmessage-length=0 -fPIC

}
CONFIG(release, debug|release) {
  DESTDIR = ./release
  OBJECTS_DIR = ./release
  unix:QMAKE_CXXFLAGS=
  unix:QMAKE_CXXFLAGS_RELEASE = -O3 -Wall -c -fmessage-length=0 -fPIC
  
}

QT -= core gui

unix {
  LIBS += -ldl
  QMAKE_LIB_FLAG +=  -Wl,--export-dynamic
  INCLUDEPATH += ../../CanOpen/include
}

win32 {
  DEFINES += WIN32_LEAN_AND_MEAN
  INCLUDEPATH += $$(BOOST_ROOT)
  INCLUDEPATH += ../../CanOpen/include
  QMAKE_CXXFLAGS += /Zc:wchar_t
}

# Sources and headers
include( CanBusAccess.pri )
