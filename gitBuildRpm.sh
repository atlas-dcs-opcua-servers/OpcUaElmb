#!/bin/bash

TAG=$1
TAG_ARRAY=(${TAG//-/ })
RPM_VERSION=${TAG_ARRAY[1]}
RPM_RELEASE=${TAG_ARRAY[2]}
echo $TAG
echo $RPM_VERSION
echo $RPM_RELEASE

#tar cf rpmbuild/SOURCES/checkout.tar 
#gzip -1 rpmbuild/SOURCES/checkout.tar 

VERSION_ADDITIONAL="\nBuilt by: $USER on `date` on $HOSTNAME arch=`arch` "
VERSION_STR="$VERSION_STR $VERSION_ADDITIONAL"

echo "#define VERSION_STR \"$VERSION_STR\"" > Server/src/Version.h
sed "s/<SoftwareVersion>[0-9.a-zA-Z]*<\/SoftwareVersion>/<SoftwareVersion>$VERSION_STR<\/SoftwareVersion>/ "  bin/ServerConfig.xml.linux > bin/ServerConfig.xml.linux.new
#mv checkout/bin/ServerConfig.xml.linux.new checkout/bin/ServerConfig.xml.linux
echo "%define version $RPM_VERSION" > RPMS/build_from_svn_tag/rpmbuild/SPECS/OpcUaCanOpenServer.spec
echo "%define release $RPM_RELEASE" >> RPMS/build_from_svn_tag/rpmbuild/SPECS/OpcUaCanOpenServer.spec
cat RPMS/build_from_svn_tag/OpcUaCanOpenServer.spec.git >> RPMS/build_from_svn_tag/rpmbuild/SPECS/OpcUaCanOpenServer.spec
cd RPMS/rpmbuild
rpmbuild -bb RPMS/build_from_svn_tag/rpmbuild/SPECS/OpcUaCanOpenServer.spec
if [ $? -eq 0 ]; then
	echo "rpmbuild finished OK, your rpm should be here: `pwd`/rpmbuild/RPMS/x86_64/"
else
	echo "rpmbuild finished with error code, sorry, no RPM"
fi

