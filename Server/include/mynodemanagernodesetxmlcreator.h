#ifndef MYNODEMANAGERNODESETXMLCREATOR_H
#define MYNODEMANAGERNODESETXMLCREATOR_H

#include "nodemanagernodesetxml.h"

// Factory to instantiate NodeManager implementations derived from NodeManagerNodeSetXml
// Creation of NodeManagers is based on the namespace URI
class MyNodeManagerNodeSetXmlCreator: public NodeManagerNodeSetXmlCreator
{
    UA_DISABLE_COPY(MyNodeManagerNodeSetXmlCreator);
public:

	MyNodeManagerNodeSetXmlCreator();

	virtual ~MyNodeManagerNodeSetXmlCreator();

    // Creates an instance of NodeManagerNodeSetXml or a class derived from NodeManagerNodeSetXml
	NodeManagerNodeSetXml* createNodeManager(const UaString& sNamespaceUri);

    NodeManagerNodeSetXml* gwtNodeXmlManager() { return m_pNodeManager; }

protected:
    NodeManagerNodeSetXml* m_pNodeManager;
};

#endif // MYNODEMANAGERNODESETXMLCREATOR_H
