/******************************************************************************
** opcserver.cpp
**
** Copyright (C) 2008-2009 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unified-automation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: C++ OPC Server SDK sample code
**
** Description: Main OPC Server object class.
**
******************************************************************************/
#include "OpcServer.h"
#include "uamutex.h"
#include "srvtrace.h"
#include "UaCanTrace.h"
#include "serverconfigxml.h"

ServerConfigXml*      OpcServer::m_pServerConfig =  0;


/** Basic server configuration class using the XML file format for internal use in the class OpcServer.*/
class ServerConfigBasicXml: public ServerConfigXml
{
public:
    ServerConfigBasicXml(const UaString& sXmlFileName, const UaString& sApplicationPath, OpcServerCallback* pOpcServerCallback);
    ~ServerConfigBasicXml(){}
    UaStatus afterLoadConfiguration(){return OpcUa_Good;}
    UaStatus startUp(ServerManager*){return OpcUa_Good;}
    UaStatus shutDown(){return OpcUa_Good;}
    Session* createSession(OpcUa_Int32 sessionID, const UaNodeId &authenticationToken);
    UaStatus logonSessionUser(Session* pSession, UaUserIdentityToken* pUserIdentityToken);
private:
    OpcServerCallback* m_pOpcServerCallback;
};

/** Destruction. */
OpcServer::~OpcServer()
{

    if ( m_isStarted != OpcUa_False )
    {
        UaLocalizedText reason("en","Application shut down");
        stop(0, reason);
    }

    if ( m_pUaModule )
    {
        delete m_pUaModule;
        m_pUaModule = NULL;
    }

    if ( m_pCoreModule )
    {
        delete m_pCoreModule;
        m_pCoreModule = NULL;
    }

    // Delete all node managers
    list<NodeManager*>::iterator it;
    for ( it = m_listNodeManagers.begin(); it != m_listNodeManagers.end(); it++ )
    {
        delete (*it);
        (*it) = NULL;
    }
    m_listNodeManagers.clear();

    if ( m_pServerConfig )
    {
        delete m_pServerConfig;
        m_pServerConfig = NULL;
    }
}

/** Sets the server configuration by passing the path of the configuration file.
* One of the overloaded methods needs to be called to give the server a valid configuration.<br> This version forces
* the server to use the default implementation for the ServerConfig object and allows to specify the configuration
* file and the path to the application or the directory containing the configuration and the PKI store..
* @param configurationFile Path and file name of the configuration file. The file can be one of the two file formats supported 
*        by the SDK, a XML file format and an INI file format. The XML file format requires linking of the XML parser module and
*        needs to be activated by compiling this class with the compiler switch SUPPORT_XML_CONFIG
* @param applicationPath The path of the configuration file and PKI store used to replace path placeholders in the configuration file
* @return Success code for the operation. Return 0 if the call succeeded and -1 if the call failed. 
*         This call fails if it is called after starting the server with the method start.
*/
int OpcServer::setServerConfig(const UaString& configurationFile, const UaString& applicationPath)
{
    UaMutexLocker lock(&m_mutex);
    if ( m_isStarted != OpcUa_False )
    {
        // Error, the method is called after the server was started
        return -1;
    }

    m_configurationFile = configurationFile;
    m_applicationPath = applicationPath;
//	m_CanTraceFile = canTraceFile;

	if ( m_pServerConfig == NULL )
    {
        UaUniString sConfigFile(m_configurationFile.toUtf8());
        sConfigFile = sConfigFile.toLower();
       if ( sConfigFile.lastIndexOf(".xml") > (sConfigFile.length() - 5) )
        {
            m_pServerConfig = new ServerConfigBasicXml(m_configurationFile, m_applicationPath/*, m_CanTraceFile*/, m_pOpcServerCallback);
        }
    }
    // Check trace settings
	if ( m_pServerConfig->loadConfiguration().isGood() )
    {
		// Check trace settings
		if (m_pServerConfig)
		{
			OpcUa_Boolean bTraceEnabled = OpcUa_False;
			OpcUa_UInt32  uTraceLevel = 0;
			OpcUa_Boolean bSdkTraceEnabled = OpcUa_False;
			OpcUa_UInt32  uSdkTraceLevel = 0;
			OpcUa_UInt32  uMaxTraceEntries = 0;
			OpcUa_UInt32  uMaxBackupFiles = 0;
			UaString      sTraceFile;
			OpcUa_Boolean bDisableFlush;

			m_pServerConfig->getStackTraceSettings(bTraceEnabled, uTraceLevel);

			m_pServerConfig->getServerTraceSettings(
				bSdkTraceEnabled,
				uSdkTraceLevel,
				uMaxTraceEntries,
				uMaxBackupFiles,
				sTraceFile, bDisableFlush);

			if (bSdkTraceEnabled != OpcUa_False)
			{
				SrvT::initTrace((UaTrace::TraceLevel)uSdkTraceLevel, uMaxTraceEntries, uMaxBackupFiles, sTraceFile, "OpcUaCanOpenServer");
				SrvT::setLocalTimeOutput(true);
				SrvT::setTraceActive(true);
				if (bTraceEnabled != OpcUa_False)
				{
					SrvT::setStackTraceActive(OpcUa_True, uTraceLevel);
				}
			}
		}

		CanOpen::UaCanTrace::initTraceComponent();

		LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << "Load Configuration";
		cout << "Load Configuration" << endl;

		return OpcUa_Good;
	}

	return 0;
}

/** Sets the server configuration by passing a server configuration object.
 One of the overloaded methods needs to be called to give the server a valid configuration.<br>
 This version allows to  pass in a ServerConfig object with a user specific implementation.
 @param pServerConfig Interface pointer of the object implementing the ServerConfig interface.
 @return Success code for the operation. Return 0 if the call succeeded and -1 if the call failed. 
         This call fails if it is called after starting the server with the method start.
*/
int OpcServer::setServerConfig(ServerConfig* pServerConfig)
{
    UaMutexLocker lock(&m_mutex);
    if ( m_isStarted != OpcUa_False )
    {
        // Error, the method is called after the server was started
        return -1;
    }

    m_pServerConfig = (ServerConfigXml*)pServerConfig;
	return 0;
}

/** Adds a node manager to the SDK.
 The node manager will be managed by this class including starting, stopping and deletion of the node manager.
 The method can be called several times for a list of node managers.
 If the method is called before start, all node managers will be started during the call of the start method. If this method is
 called if the server is already started the node manager will be started by this method.
 @return Success code for the operation. Return 0 if adding the node manager succeeded and -1 if adding the node manager failed.
 */
int OpcServer::addNodeManager(NodeManager* pNodeManager)
{
    UaMutexLocker lock(&m_mutex);

    m_listNodeManagers.push_back(pNodeManager);

    if ( m_isStarted != OpcUa_False )
    {
        // Start up node manager if server is already started
        UaStatus ret = pNodeManager->startUp(m_pServerManager);
        if ( ret.isNotGood() )
        {
            TRACE1_ERROR(SERVER_UI, "Error: OpcServer::addNodeManager - can not start up node manager [ret=0x%lx]", ret.statusCode());
            return -1;
        }
    }

    return 0;
}
/** Create the OPC server

/** Sets the callback interface for the server object.
 This callback interface needs to be implemented if the application wants to implement user authentication.
 @param pOpcServerCallback Interface pointer of the callback interface.
 @return Success code for the operation. Return 0 if the call succeeded and -1 if the call failed. 
         This call fails if it is called after starting the server with the method start.
*/
int OpcServer::setCallback(OpcServerCallback* pOpcServerCallback)
{
    UaMutexLocker lock(&m_mutex);
    if ( m_isStarted != OpcUa_False )
    {
        // Error, the method is called after the server was started
        return -1;
    }

    m_pOpcServerCallback = pOpcServerCallback;

    return 0;
}

/** Starts the OPC server
 Initializes and starts up all NodeManagers and SDK modules.
 It is possible to add more NodeManagers after the server is started.
 @return Success code for the operation. Return 0 if the server start succeeded and -1 if the server start failed.
 */
int OpcServer::start()
{
    UaMutexLocker lock(&m_mutex);
    int ret = 0;

    if ( m_isStarted != OpcUa_False )
    {
		LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << " Error, the method is called after the server was already started";
        cout << " Error, the method is called after the server was already started" << endl;
        return -1;
    }

    // Create default configuration object if not provided by the application


    if ( m_pServerConfig == NULL )
    {
		LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << "rgere us no configuration file";
        cout << "rgere us no configuration file" << endl;
        return -2;
    }

    TRACE0_IFCALL(SERVER_UI, "==> OpcServer::start");
	LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << "Server initialize";
	cout << "Server initialize" << endl;
    m_pCoreModule = new CoreModule;
    ret = m_pCoreModule->initialize();
    if ( 0 != ret )
    {
        TRACE0_ERROR(SERVER_UI, "<== OpcServer::start - can not initialize core module");
        return ret;
    }

    // Create and initialize UA server module
    m_pUaModule = new UaModule;

    UaServer *pUaServer = NULL;
	if (m_pOpcServerCallback)
    {
		pUaServer = m_pOpcServerCallback->createUaServer();
    }
    ret = m_pUaModule->initialize(m_pServerConfig,pUaServer);
    if ( 0 != ret )
    {
        return ret;
    }
    // Start core server module
	
	ret = m_pCoreModule->startUp(m_pServerConfig);

    if ( 0 != ret )
    {
		LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << "Error core server module ";
		cout << "Error core server module " << endl;
		TRACE0_ERROR(SERVER_UI, "<== OpcServer::start - can not start up Core module\n");
        return ret;
    }
    else
    {
        UaStatus uaStatus;
		m_pServerManager = m_pCoreModule->getServerManager();
        uaStatus = m_pServerConfig->startUp(m_pServerManager);
        if ( uaStatus.isNotGood() )
        {
            TRACE1_ERROR(SERVER_UI, "<== OpcServer::start - can not start up Server Config [ret=0x%lx]", uaStatus.statusCode());
            m_pCoreModule->shutDown();
            return -1;
        }
    }

    list<NodeManager*>::iterator it;
    for ( it = m_listNodeManagers.begin(); it != m_listNodeManagers.end(); it++ )
    {
        // Start up node manager
        UaStatus ret = (*it)->startUp(m_pServerManager);
        if ( ret.isNotGood() )
        {
            TRACE1_ERROR(SERVER_UI, "Error: OpcServer::start - can not start up node manager [ret=0x%lx]", ret.statusCode());
			return -1;
        }
    }

    ret = m_pUaModule->startUp(m_pCoreModule);
    if ( 0 != ret )
    {
		LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << "Can not start up UA module (possible ipv6 has wrong configuration)";
		cout << "Can not start up UA module (possible ipv6 has wrong configuration)" << endl;
        TRACE0_ERROR(SERVER_UI, "<== OpcServer::start - can not start up UA module");
        m_pCoreModule->shutDown();
        return ret;
    }

    UaString        sRejectedCertificateDirectory;
    UaEndpointArray uaEndpointArray;
	OpcUa_UInt32    nRejectedCertificatesCount;

    m_pServerConfig->getEndpointConfiguration(
        sRejectedCertificateDirectory,
		nRejectedCertificatesCount,
        uaEndpointArray);
    if ( uaEndpointArray.length() > 0 )
    {
        cout << "***************************************************" << endl;
		LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << " Server opened endpoints for following URLs: ";
        cout << " Server opened endpoints for following URLs: " << endl;
        OpcUa_UInt32 idx;
        for ( idx=0; idx<uaEndpointArray.length(); idx++ )
        {
			LOG(Log::INF, CanOpen::UaCanTrace::NoTrace) << uaEndpointArray[idx]->sEndpointUrl().toUtf8();
            std::cout << uaEndpointArray[idx]->sEndpointUrl().toUtf8() << std::endl;
        }
		std::cout << "***************************************************" << std::endl;
    }

    m_isStarted = OpcUa_True;

    TRACE0_IFCALL(SERVER_UI, "<== OpcServer::start");
    return 0;
}

/** Stops the OPC server
 Shutdown and deletes all SDK modules and NodeManagers.
 @param secondsTillShutdown Seconds till shutdown of the server if clients are connected.
 @param shutdownReason      Reason for the shutdown.
 @return Success code for the operation. Return 0 if the server stop succeeded and -1 if the server stop failed.
 */
int OpcServer::stop(OpcUa_Int32 secondsTillShutdown, const UaLocalizedText& shutdownReason)
{
    TRACE0_IFCALL(SERVER_UI, "==> OpcServer::stop");

    UaMutexLocker lock(&m_mutex);
    if ( m_isStarted == OpcUa_False )
    {
        // Error, the server is not started
        return -1;
    }

    m_isStarted = OpcUa_False;

    // Check if we have active clients and sends shutdown information to clients
    // and wait the defined time if clients are connected to allow them to disconnect after they received the shutdown information
    OpcUa_UInt32 clientCount = m_pServerManager->startServerShutDown(secondsTillShutdown, shutdownReason);
    if ( clientCount > 0 )
    {
        UaThread::sleep(secondsTillShutdown);
    }

    // Stop UA server module
    if ( m_pUaModule )
    {
        m_pUaModule->shutDown();
        delete m_pUaModule;
        m_pUaModule = NULL;
    }

    // Stop core server module
    if ( m_pCoreModule )
    {
        m_pCoreModule->shutDown();
        delete m_pCoreModule;
        m_pCoreModule = NULL;
    }

    // Stop all node managers
    list<NodeManager*>::iterator it;
    for ( it = m_listNodeManagers.begin(); it != m_listNodeManagers.end(); it++ )
    {
        // Shut down node manager
        (*it)->shutDown();
//        delete (*it);
//        (*it) = NULL;
    }
    m_listNodeManagers.clear();

    // Stop server config
    if ( m_pServerConfig )
    {
        m_pServerConfig->shutDown();
        delete m_pServerConfig;
        m_pServerConfig = NULL;
    }

    TRACE0_IFCALL(SERVER_UI, "<== OpcServer::stop");
    SrvT::closeTrace();

    return 0;
}

/** Returns the default node manager for server specific nodes in namespace one.
 This node manager can be used to create objects and variables for data access. It can not be used for enhanced OPC UA features. Using features
 like events, methods and historical access requires the implementation of a specific node manager.
 @return NodeManagerConfig interface used to add and delete UaNode objects in the node manager. Returns NULL if the server is not started.
 */
NodeManagerConfig* OpcServer::getDefaultNodeManager()
{
    UaMutexLocker lock(&m_mutex);
    if ( m_isStarted == OpcUa_False )
    {
        // Error, the server is not started
        return NULL;
    }

    return m_pServerManager->getNodeManagerNS1()->getNodeManagerConfig();
}

/** construction
 @param sIniFileName Path and file name of the INI configuration file.
 @param sApplicationPath The path of the configuration file and PKI store used to replace path placeholders in the configuration file
 @param pOpcServerCallback Interface pointer of the callback interface.
 */
/*
ServerConfigBasicIni::ServerConfigBasicIni(const UaString& sIniFileName, const UaString& sApplicationPath, OpcServerCallback* pOpcServerCallback)
: ServerConfigSettings(sIniFileName, sApplicationPath),
  m_pOpcServerCallback(pOpcServerCallback)
{}
*/
/** Creates a session object for the OPC server.
 *  @param sessionID            Session Id created by the server application. 
 *  @param authenticationToken  Secret session Id created by the server application. 
 *  @return                     A pointer to the created session.
 */
/*
Session* ServerConfigBasicIni::createSession(OpcUa_Int32 sessionID, const UaNodeId &authenticationToken)
{
    if ( m_pOpcServerCallback )
    {
        return m_pOpcServerCallback->createSession(sessionID, authenticationToken);
    }
    else
    {
        return new UaSession(sessionID, authenticationToken);
    }
}
*/
/** Validates the user identity token and sets the user for a session.
 *  @param pSession             Interface to the Session context for the method call
 *  @param pUserIdentityToken   Secret session Id created by the server application. 
 *  @return                     Error code.
 */
/*
UaStatus ServerConfigBasicIni::logonSessionUser(
    Session*             pSession,
    UaUserIdentityToken* pUserIdentityToken)
{
    OpcUa_Boolean  bEnableAnonymous;
    OpcUa_Boolean  bEnableUserPw;

    // Get the settings for user identity tokens to support
    getUserIdentityTokenConfig(bEnableAnonymous, bEnableUserPw);

    if ( pUserIdentityToken->getTokenType() == OpcUa_UserTokenType_Anonymous )
    {
        if ( bEnableAnonymous == OpcUa_False )
        {
            return OpcUa_Bad;
        }
        else
        {
            return OpcUa_Good;
        }
    }
    else if ( pUserIdentityToken->getTokenType() == OpcUa_UserTokenType_UserName )
    {
        if ( bEnableUserPw == OpcUa_False )
        {
            return OpcUa_Bad;
        }
        else
        {
            if ( m_pOpcServerCallback )
            {
                return m_pOpcServerCallback->logonSessionUser(pSession, pUserIdentityToken);
            }
            else
            {
                return OpcUa_Bad;
            }
        }
    }

    return OpcUa_Bad;
}
*/

/** construction
 @param sXmlFileName Path and file name of the XML configuration file.
 @param sApplicationPath The path of the configuration file and PKI store used to replace path placeholders in the configuration file
 @param pOpcServerCallback Interface pointer of the callback interface.
 */
ServerConfigBasicXml::ServerConfigBasicXml(const UaString& sXmlFileName, const UaString& sApplicationPath, OpcServerCallback* pOpcServerCallback)
: ServerConfigXml(sXmlFileName, sApplicationPath,"",""),
  m_pOpcServerCallback(pOpcServerCallback)
{

}

/** Creates a session object for the OPC server.
 *  @param sessionID            Session Id created by the server application. 
 *  @param authenticationToken  Secret session Id created by the server application. 
 *  @return                     A pointer to the created session.
 */
Session* ServerConfigBasicXml::createSession(OpcUa_Int32 sessionID, const UaNodeId &authenticationToken)
{
    if ( m_pOpcServerCallback )
    {
        return m_pOpcServerCallback->createSession(sessionID, authenticationToken);
    }
    else
    {
        return new UaSession(sessionID, authenticationToken);
    }
}

/** Validates the user identity token and sets the user for a session.
 *  @param pSession             Interface to the Session context for the method call
 *  @param pUserIdentityToken   Secret session Id created by the server application. 
 *  @return                     Error code.
 */
UaStatus ServerConfigBasicXml::logonSessionUser(
    Session*             pSession,
    UaUserIdentityToken* pUserIdentityToken)
{
    OpcUa_Boolean  bEnableAnonymous;
    OpcUa_Boolean  bEnableUserPw;
	OpcUa_Boolean  bEnableCertificate;
	OpcUa_Boolean  bEnableKerberosTicket;
    // Get the settings for user identity tokens to support
	getUserIdentityTokenConfig(bEnableAnonymous, bEnableUserPw, bEnableCertificate, bEnableKerberosTicket);

    if ( pUserIdentityToken->getTokenType() == OpcUa_UserTokenType_Anonymous )
    {
        if ( bEnableAnonymous == OpcUa_False )
        {
            return OpcUa_Bad;
        }
        else
        {
            return OpcUa_Good;
        }
    }
    else if ( pUserIdentityToken->getTokenType() == OpcUa_UserTokenType_UserName )
    {
        if ( bEnableUserPw == OpcUa_False )
        {
            return OpcUa_Bad;
        }
        else
        {
            if ( m_pOpcServerCallback )
            {
                return m_pOpcServerCallback->logonSessionUser(pSession, pUserIdentityToken);
            }
            else
            {
                return OpcUa_Bad;
            }
        }
    }

    return OpcUa_Bad;
}
