/******************************************************************************
** Copyright (C) 2006-2008 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unifiedautomation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
** 
** Project: OPC sample server
**
** Description: Main entry for the application.
******************************************************************************/
#include "OpcServer.h"
#include "uaplatformlayer.h"
#include "uathread.h"
#include "mynodemanagernodesetxmlcreator.h"

#include "UaCanTrace.h"

#include "Version.h"

using namespace AddressSpace;

#include "xmldocument.h"

#include "shutdown.h"

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <boost/program_options.hpp>

#ifndef WIN32
#  include <unistd.h>
#  include <limits.h> 
#endif
bool createCertificateOnly;
bool noExit;

unique_ptr<UserDevice::ControlInterface> createControlInterface(void);
//unique_ptr<OpcServer> pServer;
OpcServer* pServer;

using namespace boost::program_options;

int OpcServerMain(const char* szAppPath, const char *p_sServerConfig,const char *p_sDeviceConfig)
{
    int ret = 0;
	UaString sDeviceConfig;

    //- Initialize the environment --------------
    // Initialize the XML Parser
    UaXmlDocument::initParser();
    // Initialize the UA Stack platform layer
    ret = UaPlatformLayer::init();
    //-------------------------------------------
//	CanOpen::UaCanTrace *m_Trace = new CanOpen::UaCanTrace();

	if (ret == 0)
	{
		UaString sConfigFileName;

		// Create configuration file name
		if (p_sServerConfig) {

			sConfigFileName = "";
			sConfigFileName = p_sServerConfig;
		}
		else {
			sConfigFileName = szAppPath;
			sConfigFileName += "/ServerConfig.xml";
		}

		if (p_sDeviceConfig) {
	
			sDeviceConfig = p_sDeviceConfig;
		}
		else {
			sDeviceConfig = "";
			sDeviceConfig = szAppPath;
			sDeviceConfig += "/OPCUACANOpenServer.xml";
		}


		//- Start up OPC server ---------------------
		// This code can be integrated into a start up
		// sequence of the application where the
		// OPC server should be integrated
		//-------------------------------------------
		// Create and initialize server object

//		OpcServer* pServer = new OpcServer;
//		pServer = unique_ptr<OpcServer>(new OpcServer());
		pServer = new OpcServer();
		pServer->setServerConfig(sConfigFileName, szAppPath);
		pServer->setApplicationPathConfig(szAppPath);
		pServer->setDeviceConfig(sDeviceConfig);

		if (!createCertificateOnly)
		{
			cout << "Load Configuration" << endl;

			cout << "Config file " << sDeviceConfig.toUtf8() << endl;

			cout << "Set Server Config " << sConfigFileName.toUtf8() << endl;
			cout << "Create Interface" << endl;
			shared_ptr<UserDevice::ControlInterface> pControlInterface = createControlInterface();
			pServer->setControlInterface(pControlInterface);

			if (pControlInterface->CreateXmlParser(sDeviceConfig/*,devicelog*/).isBad())
			{
				cout << "Parser error" << endl;
				return OpcUa_Bad;
			}

			// XML UANodeSet file to load
			UaString sNodesetFile(UaString("%1/AddressSpace.xml").arg(szAppPath));
			// We create our own BaseNode factory to create the user data from XML
			UaBase::BaseNodeFactory* pBaseNodeFactory = new UaBase::BaseNodeFactory;
			// We create our own NodeManager creator to instantiate our own NodeManager
			MyNodeManagerNodeSetXmlCreator* pNodeManagerCreator = new MyNodeManagerNodeSetXmlCreator();
			UaNodeSetXmlParserUaNode* pXmlParser = new UaNodeSetXmlParserUaNode(sNodesetFile, pNodeManagerCreator, pBaseNodeFactory, NULL);
			// Add UANodeSet XML parser as module
			pServer->addModule(pXmlParser);
			pServer->setTypeCreator(pNodeManagerCreator);

			NmBuildingAutomation *pMyNodeManager = new NmBuildingAutomation(pControlInterface, "OpcUaCanOpenServer");
			if (pMyNodeManager == 0)
			{
				cout << "Could not Create Node Manager" << endl;
				return OpcUa_Bad;
			}
			ret = pServer->addNodeManager(pMyNodeManager);
			if (ret != 0)
			{
				cout << "Could not Create Node Manager" << endl;
				return ret;
			}
			pServer->setAutomationNode(pMyNodeManager);
			// Start server object
			cout << "Start Server" << endl;
		}

		ret = pServer->start();

		if (ret != 0)
		{
			cout << "Server Could not start" << endl;
		}
		else
		{
		//-------------------------------------------

		cout << "***************************************************" << endl;
		cout << " Press " << SHUTDOWN_SEQUENCE << " to shutdown server" << endl;
		cout << "***************************************************" << endl;
		// Wait for user command to terminate the server thread.
		while (ShutDownFlag() == 0)
		{
			UaThread::msleep(1000);
		}
		cout << "***************************************************" << endl;
//		LOG(Log::INF) << " Shutting down server";
		cout << " Shutting down server" << endl;
		cout << "***************************************************" << endl;
		}

        //- Stop OPC server -------------------------
        // This code can be integrated into a shut down
        // sequence of the application where the
        // OPC server should be integrated
        //-------------------------------------------
        // Stop the server and wait three seconds if clients are connected
        // to allow them to disconnect after they received the shutdown signal
		pServer->closeInterface();
//		pServer->stop(3, UaLocalizedText("", "User shut down"));
//        delete pServer;
//        pServer = NULL;
        //-------------------------------------------
    }

	cout << "Server stopped" << endl;
//	exit(0);
	// Cleanup the XML Parser
	UaXmlDocument::cleanupParser();

    //- Cleanup the environment --------------
    //-------------------------------------------


    return ret;
}


int main(int argc, char* argv[])
{

    int ret = 0;
    char *pszFind;
	const char *pDeviceConfig = 0;
	const char *pServerConfig = 0;

//	const char *pLogFile = 0;

    RegisterSignalHandler();

    // Extract application path
#ifdef _WIN32
    char szAppPath[MAX_PATH];
	memset(szAppPath, 0, sizeof szAppPath);
    GetModuleFileNameA(NULL, szAppPath, MAX_PATH);
    szAppPath[MAX_PATH-1] = 0;
    pszFind = strrchr(szAppPath, '\\');
#else
    char szAppPath[PATH_MAX];
	memset( szAppPath, 0, sizeof szAppPath  );
    readlink("/proc/self/exe", szAppPath, sizeof(szAppPath));
    szAppPath[PATH_MAX-1] = 0;
    pszFind = strrchr(szAppPath, '/');
#endif
    if (pszFind)
    {
        *pszFind = 0; // cut off appname
    }

	createCertificateOnly = false;
	noExit = false;
	bool printVersion = false;
	string logFile;
    options_description desc ("Allowed options");
	// boost::program_options::value< vector<string> >(),
	desc.add_options()
			("config_file", "A path to the config file")
			("server_config", "server config files")
			("cs", bool_switch(&createCertificateOnly), "Create new certificate and exit" )
			("help", "Print help")
			("noex", bool_switch(&noExit), "no Exit if cannot open canbus")
//			("l", value< string > (&logFile), "Logfile to print to")
			("version", bool_switch(&printVersion), "Print version and exit")
			;
	positional_options_description p;
	p.add("config_file", 1);
	p.add("server_config", 2);

	variables_map vm;
	try
	{
		store(command_line_parser(argc,argv)
				.options(desc)
				.style(command_line_style::allow_long_disguise | command_line_style::unix_style)
				.positional(p)
				.run(),
				vm);
	}
	catch (boost::exception &e)
	{
		cout << "Couldn't interpret command line, please run with -help " << endl ;
		return 1;
	}
	notify(vm);
	if (vm.count("help"))
	{
		cout << desc << endl;
		return 0;
	}

	if (printVersion)
	{
		std::cout << VERSION_STR << std::endl;
		return 0;
	}
	else
	{

		if (vm.count("config_file") > 0)
			pDeviceConfig = vm["config_file"].as< string > ().c_str();
		if (vm.count("server_config") > 0)
			pServerConfig = vm["server_config"].as< string >().c_str();

		//-------------------------------------------
		// Call the OPC server main method

		OpcServerMain(szAppPath,pServerConfig,pDeviceConfig);

		//-------------------------------------------
		// Cleanup the UA Stack platform layer
		UaPlatformLayer::cleanup();

	}

//	return ret;
}
