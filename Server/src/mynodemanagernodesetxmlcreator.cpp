#include "mynodemanagernodesetxmlcreator.h"


MyNodeManagerNodeSetXmlCreator::MyNodeManagerNodeSetXmlCreator()
{
}

MyNodeManagerNodeSetXmlCreator::~MyNodeManagerNodeSetXmlCreator()
{
}

// Creates an instance of NodeManagerNodeSetXml or a class derived from NodeManagerNodeSetXml
NodeManagerNodeSetXml* MyNodeManagerNodeSetXmlCreator::createNodeManager(const UaString& sNamespaceUri)
{
	m_pNodeManager = NodeManagerNodeSetXmlCreator::createNodeManager(sNamespaceUri);

	return m_pNodeManager;
}

