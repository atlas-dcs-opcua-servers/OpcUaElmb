#
# Project file for the CANopen OPC-UA server
#
# To generate the Visual Studio project:
#   qmake -t vcapp Server.pro
# To generate the Makefile:
#   qmake Server.pro
#
# (or start from the 'SUBDIRS' .pro file to recursively generate the projects)
#
TEMPLATE = app
TARGET   = OpcUaCanOpenServer
DESTDIR  = ../bin
CONFIG -= qt
# Installation-dependent stuff: change according to your installation
win32 {
  # On a 64-bit PC get the tools from the 32-bit folder
  UAPROGFILES = "C:/Program Files"
  exists( "C:/Program Files (x86)" ) {
    UAPROGFILES = "C:/Program Files (x86)"
  }
  message( "Defined UAPROGFILES=$$UAPROGFILES" )

  BoostDir    = $$(BOOST_ROOT)
  UaOpcSdk    = "$$UAPROGFILES/UnifiedAutomation/UaSdkCppBundleEval"
  XsdTools    = "$$UAPROGFILES/CodeSynthesis XSD 3.3"
  UaOpcSdkSsl = "$$UaOpcSdk/third-party/win32/vs2010sp1/openssl"
  UaOpcSdkXml = "$$UaOpcSdk/third-party/win32/vs2010sp1/libxml2"
  XsdToolsLib = "$$XsdTools/lib/vc-10.0"
}
unix {
  exists($$(UNIFIED_AUTOMATION_OPCUA_SDK)) {
    message("using environment variable UNIFIED_AUTOMATION_OPCUA_SDK")
    UaOpcSdk = $$(UNIFIED_AUTOMATION_OPCUA_SDK)
  }
  !exists($$(UNIFIED_AUTOMATION_OPCUA_SDK)) {
    message("environment variable UNIFIED_AUTOMATION_OPCUA_SDK is empty, using hardcoded path")
    UaOpcSdk = /winccoa/Development/Common/automation/1.3.3/linux-src/sdk
  }
  message("using unified automation SDK located at: $$UaOpcSdk")
}

# Qt modules used (+) or not used (-)
QT -= qt gui core

# Create a console app, in debug or release mode
CONFIG += console warn_on exceptions flat debug_and_release

debug {
  OBJECTS_DIR = ./debug
}

release {
  OBJECTS_DIR = ./release
}

# NB: The following DEFINES were defined in the Unified Automation example project:
DEFINES += XSD_TREE
DEFINES += SUPPORT_XML_CONFIG
DEFINES += _UA_STACK_USE_DLL
DEFINES += OPCUA_SUPPORT_SECURITYPOLICY_BASIC128RSA15=1
DEFINES += OPCUA_SUPPORT_SECURITYPOLICY_BASIC256=1
DEFINES += OPCUA_SUPPORT_SECURITYPOLICY_NONE=1
DEFINES += OPCUA_SUPPORT_PKI=1
win32 {
  DEFINES += _CONSOLE _CRT_SECURE_NO_WARNINGS
  DEFINES += _CRT_SECURE_NO_DEPRECATE
  DEFINES += _UA_STACK_USE_DLL
  DEFINES += _USE_MATH_DEFINES
  DEFINES += WIN32_LEAN_AND_MEAN
  DEFINES += _SCL_SECURE_NO_WARNINGS
}

# Flags and LIBS stuff
win32 {
  QMAKE_CXXFLAGS += /Zc:wchar_t /MP

  LIBS += -L"$$UaOpcSdk/lib"
  LIBS += -L"$$XsdToolsLib"
  LIBS += -L"C:\boost_1_50_0\stage\lib"

  CONFIG(debug, debug|release) {
    LIBS += -L"../Can/CanBusAccess/debug"
    LIBS += -L"../Configuration/debug"
    LIBS += -L"$$UaOpcSdkSsl/out32dll.dbg"
    LIBS += -L"$$UaOpcSdkXml/out32dll.dbg"
    LIBS += coremoduled.lib
    LIBS += uabased.lib
    LIBS += uamoduled.lib
    LIBS += uamodelsd.lib
    LIBS += uapkid.lib
    LIBS += uastackd.lib
    LIBS += xmlparserd.lib
    LIBS += libeay32d.lib
    LIBS += libxml2d.lib
    LIBS += xerces-c_3d.lib
  }
  CONFIG(release, debug|release) {
    LIBS += -L"../Can/CanBusAccess/release"
    LIBS += -L"../Configuration/release"
    LIBS += -L"$$UaOpcSdkSsl/out32dll"
    LIBS += -L"$$UaOpcSdkXml/out32dll"
    LIBS += coremodule.lib
    LIBS += uabase.lib
    LIBS += uamodule.lib
    LIBS += uamodels.lib
    LIBS += uapki.lib
    LIBS += uastack.lib
    LIBS += xmlparser.lib
    LIBS += libeay32.lib
    LIBS += libxml2.lib
    LIBS += xerces-c_3.lib
  }
  LIBS += crypt32.lib
  LIBS += rpcrt4.lib
  LIBS += ws2_32.lib
  LIBS += CanBusAccess.lib
  LIBS += opcuacoconfig.lib
}

unix {
  LIBS += -Wl,--export-dynamic

  # NB: -luamodule should come before -luabase or it does not compile
  LIBS += -L"$$UaOpcSdk/lib"
  #LIBS += -L"../bin"
  CONFIG(debug, debug|release) {
    QMAKE_CXXFLAGS += -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -fno-strict-aliasing
    LIBS += -L"../Can/CanBusAccess/debug"
    LIBS += -L"../Configuration/debug"
    LIBS += $$UaOpcSdk/lib/libuastackd.so
    LIBS += -lcoremoduled
    LIBS += -luamoduled
    LIBS += -luabased
    LIBS += -luamodelsd
    LIBS += -luapkid
    LIBS += -lxmlparserd
  }
  CONFIG(release, debug|release) {
    QMAKE_CXXFLAGS += -O3 -c -fmessage-length=0 -fPIC -fno-strict-aliasing
    LIBS += -L"../Can/CanBusAccess/release"
    LIBS += -L"../Configuration/release"
    LIBS += $$UaOpcSdk/lib/libuastack.so
    LIBS += -lcoremodule
    LIBS += -luamodule
    LIBS += -luabase
    LIBS += -luamodels
    LIBS += -luapki
    LIBS += -lxmlparser
  }
  LIBS += -lxml2
  LIBS += -lxerces-c
  LIBS += -lCanBusAccess
  LIBS += -lopcuacoconfig
  LIBS += -lboost_program_options-mt
}

# INCLUDE stuff
INCLUDEPATH += ./include
INCLUDEPATH += ../Can/CanBusAccess
INCLUDEPATH += ../Configuration
INCLUDEPATH += ../AddressSpace/include
INCLUDEPATH += ../CanOpen/include

INCLUDEPATH += $$UaOpcSdk/include/uabase
INCLUDEPATH += $$UaOpcSdk/include/uapki
INCLUDEPATH += $$UaOpcSdk/include/uaserver
INCLUDEPATH += $$UaOpcSdk/include/uastack
INCLUDEPATH += $$UaOpcSdk/include/xmlparser
win32 {
  INCLUDEPATH += $$UaOpcSdkSsl/inc32
  INCLUDEPATH += $$UaOpcSdkXml/include/libxml
  INCLUDEPATH += $$BoostDir
}
INCLUDEPATH += $$XsdTools/include

# Sources and headers
include( Server.pri )

# XSD tool code generation
# (NB: does not get included in Visual Studio project?)

xsdcommand = cxx-tree --generate-polymorphic --output-dir ../Configuration \
		--hxx-suffix .h --cxx-suffix .cpp 
xsdfiles.target = ../Configuration/CANOpenServerConfig.cpp
xsdfiles.target += ../Configuration/CANOpenServerConfig.h 
win:  xsdfiles.commands = xsd.exe $$xsdcommand ../Configuration/CANOpenServerConfig.xsd
unix: xsdfiles.commands = xsdcxx  $$xsdcommand ../Configuration/CANOpenServerConfig.xsd
xsdfiles.depends = ../Configuration/CANOpenServerConfig.xsd \
QMAKE_EXTRA_TARGETS += xsdfiles

