#!/bin/bash

source ~/.bashrc
export LD_LIBRARY_PATH=/det/dcs/fwComponents/dim/linux:$LD_LIBRARY_PATH

export

echo HOST $HOST
echo HOSTNAME $HOSTNAME

ps efaux | grep -v grep | grep /opt/OpcUaCanOpenServer/bin/OpcUaCanOpenServer
# if not found - equals to 1, start it
if [ $? -eq 1 ]
then
echo OpcUaCanOpenServer not running! Restarting...
/opt/OpcUaCanOpenServer/bin/OpcUaCanOpenServer >> /localscratch/OpcUaCanOpenServer.log 2>&1 &
else
echo OpcUaCanOpenServer running.
fi

echo Done.