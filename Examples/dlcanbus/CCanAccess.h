/*
 * CCanAccess.h
 *
 *  Created on: Apr 4, 2011
 *      Author: vfilimon
 */

#ifndef CCANACCESS_H_
#define CCANACCESS_H_

class CCanCallback;

class CCanAccess {
public:
	CCanAccess() {};
	virtual bool createBUS(const char * ,const char *, CCanCallback * ccc = 0) = 0 ;
	virtual void setCallBack(CCanCallback *callback) { cb = callback; }
	virtual bool sendRemoteRequest(short ) = 0;
	virtual bool sendMessage(short , unsigned char, unsigned char *) = 0;
	virtual bool getErrorMessage(long , char **) { return false; }
	int getCanBusHandler() { return handler; }
	void setCanBusHandler(int h) { h = handler; }
	char *getBusName() { return busName; }
	virtual ~CCanAccess() {};
protected:
	char *busName;
	int handler;
	CCanCallback *cb;
};

#endif /* CCANACCESS_H_ */
