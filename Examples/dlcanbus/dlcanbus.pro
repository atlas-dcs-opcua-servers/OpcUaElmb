TEMPLATE = lib
TARGET   = dlcanbus
CONFIG  += staticlib thread debug_and_release

CONFIG(debug, debug|release) {
  DESTDIR = ../Debug
  OBJECTS_DIR = ./debug
  unix:QMAKE_CXXFLAGS += -O0 -g3 -Wall -c -fmessage-length=0 -fPIC
}
CONFIG(release, debug|release) {
  DESTDIR = ../Release
  OBJECTS_DIR = ./release
  unix:QMAKE_CXXFLAGS += -O3 -Wall -c -fmessage-length=0 -fPIC
}

QT -= core gui

unix {
  LIBS += -ldl
  QMAKE_LIB_FLAG +=  -Wl,--export-dynamic
}
win32:DEFINES += WIN32_LEAN_AND_MEAN
HEADERS += ./CCanAccess.h
HEADERS += ./CCanCallback.h
HEADERS += ./canaccess.h
HEADERS += ./dlcanbus.h 
 
SOURCES += ./canaccess.cpp
SOURCES += ./dlcanbus.cpp
