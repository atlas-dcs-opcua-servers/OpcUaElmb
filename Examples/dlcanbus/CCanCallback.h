#ifndef _CCanCallback_h
#define _CCanCallback_h

#ifdef WIN32
#include <time.h>
#include "Winsock2.h"
#else
#include <sys/time.h>
#endif

typedef struct CanMsgStruct
{
	long c_id;
	unsigned char c_ff;
	unsigned char c_dlc;
	unsigned char c_data[8];
	struct timeval	c_time;
} canMessage;

//typedef struct CanMsgStruct canMessage;

class CCanCallback
{
public:
	CCanCallback() {};
public:
   	virtual int FireOnChange(int bHandler, CanMsgStruct *msg)=0;
   	virtual int FireOnError(int bHandler, const char *errorMessage, time_t *timestamp)=0;
};
#endif
