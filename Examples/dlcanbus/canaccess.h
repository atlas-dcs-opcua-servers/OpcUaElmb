#ifndef CANACCESS_H
#define CANACCESS_H

#pragma once

#include "dlcanbus.h"
#include "CCanAccess.h"
#include <map>
#include <string>
#include "CCanCallback.h"

using namespace std;

class Canaccess {

public:
    Canaccess();
    int getNumberOfChannels();
//	string getChannelName(int);
//	static CCanAccess *findCanbus(int i);
   
    int openCanBus(string name,string parameters,CCanCallback * ccc = 0);
	CCanAccess *getCanAccess(int n) { return isCanPortOpen(n) ? chName[n] : 0;  }
	void setCallBack(int n,CCanCallback *cb) { if (isCanPortOpen(n) ) chName[n]->setCallBack(cb); }
	void closeCanBus(int);
	void closeCanBus(CCanAccess *cca) { closeCanBus(cca->getCanBusHandler()); }
	bool sendRemoteRequest(int n,short cobId) { return chName[n]->sendRemoteRequest(cobId); }
	bool sendMessage(int n,short cobId, unsigned char len, unsigned char *data) { return isCanPortOpen(n) ? chName[n]->sendMessage(cobId,len,data): false; }
	bool getErrorMessage(int n,long code, char **mess) { return isCanPortOpen(n) ? chName[n]->getErrorMessage(code,mess): false; }


private:
	bool isCanPortOpen(int i) { return (chName.find(i) != chName.end()); }
	map<string,dlcanbus *> Component;
	map<int,CCanAccess *> chName;
	map<string,int> ScanManagers;

    static int g_numChannels;
};

#endif // CANACCESS_H
