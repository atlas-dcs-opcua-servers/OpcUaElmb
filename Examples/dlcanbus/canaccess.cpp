#ifdef WIN32
#include "Stdafx.h"
#endif

#include "canaccess.h"

int Canaccess::g_numChannels;

Canaccess::Canaccess()
{
  g_numChannels = 1;
}

/*
int Canaccess::getNumberOfChannels()
{
    return g_numChannels;
}

string Canaccess::getChannelName(int noc)
{
	  
}
*/

void Canaccess::closeCanBus(int n)
{
	char *name;
	map<int,CCanAccess *>::iterator it = chName.find(n);
	name = chName[n]->getBusName();
	map<string,int>::iterator hiter = ScanManagers.find(string((char *)name));
	delete chName[n];
	chName.erase(it);
	ScanManagers.erase(hiter);
}

int Canaccess::openCanBus(string name,string parameters,CCanCallback *cb)
{
	map<string,dlcanbus *>::iterator itcomponent;
    map<int,CCanAccess *>::iterator it;
	map<string,int>::iterator hiter;
	dlcanbus *dlcan;
	string nameComponent,nameOfCh;

	hiter = ScanManagers.find(name);
    if (! ( hiter == ScanManagers.end() ) ) {
		return (*hiter).second;
    }

	nameComponent = name.substr(0,name.find_first_of(':'));
	nameOfCh = name.substr(name.find_first_of(':')+1);

	itcomponent = Component.find(nameComponent);

	if (! (itcomponent == Component.end() )) {
		dlcan = (*itcomponent).second;
	}
	else {
		dlcan = new dlcanbus();
		if (dlcan->openInterface((char *)nameComponent.c_str()))
			Component.insert(map<string,dlcanbus *>::value_type(nameComponent,dlcan));
		else return 0;
	}
	            
	CCanAccess *tcca = dlcan->maker_CanAccessObj();
	if (tcca != NULL) {
		const char * na = nameOfCh.c_str();
		const char * pa = parameters.c_str();
		bool c = tcca->createBUS( na,pa, cb); 
		if (c){
			ScanManagers.insert(map<string,int>::value_type(name,++g_numChannels));
			chName.insert(map<int,CCanAccess *>::value_type(g_numChannels,tcca));
			tcca->setCanBusHandler(g_numChannels);
			return g_numChannels;
		}
	}
    return 0;
}

/*
bool Canaccess::getTseg1(int i,unsigned int &t)
{ 
	if (isCanOpen(i)) {
			t =  findCanbus(i)->getTseg1();
			return true;
	}
	else return false;
}

bool Canaccess::getTseg2(int i,unsigned int &tseg2)
{ 
	if (isCanOpen(i)) {
			tseg2 =  findCanbus(i)->getTseg2();
			return true;
	}
	else return false;
}

bool Canaccess::getSjw(int i,unsigned int &t)
{ 
	if (isCanOpen(i)) {
			t =  findCanbus(i)->getSjw();
			return true;
	}
	else return false;
}
    
bool Canaccess::getNoSamp(int i,unsigned int &t)
{ 
	if (isCanOpen(i)) {
			t =  findCanbus(i)->getNoSamp();
			return true;
	}
	else return false;
}

bool Canaccess::getSyncmode(int i,unsigned int &t)
{ 
	if (isCanOpen(i)) {
			t =  findCanbus(i)->getSyncmode();
			return true;
	}
	else return false;
}

bool Canaccess::getBaudRate(int i,long &br)
{
	if (isCanOpen(i)) {
			br =  findCanbus(i)->getBaudRate();
			return true;
	}
	else return false;
}

bool Canaccess::getBusParameters(int i)
{
	if (isCanOpen(i)) {
			return findCanbus(i)->getBusParameters();
	}
	else return false;
}
{ if (isCanPortOpen(i)) return ScanManagers[i]; else return 0; }
*/
