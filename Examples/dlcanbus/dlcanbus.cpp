// This is the main DLL file.
#ifdef WIN32
#include "Stdafx.h"
#endif

#include "dlcanbus.h"

/*
 * dlcanbus.cpp
 *
 *  Created on: Apr 4, 2011
 *      Author: vfilimon
 */

#ifndef WIN32
#include "dlfcn.h"
#endif

dlcanbus::dlcanbus() {

	p_libComponent = NULL;
	maker_CanAccessObj = NULL;
//	if (!openInterface(name) )
//		p_canObj = NULL;
}

dlcanbus::~dlcanbus() {

//	if (p_canObj)
//		delete p_canObj;
#ifdef WIN32
	FreeLibrary(p_libComponent);
#else
	dlclose(p_libComponent);
#endif
}

bool dlcanbus::openInterface(char *ncomponent)
{
	char name[BUF_SIZE];
#ifndef WIN32
	char *err;
#endif
	wchar_t wname[2*BUF_SIZE];
	//	const char * err;
	componentName = ncomponent;
#ifdef WIN32
	sprintf(name,"%scan.dll",ncomponent);
	size_t len = strlen(name)+1;
	//	wchar_t *wname = new wchar_t[len];
	memset(wname,0,2*len);
	::MultiByteToWideChar(  CP_ACP, NULL,name, -1, wname,(int)len );

	p_libComponent = LoadLibrary(wname);
	//	delete []wname;
#else
	sprintf(name,"lib%scan.so",ncomponent);
	p_libComponent = dlopen(name, RTLD_NOW);
#endif
	if (p_libComponent == 0) {
#ifdef WIN32
		long err = GetLastError();
		cout << "Cannot create component " << name << " " << err << endl;
		return false;
#endif
#ifndef WIN32
		err = dlerror();
		if (err) {
			cout << "dlopen " <<  err << endl;
			return false;
		}
#endif
	}
#ifndef WIN32
	maker_CanAccessObj = (create_canObj *)dlsym(p_libComponent,"getCanbusAccess");
	err = dlerror();
	if (err) {
		cout << "dlsym " << err << endl;
		return false;
	}
#else
	maker_CanAccessObj = (create_canObj *)GetProcAddress(p_libComponent,"getCanbusAccess");
#endif
	if (!maker_CanAccessObj)
	{
#ifdef WIN32
		long err = GetLastError();
		cout << "Cannot create component " << name << " " << err << endl;
		FreeLibrary(p_libComponent);
#else
		dlclose(p_libComponent);
#endif
		cout << "Process load FAILED!" << endl;
		return false;
	}
	//	p_canObj = maker_CanAccessObj();

	return true;
}
