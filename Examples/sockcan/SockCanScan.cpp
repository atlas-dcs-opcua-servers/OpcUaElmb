/*
 * SockCanScan.cpp
 *
 *  Created on: Jul 21, 2011
 *      Author: vfilimon
 */


//#include "STCANServer.h"
#include "SockCanScan.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction

#include <time.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <net/if.h>

#include <linux/can.h>
#include <linux/can/raw.h>

extern "C" CCanAccess *getCanbusAccess()
	{
		CCanAccess *cc;
		cc = new CSockCanScan();
		printf("CSockCanScan\n");
		return cc;
	}

void* CSockCanScan::CanScanControlThread(void *pCanScan)
{
   	canMessage cmsg;
   	struct can_frame  sockmsg;
   	CSockCanScan *ppcs = reinterpret_cast<CSockCanScan *>(pCanScan);
   	ppcs->run_can = true;
   	int sock = ppcs->sock;
//   	struct msghdr msg;
   	unsigned int nbytes;
	printf("Start Sock Component\n");
   	while (ppcs->run_can) {

		/* these settings may be modified by recvmsg() */
		nbytes = read(sock, &sockmsg, sizeof(struct can_frame));

		if (nbytes < 0) {
//			perror("read");
			continue;
		}

		if (nbytes < sizeof(struct can_frame)) {
//			printf("read: incomplete CAN frame\n");
			continue;
		}
		cmsg.c_id = sockmsg.can_id;
		cmsg.c_time= time(NULL);
		cmsg.c_dlc = sockmsg.can_dlc;
		memcpy(&cmsg.c_data[0],&sockmsg.data[0],8);
		ppcs->cb->FireOnChange(sock, &cmsg);
	}

	pthread_exit(NULL);
    return NULL;
}

int CSockCanScan::FinalConstruct(const char *name, const char *parameters,CCanCallback *callb)
{
	cb = callb;
	run_can = true;
	name_of_port = string(name);

	if ((sock = configureCanboard(name,parameters)) < 0)
		return false;
   	m_idCanScanThread =
		pthread_create(&m_hCanScanThread,NULL,&CanScanControlThread,
	                                    (void *)this);

	return (!m_idCanScanThread);
}

CSockCanScan::~CSockCanScan()
{
	run_can = false;
// close socket
}

int CSockCanScan::configureCanboard(const char *name,const char *parameters)
{
	const char				*com;

    unsigned int tseg1 = 0;
    unsigned int tseg2 = 0;
    unsigned int sjw = 0;
    unsigned int noSamp = 0;
    unsigned int syncmode = 0;
	struct sockaddr_can addr;
//	struct can_frame frame;
//	int nbytes, i;
	struct ifreq ifr;

	int		delta,numPar,br;


//	tUcanInitCanParam	InitParam;

	com = strchr(name,':');
	if (com == NULL) {
		com = name;
	}
	else {
		delta = com - name;
		com++;
	}
	printf("Device %s %s\n",name,com);
	if (parameters)
		numPar = sscanf(parameters,"%d %d %d %d %d %d",&br,&tseg1,&tseg2,&sjw,&noSamp,&syncmode);

//    ATLTRACE("BaudRate = %d %s %s %d\n",br,name,com,numPar);
//    ATLTRACE("Parameters  = %d %d %d %d %d %d\n",br,tseg1,tseg2,sjw,noSamp,syncmode);

		/* Set baud rate to 125 Kbits/second.  */

	sock = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if (sock < 0)   {
       // fill out initialisation struct
		perror("Cannot open the socket");
		return 0;
	}
	memset(&ifr.ifr_name, 0, sizeof(ifr.ifr_name));
	strncpy(ifr.ifr_name, com, sizeof(com));

	if (ioctl(sock, SIOCGIFINDEX, &ifr) < 0) {
		perror("SIOCGIFINDEX");
		return 0;
	}

	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("bind");
		return 0;
	}

	// initialize CAN interface

	return sock;
}

int CSockCanScan::sendErrorCode(int status)
{
	/*
	time_t ftTimeStamp;

	time(&ftTimeStamp);
	cb->FireOnError(numCanHandle,	status,	&ftTimeStamp );
*/
	return status;
}

bool CSockCanScan::sendMessage(short cobID, unsigned char len, unsigned char *message)
{
	struct can_frame frame;
	short Status = 0;
	unsigned int nbytes;
	unsigned char *buf = message;
	frame.can_id = cobID;
	frame.can_dlc = len;
	int l, l1;
	l = len;
	printf("Send Message %d %d\n",cobID,len);
	do {
		if (l > 8) {
			l1 = 8; l = l - 8;
		}
		else l1 = l;
		frame.can_dlc = l1;
		memcpy(frame.data,buf,l1);

		nbytes = write(sock, &frame, sizeof(struct can_frame));
		if (nbytes <sizeof(struct can_frame)) {
			return sendErrorCode(-1);
		}
		buf = buf + l1;
	}
	while (l > 8);
	return sendErrorCode(Status);
}

bool CSockCanScan::sendRemoteRequest(short cobID)
{
	struct can_frame frame;
	int nbytes;
	frame.can_id = cobID + CAN_RTR_FLAG	;
	frame.can_dlc = 0;
printf("Remote Request %d\n",cobID);
	nbytes = write(sock, &frame, sizeof(struct can_frame));
	return sendErrorCode(nbytes);
}

bool CSockCanScan::createBUS(const char *name ,const char *parameters ,CCanCallback *callback)
{
	if (FinalConstruct((const char *)name,parameters,callback) == true) {
		return true;
	}
	else {
		return false;
	}
}

