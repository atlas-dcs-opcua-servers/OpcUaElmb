/*
 * SockCanScan.h
 *
 *  Created on: Jul 21, 2011
 *      Author: vfilimon
 */

#ifndef SOCKCANSCAN_H_
#define SOCKCANSCAN_H_

#include <pthread.h>
#include <unistd.h>

#include <string>
using namespace std;
#include "CCanAccess.h"
#include "CCanCallback.h"


class CSockCanScan : public CCanAccess
{
public:
	CSockCanScan() { } ;
	int FinalConstruct(const char *port, const char *parameters,CCanCallback *);
	string &getNamePort() { return name_of_port; }
	int getHandler() { return sock; }
	int sendErrorCode(int);

	virtual ~CSockCanScan();

    virtual bool sendMessage(short cobID, unsigned char len, unsigned char *message);
	virtual bool sendRemoteRequest(short cobID);
	virtual bool createBUS(const char *name ,const  char *parameters ,CCanCallback *callback);
	bool run_can;
	int numModule;
	int numChannel;
	int numCanHandle;
	int sock;

	CCanCallback *cb;

private:

#define Timeout  1000
   	pthread_t   m_hCanScanThread;
   	int         m_idCanScanThread;

	int configureCanboard(const char *,const char *);
	static void* CanScanControlThread(void *);
	string name_of_port;

};


#endif /* SOCKCANSCAN_H_ */
