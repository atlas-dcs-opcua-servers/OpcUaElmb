TEMPLATE = lib
TARGET   = sockcan
CONFIG  += shared thread debug_and_release

CONFIG(debug, debug|release) {
  DESTDIR = ../Debug
  OBJECTS_DIR = ./debug
}
CONFIG(release, debug|release) {
  DESTDIR = ../Release
  OBJECTS_DIR = ./release
}

QT -= core gui

INCLUDEPATH += ./../dlcanbus

HEADERS += ./SockCanScan.h
SOURCES += ./SockCanScan.cpp
