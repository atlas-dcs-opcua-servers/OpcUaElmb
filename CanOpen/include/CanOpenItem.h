#ifndef __CANOPENITEM_H__
#define __CANOPENITEM_H__

#include <opcua_basedatavariabletype.h>
#include "CanBusAccess.h"
#include "uaobjecttypes.h"
#include "UserDeviceItem.h"
#include "CANOpenServerConfig.h"
#include "InterfaceUserDataBase.h"

#pragma once

namespace CanOpen
{
	/** @brief class defined method to exchange CanOpen data with UaVariable
	 * The Class defines the basic operation of UaVariable
	 * which takes data from CAN message and put date into CAN message
	 */
	using namespace UserDevice;
	static OpcUa_Boolean  TypeConversion = true;
	static OpcUa_UInt32   InitTimeout = 10;
	static OpcUa_Boolean  InitCalc = true;
	static OpcUa_Boolean  noSendDisconnectedSDO = false;

	class CanOpenItem : public UserDeviceItem
	{
	public:

		CanOpenItem(pUserDeviceStruct *parent,::xml_schema::type *conf,UaNode *pAS,OpcUa_UInt code):
			UserDeviceItem(parent,conf,pAS,code)
		{
		};
		
		CanOpenItem() = default;

		virtual UaVariant UnPack() {
			UaVariant a;
			return a;
		};							// Unpack data from device message 
		virtual UaStatus pack(const OpcUa_Variant *) {
			return OpcUa_Good;
		};		// pack data to device message

		virtual UaStatus write(UaDataValue &udv)			// pass data to device
		{
			InterfaceUserDataBase * device = static_cast<InterfaceUserDataBase *>(getParentDevice());
			UaStatus ret = device->sendDeviceMessage(getCodeFunction(),&udv);
			return ret;
		}

		virtual	UaStatus read()							// read data from device
		{
			InterfaceUserDataBase * device = static_cast<InterfaceUserDataBase *>(getParentDevice());

			UaStatus ret = device->getDeviceMessage(getCodeFunction());
			return ret;
		}

		static UaStatus convertOpcUaType(const OpcUa_Variant *sData, OpcUa_Variant *oData, OpcUa_BuiltInType bType);
		
		virtual ~CanOpenItem(void)
		{
		} ;


		virtual UaStatus getDeviceMessage(OpcUa_UInt32 commandCode)
		{
			OpcUa_ReferenceParameter(commandCode);
			return OpcUa_Good;
		}

		virtual UaStatus sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value = 0)
		{
			InterfaceUserDataBase * device = static_cast<InterfaceUserDataBase*>(getParentDevice());
			UaStatus ret = device->sendDeviceMessage(code, value);
			return ret;
		}

		virtual UaStatus connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *blink)
		{
			OpcUa_ReferenceParameter(code);
			OpcUa_ReferenceParameter(conf);
			OpcUa_ReferenceParameter(blink);
			return OpcUa_Good;

		}
		virtual UaVariant getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf)
		{
			OpcUa_ReferenceParameter(code);
			OpcUa_ReferenceParameter(conf);
			UaVariant val;
			val.clear();
			return val;
		}

	};
}
#endif
  
