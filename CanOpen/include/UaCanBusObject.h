#ifndef __UACANBUSOBJECT_H__
#define __UACANBUSOBJECT_H__

#include "uaobjecttypes.h"
#include "NmBuildingAutomation.h"
#include "UaControlDeviceGeneric.h"

#include "CANOpenServerConfig.h"
//#include "CanObject.h"

using namespace AddressSpace;

namespace CanOpen
{
	/**
	* @brief Top level object describing CANBUS has IOManager for PDO interaction
	**/
	class UaCanBusObject:
		public UaControlDeviceGeneric
	{
	public:
		/** Constructor
		* @param ca XML description of canbus in the system
		* @param newNodeId ua node id
		* @param pNodeManager pointer to node manager
		*/
		UaCanBusObject(CANBUS *ca,
			const UaNodeId& newNodeId,
			NmBuildingAutomation* pNodeManager
		);
		//! destructor 
		//! close io manager
		virtual ~UaCanBusObject(void) 
		{ 

		} 

		//! @return io manager
//		IOManager	*getPDOIOManager() 
//		{ 
//			return ((CanObject *)getUserData())->getIOManager();
//		}

	private:

	};
}

#endif