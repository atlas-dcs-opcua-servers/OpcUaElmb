#ifndef __CANOPENITEM_H__
#define __CANOPENITEM_H__

#include <opcua_basedatavariabletype.h>
#include "CanBusAccess.h"
#include "uaobjecttypes.h"
#include "UserDeviceItem.h"
#include "CANOpenServerConfig.h"
#include "InterfaceUserDataBase.h"

namespace CanOpen
{
	/** @brief class defined method to exchange CanOpen data with UaVariable
	 * The Class defines the basic operation of UaVariable
	 * which takes data from CAN message and put date into CAN message
	 */
	using namespace UserDevice;

	static OpcUa_UInt32   InitTimeout = 10;
	class UserDeviceItem : public UserDeviceItem
	{
	public:

		UserDeviceItem(UserDataBase *parrent,::xml_schema::type *conf,UaNode *pAS,OpcUa_UInt code):
			UserDeviceItem(parrent,conf,pAS,code)
		{
		};
		
//		static UaStatus convertOpcUaType(const OpcUa_Variant *sData, OpcUa_Variant *oData, OpcUa_BuiltInType bType);
		
		virtual ~UserDeviceItem(void)
		{
		} ;


		virtual UaStatus connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *blink)
		{
			OpcUa_ReferenceParameter(code);
			OpcUa_ReferenceParameter(conf);
			OpcUa_ReferenceParameter(blink);
			return OpcUa_Good;

		}

	};
}
#endif
  
