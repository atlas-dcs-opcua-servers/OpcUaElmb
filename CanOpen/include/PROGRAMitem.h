#ifndef __CANBUSOBJECT_H__
#define __CANBUSOBJECT_H__

#include <opcua_basedatavariabletype.h>

#include "CANOpenServerConfig.h"

#include <time.h>
#include <list>

namespace CanOpen 
{
	using namespace std;

	/**
	* @brief This class comprises all properties and functions to control Can bus
	* This class has the implementation  of two main abstract function from CanObject getDeviceMasage and sendDeviceMessage
	*/
	class PROGRAMitem:
		public CanObject
	{
		UA_DISABLE_COPY(CanBusObject);

	public:

		/** @brief Constructor
		* @param interf pointer to parent object usually null for CanBusObject
		* @param ca XML configuration entry
		* @param link pointer to Ua Node
		* @param code code of function
		*/
		CanBusObject(UserDataBase *interf,CANBUS *ca,UaNode *link,int code);

		//! Destructor
		virtual ~CanBusObject(void);
		/** Callback function to set bus error
		* @param err error code
		* @param errmsg error message
		* @param tv timestamps
		*/
		virtual void setPortError(const int err, const char *errmsg, timeval &tv);

		//! this function is dummy for CanBusObject. There is no any readable data
		virtual UaStatus getDeviceMessage(OpcUa_UInt32 code)
		{
			OpcUa_ReferenceParameter(code);
			return OpcUa_Good;
		} 

		//! open can bus
		void openCanBus();

		/** send message to canbus
		* @param code code of function
		* @param value data to send
		*/
		virtual UaStatus sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value = 0);

		//! send sync message
		void sendSync()  
		{
			if( m_pCommIf ) m_pCommIf->sendMessage(CANOPEN_SYNC_COBID,0,0);
		}
		
		/** @brief connect function code to ua node to pass data 
		* @param code function code defines the operation
		* @param conf XML object containing the configuration information for this object
		* @param blink ua node to pass data
		*/
		virtual UaStatus connectCode(OpcUa_UInt32 code,::xsd::cxx::tree::type *conf,UaNode *blink);

		//! get main can bus callback function
		CCanUACallback *getCanUACallback() { return m_pCallback; }

		/** @brief Add pdo object
		* @param pdoo pointer to CanPDOObject
		*/
		void addPDO(CanPDOObject * pdoo) { m_pCallback->addPDO(pdoo); }
		busNodes & getListNodes() { return m_pCallback->getListNodes(); }

		/** @brief add Can node to the bus
		* @param pnode node to add
		*/
		void addNode(CanNodeObject *pnode) { m_pCallback->addCanNode(pnode); }
		//! send nmt message after startup
		void sendInitNodeNmt() { m_pCallback->sendInitNodeNmt(); }
		//! initialize the bus. send rtr after startup
		void initialize();
		
		//! start synch interval thread to send sync message
		void startSyncInterval() { m_pSyncIn->startSyncInterval(); }

		//! start nodeguarding interval thread to send NG messages
		void startNodeGuardingInterval() { m_pNgIn->startNodeGuardingInterval(); }

		string &getCanBusName() { return m_sBusName; }
		string &getCanBusAddress() { return m_sBusAddress; }

		void waitData(); //! wait initial data after startup

	private:

		CCanUACallback	*	m_pCallback; 	//! Call back class to take data from can bus

		SyncInterval	*	m_pSyncIn;		//! Sync interval thread object
		NodeGuardingInterval * m_pNgIn;		//! Node Guarding thread object

		OpcUa::BaseVariableType *m_iPortError;		//! Can Bus error reading from driver
		OpcUa::BaseVariableType *m_sPortErrorDescription; //! Description of the port error

		string m_sBusName;  //! bus name
		string m_sBusParameters;  //! buss parameters
		string m_sBusAddress;  //! bus address (port)
	};
}
#endif

