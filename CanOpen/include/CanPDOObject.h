#ifndef __CANPDOOBJECT_H__
#define __CANPDOOBJECT_H__

#include "uaobjecttypes.h"
#include "uasemaphore.h"
#include "CanOpenObject.h"
#include "uabytearray.h"
#include "Ids.h"
//#include "BuildingAutomationTypeIds.h"
#include "CanOpenData.h"
#include <vector>
#include <map>
#include <opcua_basedatavariabletype.h>

namespace CanOpen
{
	class CanPDOItem;

	/** @brief The Class represents the properties and operations of canopen messages.<br>
	* 
	*/
	class CanPDOObject: public CanOpenObject
	{
	public:

		/**
		* @brief CanPDOObject Constructor
		* @param par a parent CanNode Object
		* @param conf XML configuration information for this object
		* @param blink UaNode which represent the PDO in Address space
		* @param code - function code of operation
		*/
		CanPDOObject(pUserDeviceStruct *par,
			PDO *conf,UaNode *blink,OpcUa_UInt32 code);

		virtual ~CanPDOObject(void)
		{
//			cout << "Delete " << getCobId() << endl;
		};

		virtual void pass(const CanMsgStruct *);

//		UaStatus pack(const OpcUa_Variant *sData,CanPDOItem *item);
		void setItem(OpcUa_UInt16 ch,CanPDOItem *pdoi);
		virtual UaStatus getDeviceMessage(OpcUa_UInt32 commandCode)
		{ 
			OpcUa_ReferenceParameter(commandCode);
			return OpcUa_Good;
		} 
		virtual UaStatus sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value = 0) ;
		OpcUa_UInt32	getNumberChennal() { return m_iMaxCount; }
		UaByteArray& getBuffer(OpcUa_UInt16 ch) { return m_pdoBuffer[ch]; } 
		void setBuffer(	UaByteArray& buf,OpcUa_UInt16 ch) { m_pdoBuffer[ch] = buf; } 
		virtual void getInitRTRData();

		/** @brief connect function code to ua node to pass data 
		* @param code function code defines the operation
		* @param conf XML object containing the configuration information for this object
		* @param blink ua node to pass data
		*/
		virtual UaStatus connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *blink);
		virtual UaVariant getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf);

		/// Send rtr message
		virtual void sendRTR() 
		{ 
			getCanBus()->sendRemoteRequest(getCobId());
		}
		
		/// add node id to base cobid of pdo
		/**
		* This function used for standard ELMB distribution pdo TPDO and RPDO.
		* In configuration file written only base number.
		*/ 
		void addNodeId(bool type); 
		

		virtual void waitRTRData() 
		{ 
			if (m_bInitRTR) {
				waitOperation(0);
			}
		}

	protected:
		OpcUa_Boolean		m_bInitRTR;		///< Send or not in start up time rtr message
		OpcUa::BaseVariableType *m_pRTRCommand;		///< RTR Command item


		vector<UaByteArray> m_pdoBuffer;  ///< pdo buffer also can save the date from ELMB channels
		multimap<OpcUa_UInt16,CanPDOItem *> m_cPDOs;  ///< set of items coming with this pdo

	};

}
#endif
