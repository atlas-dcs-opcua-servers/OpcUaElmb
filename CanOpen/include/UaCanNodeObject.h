#ifndef __UACANNODEOBJECT_H__
#define __UACANNODEOBJECT_H__

#include "NmBuildingAutomation.h"
#include "UaControlDeviceGeneric.h"
#include "iomanager.h"
#include "CANOpenServerConfig.h"
#include "UaDeviceNodeType.h"

#include "CanObject.h"
#include "CanPDOObject.h"
#include "CanPDO16Object.h"

namespace CanOpen
{
	class IOManagerSDO;
	/** @brief This class provides the creation of all properties and object belonging th can node devices
	* Each object of this class has a io manager to send and receive sdo messages
	* creation on can node structure can be based on node type or XML entry
	*/
	class UaCanNodeObject:
		public AddressSpace::UaControlDeviceGeneric
	{
	public:
		/** @brief Constructor based on XML description
		* @param nd XML entry describing the can node
		* @param newNodeId ua node id
		* @param pNodeManager pointer to node manager
		* @param pcanbus pointer to parent hardware interface object (can bus)
		*/
//		UaCanNodeObject(NODE *nd, const UaNodeId& newNodeId, NmBuildingAutomation *pNodeManager,UserDataBase* pcanbus );

		/** @brief Constructor based on type information
		* @param nd XML entry describing the can node
		* @param pNodeType pointer to can node type information
		* @param newNodeId ua node id
		* @param pNodeManager pointer to node manager
		* @param pcanbus pointer to parent hardware interface object (can bus)
		*/
		UaCanNodeObject(NODE *nd,UaObject *pNodeType , const UaNodeId& newNodeId, AddressSpace::NmBuildingAutomation *pNodeManager, UserDevice::pUserDeviceStruct* pcanbus);

		/** 
		* Destructor
		*/
		virtual ~UaCanNodeObject(void);
		//! get io manager
		IOManager	*getSDOIOManager()
		{
			pUserDeviceStruct *p = static_cast<pUserDeviceStruct *>(getUserData());
			return static_cast<CanObject *>(p->getDevice())->getIOManager();
		}
	private:

//		void setInitValue(UaVariant &uav,OpcUa_BuiltInType nType);

		/** @brief create sdo object  belonging to node based on XML description
		* @param sn XMl description of can node
		* @param pNodeManager pointer to node manager
		* @return error description
		*/
		UaStatus createSDOs(NODE *sn, AddressSpace::NmBuildingAutomation *pNodeManager);

		/** @brief create sdo type object  
		* @param sn XMl description of can node type
		* @param pNodeManager pointer to node manager
		* @return error description
		*/
		UaStatus createSDOs(NODETYPE *sn, AddressSpace::NmBuildingAutomation *pNodeManager);

		/** @brief create sdo item object  belonging to node based on XML description
		* @param sdoi XMl description of can node
		* @param pNodeManager pointer to node manager
		* @return error description
		*/
		UaStatus createSDOITEMs(NODE *sdoi, AddressSpace::NmBuildingAutomation *pNodeManager);

		/** @brief create sdo item type object
		* @param sdoi XMl description of can node type
		* @param pNodeManager pointer to node manager
		* @return error description
		*/
		UaStatus createSDOITEMs(NODETYPE *sdoi, AddressSpace::NmBuildingAutomation *pNodeManager);

		/** @brief Create PDO
		* @param pit XML description 
		* @param pNodeManager pointer to node manager
		* @param type false is pdo, true (TPDO* or RPDO*)
		* @return error description
		*/

		UaStatus createPDOs(PDO &pit, AddressSpace::NmBuildingAutomation *pNodeManager,bool type);

		/** @brief Create PDO with two byte selector channels
		* @param pit XML description
		* @param pNodeManager pointer to node manager
		* @param type false is pdo, true (TPDO* or RPDO*)
		* @return error description
		*/
		UaStatus createPDO16s(PDO16 &pit, AddressSpace::NmBuildingAutomation *pNodeManager, bool type);

		/** @brief create  Programs object  belonging to node based on XML description
		* @param sn XMl description of can node
		* @param pNodeManager pointer to node manager
		* @return error description
		*/
		UaStatus createPROGRAMs(NODE *sn, AddressSpace::NmBuildingAutomation *pNodeManager);

//		template<typename PDOITEMITERATOR>
//		UaStatus createPDOs(const char *pdon, OpcUa_Byte nc, PDOITEMITERATOR &, NmBuildingAutomation *,bool);

		/** @brief Create pdos
		* @param xsdObject XMl description of can node type
		* @param pNodeManager pointer to node manager
		* @return error description
		*/
		UaStatus createAllTypesPDOs(NODE *xsdObject, AddressSpace::NmBuildingAutomation *pNodeManager);
		
		/** @brief Create pdo types
		* @param nd XML description of can node type
		* @param pNodeManager pointer to node manager
		* @return error description
		*/
		UaStatus createAllTypesPDOs(NODETYPE *nd, AddressSpace::NmBuildingAutomation *pNodeManager);

		template<typename NODEDEF>
		/** @brief create calculation item
		* @param nd XMl description of can node or type  (template parameter)
		* @return error description
		*/
		UaStatus createItems(NODEDEF *nd, AddressSpace::NmBuildingAutomation *pNodeManager);

//		IOManagerSDO	*m_pIOManagerSDO;  //! sdo operation manager
	};
}
#endif
