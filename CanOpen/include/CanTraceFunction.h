#pragma once
enum lCanTraceFunction {
	lNoTrace = 0,			/*!< Trace_Function: No Trace         */
	lPdoMessage = 1,		/*!< Trace_Function: PdoMessages      */
	lSdoMessage = 2,		/*!< Trace_Function: SdoMessages      */
	lSegSdoMessage = 4,		/*!< Trace_Function: SegSdoMessages   */
	lNGMessage = 8,			/*!< Trace_Function: NG Messages    */
	lEmergMessage = 16,		/*!< Trace_Function: Emergency Messages     */
	lNMTMessage = 32,		/*!< Trace_Function: MNT Messages     */
	lCanTrace = 64,
	lPdo1Message = 128,
	lPdo2Message = 256,
	lPdo3Message = 512,
	lPdo4Message = 1024

};

enum CanTraceFunction {
	NoTrace = 0,			/*!< Trace_Function: No Trace         */
	PdoMessage = 1,			/*!< Trace_Function: PdoMessages      */
	SdoMessage = 2,			/*!< Trace_Function: SdoMessages      */
	SegSdoMessage = 3,		/*!< Trace_Function: SegSdoMessages   */
	NGMessage = 4,			/*!< Trace_Function: NG Messages    */
	EmergMessage = 5,		/*!< Trace_Function: Emergency Messages     */
	NMTMessage = 6,			/*!< Trace_Function: MNT Messages     */
	CanTrace = 7,
	Pdo1Message = 8,
	Pdo2Message = 9,
	Pdo3Message = 10,
	Pdo4Message = 11

};
