#ifndef __SYNCINTERVAL_H__
#define __SYNCINTERVAL_H__

#include "uabase.h"
#include "uastring.h"
#include "statuscode.h"
#include "uaarraytemplates.h"
#include "uathread.h"
#include "opcua_basedatavariabletype.h"

namespace CanOpen
{
	class CanBusObject;

	class SyncInterval: public UaThread
	{
		UA_DISABLE_COPY(SyncInterval);
	public:

		/* construction / destruction */
		SyncInterval(CanBusObject *pCI, OpcUa::BaseVariableType* pVar = NULL);

		virtual ~SyncInterval();
		
		void startSyncInterval() { start(); }

		/* Get Sync Interval */
		//    OpcUa_UInt64 getSyncInterval();


		/* Set Controller status and data */
		void setSyncInterval(OpcUa::BaseVariableType* pVar) { m_iSyncIntervalVariable = pVar; }

	private:
		// Simulation Thread main function
		void run();
		OpcUa::BaseVariableType* m_iSyncIntervalVariable;
		OpcUa_Boolean m_stop;
		CanBusObject *m_pCanIn;
	};
}
#endif 
