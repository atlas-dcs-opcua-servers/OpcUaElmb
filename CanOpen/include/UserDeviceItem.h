#ifndef __USERDEVICEITEM_H__
#define __USERDEVICEITEM_H__

#include <opcua_basedatavariabletype.h>
#include "CanBusAccess.h"

#include "uaobjecttypes.h"
#include "UserDeviceStruct.h"
#include "CANOpenServerConfig.h"
#include "InterfaceUserDataBase.h"

namespace UserDevice
{
	/** @brief Abstract class defined method to exchange data with UaVariable
	 * The Class defines the basic operation of UaVariable
	 * which takes data from CAN message and put date into CAN message
	 */
	class UserDeviceItem : public InterfaceUserDataBase
	{
	public:

		UserDeviceItem(pUserDeviceStruct *parrent,::xml_schema::type *conf,UaNode *pAS,OpcUa_UInt code):
			InterfaceUserDataBase(parrent,conf,pAS,code)
		{
		};
	
		OpcUa_BuiltInType getItemType() { return itemType; }
		
		virtual UaVariant UnPack() = 0;							// Unpack data from device message 
		virtual UaStatus pack(const OpcUa_Variant *) = 0;		// pack data to device message

		virtual UaStatus write(UaDataValue &udv) = 0;			// pass data to device

		virtual	UaStatus read()	= 0;						    // read data from device

		OpcUa::BaseDataVariableType *getUaEntry() 
		{ 
			return static_cast<OpcUa::BaseDataVariableType *>(getAddressSpaceEntry()); 
		}

		virtual ~UserDeviceItem(void) {} ;

	protected:
	 
		OpcUa_BuiltInType itemType;		// Data type
	};
}
#endif
  
