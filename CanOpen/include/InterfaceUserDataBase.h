#ifndef __INTERFACEUSERDATABASE_H__
#define __INTERFACEUSERDATABASE_H__

#include "uabasenodes.h"
#include "uadatavalue.h"
#include "UserDeviceStruct.h"

#include <iostream>

namespace UserDevice
{
	/**
	 * @brief Abstract class collects the basic properties and function of the Device
	 * This class defines the interface which OpcUaCanOpen Server uses to connect to the hardware part and
	 * "send and get" functions to exchange data
	 */
	class InterfaceUserDataBase : public UserDeviceStruct
	{
		UA_DISABLE_COPY(InterfaceUserDataBase);

	public:
		/** @brief Constructor
		* Also create the shared mutex which can be used to synchronize operation on device using lock/unlock function
		* @param parent the pointer on parent device
		* @param conf XML definition for device
		* @param pAS UaNode callback node to pass data ( if needed ) 
		* @param code function code defines data or operation
		*/
		InterfaceUserDataBase(pUserDeviceStruct *parent,::xsd::cxx::tree::type *conf,UaNode *pAS,OpcUa_UInt code)  :
			UserDeviceStruct(parent,conf,pAS,code)
		{
			//if (parent) {
			//	m_pSharedMutex = static_cast<InterfaceUserDataBase *>(*parent->pPoint). ->getShareMutex();
			//	m_pSharedMutex->addReference();
			//}
			//else {
			//	m_pSharedMutex = new UaMutexRefCounted();
			//}
		}

		////! lock shared mutex to protect 
		//void lock() {
		//	m_pSharedMutex->lock();
		//}

		////! unlock shared mutex
		//void unlock() { 
		//	m_pSharedMutex->unlock();
		//}

		////! get mutex
		//::UaMutexRefCounted* getShareMutex() { return  m_pSharedMutex; }

		//void releaseMutex()
		//{
		//	m_pSharedMutex->releaseReference();
		//	if (m_pSharedMutex->referenceCount() == 0)
		//	{
		//		m_pSharedMutex = NULL;
		//	}
		//}

		//void addReference()
		//{
		//	if (m_pSharedMutex)
		//	{
		//		m_pSharedMutex->addReference();
		//	}
		//}

		//OpcUa_Int32 countMutex()
		//{
		//	return m_pSharedMutex->referenceCount();
		//}



		//! Destructor
		virtual ~InterfaceUserDataBase(void)
		{
			//if ( m_pSharedMutex )
			//{
			//	// Release our local reference
			//	m_pSharedMutex->releaseReference();
			//	m_pSharedMutex = NULL;
			//}

		};

		/** @brief read data defined of function code from device 
		* Abstract function to take can messages based on type id (buldingAutomationTypeIds.h)
		* @param code function code defining of operation (send command ,data or read data)
		*/
		virtual UaStatus getDeviceMessage(OpcUa_UInt32 code) = 0;  

		/** @brief send data or command on device
		* Abstract function to send data  based on type id (buldingAutomationTypeIds.h)
		* @param code function code
		* @param value data to send
		*/
		virtual UaStatus sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value = 0) = 0;

		/** @brief Connect UaVariable with function code
		* @param code function code
		* @param conf XML object containing the configuration information for this object
		* @param  node UaVariable containing data to send /received
		*/
		virtual UaStatus connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *node) = 0;
		virtual UaVariant getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf) = 0;
	//protected:
	//	UaMutexRefCounted* m_pSharedMutex;	//!  shared mutex should use to protect operation on device
	};

}
#endif
