#ifndef __IOTERANSACTIONCANOPEN_H__
#define __IOTERANSACTIONCANOPEN_H__

#include "iomanager.h"
#include "variablehandleuanode.h"
#include "opcuatypesinternal.h"
#include "CanPDOObject.h"

namespace AddressSpace
{

	using namespace CanOpen;

	typedef UaPointerArray<VariableHandleUaNode> PVariableHandleUaNodeArray;
	typedef UaPointerArray<CanPDOObject> PCanPDOObjectArray;

	/** @brief The class contains common feature for pdo and sdo context classes
	*/
	class IOTransactionContextCanOpen
	{
	public:
		/** construction with Variable initialization */
		IOTransactionContextCanOpen()
		{
			m_pCallback          = NULL;
			m_hTransaction       = 0;
			m_totalItemCountHint = 0;
			m_maxAge             = 0;
			m_pIOManager   = NULL;
			m_timestampsToReturn = OpcUa_TimestampsToReturn_Both;
			m_pSession           = NULL;
			m_transactionType = IOManager::TransactionInvalid;
			m_returnDiagnostics  = 0;
		};
		/** destruction */
		virtual ~IOTransactionContextCanOpen()
		{
			if ( m_pSession )
			{
				m_pSession->releaseReference();
			}
			OpcUa_UInt32 count = m_arrUaVariableHandles.length();
			for ( OpcUa_UInt32 i=0; i<count; i++ )
			{
				if (m_arrUaVariableHandles[i]) {
					m_arrUaVariableHandles[i]->releaseReference();
					m_arrUaVariableHandles[i] = NULL;
				}
			}

		};

		/** Set the Session of the current Session.
		*  @param pSession a pointer to the Session to be set.
		*/
		void setSession(Session* pSession)
		{
			if ( pSession )
			{
				m_pSession = pSession;
				pSession->addReference();
			}
		};

		/** Get the actual Session via pointer.
		*  @return the actual Session.
		*/
		inline Session* pSession()
		{
			return m_pSession;
		};

		IOManager::TransactionType	m_transactionType;
		OpcUa_UInt32				m_returnDiagnostics;
		IOManager*					m_pIOManager; 
		IOManagerCallback*			m_pCallback;
		OpcUa_UInt32				m_hTransaction;
		OpcUa_UInt32				m_totalItemCountHint;
		OpcUa_Double				m_maxAge;
		OpcUa_TimestampsToReturn	 m_timestampsToReturn;

		OpcUa_UInt32				m_nAsyncCount;
		UaUInt32Array				m_arrCallbackHandles;
		PVariableHandleUaNodeArray  m_arrUaVariableHandles;

	private:
		Session*					m_pSession;
	};

	/** @brief The class contains a set of pdo object to send
	*
	*/
	class IOTransactionContextPDO: public IOTransactionContextCanOpen
	{
	public:
		IOTransactionContextPDO(): IOTransactionContextCanOpen()
		{
			m_iPDONumber.clear();
		}
		OpcUa_Boolean isNewCanPDOObject(CanPDOObject *cPDO)
		{
			if (m_iPDONumber.size()) {
				for(OpcUa_UInt32 t = 0; t < m_iPDONumber.size(); t++)
				{
					if (m_arrCanPDOObject[t] == cPDO) return false;
				}
			}
			return true;
		}
		PCanPDOObjectArray	m_arrCanPDOObject;
		std::vector<OpcUa_UInt>		m_iPDONumber;
	};

	class IOTransactionContextSDO: public IOTransactionContextCanOpen
	{
	public:
		IOTransactionContextSDO(): IOTransactionContextCanOpen()
		{
			m_pIOManager   = NULL;
		}

		~IOTransactionContextSDO()
		{
			m_arrWriteValues.clear();
		}
		PDataValueArray		m_arrWriteValues;
		UaUInt32Array       m_arrSamplingHandles;
	};

}
#endif 
