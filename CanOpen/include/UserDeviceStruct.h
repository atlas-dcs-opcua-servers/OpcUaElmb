#ifndef __USERDATASTRUCT_H__
#define __USERDATASTRUCT_H__

#include "userdatabase.h"
#include "uabasenodes.h"
#include "uadatavalue.h"
#include <xsd/cxx/tree/types.hxx>
#include "boost/shared_ptr.hpp"
#include <iostream>
#include <memory>

namespace UserDevice
{

	class UserDeviceStruct;

	/** @brief UserDeviceStruct class is a generic representing the basic property of the user devices
	 * User device classes should base on this class<br>
	 * Instants of this class contain the link to the parent object (is exist), link to UaNode and function code describing the possible operation
	 */

	class pUserDeviceStruct : public UserDataBase
	{
	public:
		std::shared_ptr<UserDeviceStruct> pPoint;

		pUserDeviceStruct(UserDeviceStruct *p)
		{
//			pPoint = std::make_shared<UserDeviceStruct>(p);
			pPoint = std::shared_ptr<UserDeviceStruct>(p);
		}

		pUserDeviceStruct(const pUserDeviceStruct &a) 
		{ 
			pPoint = a.pPoint;
		}
		pUserDeviceStruct &operator=(const pUserDeviceStruct a) 
		{ 
			pPoint = a.pPoint;
		}
		pUserDeviceStruct() = default;
		UserDeviceStruct * getDevice() { return pPoint.get();  }

	};

	class UserDeviceStruct
	{
		UA_DISABLE_COPY(UserDeviceStruct);

	public:
		/**
		* constructor
		* @param parent the parent object
		* @param conf XML description of user device
		* @param pAS pointer to UaNode to exchange data
		* @param code function code
		*/
		UserDeviceStruct(pUserDeviceStruct *parent , ::xsd::cxx::tree::type *conf, UaNode *pAS, OpcUa_UInt code)
		{
			if (parent)
				m_pParent = new pUserDeviceStruct(*parent);
			else m_pParent = 0;

			OpcUa_ReferenceParameter(conf);
			p_mAddressSpaceEntry = pAS;
			m_iCodeFunction = code;
			if (parent) {
				m_pSharedMutex = parent->pPoint->getShareMutex();
				m_pSharedMutex->addReference();
			}
			else {
				m_pSharedMutex = new UaMutexRefCounted();
			}

		}
		//! lock shared mutex to protect 
		void lock() {
			m_pSharedMutex->lock();
		}

		//! unlock shared mutex
		void unlock() {
			m_pSharedMutex->unlock();
		}

		//! get mutex
		::UaMutexRefCounted* getShareMutex() { return  m_pSharedMutex; }

		void releaseMutex()
		{
			m_pSharedMutex->releaseReference();
			if (m_pSharedMutex->referenceCount() == 0)
			{
				m_pSharedMutex = NULL;
			}
		}
		OpcUa_Int32 countMutex()
		{
			return m_pSharedMutex->referenceCount();
		}



		virtual ~UserDeviceStruct(void)
		{
			if (m_pSharedMutex)
			{
				// Release our local reference
				m_pSharedMutex->releaseReference();
				m_pSharedMutex = NULL;
			}
//			delete m_pParent;

		};

		OpcUa_UInt getCodeFunction() { return m_iCodeFunction; }


		UserDeviceStruct * getParentDevice() {
			return m_pParent->pPoint.get();
		}


		pUserDeviceStruct * getTopInterface()
		{
			pUserDeviceStruct *top = m_pParent;
			while (top->pPoint->getParent())
			{

				top = top->pPoint->getParent();
			}
			return top;
		}

		pUserDeviceStruct * getParent() {
			return m_pParent;
		}

		virtual void setInvalid()
		{
			std::cout << "Delete Variable " << p_mAddressSpaceEntry->displayName(0).toString().toUtf8() << std::endl;
		}


		UaNode *getAddressSpaceEntry() { return p_mAddressSpaceEntry;  }

	private:
		UaNode *p_mAddressSpaceEntry;		// UaNode to pass the date to client or device
		OpcUa_UInt m_iCodeFunction;			// code of function
		pUserDeviceStruct *m_pParent;		// parent device
	protected:

		UaMutexRefCounted* m_pSharedMutex;	//!  shared mutex should use to protect operation on device

	};

}
#endif
