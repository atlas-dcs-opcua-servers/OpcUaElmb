#ifndef __EMERGENCY_H__
#define __EMERGENCY_H__

#include "uavariant.h"
#include "CanObject.h"
#include "CANOpenServerConfig.h"
#include "CANopen.h"
#include "CanBusAccess.h"
#include "UaCanTrace.h"

namespace CanOpen
{

	/** @brief This class ensure Emergency data 
	*/
	class Emergency :
		public CanObject
	{
	public:

		/** @brief Constructor
		* @param interf the parent interface in this case is a CanBUsObject
		* @param ca XML description of node properties
		* @param blink the UaNode which control this device
		* @param code function code of operation
		*/
		Emergency(pUserDeviceStruct *interf, NODE *ca, UaNode *blink, OpcUa_UInt32 code) :
			CanObject(interf, ca, blink, code) {};

		/** Destructor */
		virtual ~Emergency(void) {};

		/** @brief Read data From device
		* There is no any date for this object
		* @param code function code of operation
		*/
		virtual UaStatus getDeviceMessage(OpcUa_UInt32 code)
		{
			OpcUa_ReferenceParameter(code);
			return OpcUa_Good;
		}
		virtual UaStatus sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value = 0)
		{
			OpcUa_ReferenceParameter(code);
			OpcUa_ReferenceParameter(value);
			return OpcUa_Good;
		}


		/** @brief connect function code to ua node to pass data
		* @param code function code defines the operation
		* @param conf configuration data to link node
		* @param blink ua node to pass data
		*/
		virtual UaStatus connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *blink)
		{
			CanObject * cno = static_cast<CanObject *>(getParentDevice());
			UaStatus ret = cno->connectCode(code, conf, blink);
			return ret;
		}
		virtual UaVariant getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf)
		{
			CanObject * cno = static_cast<CanObject *>(getParentDevice());
			return cno->getPropertyValue(code,conf );
		}

	};
}
#endif
