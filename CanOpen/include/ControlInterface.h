#ifndef __CONTROLINTERFACE_H__
#define __CONTROLINTERFACE_H__

#include "AbstractHardware.h"
#include "UserDeviceStruct.h"
#include "nodemanagernodesetxml.h"
#include <string>

using namespace std;

namespace AddressSpace
{
	class NmBuildingAutomation;

}

namespace UserDevice {
/**
* @namespace UserDevice
* @brief Generic interface for user devices
* 
* SDK toolkit uses the property of UserDataBase * type to save the pointer to user defined hardware object <br>
* Class ControlInterface allows to initialize the hardware part and create the pointer to user object<br>
* All user objects must inherit the InterfaceUserDataBase class. <br>
* A pointer to UserDataBase interface class is used SDK to connect user classes to UaVariable and UaObject <br>
* UserDataBase_ptr is a smart pointer which allows to share user class between several UaNode in safe mode and <br>
* In this case it is possible to create UaNode in runtime and connect to an object of user class already created.<br>
* AbstractHardware, DEVICE, UserDrviceStruct and UserDeviceItem are the service class to help create user classes to represent hardware structure.
*/
	
	class AbstractHardware;

/** @brief ControlInterface is an interface class to connect OpcUa Address Space to CANOpen hardware
* During creation of Address space the function "createInterfaceEntry" is called. <br>
* The file "BuildingAutomationTypeIds.h" contains all possible operation codes <br>
* This class is an abstract and it is necessary to implement all abstract methods for concrete hardware structure 
*/
	class ControlInterface
	{
	public:

		/// Dummy Constructor
		ControlInterface() {};

		/// Destructor
		virtual ~ControlInterface() {};

		/**
		* This function should be used to initialize user object structure
		*/
		virtual UaStatus CreateXmlParser(UaString &sXmlFileName)=0;			// initialize interface and devices

		virtual bool isTraceVariable(UaNodeId uaTraceId)
		{ 
			OpcUa_ReferenceParameter(uaTraceId);
			return OpcUa_False;
		}

		virtual bool isDeviceVariable(UaNodeId) = 0;

		virtual void putTraceFunctions(OpcUa_DataValue *) = 0;
		
		virtual UaStatus initialize()=0;			// initialize interface and devices
		virtual void	closeInterface()=0;

		/**
		* @brief Create application address space
		* @param nManager Node manager for application nodes
		*/
		virtual UaStatus CreateAddressSpace(AddressSpace::NmBuildingAutomation *nManager, NodeManagerNodeSetXml* pXmlNodeManager, NodeManagerConfig *pDefaultNodeManager)=0;

		virtual IOManager* getIOManager(UaNode* pUaNode,  OpcUa_Int32 attributeId) = 0;

		virtual VariableHandle* getVariableHandler(Session* session, VariableHandle::ServiceType serviceType, OpcUa_NodeId *nodeid, OpcUa_Int32 attributeId)
		{
			OpcUa_ReferenceParameter(session);
			OpcUa_ReferenceParameter(serviceType);
			OpcUa_ReferenceParameter(nodeid);
			OpcUa_ReferenceParameter(attributeId);

			return NULL;
		}

		UaNodeId getTypeNodeId(OpcUa_UInt32 numericIdentifier)
		{
			return UaNodeId(numericIdentifier, m_pTypeNodeManager->getNameSpaceIndex());
		}

		AddressSpace::NmBuildingAutomation *getNodeManager() const { return m_pNodeManager; }
		NodeManagerNodeSetXml *getNodeTypeManager() const  { return m_pTypeNodeManager; }
		/**
		* Create interface class based on configuration information and type ID
		* @param parent - the interface object which is a parent of creating one
		* @param code - operation code
		* @param conf - XML configuration of creating object
		* @param node - UaNode which take date from hardware or send commands
		* @return point to smart pointer of InterfaceUserDataBase
		*/
//		virtual UserDataBase_ptr<InterfaceUserDataBase> *createInterfaceEntry(UserDataBase*parent , int code , ::xsd::cxx::tree::type* conf, UaNode* node)=0;
		virtual pUserDeviceStruct *createInterfaceEntry(pUserDeviceStruct*parent , int code , ::xsd::cxx::tree::type* conf, UaNode* node)=0;

	protected:
		UaString m_sXmlFileName;
		
		AddressSpace::NmBuildingAutomation *m_pNodeManager;
		NodeManagerConfig *m_pDefaultNodeManager;
		NodeManagerNodeSetXml* m_pTypeNodeManager;
	private:


	};
}

#endif