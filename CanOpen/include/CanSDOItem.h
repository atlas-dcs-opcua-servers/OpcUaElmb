#ifndef __CANSDOITEM_H__
#define __CANSDOITEM_H__

#include <opcua_basedatavariabletype.h>
#include "CanBusAccess.h"

#include "CanOpenItem.h"
#include "CanSDOObject.h"

namespace CanOpen
{
	class CanSDOObject;
	/**
	 * The Class defines the basic operation of any item 
	 * which takes data from CAN message and put date into CAN message
	 */
	class CanSDOItem : public CanOpenItem
	{
	public:
		CanSDOItem(pUserDeviceStruct *par,
			SDOITEM_TYPE1 *conf,UaNode *blink,int code);
		CanSDOItem(pUserDeviceStruct *par,
			SDOITEM_TYPE2 *conf,UaNode *blink,int code);
		CanSDOItem(pUserDeviceStruct *par,
			SDOITEM_TYPE3 *conf,UaNode *blink,int code);
		

		virtual UaVariant UnPack();
		virtual UaStatus pack(const OpcUa_Variant *);
		virtual UaStatus write(UaDataValue &udv);
		virtual	UaStatus read(); 

		virtual ~CanSDOItem(void) 
		{
		} ;
		
		virtual UaStatus connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *blink);
		virtual UaVariant getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf);


		OpcUa_Byte getSubIndex()		
		{ 
			return m_subind;
		}

		CanSDOObject* getCanSDOObject() {
			CanSDOObject * in = static_cast<CanSDOObject *>(getParentDevice());
			return	in;
		} 
		OpcUa_UInt32 getTimeout()	{return m_Timeout; }
	protected:
		OpcUa_Byte m_subind;
		OpcUa_UInt32 m_Timeout;
	};
}
#endif
  
