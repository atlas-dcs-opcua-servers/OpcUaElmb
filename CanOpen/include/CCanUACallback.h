#ifndef _CCanUACallback_H_
#define _CCanUACallback_H_

#include "uaobjecttypes.h"
#include "CCanAccess.h"
#include <map>

namespace CanOpen
{
	class CanBusObject;
	class CanNodeObject;
	class CanPDOObject;

	/** The collection of CanPDOObject */
	typedef std::map<OpcUa_UInt32,CanPDOObject * > busPdos;
	/** The collection of CanPDOObject */
	typedef	std::map<OpcUa_UInt16,CanNodeObject * > busNodes;

	/**
	 * @brief This class takes the can massage and distribute it to ua variables.<br>
	 * 
	 */
	class CCanUACallback
	{
	public:
		/** @brief Constructor
		* @param cB pointer to CanBusObject
		*/
		CCanUACallback(CanBusObject *cB);
		virtual ~CCanUACallback(void);

		//! Callback function to distribute can message to servers items
		virtual void FireOnChange(const CanMsgStruct &);				
		//! Callback function to send error message to UaVariable PortError and Description
		virtual void FireOnError(const int, const char * , timeval &);

		//! Add pdo message which can take from can bus
		void addPDO(CanPDOObject *pdoObject);
		//! Add Can Node existing on the bus
		void addCanNode(CanNodeObject *cno);
		/** @return The list of node */
		busNodes& getListNodes() { return  NODEs; }
		
		void ConnectCallback(); //!< Function is to connect callback function to signals
//		void initialize();   //!< to initialize the device on the bus

		void sendInitNodeNmt();  //!< Send initial nmt message
		void waitAllData();		//!< Wait when all asking messages coming
		void waitRTRData();    //!< Wait answer on rtr message

		void executeRTR();

	private:
		CanBusObject *cBus; //!< CanBus connecting with this callback 

		busPdos PDOs;	//!< set of PDOs on the bus
		busNodes NODEs;  //!< set of nodes on the bus
	};
}
#endif
