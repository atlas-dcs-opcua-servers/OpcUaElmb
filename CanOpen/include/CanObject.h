#ifndef __CANOBJECT_H__
#define __CANOBJECT_H__

//#include "BuildingAutomationTypeIds.h"
#include "Ids.h"
#include "uamutex.h"
#include "InterfaceUserDataBase.h"
#include "CanBusAccess.h"

#include "CANopen.h"
#include "iomanager.h"


using namespace UserDevice;
using namespace CanModule;

namespace CanOpen
{
	/** @brief This class collects the basic properties and function of the can bus and can node.
	 * The class defines several function to send nmt messages to can node.
	 * Each node has unique identifier node id. To send nmt message to all nodes on the bus node id = 0 is used.
	 */
	class CanObject : public InterfaceUserDataBase
	{
		UA_DISABLE_COPY(CanObject);

	public:
		/** Constructor
		* @param parent pointer to parent object usually null for CanBusObject
		* @param conf XML configuration entry
		* @param pAS pointer to Ua Node
		* @param code function code of can object
		*/
		CanObject(pUserDeviceStruct *parent,::xsd::cxx::tree::type *conf,UaNode *pAS,OpcUa_UInt32 code):
		  InterfaceUserDataBase(parent,conf,pAS,code)
		{
			m_CanNodeId = 0;
			m_pCommIf = 0;
			m_pIOManager = 0;
		}

		/** Destructor */
		virtual ~CanObject(void)
		{
			if (m_pIOManager) {
				delete m_pIOManager;
				m_pIOManager = 0;
			}
		}
		
		/** get Node Id 
		*@return node id
		*/
		OpcUa_Byte getCanNodeId() { 
			return m_CanNodeId;
		}
		 
		/** Send nmt message 
		* @param nmt can be   (ignore, start, stop,preOperational, reset)
		*/
		void sendNMT(enum ::NMTcommand nmt)
		{
			unsigned char mes[8];
			mes[1] = getCanNodeId();
			mes[0] = nmt;
			if (m_pCommIf) m_pCommIf->sendMessage(0,2,mes);
		}

		/** Save initial nmt message which will send to initialize devices
		* @param iNMT can be   (ignore, start, stop,preOperational, reset)
		*/
		virtual void setInitNmt( enum NMTcommand iNMT)
		{	
			m_initNMTcommand  = iNMT;
		}

		/** Save initial nmt message which will send to initialize devices
		* @param sNmt must be  string ( "start", stop","preOperational", "reset" or empty) 
		*/
		virtual void setInitNmt( std::string sNmt) 
		{
			if (sNmt.empty()) { setInitNmt(nop); return; }
			if (sNmt == "start") { setInitNmt(start); return; }
			if (sNmt == "stop") { setInitNmt(stop); return; }
			if (sNmt == "reset") { setInitNmt(reset); return; }
			if (sNmt == "preOperational") { setInitNmt(preOperational); return; }
			if (sNmt == "none") { setInitNmt(nop); return; }
		}

		/** send initial nmt message */
		virtual void sendInitNmt() { if (m_initNMTcommand != nop ) sendNMT(m_initNMTcommand); }

//		virtual UaStatus connectCode(OpcUa_UInt32 code,UaNode *blink) = 0;

		/** get pointer to can bus interface 
		* @return pointer to can bus interface
		*/
		CCanAccess *getCanBusInterface() { return m_pCommIf; } 	
		
		/** save pointer to can bus interface
		* @param ccac pointer to can bus interface
		*/
		void setCanBusInterface(CCanAccess * ccac) { m_pCommIf = ccac; } 
		
		IOManager * getIOManager() { return  m_pIOManager; }

	protected:

		CCanAccess	*m_pCommIf;			//!< Address of can interface

		OpcUa_Byte	m_CanNodeId;	//!< Node Id for bus = 0
		
		enum NMTcommand         m_initNMTcommand;	//!< initial NMT commands
		IOManager	*m_pIOManager;  //! sdo operation manager
	};
}
#endif
