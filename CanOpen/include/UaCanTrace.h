/******************************************************************************
** uacantrace.h
**
** ** Project: C++ OPC SDK base module
**
** Class provides trace functionality.
**
******************************************************************************/
#ifndef UACANTRACE_H
#define UACANTRACE_H

#include "NmBuildingAutomation.h"
#include <iomanip>
#include "uatrace.h"
#include "uavariant.h"
#include "opcua_basevariabletype.h"
#include "uaplatformdefs.h"
#include "LogIt.h"
#include <list>
#include "CCanAccess.h"
#include "CanTraceFunction.h"

namespace CanOpen
{
	/** 
	*  A class used for trace outputs (PDO, SDO,...).
	*/
	
	
	class UaCanTrace : UaTrace, public OpcUa::BaseVariableType
	{
	public:
		/// @brief Definition of the TraceFunction Enumeration

		//enum CanTraceFunction {
		//	NoTrace = 0,			/*!< Trace_Function: No Trace         */
		//	PdoMessage = 1,			/*!< Trace_Function: PdoMessages      */
		//	SdoMessage = 2,			/*!< Trace_Function: SdoMessages      */
		//	SegSdoMessage = 4,		/*!< Trace_Function: SegSdoMessages   */
		//	NGMessage = 8,			/*!< Trace_Function: NG Messages    */
		//	EmergMessage = 16,		/*!< Trace_Function: Emergency Messages     */
		//	NMTMessage = 32,		/*!< Trace_Function: MNT Messages     */
		//	allMessage = 64
		//};
	
		UaCanTrace(
			const UaNodeId&    nodeId,
			const UaString&    name,
			OpcUa_UInt16       browseNameNameSpaceIndex,
			const UaVariant&   initialValue,
			OpcUa_Byte         accessLevel,
			NodeManagerConfig* pNodeConfig
			);
		virtual ~UaCanTrace();

		struct pdomes
		{
			CanMsgStruct c;
			pdomes(const CanMsgStruct& cms) : c(cms) {}
		};

		struct sdomes
		{
			UaByteArray buf;
			sdomes(const UaByteArray& cms) : buf(cms) {}
		};
		
//		virtual UaStatus setValue(Session *pSession, const UaDataValue &dataValue, OpcUa_Boolean checkAccessLevel = OpcUa_True);
		
		friend inline std::ostream& operator<<(std::ostream& o, const pdomes& ps)
		{
			int len = (int)ps.c.c_dlc;
			o << std::hex << "cobId=" << ps.c.c_id << " length=" << len << " data=";
			for (int i = 0; i < len; i++)
				{ o << " " << std::hex << std::setfill('0') << std::setw(2) << (int)(ps.c.c_data[i]); };
			return(o);
		}

		friend inline std::ostream& operator<<(std::ostream& o, const sdomes& ps)
		{
			o << " data=";
			for (int i = 4; i < 8; i++)
			{
				o << " " << std::setfill('0') << std::setw(2) << std::hex << (int)(ps.buf.data()[i]);
			};
			return(o);
		}

		static void putTraceFunctions(OpcUa_UInt32);

		static void initTraceComponent(CanOpenOpcServerConfig::StandardMetaData_optional Log_optional);
		static std::map<int,Log::LogComponentHandle> m_CanTraceComponent;
		static std::map<string,int > m_logLevel;

	private:

	};
}
#endif // UACANTRACE_H
