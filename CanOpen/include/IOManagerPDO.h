#ifndef __IOMANAGERPDO_H__
#define __IOMANAGERPDO_H__

#include "iomanager.h"

namespace CanOpen
{
	/** @brief The pdo operation manager class is to send pdo.
	* @details
	* This class ensure mapping data from PDO items, save and 
	* send PDO message on the bus.<br>
	* All input/output managers in Ua SDK toolkit have a standard interface
	* which have execute reading , writing and monitoring data from device.<br>
	* This manager have to ensure only write operation on the can bus.
	*/
	class IOManagerPDO : 
		public IOManager
	{
		UA_DISABLE_COPY(IOManagerPDO);
	public:
		/* construction / destruction */
		IOManagerPDO();
		virtual ~IOManagerPDO();

		//- Interface IOManager ------------------------------------------------------------------
		UaStatus beginTransaction(IOManagerCallback* pCallback, const ServiceContext& serviceContext, OpcUa_UInt32 hTransaction, OpcUa_UInt32 totalItemCountHint,
			OpcUa_Double maxAge, OpcUa_TimestampsToReturn timestampsToReturn, TransactionType transactionType, OpcUa_Handle& hIOManagerContext);

		//! dummy function. monitoring operations execute by standard IO manager
		UaStatus beginStartMonitoring(OpcUa_Handle hIOManagerContext, OpcUa_UInt32 callbackHandle, IOVariableCallback* pIOVariableCallback, 
			VariableHandle* pVariableHandle, MonitoringContext& monitoringContext);
		//! dummy function.  monitoring operations execute by standard IO manager
		UaStatus beginModifyMonitoring(OpcUa_Handle hIOManagerContext, OpcUa_UInt32 callbackHandle, OpcUa_UInt32 hIOVariable, MonitoringContext& monitoringContext);
		UaStatus beginStopMonitoring(OpcUa_Handle hIOManagerContext, OpcUa_UInt32 callbackHandle, OpcUa_UInt32 hIOVariable);

		//! dummy function. read operation executes by standard IO manager
		UaStatus beginRead(OpcUa_Handle hIOManagerContext, OpcUa_UInt32 callbackHandle, VariableHandle* pVariableHandle, OpcUa_ReadValueId* pReadValueId);

		UaStatus beginWrite(OpcUa_Handle hIOManagerContext, OpcUa_UInt32 callbackHandle, VariableHandle* pVariableHandle, OpcUa_WriteValue* pWriteValue);
		UaStatus finishTransaction(OpcUa_Handle        hIOManagerContext);
		//- Interface NodeManager ------------------------------------------------------------------

	private:
//		UaMutex       m_mutex;
	
	};
}
#endif // __IOMANAGERPDO_H__
