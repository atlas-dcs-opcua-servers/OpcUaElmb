#ifndef __CANNODEOBJECT_H__
#define __CANNODEOBJECT_H__

#include "uavariant.h"
#include "CanObject.h"
#include <opcua_basedatavariabletype.h>
#include "CANOpenServerConfig.h"
#include "CANopen.h"
#include "CanBusAccess.h"
#include "UaCanTrace.h"
#include <map>
#include "IOManagerSDO.h"

namespace CanOpen
{
	class CanSDOObject;

	/** @brief This class ensure the communication with CanOpen node 
	* @details
	* Can Node has unique node id.<br> CanOpen protocol defines several can message which cobid calculates based on nodeid.<br>
	* These are emergency messages, nodeguarding and bootup messages<br>
	* This object has links to UaVariable which the data are passed to <br>
	* Also this object ensure the synchronization of sdo operation. Should be one operation in one moment.<br>
	* Each can node has a state which is taken from NG or HB message.<br>
	* Also CanNodeObject ensue to recover after rebooting or disconnection of node in proper state 
	*/
	class CanNodeObject:
		public CanObject
	{
	public:

		/** @brief Constructor
		* @param interf the parent interface in this case is a CanBUsObject
		* @param ca XML description of node properties
		* @param blink the UaNode which control this device
		* @param code function code of operation
		*/
		CanNodeObject(pUserDeviceStruct *interf,NODE *ca,UaNode *blink,OpcUa_UInt32 code);

		/** Destructor */
		virtual ~CanNodeObject(void);

		/** @brief Read data From device 
		* There is no any date for this object
		* @param code function code of operation
		*/
		virtual UaStatus getDeviceMessage(OpcUa_UInt32 code)
		{ 
			OpcUa_ReferenceParameter(code);
			return OpcUa_Good;
		} 
		virtual UaStatus sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value = 0);

		void passSDO( const CanMsgStruct *cms);							//! send SDO message
		virtual void setState(const CanMsgStruct *cms) ;				//! set value of nmt and bootup message
		virtual void setEmergencyEvent(const CanMsgStruct *cms);		//! set Emergency event
		void setRequestSDO(CanSDOObject *msdo)							//! lock SDO operation
		{
			m_mutexReqSDO.lock();
			m_requestedSDO = msdo;
		}

		void freeRequestSDO()								//! unlock SDo operation
		{
			m_requestedSDO = 0;
			m_mutexReqSDO.unlock();
		}

		/** @brief connect function code to ua node to pass data 
		* @param code function code defines the operation
		* @param conf configuration data to link node
		* @param blink ua node to pass data
		*/
		virtual UaStatus connectCode(OpcUa_UInt32 code,::xsd::cxx::tree::type *conf,UaNode *blink);
		virtual UaVariant getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf);

		//! send NG message for this node
		virtual void sendNG();			
		
		/**  @brief Handle the ng message
		* Set state or recover after bootup message
		* @param st state byte from ng or boot up message
		*/
		void NGComing(OpcUa_Byte st)
		{
			if (st == 0) {
				setLastState();
				return;
			}

			if (m_currentState == NMT_DISCONNECTED) {
				setLastState();
			}

			m_repeatedCounter = ngCounter;
			m_currentState = st;
		}
		
		void setInitLive() {
			m_currentState = 0;
			m_lastState = 0;
			m_repeatedCounter = ngCounter;
		}

		void decNGCounter()			//! counter of missing NG message ( 0 means node disconnected )
		{
			if (isAlive())
			{
			    m_repeatedCounter--;
				if (m_repeatedCounter == 0)
				{
					setDisconnectedState();
				}
			}
		}
		
		static OpcUa_Int16   ngCounter;
		static OpcUa_Boolean DisconectRecovery;

		OpcUa_Boolean isAlive() 
		{ 
			return !(m_currentState == NMT_DISCONNECTED);
		}


	private:
		UaMutex	m_mutexReqSDO;   //! sdo mutex to protect sdo operation . should one on one moment
		CanSDOObject *m_requestedSDO;  //! current sdo object executing sdo operation
		void setDisconnectedState(); //! set disconnection state 1 for node
		void setStateRecursevely(UaNode *p,bool state);

		void setLastState();		//! set the last state after disconnection


		OpcUa_Byte m_currentState;
		OpcUa_Byte m_lastState;
		OpcUa_Int16 m_repeatedCounter;

		OpcUa::BaseVariableType *m_state;
		OpcUa::BaseVariableType *m_iBootupCounter;
		OpcUa::BaseVariableType *m_EmergencyErrorCode;
		OpcUa::BaseVariableType *m_EmergencyCounter;
		OpcUa::BaseVariableType *m_Error;
		OpcUa::BaseVariableType *m_SpecificErrorCodeByte1;
		OpcUa::BaseVariableType *m_SpecificErrorCodeByte2;
		OpcUa::BaseVariableType *m_SpecificErrorCodeByte3;
		OpcUa::BaseVariableType *m_SpecificErrorCodeByte4;
		OpcUa::BaseVariableType *m_SpecificErrorCodeByte5;
		list<OpcUa::BaseVariableType *> m_programList;
	};
}
#endif
