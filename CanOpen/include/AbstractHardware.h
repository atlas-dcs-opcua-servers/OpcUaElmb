#ifndef __ABSRACTHARDWARE_H__
#define __ABSRACTHARDWARE_H__

#include "InterfaceUserDataBase.h"

namespace UserDevice {

	/** @brief Basic abstract class to create hardware structure.
	* @details
	*	AbstracHardware class has only one abstract function
	*	to create an object representing hardware part and return the pointer on it.
	*/
	class AbstractHardware
	{ 
	public:
		AbstractHardware() {}; 

		/** @brief Create an object representing part of the hardware.
		* @details
		* This function ensure a link between OpcUa Address space node (object or variable) 
		* and hardware object to execute the command , take data, write data
		* @param parent the object representing the parent part of hardware
		* @param conf XML object containing the configuration information for this object
		* @param blink OpcUa node using information from this user object
		* @param code - function code of operation
		* @return the pointer on created object (null means an error)
		**/
		virtual InterfaceUserDataBase *createHardwareEntry(pUserDeviceStruct *parent, ::xsd::cxx::tree::type *conf,UaNode *blink,int code) = 0;
	};

	/** @brief DEVICE class is a template to create an object to control hardware device.
	* @tparam T is a parameter means the class of created object
	* @tparam P is a parameter represents of the type XML information
	* The DEVICE class defines the abstract function inheriting from AbstarctHardware class.
	* DEVICE class is to create an abject which take care about type of hardware.
	*/
	template<class T,class P>
	class DEVICE : public AbstractHardware {
	public : 
		DEVICE() {}; 
		~DEVICE() {}; 

		/** The function create an object of type T
		* @param parent the object representing the parent part of hardware
		* @param conf XML object containing the configuration information for this object
		* @param blink OpcUa node using information from this user object
		* @param code - function code of operation
		* @return the pointer of created object (null means an error)
		*/
		InterfaceUserDataBase *
			createHardwareEntry(pUserDeviceStruct* parent, ::xsd::cxx::tree::type *conf,UaNode *blink,int code)
		{ 
			T *t = new T(parent,(P *)conf,blink,code);
			return (InterfaceUserDataBase *)t;
		} 
	};
}
#endif