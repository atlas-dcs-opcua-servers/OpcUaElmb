#ifndef _SAMPLINGSDO_H
#define _SAMPLINGSDO_H

#include "samplingengine.h"
#include "iomanager.h"
#include "UaControlDeviceItem.h"


namespace AddressSpace
{
	/***
	 * SamplingSDo class uses thread manager from SamplingEngine class
	 * Sampling rate defines in server configuration class
	 */
	class SamplingSDO : public SamplingExecution
	{
	public:
		SamplingSDO();
		~SamplingSDO() 	{	};
		virtual void sample();
		UaControlDeviceItem *	m_pUaCanSDOItem;
		OpcUa_Int32		m_nSamplingInterval;
		IOVariableCallback* m_pIOVariableCallback;
	};
}
#endif