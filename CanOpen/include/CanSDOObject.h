#ifndef __CANSDOOBJECT_H__
#define __CANSDOOBJECT_H__
#pragma once;
#include "uaobjecttypes.h"
#include "CanOpenObject.h"
#include "uasemaphore.h"
#include "CanOpenData.h"

namespace CanOpen
{
	class CanSDOItem;
	class CanNodeObject;

	/**
	*	class CanSDOObject ensures the SDO operation on the bus
	*/
	class CanSDOObject:
		public CanOpenObject
	{
	public:
		CanSDOObject(pUserDeviceStruct *par,
			SDO *conf,UaNode *blink,int code);
/*
		CanSDOObject(InterfaceUserDataBase *par,
			SDOITEM_TYPE2 *conf,UaNode *blink,int code);
			*/
		virtual ~CanSDOObject(void);

		virtual UaStatus getDeviceMessage(OpcUa_UInt32 code) ; 
		virtual UaStatus sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value = 0) ;
		virtual void pass(const CanMsgStruct *);

		/** @brief connect function code to ua node to pass data 
		* @param code function code defines the operation
		* @param conf XML object containing the configuration information for this object
		* @param blink ua node to pass data
		*/
		virtual UaStatus connectCode(OpcUa_UInt32 code,::xsd::cxx::tree::type *conf,UaNode *blink);
		virtual UaVariant getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf);

		virtual bool isMsgForObject(const CanMsgStruct *);
		
		virtual UaStatus readSdo(UaDataValue&); 
		virtual UaStatus writeSdo(UaDataValue &udv);
		OpcUa_BuiltInType getItemType();
		UaDateTime& getLastTimeStamp() { return m_udt; }
		OpcUa_Byte getSubIndex();
		OpcUa_UInt16 getIndex() { return m_ind; }
		void setIndex(OpcUa_UInt16 ind) { m_ind=ind; }
		CanSDOItem *getSDOItem() { return m_pSDOItem; }
		OpcUa_UInt32 m_iLength;
		UaByteArray m_segSdoBuffer;
		void setSDOItem(CanSDOItem *csdo) ;
		void freeSDOItem() ;
		void setTimeout(OpcUa_UInt32 t) { m_iTimeout =  t; }

	private:
		CanSDOItem * m_pSDOItem;
		OpcUa_UInt16 m_ind;
	};
}
#endif
