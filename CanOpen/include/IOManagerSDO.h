#ifndef __IOMANAGERSDO_H__
#define __IOMANAGERSDO_H__

#include "iomanager.h"
#include "uathreadpool.h"
#include "samplingengine.h"
#include "IOTransactionContextCanOpen.h"
#include "UaCanNodeObject.h"
#include "SamplingSDO.h"

namespace CanOpen
{
	/** @brief IOManagerSDO is a manger ro send and receive SDO massage.
	* @details
	* To develop IOManager in Ua Opc toolkit there is an specific interface.<br>
	* It has several abstract function which must be redefine to ensure needed functionality. <br>
	* in order to execute sdo operation in parallel each node has this manager in OpcUaCanOpenServer. <br>
	*/

	class IOManagerSDO : public IOManager
	{
		UA_DISABLE_COPY(IOManagerSDO);
	public:

		/** construction
		* @param pCanNode is a pointer to the UaCanNodeObjec which the manager is belonged to
		*/
		IOManagerSDO(UaCanNodeObject *pCanNode);
		
		/** destruction */
		virtual ~IOManagerSDO();

		//- Interface IOManager ------------------------------------------------------------------
		UaStatus beginTransaction(IOManagerCallback* pCallback, const ServiceContext& serviceContext, OpcUa_UInt32 hTransaction, OpcUa_UInt32 totalItemCountHint,
			OpcUa_Double maxAge, OpcUa_TimestampsToReturn timestampsToReturn, TransactionType transactionType, OpcUa_Handle& hIOManagerContext);
		//! dummy function. it does not use in this manager. monitoring operations execute by standard IO manager
		UaStatus beginStartMonitoring(OpcUa_Handle hIOManagerContext, OpcUa_UInt32 callbackHandle, IOVariableCallback* pIOVariableCallback, 
			VariableHandle* pVariableHandle, MonitoringContext& monitoringContext);
		UaStatus beginModifyMonitoring(OpcUa_Handle hIOManagerContext, OpcUa_UInt32 callbackHandle, OpcUa_UInt32 hIOVariable, MonitoringContext& monitoringContext);
		UaStatus beginStopMonitoring(OpcUa_Handle hIOManagerContext, OpcUa_UInt32 callbackHandle, OpcUa_UInt32 hIOVariable);
		UaStatus beginRead(OpcUa_Handle hIOManagerContext, OpcUa_UInt32 callbackHandle, VariableHandle* pVariableHandle, OpcUa_ReadValueId* pReadValueId);
		UaStatus beginWrite(OpcUa_Handle hIOManagerContext, OpcUa_UInt32 callbackHandle, VariableHandle* pVariableHandle, OpcUa_WriteValue* pWriteValue);
		UaStatus finishTransaction(OpcUa_Handle        hIOManagerContext);
		//- Interface NodeManager ------------------------------------------------------------------

		/** Set the sampling rates provided by the OPC server. 
		*/
		UaStatus setAvailableSamplingRates(const UaUInt32Array& availableSamplingRates);

		void executeRead(AddressSpace::IOTransactionContextSDO* pTransactionContext);
		void executeWrite(AddressSpace::IOTransactionContextSDO* pTransactionContext);
		void executeMonitorBegin(AddressSpace::IOTransactionContextSDO* pTransactionContext);
		void executeMonitorStop(AddressSpace::IOTransactionContextSDO* pTransactionContext);
		void executeMonitorModify(AddressSpace::IOTransactionContextSDO* pTransactionContext);
		const OpcUa_UInt32 def_sample = 300;
//		UaMutex	m_mutexSDO;   //! sdo mutex to protect sdo operation . should one on one moment
	private:
		UaUInt32Array sdoSamplingRates;
		
		UaCanNodeObject			*m_pCanNode;

		UaMutex                 m_mutex;

		UaThreadPool			*m_pThreadPool;
		SamplingEngine			*m_pSamplingEngine;

		HandleManager<AddressSpace::SamplingSDO>	m_handlesSampleItems;

	};			

	class IOManagerSDOJob: public UaThreadPoolJob
	{
		UA_DISABLE_COPY(IOManagerSDOJob);
	public:
		IOManagerSDOJob(AddressSpace::IOTransactionContextSDO* pTransactionContext)
		{
			m_pTransactionContext = pTransactionContext;
		};

		virtual ~IOManagerSDOJob()
		{
			if(m_pTransactionContext)
			{
				delete m_pTransactionContext;
			}
		};

		void execute();

	private:
		AddressSpace::IOTransactionContextSDO* m_pTransactionContext;
	};

}
#endif // __IOMANAGERSDO_H__
