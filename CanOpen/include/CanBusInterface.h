#ifndef __CANBUSINTERFACE_H__
#define __CANBUSINTERFACE_H__

#include "UaCalcItem.h"
#include "CANOpenServerConfig.h"
#include "CanBusAccess.h"
#include "CanBusObject.h"
#include "ControlInterface.h"
#include "UaCanBusObject.h"
#include "UaDeviceMethod.h"

#include "NmBuildingAutomation.h"
#include "UaCanTrace.h"

#include <list>
#include <map>
#include <vector>
#include <string>

/**
* @mainpage OpcUa CanOpenServer
* @section intro_sec Introduction
* OpcUaCanOpenServer is a program interface between CanOpen devices and any SCADA system
* having OpcUa client. <br> 
* The server has been developed by using Unified Automation (UA) C++ toolkit. <br>
* The server creates the address space and hardware connection objects based on XML configuration file. <br>
* XSD technology is used to check syntax of XML configuration file. <br>
* @image html main.jpg
*
* @section instal_sec Installation
* The installation rpm package is used to install all needed files
* @section usage_sec Usage
* The general form to start server is <br>
* <b>./OpcUaCanOpenServer "configuration file name".xml.</b><br>
* If the XML file name is omit the default configuration file name is used (OPCUACANopenServer.xml) <br>
* <b>./OpcUaCanOpenServer -cs</b><br> This form  is used to create the OPC server certificate. The post script of rpm uses this form. <br>
* Also during start up it is possible to point out the log file <br>
* <b>./OpcUaCanOpenServer "configuration file name.xml" -l "log file name".log</b> <br>
* the relative and absolute path can be used<br><br>
* The OpcUaCanOpenServer address space have the "TraceLevel" item. This Item defines what kind of CANOpen message will be store.
*
* In order to start store the following value can be written in this item:
*
* - 0, No Trace
* - 1, Pdo Messages
* - 2, Sdo Messages
* - 4, Segmented Sdo Messages
* - 8, NG Messages
* - 16, Emergency Messages

* This is bit code it mains that in case value = 3 the Pdo And Sdo message will be put in log file

* @section descrip_sec General Concepts
*	- Configuration file
*	- Define all object types in the system
*	- Create UaObject type
*	- Create AddressSpace 
*	- CanOpen object Structure
*	- Connection to hardware interface objects
*	- Interface to Canbus Driver
* @subsection config_sec XML configuration file
* The OpcUaCanOpen server is configured based on the XML file. <br>
* The description of this file  can be found <a href="http://atlas-dcs-opcua.web.cern.ch/atlas-dcs-opcua/OPCUACANopenConfigFile%20_%20DcsHelp%20_%20ATLAS.html">Config File</a> .
* The parser of XML file has been prepared using the "Code Sythis" xsd utility. This utility can create the parser classes based on XSD file
* which can be easily used to create internal object structure to further processing.<br> 
* The name of XSD file for this server is CANOpenServerConfig.xsd.<br>
* 
* @subsection address_sec Create Address Space
* OpcUa toolkit proposes to create the address space by overwriting the function afterStartup of NodeManager class.<br>
* The Address space is a set of UaNode which must have unique node id.
* The creation was done in two steps:
*	- create the set of types of canopen object as UaNode structure. All canopen object has a numeric value which is defined in BuildingAutomationTypesId.h file
*     The node id is created from numeric value of canopen function code. 
*	- create the set of UaNode based on XML configuration file. Each UaVariable and UaObject has a corresponding object from conopen structure.
*	  which is created using createInterfaceEntry(UserDataBase* parent, int code, ::xml_schema::type* conf , UaNode* pAS) function from CanBusInterface class.
*
* There are two generic class:
*	- UaControlDeviceGeneric - using to create set of UaNodes ( object and variable ) based on XML configuration and type information
*	- UaCaontrolDeviceItem - crating of UaVariable and canopen object using createInterfaceEntry
* 
*  @subsection canopen_sec CanOpen object Structure
* There are two types of objects representing the the can bus system
* - CanBusObject and CanNodeOBject inherit property and method from CanObject class. These object is to exchange data with can can system.
* - The objects which execute the canopen protocol inherit properties and methods from CanOpenObject class
*
* All objects have a standard constructor with parameters:
*   - UserDataBase *parent is a link to the top level object. For example for CanPDOItem it is CanPDOObject. This pointer allows to create the structure of can system.
*   - ::xsd::cxx::tree::type *conf is a configuration data for creating object. 
*   - UaNode *pAS is a link to the UaVariable to set data if needed
*
* Also any CanObject must have  connectCode(OpcUa_UInt32 code,UaNode *blink). This function connects a function code with UaVariable if this object have to take care about it.
* @image html canopen.jpg
*<br>
*  @subsection connection_sec Connection to hardware interface objects
* The class CanBusInterface is a broker class which has only on function  createInterfaceEntry(UserDataBase* parent, int code, ::xml_schema::type* conf , UaNode* pAS).<br>
* This function based on function code create the proper canopen class.<br>
* if the broker has no information about which class has to be created it call cannectCode function of parent to take care about this link. 
* The createInterfaceEntr returns the pointer on object which has the standard interface functions:
* - sendDeviceMessage and getDeviceMessage for CanObject. Using this function UaControlDeviseGeneric object can execute command and send/receive data from can bus.
* - read and write for CanOpenObject. These function are used by UaCantrolDeviceItem to send or receive data. 
* Usually read function execute getDeviceMessage from the parent object to get can message and extract value and save it to UaVariable. 
* The write function packs value to can message and send 
*
*<br>
* @subsection canopen__readot_sec CanOpen readout
* <br>
* The main structure of of CanOpen readout system is shown below.<br>
* @image html readout.jpg
* Can interface component is hardware dependent class. The server downloads it during startup depend on hardware using at the system.<br>
* It is possible to use different can cards and driver in one system br<>
* The Can object sends can messages or send rtr message via interface class and receive date from callback class.<br>
*/
extern bool noExit;
namespace CanOpen {

	class UserDevice::AbstractHardware;
	typedef  std::map<string, vector<UaControlVariable *> > mapDataVariable;


/** @brief An implementation of ControlInterface for can bus
* CanBusInterface is an interface class to connect OpcUa Address Space to CANOpen hardware
*/
	class CanBusInterface: public ControlInterface
	{
	public:

		class predCanBus
		{
		public:
			predCanBus(string s)
			{
				nport = s;
			}
			bool operator()(CanBusObject *cbo)
			{
				string bname = cbo->getCanBusAddress();
				int n = bname.find(':');
				int l = bname.length() - (n+1);
				bool b = bname.compare(n+1,l, nport) == 0;
				return b;
			}
		private:
			string nport;
		};

		CanBusInterface();
		virtual ~CanBusInterface();
		virtual void	closeInterface();

		/// This function should be used to initialize hardware dependent part
		virtual UaStatus initialize();			// initialize interface and devices
		
		virtual UaStatus CreateXmlParser(UaString &sXmlFileName/*,UaString &sLogFileName*/);		
		/**
		* Create interface class based on configuration information and type ID
		* @param parent - the interface object which is a parent of creating one
		* @param code - operation code
		* @param conf - xml configuration of creating object
		* @param pAS - UaNode which take date from hardware or send commands
		*/
		virtual pUserDeviceStruct *createInterfaceEntry(pUserDeviceStruct* parent, int code, ::xml_schema::type* conf , UaNode* pAS);

		virtual IOManager* getIOManager(UaNode* pUaNode,  OpcUa_Int32 attributeId);

		virtual VariableHandle* getVariableHandler(Session* session, VariableHandle::ServiceType serviceType, OpcUa_NodeId *nodeid, OpcUa_Int32 attributeId);


//		bool isCanObject(int code) { return (find(m_CanObjects.begin(),m_CanObjects.end(),code) != m_CanObjects.end()); }

//		bool isCanItem(int code) { 
//			return (find(m_CanItems.begin(),m_CanItems.end(),code) != m_CanItems.end());
//		}
//		bool isCanMethod(int code) { return (find(m_CanMethods.begin(),m_CanMethods.end(),code) != m_CanMethods.end()); }

		virtual bool isTraceVariable(UaNodeId uaTraceId)
		{
			return (uaTraceId == UaNodeId("TraceLevel", getNodeManager()->getNameSpaceIndex()));
		}
		virtual void putTraceFunctions(OpcUa_DataValue *mvar)
		{
			OpcUa_Variant vdata;
			vdata	=  mvar->Value;
			UaCanTrace::putTraceFunctions(vdata.Value.Int32);
		}

		virtual bool isDeviceVariable(UaNodeId uaDevVar)
		{
			bool ret = true;
			OpcUa_UInt32 nn = uaDevVar.identifierNumeric();
			if (nn == BA_STARTPROGRAM || nn == BA_CALCITEM)
				ret = false ;
			return ret;
		}

		static void initCalculation();

		static void addCalcCompilerItem(AddressSpace::UaCalcItem *ucci);

		/**
		* Create Address Space
		* @param nManager - pointer to node manager
		*/
//		virtual UaStatus CreateAddressSpace(NmBuildingAutomation *nManager);
		virtual UaStatus CreateAddressSpace(AddressSpace::NmBuildingAutomation *nManager, NodeManagerNodeSetXml* pXmlNodeManager, NodeManagerConfig *pDefaultNodeManager);

		/**  Get UA Server can trace settings.       
	     *  @return                 Error code.
		 */
		virtual UaStatus getCanTraceSettings(
			OpcUa_UInt32&  uMaxTraceEntries,    // Maximum number of trace entries in one trace file
			OpcUa_UInt32&  uMaxBackupFiles,     // Maximum number of backup files
			UaString&      sTraceFile           // Name and path of the trace file
			) 
		{
			uMaxTraceEntries =  m_uCanMaxTraceEntries;
			uMaxBackupFiles =  m_uCanMaxBackupFiles;
			sTraceFile =   m_sLogFileName;
			return OpcUa_Good;
		}

		UaNodeId getCanNodeNodeId(UaNode *pNode);
		
//		UaStatus createTypeNodes();  //! create of ua node types
		UaStatus createNodeTypes();  //! create of cannode types
		UaStatus createCanbuses();   //! create of canbuses

		UaStatus createNodes(CANBUS::NODE_sequence &nd, UaControlDeviceGeneric *pcanbus);

		static UaStatus CompileItems(OpcUa::BaseObjectType *pParent, CanOpenOpcServerConfig::ITEM_sequence &itnd, CanOpenOpcServerConfig::REGEXPR_sequence &regex, NmBuildingAutomation * nManager);

		static UaStatus createListInitmethod(CanOpenOpcServerConfig::atStartup_sequence &ex, UaControlDeviceGeneric *parent, NmBuildingAutomation *nManager);

		static UaStatus createListInitSet(CanOpenOpcServerConfig::SET_sequence &ex, UaControlDeviceGeneric *parent, NmBuildingAutomation *nManager);

		static UaStatus executeInitMethod();
		static UaStatus variableInitSet();
		
		static UaStatus createRegVariable(ITEM &itemvar, CanOpenOpcServerConfig::REGEXPR_sequence &regex, mapDataVariable& pvariables, OpcUa::BaseObjectType *pcn, OpcUa_Int32 &minVar, NmBuildingAutomation *nManager);

		static unique_ptr<CanBusAccess>	g_pCanaccess ;			//! pointer to CAN interface
//		static UaCalcCompiler *g_pCompiler;
		static list<UaDeviceMethod<UaControlDeviceGeneric> *>		m_listInitMethod;
		struct InitSet {
			OpcUa::BaseDataVariableType *setVar;
			UaString value;
		};
		static list<struct InitSet>	m_listInitSet;
		static vector< AddressSpace::UaCalcItem *> m_vCCI;  //! set of formulas
		UaCanTrace *m_Trace;
	private:

		list<CanBusObject *>	m_CanBusList;						// the list of CanBus in the system
		map<OpcUa_UInt32,UserDevice::AbstractHardware *> factory;	// map name of class to provide an object

//		vector<OpcUa_UInt32> m_CanObjects;					// map code functions of objects
//		vector<OpcUa_UInt32> m_CanMethods;					// map code functions of methods
//		vector<OpcUa_UInt32> m_CanItems;					// map code functions of items

		/** XML configuration structure */
		CanOpenOpcServerConfig::NODETYPE_sequence	m_nodetypes;	//! list of nodes
		CanOpenOpcServerConfig::CANBUS_sequence		m_canbuses;		//! list of can-buses
		CanOpenOpcServerConfig::ITEM_sequence		m_items;		//! list of calculated items 
		CanOpenOpcServerConfig::REGEXPR_sequence	m_regexprisson;	//! list of regular expression 
		CanOpenOpcServerConfig::SETTINGS_optional	m_settings;		//! server settings 
		CanOpenOpcServerConfig::StandardMetaData_optional	m_logs;		//! global log settings 
		CanOpenOpcServerConfig::atStartup_sequence	m_execute;		//! server set tings 
		CanOpenOpcServerConfig::SET_sequence	m_setvalue;		//! server set tings 

			UaString	m_sLocale;
		UaString	m_sLogFileName;
		OpcUa_UInt32  m_uCanMaxTraceEntries;    // Maximum number of trace entries in one trace file
		OpcUa_UInt32  m_uCanMaxBackupFiles;     // Maximum number of backup files
	};
}

#endif
