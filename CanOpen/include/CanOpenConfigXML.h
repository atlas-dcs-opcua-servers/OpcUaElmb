/******************************************************************************
** canopenconfigxml.h
**
**
** Project: C++ OPC Server SDK sample code
**
** Description: Configuration Address Space for the CANOpen OPC Ua Server.
**
******************************************************************************/
#ifndef CANOPENCONFIGXML_H
#define CANOPENCONFIGXML_H

#include "xmldocument.h"
#include "opcua_core.h"
#include "opcua_dataitemtype.h"
#include "OpcServer.h"
#include "uaobjecttypes.h"
#include "NmBuildingAutomation.h"
//#include "BuildingAutomationTypeIds.h"

#include "CANOpenServerConfig.h"

/**
* Class to create Address Space
*/

class CANopenConfigXml
{
    UA_DISABLE_COPY(CANopenConfigXml);
public:

//    CANOpenConfigXml(NmBuildingAutomation *ba, const UaString& sXmlFileName, const UaString& sApplicationPath,Canaccess *cana);

/** construction.
* @param pServer an instance of Server class to create 
* @param sXmlFileName the name of the configuration file
*/

	CANopenConfigXml(OpcServer* pServer,UaString &sXmlFileName);
    /** destruction */

	virtual ~CANopenConfigXml();

    /**  Load the configuration from the configure file.
     *  First method called after creation of ServerConfig. Must create all NodeManagers
     *  before method startUp is called.
     *  @return               Error code.
     */
    UaStatus CreateAddressSpace();

    /* Save the configuration to the configure file. */

    inline UaString sXmlFileName() const {return m_sXmlFileName;}

private:
		// parser variable
	CanOpenOpcServerConfig::NODETYPE_sequence vNodeType;
	CanOpenOpcServerConfig::CANBUS_sequence vCanbus;
	CanOpenOpcServerConfig::ITEM_sequence vItem;
	CanOpenOpcServerConfig::REGEXPR_sequence vReg;

	OpcServer *		m_pServer;

    UaString		m_sXmlFileName;	
};


#endif // CANOPENCONFIGXML_H
