#ifndef __NODEGUARDINGINTERVAL_H__
#define __NODEGUARDINGINTERVAL_H__

#include "uabase.h"
#include "uastring.h"
#include "statuscode.h"
#include "uaarraytemplates.h"
#include "uathread.h"
#include "opcua_basedatavariabletype.h"

namespace CanOpen
{
	class CanBusObject;

	class NodeGuardingInterval: public UaThread
	{
		UA_DISABLE_COPY(NodeGuardingInterval);
	public:

		/* construction / destruction */
		NodeGuardingInterval(CanBusObject *pCI, OpcUa::BaseVariableType* pVar = NULL);
		virtual ~NodeGuardingInterval();

		/* Set Controller status and data */
		void setNodeGuardingInterval(OpcUa::BaseVariableType* pVar) { m_iNgIntervalVariable = pVar; }
		void startNodeGuardingInterval() { start(); }

	private:
		// Simulation Thread main function
		void run();
		OpcUa::BaseVariableType* m_iNgIntervalVariable;
		OpcUa_Boolean m_stop;
		CanBusObject *m_pCanIn;   // Canbus object 
	};
}
#endif 
