#ifndef __CANOPENOBJECT_H__
#define __CANOPENOBJECT_H__

#include "uaobjecttypes.h"
#include "uasemaphore.h"
#include "InterfaceUserDataBase.h"
#include "CanBusAccess.h"

#include "CanOpenData.h"
#include "CANOpenServerConfig.h"
#include "CanBusObject.h"

namespace CanOpen
{
	/**
	* @namespace CanOpen
	* @brief Classes and methods to implement the CANOpen protocol for can bus device 
	*/
	class CanObject;

	/** @brief CanOpenObject class represents the can open messages which can be sent or received via can bus
	*
	*/
	class CanOpenObject : public InterfaceUserDataBase
	{

		/**
		* @brief This class defines operations for canopen protocol.
		*/
	public:

		/** Constructor
		* @param parent pointer to parent object usually null for CanBusObject
		* @param conf XML configuration entry
		* @param blink pointer to Ua Node
		* @param code function code for this object
		*/
		CanOpenObject(     
			pUserDeviceStruct *parent,::xml_schema::type *conf	,UaNode *blink, int code);
		
		/** Destructor */
		virtual ~CanOpenObject(void);

		CCanAccess *getCanBus();
		
		const char *getBusName() {
			CanBusObject * cb = static_cast<CanBusObject *>(getTopInterface()->getDevice());
			const char *bn = cb->getCanBusName().c_str() ; 
			return bn;
		}
		
		OpcUa_UInt16 getCobId()
		{
			return m_cobId;
		}
		
//		virtual UaStatus getDeviceMessage(OpcUa_UInt32) = 0; 
//		virtual UaStatus sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value = 0) = 0;

		virtual void pass(const CanMsgStruct *) = 0;

		virtual bool isMsgForObject(const CanMsgStruct *);

		DirectionType getDirectionType() { return m_direction; }
		UaStatus waitOperation(OpcUa_UInt timeout);
		UaStatus waitOperation();

		virtual void messageCame() { 
			signalOperation();
		}

		virtual UaStatus releaseOperation();
		virtual UaStatus signalOperation()
		{ 
			if ((m_iMaxCount - m_iCount) > 0)
				++m_iCount; 
			return	m_waitMessage.post(1);
		}

		UaByteArray m_buffer;
		UaDateTime m_udt;

	protected:
	
		OpcUa_UInt32 m_iTimeout;
		UaSemaphore  m_waitMessage;

		OpcUa_UInt16	m_cobId;
		DirectionType	m_direction;
		OpcUa_UInt32	m_iMaxCount;
		OpcUa_UInt32	m_iCount;
	};
}
#endif
  
