#ifndef __CANOPENDATEADDRESS_H__
#define __CANOPENDATEADDRESS_H__

enum CanOpenItemType
    {
        PDOItem,
		ELMBItem,
		SB2Item,
        SDOItem,
        Emergency
    };

enum DirectionType 	{ CAN_IN, CAN_OUT,	CAN_IO	};

class CanOpenDataAddress : public UserDataBase
{
    UA_DISABLE_COPY(CanOpenDataAddress);
public:
    CanOpenDataAddress(
		CanOpenItemType itemType,
        OpcUa_UInt16   dataAddress,
		OpcUa_UInt16   numberByte,
        OpcUa_UInt16   bitNumber,
		OpcUa_UInt16   ch)
    : m_itemType(itemType),
      m_dataAddress(dataAddress),
      m_numberByte(numberByte),
	  m_bitNumber(bitNumber),
	  m_channel(ch)
    {}
    virtual ~CanOpenDataAddress(){}
	static bool getDirection(const char * ndir,DirectionType &d)
	{
		if(!strcmp(ndir,"R")) d = CAN_IN;
		else {
			if(!strcmp(ndir,"W")) d = CAN_OUT;
			else {
				if(!strcmp(ndir,"RW")) d = CAN_IO;
				else return false;
			}
		}
		return true;
	}


	static bool getOpcUaType(const char * ndir,OpcUa_BuiltInType &d)
	{
		if(!strcmp(ndir,"Int32")) d = OpcUaType_Int32;
		else {
			if(!strcmp(ndir,"Int16")) d = OpcUaType_Int16;
			else {
				if(!strcmp(ndir,"UInt32")) d = OpcUaType_UInt32;
				else {
					if(!strcmp(ndir,"UInt16")) d = OpcUaType_UInt16;
					else {
						if(!strcmp(ndir,"float")) d = OpcUaType_Float;
						else {
							if(!strcmp(ndir,"double")) d = OpcUaType_Double;
							else {
								if(!strcmp(ndir,"byte")) d = OpcUaType_Byte;
								else {
									if(!strcmp(ndir,"bit")) d = OpcUaType_Boolean;
									else {
										if (!strcmp(ndir, "bool")) d = OpcUaType_Boolean;
										else {
											if (!strcmp(ndir, "array")) d = OpcUaType_ByteString;
											else return false;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return true;
	}
    /**  */
    inline CanOpenItemType msgType() const { return m_itemType; }
    /**  */
    inline OpcUa_UInt16 dataAddress() const { return m_dataAddress; }
    /**  */
    inline OpcUa_UInt16 numberByte() const { return m_numberByte; }
    /**  */
    inline OpcUa_UInt16 bitNumber() const { return m_bitNumber; }
    /**  */
    inline OpcUa_UInt16 channel() const { return m_channel; }

private:
	DirectionType	direct;
	CanOpenItemType m_itemType;
    OpcUa_UInt16   m_dataAddress;
	OpcUa_UInt16   m_numberByte;
    OpcUa_UInt16   m_bitNumber;
	OpcUa_UInt16   m_channel;
};

#endif