#ifndef __CANPDOITEM_H__
#define __CANPDOITEM_H__

//#include "CCanCallback.h"
#include <opcua_basedatavariabletype.h>
#include "Ids.h"

#include "CanBusAccess.h"

#include "CanOpenItem.h"

namespace CanOpen
{
	class CanPDOObject;
	/**
	 *  @brief The Class defines the basic operation of any item 
	 * which takes data from CAN pdo message and put date into CAN pdo message
	 */
	class CanPDOItem : public CanOpenItem
	{
	public:
		/** @brief Constructor
		* @param parent - pointer on parent CanPdoObject
		* @param conf - XML configuration entry for this item
		* @param pAS - pointer to UaNode to pass data
		* @param code - function code of operation
		*/
		CanPDOItem(pUserDeviceStruct *parent,PDOITEM *conf,UaNode *pAS,OpcUa_UInt32 code);
		CanPDOItem() = default;
		//! retrieve value from can pdo message
		virtual UaVariant UnPack(); 
		//! put value to can pdo message
		virtual UaStatus pack(const OpcUa_Variant *); 
		//! Destructor
		virtual ~CanPDOItem(void) {} ; 

//		virtual void setItemValue(const CanMsgStruct *);
		//! Write value to UaVariable
		virtual void setItemValue();  
		/** set channel number */
		void setIndex(OpcUa_UInt16 i) { m_Index = i; }
		/** @return channel number */
		OpcUa_UInt16 getIndex()		{ return m_Index; }
		/** @return initial byte of can message for this item */
		OpcUa_Byte getStartByte()	{ return m_startByte; }
		/** @return initial bit in byte for this item */
		OpcUa_Byte getBit()			{ return m_numBit; }
		/** @return pointer to CanPDOObject */
		CanPDOObject * getCanPDOObject();

		virtual UaStatus connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *blink);
		virtual UaVariant getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf);
		/** Send value to can bus 
		* @param udv value to send
		* @return error code
		*/
		virtual UaStatus write(UaDataValue &udv);
		/** function does not use */
		virtual	UaStatus read() { return OpcUa_Bad; } 

	protected:
		OpcUa_UInt16 m_Index; //!< Channel number for ELMB for normal PDO = 0
		OpcUa_Byte m_startByte; //!< initial byte of date
		OpcUa_Byte m_numBit; //!< fir bit type bit number in byte
	};
}
#endif
  
