#include "NodeGuardingInterval.h"
//#include "BuildingAutomationTypeIds.h"
#include "uadatetime.h"
#include "uabasenodes.h"
#include "CanBusObject.h"
#include "CanNodeObject.h"
#include <thread>

using namespace UserDevice;
namespace CanOpen
{
	/* ----------------------------------------------------------------------------
	Begin Class    BaCommunicationInterface
	constructor / destructor
	-----------------------------------------------------------------------------*/
	NodeGuardingInterval::NodeGuardingInterval(CanBusObject *pCI,OpcUa::BaseVariableType* pVar )
	{
		m_stop = OpcUa_False;
		m_iNgIntervalVariable = pVar;
		m_pCanIn = pCI;
//		start();
	}

	NodeGuardingInterval::~NodeGuardingInterval()
	{
		// Signal Simulation Thread to stop
		m_stop = OpcUa_True;
		// Wait until main Thread stopped
		wait(2);
	}


	/**
	*   Class        NodeGuarding Interval
	*   Method       run
	*   Description  Thread main function.
	*/
	void NodeGuardingInterval::run()
	{
		UaDataValue dSync;
		while( m_stop == OpcUa_False )
		{

			if (m_iNgIntervalVariable != 0) {
				dSync = m_iNgIntervalVariable->value(NULL);
				const OpcUa_Variant *vSync = dSync.value();
				OpcUa_UInt32 tInterval = vSync->Value.UInt32;

				if (tInterval > 0 ) {
					
					for (busNodes::iterator cnoi = m_pCanIn->getListNodes().begin(); cnoi != m_pCanIn->getListNodes().end(); cnoi++) 
					{
							m_pCanIn->lock();
							(*cnoi).second->sendNG();
							(*cnoi).second->decNGCounter();
							m_pCanIn->unlock();
					}

					if (!m_stop) {
						LOG(Log::DBG, NGMessage) << "Interval =" << tInterval;
						std::this_thread::sleep_for(std::chrono::milliseconds(tInterval));
					}
				}
				else if (!m_stop) 
					std::this_thread::sleep_for(std::chrono::seconds(10)); 
				//UaThread::sleep(10);
			}
			else 
				if (!m_stop)
					std::this_thread::sleep_for(std::chrono::seconds(10));
		}
	}
}

