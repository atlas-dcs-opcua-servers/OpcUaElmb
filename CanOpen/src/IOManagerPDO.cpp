#include "BuildingAutomationTypeIds.h"
#include "IOManagerPDO.h"
#include "UaControlDeviceGeneric.h"
#include "UaControlDeviceItem.h"
#include "CanPDOItem.h"
#include "IOTransactionContextCanOpen.h"
#include "srvtrace.h"
#include "uabase.h"
#include "UaCanTrace.h"

using namespace AddressSpace;

namespace CanOpen
{

	/** construction */
	IOManagerPDO::IOManagerPDO()
	{

	}

	/** destruction */
	IOManagerPDO::~IOManagerPDO()
	{

	}

	/** Start a transaction.
	*  @param pCallback Callback interface used for the transaction. The IOManager must use this interface to finish the action for each passed node in the transaction.
	*  @param serviceContext General context for the service calls containing information like the session object, return diagnostic mask and timeout hint.
	*  @param hTransaction Handle for the transaction used by the SDK to identify the transaction in the callbacks. This handle was passed in to the IOManager with the beginTransaction method.
	*  @param totalItemCountHint A hint for the IOManager about the total number of nodes in the transaction. The IOManager may not be responsible for all nodes but he can use this hint if he wants to optimize memory allocation.
	*  @param maxAge Max age parameter used only in the Read service.
	*  @param timestampsToReturn Indicates which timestamps should be returned in a Read or a Publish response. 
	*  The possible enum values are:
	*      OpcUa_TimestampsToReturn_Source
	*      OpcUa_TimestampsToReturn_Server
	*      OpcUa_TimestampsToReturn_Both
	*      OpcUa_TimestampsToReturn_Neither
	*  @param transactionType Type of the transaction. The possible enum values are:
	*      READ
	*      WRITE
	*      MONITOR_BEGIN
	*      MONITOR_MODIFY
	*      MONITOR_STOP
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @return Error code
	*/
	UaStatus IOManagerPDO::beginTransaction ( 
		IOManagerCallback*       pCallback,
		const ServiceContext&    serviceContext, // ToDo handle timeout hint and diagnostic info
		OpcUa_UInt32             hTransaction,
		OpcUa_UInt32             totalItemCountHint,
		OpcUa_Double             maxAge,
		OpcUa_TimestampsToReturn timestampsToReturn,
		TransactionType          transactionType,
		OpcUa_Handle&            hIOManagerContext)
	{
		OpcUa_ReferenceParameter(maxAge);
		TRACE0_INOUT(SERVER_CORE, UA_T"--> IOManagerPDO::beginTransaction");
		UaStatus      ret = OpcUa_Good ;

		// create transaction context for pdo write operation

		if ( transactionType == IOManager::WRITE )  // only write operation is allowed
		{
			IOTransactionContextPDO* pTransaction = new IOTransactionContextPDO;
			pTransaction->setSession(serviceContext.pSession());
			pTransaction->m_pCallback          = pCallback;
			pTransaction->m_hTransaction       = hTransaction;
			pTransaction->m_timestampsToReturn = timestampsToReturn;
			pTransaction->m_totalItemCountHint = totalItemCountHint;
			pTransaction->m_transactionType    = transactionType;
			pTransaction->m_nAsyncCount			= 0;
			pTransaction->m_pIOManager    = this;
			pTransaction->m_arrCallbackHandles.create(totalItemCountHint);
			pTransaction->m_arrUaVariableHandles.create(totalItemCountHint);
			OpcUa_UInt32 count = pTransaction->m_arrUaVariableHandles.length();

			pTransaction->m_arrCanPDOObject.create(totalItemCountHint);
			for (OpcUa_UInt32 i = 0; i < count; i++)
			{
				pTransaction->m_arrUaVariableHandles[i] = NULL;
				pTransaction->m_arrCanPDOObject[i] = NULL;
			}

			
			hIOManagerContext = (OpcUa_Handle)pTransaction;

			TRACE0_INOUT(SERVER_CORE, UA_T"--> IOManagerPDO::beginWriteTransaction");
		}
		else
		{
			hIOManagerContext = 0;
			LOG(Log::DBG, UaCanTrace::PdoMessage) << " PDO Transaction Type Wrong -> " << transactionType;

			ret = OpcUa_BadInvalidArgument;
		}

		TRACE1_INOUT(SERVER_CORE, UA_T"<-- IOManagerPDO::beginTransaction [ret=0x%lx]", ret.statusCode());
		return ret;
	}

	/** Finish a transaction.
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @return Error code
	*/
	UaStatus IOManagerPDO::finishTransaction(OpcUa_Handle hIOManagerContext)
	{
		TRACE0_INOUT(SERVER_CORE, UA_T"--> IOManagerPDO::finishTransaction");
		UaStatus      ret = OpcUa_Good;
		UaObjectArray<UaStatus> retTrun;
		IOTransactionContextPDO* pTransaction = (IOTransactionContextPDO*)hIOManagerContext;

		if(pTransaction)
		{
			retTrun.create(pTransaction->m_nAsyncCount);
			OpcUa_UInt32 nRetTrun = 0;
			for(OpcUa_UInt32 i = 0; i < pTransaction->m_iPDONumber.size(); i++)
			{
				UaStatus      retloc;
				TRACE0_INOUT(SERVER_CORE, UA_T"--> IOManagerPDO::Write");

				//send PDO message

				retTrun[nRetTrun] = pTransaction->m_arrCanPDOObject[i]->sendDeviceMessage(Ba_PDOType_OUT);
				if (i < pTransaction->m_iPDONumber.size()-1)
				{
					for (OpcUa_UInt32 j = nRetTrun + 1; j < pTransaction->m_iPDONumber[i + 1]; j++)
					{
						retTrun[j] = retTrun[nRetTrun];
						nRetTrun = pTransaction->m_iPDONumber[i + 1];
					}
				}
			}

			for(OpcUa_UInt32 i = 0; i < pTransaction->m_nAsyncCount; i++)
			{
				pTransaction->m_pCallback->finishWrite(pTransaction->m_hTransaction,pTransaction->m_arrCallbackHandles[i],retTrun[i],OpcUa_True);
			}
			delete pTransaction;
		}
		else  {
			ret.setStatus(OpcUa_BadUnexpectedError,"No Transaction");
			TRACE1_INOUT(SERVER_CORE, UA_T"<-- IOManagerPDO::badFinishTransaction [ret=0x%lx]", ret.statusCode());
		}
		

		TRACE1_INOUT(SERVER_CORE, UA_T"<-- IOManagerPDO::finishTransaction [ret=0x%lx]", ret.statusCode());
		return ret;
	}

	/** Read attribute value of a node.
	* This function does not do any thing for this manager
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @param callbackHandle Handle for the node in the callback. This handle was passed in to the IOManager with the beginModifyMonitoring method.
	*  @param pVariableHandle Variable handle provided by the NodeManager::getVariableHandle. 
	*      This object contains the information needed by the IOManager to identify the node to read in its context.
	*  @param pReadValueId Context for the node to read. The context contains the IndexRange and the DataEncoding requested by the client. The other parameters of this context are already handled by the VariableHandle.
	*  @return Error code
	*/
	UaStatus IOManagerPDO::beginRead ( 
		OpcUa_Handle       hIOManagerContext,
		OpcUa_UInt32       callbackHandle,
		VariableHandle*    pVariableHandle,
		OpcUa_ReadValueId* /*pReadValueId*/) // ToDo handle index range
	{
		OpcUa_ReferenceParameter(hIOManagerContext);
		OpcUa_ReferenceParameter(callbackHandle);
		OpcUa_ReferenceParameter( pVariableHandle);
//		return  OpcUa_BadInvalidArgument;
		return OpcUa_Good; 
	}

	/** Write attribute value of a node.
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @param callbackHandle Handle for the node in the callback. This handle was passed in to the IOManager with the beginModifyMonitoring method.
	*  @param pVariableHandle Variable handle provided by the NodeManager::getVariableHandle. 
	*      This object contains the information needed by the IOManager to identify the node to read in its context.
	*  @param pWriteValue Context for the node to write. The context contains the IndexRange requested by the client. The NodeId and Attribute parameters of this context are already handled by the VariableHandle
	*  @return Error code
	*/
	UaStatus IOManagerPDO::beginWrite ( 
		OpcUa_Handle      hIOManagerContext,
		OpcUa_UInt32      callbackHandle,
		VariableHandle*   pVariableHandle,
		OpcUa_WriteValue* pWriteValue)
	{
		UaStatus      ret = OpcUa_Good;
		UaControlDeviceItem *pPDONode;
		IOTransactionContextPDO* pTransaction = (IOTransactionContextPDO*)hIOManagerContext;

		if(pTransaction)
		{
			if ( pTransaction->m_transactionType == IOManager::WRITE )
			{
				VariableHandleUaNode* vhUaNode = (VariableHandleUaNode*)pVariableHandle;

				pPDONode = (UaControlDeviceItem *)vhUaNode->pUaNode();
				CanPDOItem *t_pdoi = (CanPDOItem *)pPDONode->getUserData();
				CanPDOObject *t_pdo = t_pdoi->getCanPDOObject();

				// add variable handler ( possible not need for this operation )
				pTransaction->m_arrCallbackHandles[pTransaction->m_nAsyncCount] = callbackHandle;
				pTransaction->m_arrUaVariableHandles[pTransaction->m_nAsyncCount] = vhUaNode;
				pTransaction->m_arrUaVariableHandles[pTransaction->m_nAsyncCount]->addReference();

				if (pTransaction->isNewCanPDOObject(t_pdo))
				{
					pTransaction->m_iPDONumber.push_back(pTransaction->m_nAsyncCount);
//					pTransaction->m_arrCanPDOObject[pTransaction->m_nAsyncCount] = t_pdo; //add new PDO object to send data
					pTransaction->m_arrCanPDOObject[pTransaction->m_iPDONumber.size()-1] = t_pdo; //add new PDO object to send data
				}
				pTransaction->m_nAsyncCount++;

				t_pdoi->pack((const OpcUa_Variant *)&pWriteValue->Value.Value);  //pack PDO item into PDO object
			}
			else
			{
//				cout << "No Transaction Write " << endl;

				UA_ASSERT(false);
				ret = OpcUa_BadNotWritable;
			}
		}
		else
		{
			UA_ASSERT(false);
			ret = OpcUa_BadInvalidArgument;
		}

		return ret;
	}

	/** Start monitoring of an item.
	* This function does not do any thing for this manager
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @param callbackHandle Handle for the node in the callback. This handle was passed in to the IOManager with the beginModifyMonitoring method.
	*  @param pIOVariableCallback Callback interface used for data change callbacks to the MonitoredItem managed by the SDK.
	*  @param pVariableHandle Variable handle provided by the NodeManager::getVariableHandle. 
	*      This object contains the information needed by the IOManager to identify the node to read in its context.
	*  @param monitoringContext handle for the monitoring context.
	*  @return Error code
	*/
	UaStatus IOManagerPDO::beginStartMonitoring(
		OpcUa_Handle        hIOManagerContext,
		OpcUa_UInt32        callbackHandle,
		IOVariableCallback* pIOVariableCallback,
		VariableHandle*     pVariableHandle,
		MonitoringContext&  monitoringContext)
	{
		OpcUa_ReferenceParameter(hIOManagerContext);
		OpcUa_ReferenceParameter(callbackHandle);
		OpcUa_ReferenceParameter(pIOVariableCallback);
		OpcUa_ReferenceParameter(pVariableHandle);
		OpcUa_ReferenceParameter(monitoringContext);
		return OpcUa_BadNotSupported;
	}


	/** Notify IOManager after modifying monitoring parameters of an item.
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @param callbackHandle Handle for the node in the callback. This handle was passed in to the IOManager with the beginModifyMonitoring method.
	*  @param hIOVariable The handle of the variable in the IOManager created with beginStartMonitoring. 
	*      The handle was passed to the SDK in the callback finishStartMonitoring.
	*  @param monitoringContext handle for the monitoring context.
	*  @return Error code
	*/
	UaStatus IOManagerPDO::beginModifyMonitoring(
		OpcUa_Handle       hIOManagerContext,
		OpcUa_UInt32       callbackHandle,
		OpcUa_UInt32       hIOVariable,
		MonitoringContext& monitoringContext)
	{
		OpcUa_ReferenceParameter(hIOManagerContext);
		OpcUa_ReferenceParameter(callbackHandle);
		OpcUa_ReferenceParameter(hIOVariable);
		OpcUa_ReferenceParameter(monitoringContext);
		return OpcUa_BadNotSupported;
	}


	/** Notify IOManager after stopping monitoring of an item.
	* This function does not do any thing for this manager
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @param callbackHandle Handle for the node in the callback. This handle was passed in to the IOManager with the beginModifyMonitoring method.
	*  @param hIOVariable The handle of the variable in the IOManager created with beginStartMonitoring. 
	*      The handle was passed to the SDK in the callback finishStartMonitoring.
	*  @return Error code
	*/
	UaStatus IOManagerPDO::beginStopMonitoring(
		OpcUa_Handle   hIOManagerContext,
		OpcUa_UInt32   callbackHandle,
		OpcUa_UInt32   hIOVariable)
	{
		OpcUa_ReferenceParameter(hIOManagerContext);
		OpcUa_ReferenceParameter(callbackHandle);
		OpcUa_ReferenceParameter(hIOVariable);
		return OpcUa_BadNotSupported;
	}

}
