#include "CanPDOItem.h"
#include "CanOpenObject.h"
#include "CanOpenData.h"
#include "CanPDOObject.h"

namespace CanOpen
{
	CanPDOItem::CanPDOItem(pUserDeviceStruct *parent, PDOITEM *conf, UaNode *pAS, OpcUa_UInt32 code)
		: CanOpenItem(parent, conf, pAS, code)
	{
		UaDataValue udt;
		UaVariant val;
		UaDateTime sdt = UaDateTime::now();
		m_startByte = conf->byteindex();	/// start byte in can message
		m_numBit = conf->bit();				///  bit position in byte
		m_Index = 0;
		CanOpenDataAddress::getOpcUaType(conf->type().data(), itemType);

		if (getCanPDOObject()->getDirectionType() == CAN_OUT) {

			switch (getItemType()) {
			case OpcUaType_Boolean:
				val.setBool(false);
				break;
			case OpcUaType_Byte:
				val.setByte(0);
				break;
			case OpcUaType_Int16:
				val.setInt16(0);
				break;
			case OpcUaType_UInt16:
				val.setUInt16(0);
				break;
			case OpcUaType_Int32:
				val.setInt32(0);
				break;
			case OpcUaType_UInt32:
				val.setUInt32(0);
				break;
			case OpcUaType_Int64:
				val.setInt64(0);
				break;
			case OpcUaType_UInt64:
				val.setUInt64(0);
				break;
			case OpcUaType_Float:
				val.setFloat(0.0);
				break;
			case OpcUaType_Double:
				val.setDouble(0.0);
				break;

			default:
				val.setUInt32(0);
				break;
			}

			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
			((OpcUa::BaseDataVariableType *)pAS)->setValue(0, udt, OpcUa_False);
		}
		else
		{
			val.setStatusCode(OpcUa_BadWaitingForInitialData);
			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
			((OpcUa::BaseDataVariableType *)pAS)->setValue(0, udt, OpcUa_False);
		}
	}

	CanPDOObject *CanPDOItem::getCanPDOObject() {
		CanPDOObject *in = static_cast<CanPDOObject*>(getParentDevice());
		return in;
	}
	
	UaStatus CanPDOItem::write(UaDataValue &udv)
	{
		pack(udv.value());
		return getCanPDOObject()->sendDeviceMessage(BA_PDOOBJECT);
	}

	/**
	 * Pack data from item to can message
	 * @param sData data to pack
	 */
	UaStatus CanPDOItem::pack(const OpcUa_Variant *sData)
	{
		UaStatus ret;
		OpcUa_Byte index;
		index = (OpcUa_Byte)getIndex();
	
		OpcUa_Variant convData;
		UaStatus compatible = convertOpcUaType(sData, &convData, getItemType());
		if (compatible.isBad())
		{

			UaString sDiag = "Not compatible Data Types";
			ret.setStatus(OpcUa_BadDataEncodingUnsupported, sDiag);
			return ret;
		}

		UaByteArray& m_buffer = getCanPDOObject()->getBuffer(index);
		switch (getItemType()) {
		case OpcUaType_Boolean:
			{

				OpcUa_Byte t = 1 << getBit();
				if (convData.Value.Boolean)
					m_buffer[getStartByte()] = m_buffer[getStartByte()] | t;
				else {
					t = ~t;
					m_buffer[getStartByte()] = m_buffer[getStartByte()] & t;
				}
			}
			break;
		case OpcUaType_Byte: 


			m_buffer[getStartByte()] = convData.Value.Byte;
			break;
		case OpcUaType_Int16:
		case OpcUaType_UInt16: 
			{
				union {	
					unsigned char temp[2];
					unsigned short dtemp;
				} dt;

				dt.dtemp = convData.Value.UInt16;
				m_buffer[getStartByte()+1] = dt.temp[1];
				m_buffer[getStartByte()] = dt.temp[0];
			}
			break;
		case OpcUaType_Int32:
		{
			union {
				unsigned char temp[4];
				 int dtemp;
			} dt;
			dt.dtemp = convData.Value.Int32;

			m_buffer[getStartByte() + 3] = dt.temp[3];
			m_buffer[getStartByte() + 2] = dt.temp[2];
			m_buffer[getStartByte() + 1] = dt.temp[1];
			m_buffer[getStartByte()] = dt.temp[0];
			break;
		}
		case OpcUaType_UInt32:
		{
			union {
				unsigned char temp[4];
				unsigned int dtemp;
			} dt;
			dt.dtemp = convData.Value.UInt32;

			m_buffer[getStartByte() + 3] = dt.temp[3];
			m_buffer[getStartByte() + 2] = dt.temp[2];
			m_buffer[getStartByte() + 1] = dt.temp[1];
			m_buffer[getStartByte()] = dt.temp[0];
			break;
		}
		case OpcUaType_Float:
		{
			union {
				unsigned char temp[4];
				float dtemp;
			} dt;
			dt.dtemp = convData.Value.Float;

			m_buffer[getStartByte() + 3] = dt.temp[3];
			m_buffer[getStartByte() + 2] = dt.temp[2];
			m_buffer[getStartByte() + 1] = dt.temp[1];
			m_buffer[getStartByte()] = dt.temp[0];
			break;
		}
		case OpcUaType_Double:
		{
			union {
				unsigned char temp[8];
				double dtemp;
			} dt;

			dt.dtemp = convData.Value.Double;
			m_buffer[7] = dt.temp[7];
			m_buffer[6] = dt.temp[6];
			m_buffer[5] = dt.temp[5];
			m_buffer[4] = dt.temp[4];
			m_buffer[3] = dt.temp[3];
			m_buffer[2] = dt.temp[2];
			m_buffer[1] = dt.temp[1];
			m_buffer[0] = dt.temp[0];
			break;
		}

		case OpcUaType_ByteString:
		{

			m_buffer[7] = sData->Value.ByteString.Data[7];
			m_buffer[6] = sData->Value.ByteString.Data[6];
			m_buffer[5] = sData->Value.ByteString.Data[5];
			m_buffer[4] = sData->Value.ByteString.Data[4];
			m_buffer[3] = sData->Value.ByteString.Data[3];
			m_buffer[2] = sData->Value.ByteString.Data[2];
			m_buffer[1] = sData->Value.ByteString.Data[1];
			m_buffer[0] = sData->Value.ByteString.Data[0];
			break;
		}

		default:
			return OpcUa_Bad;
		}
		/*
		if (m_numch) {
			m_pdoBuffer[index] = m_buffer;
		}
		*/
		return OpcUa_Good;
	}

	UaVariant CanPDOItem::UnPack()
	{
		UaVariant newValue;
		newValue.clear();
	
		UaByteArray &m_buffer = getCanPDOObject()->m_buffer;
		OpcUa_Byte sByte = getStartByte();
		switch (getItemType()) {
		case OpcUaType_Boolean:
			{
				OpcUa_Byte t = 1 << getBit();
				newValue.setBool((m_buffer[sByte] & t) != 0 );
				break;
			}
		case OpcUaType_Byte:
			{
				newValue.setByte(m_buffer[sByte]);
				break;
			}
		case OpcUaType_UInt16:
			{
				unsigned short ushortTemp = (unsigned char)m_buffer[sByte+1]; 
				ushortTemp = ( ushortTemp << 8 ) + (unsigned char)m_buffer[sByte];
				newValue.setUInt16(ushortTemp);
				break;
			}
		case OpcUaType_UInt32:
			{
				unsigned int uintTemp = (unsigned char)m_buffer[sByte+3]; 
				uintTemp = ((((( uintTemp << 8 ) + (unsigned char)m_buffer[sByte+2]) << 8) + (unsigned char)m_buffer[sByte+1]) << 8) + (unsigned char)m_buffer[sByte];
				newValue.setUInt32(uintTemp);
				break;
			}
		case OpcUaType_Float:
		{
			union {
				unsigned int temp;
				float dtemp;
			} dt;
			dt.temp = (unsigned char)m_buffer[sByte+3];
			dt.temp = ((((( dt.temp << 8 ) + (unsigned char)m_buffer[sByte+2]) << 8) + (unsigned char)m_buffer[sByte+1]) << 8) + (unsigned char)m_buffer[sByte];
			newValue.setFloat(dt.dtemp);

		}
			break;
		case OpcUaType_Double:
		{
			union {
				unsigned long temp;
				float dtemp;
			} dt;
			dt.dtemp = (unsigned char)m_buffer[7];
			dt.temp = ((((( dt.temp << 8 ) + (unsigned char)m_buffer[6]) << 8) + (unsigned char)m_buffer[5]) << 8) + (unsigned char)m_buffer[4]+
					(unsigned char)m_buffer[3]+(unsigned char)m_buffer[2]+(unsigned char)m_buffer[1]+(unsigned char)m_buffer[0];
			newValue.setDouble(dt.dtemp);
		}
			break;
		case OpcUaType_Int16:
			{
				unsigned short shortTemp = (unsigned char)m_buffer[sByte+1]; 
				shortTemp = ( shortTemp << 8 ) + (unsigned char)m_buffer[sByte];
				newValue.setInt16((OpcUa_Int16)shortTemp);
				break;
			}
		case OpcUaType_Int32:
			{
				unsigned int intTemp = (unsigned char)m_buffer[sByte+3]; 
				intTemp = ((((( intTemp << 8 ) + (unsigned char)m_buffer[sByte+2]) << 8) + (unsigned char)m_buffer[sByte+1]) << 8) + (unsigned char)m_buffer[sByte];
				newValue.setInt32((OpcUa_Int32)intTemp);
				break;
			}
			/*
		case OpcUaType_ByteString:
			{
				UaByteString tmp;
				tmp.setByteString(4,(OpcUa_Byte *)(m_buffer.c_data()+sByte)); 
				newValue.setByteString(tmp,false);
				break;
			}
			*/
		default: 
			break;
		};

		return newValue;
	}

	void CanPDOItem::setItemValue()
	{
		UaDataValue dataValue;
		UaDateTime sdt;
		UaStatus Status = OpcUa_Bad;

		sdt = UaDateTime::now();

		UaVariant val = UnPack();

		dataValue.setDataValue(val, OpcUa_True, OpcUa_Good, getCanPDOObject()->m_udt,sdt);
		OpcUa::BaseDataVariableType *uaentry = getUaEntry();
		if (uaentry)
			Status = uaentry->setValue(NULL, dataValue, OpcUa_False);
	}	

	UaStatus CanPDOItem::connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *blink)
	{
		OpcUa_ReferenceParameter(conf);
		OpcUa_ReferenceParameter(code);
		OpcUa_ReferenceParameter(blink);
	//	UaDataValue udt;
	//	UaVariant val;
	//	UaDateTime sdt = UaDateTime::now();
		//PDOITEM *pdoi = (PDOITEM *)conf;

		//switch (code) {
		//case BA_PDOITEM_BIT:
		//	val.setByte(pdoi->bit());
		//	udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
		//	((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
		//	break;
		//case BA_PDOITEM_BYTEINDEX:
		//{
		//	val.setByte(pdoi->byteindex());
		//	udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
		//	((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
		//}
		//break;
		//default: return OpcUa_Bad;
		//}

		std::cout << "Empty list of children\n" << endl;
		return OpcUa_Good;
	}

	UaVariant CanPDOItem::getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf)
	{
		PDOITEM *pdoi = (PDOITEM *)conf;
		UaVariant val;
		val.clear();
		switch (code) {
		case BA_PDOITEM_BIT:
			val.setByte(pdoi->bit());
			break;
		case BA_PDOITEM_BYTEINDEX:
		{
			val.setByte(pdoi->byteindex());
		}
		break;
		}
		return val;
	}

}
