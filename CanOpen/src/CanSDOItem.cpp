//#include "BuildingAutomationTypeIds.h"
#include <opcua_basedatavariabletype.h>
#include "uabasenodes.h"
#include "CanSDOItem.h"
#include "CanSDOObject.h"
#include "CanNodeObject.h"
#include "CanOpenData.h"
#include "CanObject.h"
#include "CANopen.h"
#include <uadatetime.h>
#include "uasemaphore.h"
#include "UaCanTrace.h"
#include <iomanip>

namespace CanOpen
{
	/*
	* Constructor of CanSDOObject
	* @param par CanObject using to pass message to CanBus
	* @param conf XML configuration parameter for this item
	* @param blink pointer UaVariable
	* @param code function code
	*/
	CanSDOItem::CanSDOItem(pUserDeviceStruct *par,
			SDOITEM_TYPE1 *conf,UaNode *blink,int code) : CanOpenItem( par, conf,blink,code)
	{
		char *p;
		m_Timeout = 0;
		m_subind = (OpcUa_Byte)strtol(conf->subindex().data(),&p,16);

		CanOpenDataAddress::getOpcUaType(conf->type().data(),itemType);
	}

	/*
	* Constructor of CanSDOObject
	* @param par CanObject using to pass message to CanBus
	* @param conf XML configuration parameter for this item
	* @param blink pointer UaVariable
	* @param code function code
	*/
	CanSDOItem::CanSDOItem(pUserDeviceStruct *par,
			SDOITEM_TYPE2 *conf,UaNode *blink,int code) : CanOpenItem( par, conf,blink,code)
	{
		char *p;
		m_Timeout = 0;

		OpcUa_UInt16 ind = (OpcUa_UInt16)strtol(conf->index().data(), &p, 16);
		static_cast<CanSDOObject *>(par->getDevice())->setIndex(ind);

		m_subind = (OpcUa_Byte)strtol(conf->subindex().data(),&p,16);
		CanOpenDataAddress::getOpcUaType(conf->type().data(),itemType);
	}
	/*
	* Constructor of CanSDOOItem
	* @param par CanObject using to pass message to CanBus
	* @param conf XML configuration parameter for this item
	* @param blink pointer UaVariable
	* @param code function code
	*/
	CanSDOItem::CanSDOItem(pUserDeviceStruct *par,
			SDOITEM_TYPE3 *conf,UaNode *blink,int code) : CanOpenItem( par, conf,blink,code)
	{
		CanOpenDataAddress::getOpcUaType(conf->type().data(),itemType);
		m_Timeout = 0;
	}

	UaStatus CanSDOItem::write(UaDataValue &udv)
	{
		CanSDOObject *par = getCanSDOObject();
		par->setSDOItem(this);
		par->setTimeout(m_Timeout);

		UaStatus ret =  par->writeSdo(udv);

		par->freeSDOItem();

		return ret;
	}
	
	UaStatus CanSDOItem::read()
	{
		UaDataValue udv;

		CanSDOObject *par = getCanSDOObject();;

		par->setSDOItem(this);
		par->setTimeout(m_Timeout);

		UaStatus ret = par->readSdo(udv);
		if ( ret.isGood() ) {
			OpcUa::BaseDataVariableType *bdt = getUaEntry();
			bdt->setValue(0,udv,OpcUa_False);
//			cout << "Read " <<  bdt->browseName().toString().toUtf8() <<  endl;
		}

		par->freeSDOItem();

		return ret;
	}

	UaStatus CanSDOItem::pack(const OpcUa_Variant *sData)
	{
		UaStatus ret = OpcUa_Good;
		CanSDOObject *pSDOObject = getCanSDOObject();
		OpcUa_Variant convData;
		UaStatus compatible = convertOpcUaType(sData, &convData, getItemType());

		if (compatible.isBad())
		{
			UaString sDiag = "Not compatible Data Types";
			ret.setStatus(OpcUa_BadDataEncodingUnsupported, sDiag);
			return ret;
		}


		switch (getItemType()) {
		case OpcUaType_Boolean:
			pSDOObject->m_iLength = 1;

			pSDOObject->m_buffer[4] = convData.Value.Boolean;
			break;
		case OpcUaType_Byte:
			pSDOObject->m_iLength = 1;

			pSDOObject->m_buffer[4] = convData.Value.Byte;
			break;
		case OpcUaType_SByte:
			pSDOObject->m_iLength = 1; 

			pSDOObject->m_buffer[4] = convData.Value.SByte;
			break;
		case OpcUaType_Int16:
		case OpcUaType_UInt16: 
			{
				union {	
					unsigned char temp[2];
					unsigned short dtemp;
				} dt;

				dt.dtemp = convData.Value.UInt16;
				pSDOObject->m_buffer[4] = dt.temp[0];
				pSDOObject->m_buffer[5] = dt.temp[1];
				pSDOObject->m_iLength = 2;
			}
			break;
		case OpcUaType_Int32:
		case OpcUaType_UInt32:
		{
			union {
				unsigned char temp[4];
				unsigned long dtemp;
			} dt;

			dt.dtemp = convData.Value.UInt32;
			pSDOObject->m_buffer[4] = dt.temp[0];
			pSDOObject->m_buffer[5] = dt.temp[1];
			pSDOObject->m_buffer[6] = dt.temp[2];
			pSDOObject->m_buffer[7] = dt.temp[3];
			pSDOObject->m_iLength = 4;
		}
		break;
		case OpcUaType_Float:
		{
			union {
				unsigned char temp[4];
				float dtemp;
			} dt;

			dt.dtemp = convData.Value.Float;
			pSDOObject->m_buffer[4] = dt.temp[0];
			pSDOObject->m_buffer[5] = dt.temp[1];
			pSDOObject->m_buffer[6] = dt.temp[2];
			pSDOObject->m_buffer[7] = dt.temp[3];
			pSDOObject->m_iLength = 4;

		}
		break;
		case OpcUaType_ByteString:
			{
				union {	unsigned char temp[4];
				unsigned int dtemp;
				} dt;
				// initial download
				if (getItemType() == sData->Datatype) {			// Sdo Operation

					pSDOObject->m_iLength = sData->Value.ByteString.Length;
					pSDOObject->m_segSdoBuffer.resize(pSDOObject->m_iLength);
					pSDOObject->m_segSdoBuffer = sData->Value.ByteString;
					dt.dtemp = pSDOObject->m_iLength;
					pSDOObject->m_buffer[0] = SDO_INITIATE_DOWNLOAD_REQ | SDO_SEGMENT_SIZE_INDICATED ;
       				pSDOObject->m_buffer[1] = 0xFF & pSDOObject->getIndex();
					pSDOObject->m_buffer[2] = 0xFF & (pSDOObject->getIndex() >> 8);
					pSDOObject->m_buffer[3] = m_subind;
					pSDOObject->m_buffer[4] = dt.temp[0];
					pSDOObject->m_buffer[5] = dt.temp[1];
					pSDOObject->m_buffer[6] = dt.temp[2];
					pSDOObject->m_buffer[7] = dt.temp[3];
				}
				else {
					UaString sDiag = "Not compatible Data Types";
					ret.setStatus(OpcUa_BadDataEncodingUnsupported,sDiag);
					return ret;
				}

			}
			return OpcUa_Good;
		default:
			{
				UaString sDiag = "Not compatible Data Types";
				ret.setStatus(OpcUa_BadDataEncodingUnsupported, sDiag);
				return ret;

			}
		}

		OpcUa_Byte invalid_bytes = (OpcUa_Byte)(4 - pSDOObject->m_iLength);
		pSDOObject->m_buffer[0] = SDO_INITIATE_DOWNLOAD_REQ | SDO_EXPEDITED | SDO_DATA_SIZE_INDICATED | (invalid_bytes << SDO_DATA_SIZE_SHIFT);
		pSDOObject->m_buffer[1] = 0xFF & pSDOObject->getIndex();
		pSDOObject->m_buffer[2] = 0xFF & (pSDOObject->getIndex() >> 8);
		pSDOObject->m_buffer[3] = m_subind;

		return ret;
	}

	UaVariant CanSDOItem::UnPack()
	{
		UaVariant newValue;
		CanSDOObject *pSDOObject = getCanSDOObject();

		newValue.clear();
		


		UaByteArray& m_buffer = pSDOObject->m_buffer;
		OpcUa_Byte sByte = 4;
		switch (getItemType()) {
		case OpcUaType_Boolean:
			{
				newValue.setBool(m_buffer[sByte]);
				break;
			}
		case OpcUaType_Byte:
		case OpcUaType_SByte:
			{
				newValue.setByte(m_buffer[sByte]);
				break;
			}
		case OpcUaType_UInt16:
			{
				unsigned short ushortTemp = (unsigned char)m_buffer[sByte+1]; 
				ushortTemp = ( ushortTemp << 8 ) + (unsigned char)m_buffer[sByte];
				newValue.setUInt16(ushortTemp);
				break;
			}
		case OpcUaType_UInt32:
		{
			unsigned int uintTemp = (unsigned char)m_buffer[sByte + 3];
			uintTemp = (((((uintTemp << 8) + (unsigned char)m_buffer[sByte + 2]) << 8) + (unsigned char)m_buffer[sByte + 1]) << 8) + (unsigned char)m_buffer[sByte];
			newValue.setUInt32(uintTemp);
			break;
		}
		case OpcUaType_Float:
			{
				union {
					unsigned int temp;
					float dtemp;
				} dt;

				dt.temp = (unsigned char)m_buffer[sByte+3]; 
				dt.temp = ((((( dt.temp << 8 ) + (unsigned char)m_buffer[sByte+2]) << 8) + (unsigned char)m_buffer[sByte+1]) << 8) + (unsigned char)m_buffer[sByte];
				newValue.setFloat(dt.dtemp);
				break;
			}
		case OpcUaType_Int16:
			{
				unsigned short shortTemp = (unsigned char)m_buffer[sByte+1]; 
				shortTemp = ( shortTemp << 8 ) + (unsigned char)m_buffer[sByte];
				newValue.setInt16((OpcUa_Int16)shortTemp);
				break;
			}
		case OpcUaType_Int32:
			{
				unsigned int intTemp = (unsigned char)m_buffer[sByte+3]; 
				intTemp = ((((( intTemp << 8 ) + (unsigned char)m_buffer[sByte+2]) << 8) + (unsigned char)m_buffer[sByte+1]) << 8) + (unsigned char)m_buffer[sByte];
				newValue.setInt32((OpcUa_Int32)intTemp);
				break;
			}
		case OpcUaType_ByteString:
			{
				UaByteString tmp;
				m_buffer = pSDOObject->m_segSdoBuffer;
				tmp.setByteString(pSDOObject->m_iLength,(OpcUa_Byte *)(m_buffer.data())); 
				newValue.setByteString(tmp,false);
				break;
			}
		default: 
			break;
		};

		return newValue;
	}
	UaVariant CanSDOItem::getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf)
	{
		UaVariant val;
		val.clear();
		auto *sdoi = conf;
		switch (code) {
		case 	BA_SDOITEMTYPE2_INDEX:
		{
			char *p;
			getCanSDOObject()->setIndex(strtol(((SDOITEM_TYPE2 *)sdoi)->index().c_str(), &p, 16));
			val.setString(UaString(((SDOITEM_TYPE2 *)sdoi)->index().c_str()));
		}
		break;
		case 	BA_SDOITEMTYPE2_SUBINDEX:
			val.setString(UaString(((SDOITEM_TYPE2 *)sdoi)->subindex().c_str()));
			break;
		case 	BA_SDOITEMTYPE1_SUBINDEX:
		{
			string t = ((SDOITEM_TYPE1 *)sdoi)->subindex();
			val.setString(UaString(t.c_str()));
		}
		break;
		}
		return val;
	}

	UaStatus CanSDOItem::connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *blink)
	{
		UaStatus ret = OpcUa_Good; 
		UaDataValue udt;
		UaVariant val;
		UaDateTime sdt = UaDateTime::now();
		
		SDOITEM_TYPE *sdoi = (SDOITEM_TYPE *)conf;
		switch (code) {
		case 	BA_SDOITEMTYPE_TIMEOUT:
			m_Timeout = sdoi->timeout();
			val.setUInt32(m_Timeout);
			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
			ret = ((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			break;
		default:
			return OpcUa_Bad;
		}
		return ret;
	}


}
