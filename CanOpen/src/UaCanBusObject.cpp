#include "BuildingAutomationTypeIds.h"
#include "UaCanBusObject.h"
#include "UaCanNodeObject.h"

#include "CANOpenServerConfig.h"

namespace CanOpen
{
	/**
	* Constructor 
	* @param ca canbus XML description
	* @param newNodeId  CanBus Node ID
	* @param pNodeManager reference to node Manager for this node
	**/
	UaCanBusObject::UaCanBusObject(CANBUS *ca,
		const UaNodeId& newNodeId,
		NmBuildingAutomation* pNodeManager
	)
		: UaControlDeviceGeneric(UaString(ca->name().c_str()), newNodeId, pNodeManager,Ba_CanBusType,(::xml_schema::type *)ca,0)
	{
		/**************************************************************
		* Create the PDO IO manager
		**************************************************************/
	
//		m_pIOManagerPDO = new IOManagerPDO; // IO manager for writing PDO

	}
}
