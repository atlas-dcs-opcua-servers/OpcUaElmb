#include "CanOpenItem.h"

namespace CanOpen
{
	UaStatus CanOpenItem::convertOpcUaType(const OpcUa_Variant *sData, OpcUa_Variant *oData, OpcUa_BuiltInType bType)
	{
		UaStatus ret = false;
		UaVariant tmpVar;
		if (sData->Datatype == bType) {
			tmpVar = *sData;
			tmpVar.copyTo(oData);
			return true;
		}
		else {
			if (sData->Datatype == OpcUaType_ByteString)
				return OpcUa_False;
			if (TypeConversion) {
	
				tmpVar = *sData;
				ret = tmpVar.changeType(bType, OpcUa_False);
				if (ret.isGood()) {
					tmpVar.copyTo(oData);
				}

			}
		}
		return ret;
	}

}
