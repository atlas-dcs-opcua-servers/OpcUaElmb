/******************************************************************************
** serverconfigxml.cpp
**
** Copyright (C) 2008-2009 Unified Automation GmbH. All Rights Reserved.
** Web: http://www.unified-automation.com
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: C++ OPC Server SDK sample code
**
** Description: Configuration management class for the OPC Server.
**
******************************************************************************/
#include "CanOpenConfigXML.h"
#include "servermanager.h"
#include "srvtrace.h"
#include "uaunistring.h"

#ifdef _WIN32_WCE
    #include <Winsock2.h>
#endif /* _WIN32_WCE */

using namespace AddressSpace;
/* ----------------------------------------------------------------------------
    Begin Class    CANopenConfigXml
-----------------------------------------------------------------------------*/

/** @brief Construction
 * @param sXmlFileName the file name of the XML file.
 * @param pServer the server to be create the address space
 */
CANopenConfigXml::CANopenConfigXml(OpcServer* pServer, UaString &sXmlFileName)
{
    m_sXmlFileName          = sXmlFileName;
	m_pServer = pServer;
}

/** Destruction */
CANopenConfigXml::~CANopenConfigXml()
{
}

/** Load the configuration from the configure file and generate node managers.
 *  First method called after creation of ServerConfig. Must create all NodeManagers
 *  before method startUp is called.
 *  @return         Error code.
 */
UaStatus CANopenConfigXml::CreateAddressSpace()
{
    UaStatus ret = OpcUa_True;

	try {
		auto_ptr< ::CanOpenOpcServerConfig > vDoc( CanOpenOpcServerConfig_(m_sXmlFileName.toUtf8()));
		
		vNodeType = vDoc->NODETYPE();
		vCanbus = vDoc->CANBUS();
		vItem = vDoc->ITEM();
		vReg = vDoc->REGEXPR();

		cout << "Create Node Manager, Bus number = "<< vCanbus.size() << endl;
	}
	catch (const xml_schema::exception& e)
	{
		cerr << e.what() << endl;
		return -1;
	}

	return ret;
}
