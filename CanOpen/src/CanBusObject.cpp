//#include "BuildingAutomationTypeIds.h"
#include "CanBusInterface.h"

#include "CanBusObject.h"
//#include "CCanUACallback.h"


namespace CanOpen
{

	CanBusObject::CanBusObject(pUserDeviceStruct *parent, CANBUS *ca, UaNode *blink, int code)
		: CanObject(parent, ca, blink, code)
	{
		m_sBusName = ca->name();

		if (ca->canParam() != NULL)
			m_sBusParameters = ca->speed() + " " + ca->canParam().get();
		else
			m_sBusParameters = ca->speed();

		m_sBusAddress = ca->type() +":" + ca->port();
		setInitNmt((ca->nmt() == NULL) ? string() : ca->nmt().get());
		m_pCallback	= new CCanUACallback(this); 
	
	}

	void CanBusObject::initialize()
	{
		m_pCallback->executeRTR();
	}

	void CanBusObject::openCanBus(void)
	{
		m_pCommIf = CanBusInterface::g_pCanaccess->openCanBus(m_sBusAddress,m_sBusParameters);
		if (m_pCommIf)
		// Create the the callback interface for input messages
			m_pCallback->ConnectCallback(); 
		else {
			LOG(Log::INF) << " bus=" << m_sBusName.data() << " status bad  " << "Could not open the bus ";
			if (!noExit) {
				exit(-1);
			}
		}
	}

	void CanBusObject::closeCanBus(void)
	{
//		LOG(Log::INF) << "Close Synch";
		if (m_pSyncIn) {
			delete m_pSyncIn;
			m_pSyncIn = NULL;
		}
//		LOG(Log::INF) << "Close NG";

		if (m_pNgIn) {
			delete m_pNgIn;
			m_pNgIn = NULL;
		}

//		LOG(Log::INF) << " bus=" << m_sBusName.data() << " Close the bus ";


		if (m_pCommIf) {
			CanBusInterface::g_pCanaccess->closeCanBus(m_pCommIf);
		// Delete the the callback interface for input messages
			if (m_pCallback) {
				delete m_pCallback;
				m_pCallback = NULL;
			}
		}
		m_pCommIf = NULL;
//		LOG(Log::INF) << "Close CallBack";

	}

	CanBusObject::~CanBusObject(void)
	{
//		LOG(Log::INF) << "Del Interface";

		closeCanBus();
		
		if (m_pSyncIn) {
			delete m_pSyncIn;
			m_pSyncIn = NULL;
		}
		if (m_pNgIn) {
			delete m_pNgIn;
			m_pNgIn = NULL;
		}
		if (m_pCommIf) {
			if (m_pCallback) {
				delete m_pCallback;
				m_pCallback = NULL;
			}
			delete m_pCommIf;
			m_pCommIf = NULL;
		}
		
	}

	UaStatus CanBusObject::sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value)
	{
		UaStatus ret =  OpcUa_Good;
		string nameMethod;

		lock();
		switch (code) {
		case BA_CANOBJECT_START:
			nameMethod = "NMT Start";
			sendNMT(start);
			break;
		case BA_CANOBJECT_STOP:
			nameMethod = "NMT Stop";
			sendNMT(stop);
			break;
		case BA_CANOBJECT_PREOPERATION:
			nameMethod = "NMT PreOperation";
			sendNMT(preOperational);
			break;
		case BA_CANOBJECT_RESET:
			nameMethod = "NMT Reset";
			sendNMT(reset);
			break;
		case BA_CANBUS_SYNCH:
			nameMethod = "Synch";
			sendSync();
			break;
		case BA_CANBUS_SYNCHCOMMAND:
			{
				nameMethod = "Synch";

				OpcUa_Boolean command = value->value()->Value.Byte;
				if (command) 
					sendSync();
			}
			break;
		case BA_CANOBJECT_NMT:
			{
				OpcUa_Byte command = value->value()->Value.Byte;
				nameMethod = "NMT " + command;
				sendNMT((::NMTcommand)command);
			}
			break;
		default:
			ret = OpcUa_Bad;
		}
		LOG(Log::DBG, NMTMessage) << " bus=" << getCanBusName() << "node=0" << " phase=" << nameMethod;

		unlock();
		return ret; 
	}
	
	UaStatus CanBusObject::connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *blink)
	{
		UaDataValue udt;
		UaVariant val;
		OpcUa::BaseDataVariableType * linkObject = static_cast<OpcUa::BaseDataVariableType *>(blink);
		UaDateTime sdt = UaDateTime::now();
		CANBUS *canb = (CANBUS *)conf;
		switch (code) {
			
		case BA_CANOBJECT_NMT: 
			linkObject->setDataType(OpcUaType_UInt32);
			val.setUInt32(0);
			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
			linkObject->setValue(0, udt, OpcUa_False);
			linkObject->setValueHandling(UaVariable_Value_Cache);
			break;
		case BA_CANBUS_SYNCHCOMMAND:
			linkObject->setDataType(OpcUaType_UInt32);
			val.setUInt32(0);
			udt.setDataValue(val,OpcUa_False,OpcUa_Good,sdt,sdt);
			linkObject->setValue(0,udt,OpcUa_False);
			linkObject->setValueHandling(UaVariable_Value_Cache);
			break;

		case BA_CANBUS_SYNCHINTERVAL:
			linkObject->setDataType(OpcUaType_UInt32);
			if (canb->SYNC()) {
				val.setUInt32(canb->SYNC().get().interval());
			}
			else
			{
				val.setUInt32(0);
			}
			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
			linkObject->setValue(0, udt, OpcUa_False);
			m_pSyncIn = new SyncInterval(this, linkObject);
			if (!m_pSyncIn) return OpcUa_Bad;
			break;
		case BA_CANBUS_NODEGUARDINGINTERVAL:
			linkObject->setDataType(OpcUaType_UInt32);
			if (canb->NODEGUARD())
			{
				val.setUInt32(canb->NODEGUARD().get().interval());
			}
			else
			{
				val.setUInt32(0);
			}
			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
			linkObject->setValue(0, udt, OpcUa_False);
			m_pNgIn = new NodeGuardingInterval(this, linkObject);
			if (!m_pNgIn) return OpcUa_Bad;
			break;
		case BA_CANBUS_PORTERROR: 
			m_iPortError = linkObject;
			val.setUInt32(canb->NODEGUARD().get().interval());
			val.setUInt32(0);
			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
			linkObject->setValue(0, udt, OpcUa_False);
			break;
		case BA_CANBUS_PORTERRORDESCRIPTION: 
			m_sPortErrorDescription = linkObject;
			val.setString(UaString(""));
			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
			linkObject->setValue(0, udt, OpcUa_False);
			break;
		//case BA_CANBUS_PORT:
		//	val.setString(UaString(canb->port().c_str()));
		//	udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
		//	linkObject->setValue(0, udt, OpcUa_False);
		//	break;
		//case BA_CANBUS_TYPE:
		//	val.setString(UaString(canb->type().c_str()));
		//	udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
		//	linkObject->setValue(0, udt, OpcUa_False);

		//	break;
		//case BA_CANBUS_SPEED:
		//	val.setString(UaString(canb->speed().c_str()));
		//	udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
		//	linkObject->setValue(0, udt, OpcUa_False);

		//	break;

		default: 
			return OpcUa_Bad;
		}
		return OpcUa_Good;
	}

	UaVariant CanBusObject::getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf)
	{
		UaVariant val;
		val.clear();
		CANBUS *canb = (CANBUS *)conf;
		switch (code) {

		case BA_CANBUS_PORT:
			val.setString(UaString(canb->port().c_str()));
			break;
		case BA_CANBUS_TYPE:
			val.setString(UaString(canb->type().c_str()));
			break;
		case BA_CANBUS_SPEED:
			val.setString(UaString(canb->speed().c_str()));
			break;
		}
		return val;
	}

	void CanBusObject::setPortError(const int err, const char *errmsg, timeval &tv)
	{
		UaDataValue dataValue;
		UaDateTime udt,sdt;
		UaVariant val;
		UaStatus Status;

		udt = UaDateTime::fromTime_t(tv.tv_sec);	// Time from can message
		udt.addMilliSecs(tv.tv_usec/1000);
		sdt = UaDateTime::now();					// server time

		val.setInt32(err);
		dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
		Status = m_iPortError->setValue(NULL, dataValue, OpcUa_False);
		val.setString(errmsg);
		dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
		Status = m_sPortErrorDescription->setValue(NULL, dataValue, OpcUa_False);

	}

	void CanBusObject::waitData()
	{

		if (m_initNMTcommand == start)
			m_pCallback->waitAllData();
		else 
			m_pCallback->waitRTRData();
	}

}
