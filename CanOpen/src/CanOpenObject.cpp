#include "CanOpenObject.h"
//#include "BuildingAutomationTypeIds.h"
#include "Ids.h"
#include "CanObject.h"
#include "UaCanTrace.h"

namespace CanOpen
{
	#define gl_Timeout 2000;
	
//	using namespace AddressSpace;

	CanOpenObject::CanOpenObject(
		pUserDeviceStruct *par,
			::xml_schema::type *conf,UaNode *blink, int code
			)  : InterfaceUserDataBase(par,conf,blink,code),
//			m_waitMessage(0, (code == Ba_PDOType) ? ((((PDO *)conf)->numch() == 0) ? 1 : ((PDO *)conf)->numch()) : 1)
			m_waitMessage(0, 1)
	{
		m_buffer.resize(8);
//		m_iTimeout = 0;
	}

	CanOpenObject::~CanOpenObject(void)
	{
	}

	///
	/// check if message for this object (cobId)
	/// @param cmg can message structure
	///
	bool  CanOpenObject::isMsgForObject(const CanMsgStruct *cmg)
	{
		return (getCobId() == cmg->c_id);
	}
	
	CCanAccess *CanOpenObject::getCanBus()
	{ 
		return ((CanObject *)getParentDevice())->getCanBusInterface();
	}

	UaStatus CanOpenObject::waitOperation(OpcUa_UInt tout)
	{
		UaStatus ret = OpcUa_Good;

		while ( m_iMaxCount - m_iCount > 0) {
			OpcUa_StatusCode status = m_waitMessage.timedWait(tout);
			if (status ==  OpcUa_GoodNonCriticalTimeout) {
				LOG(Log::DBG, SdoMessage) << "bus=" << getBusName() << " node=" << std::hex << (int)getCobId() <<" status=Timeout expire " << std::dec << tout ;
				ret.setStatus(OpcUa_BadTimeout," Timeout expire");
				return ret;
			}
			else ret = status;
		}
		m_iCount = 0;
		return ret;
	}
	UaStatus CanOpenObject::waitOperation()
	{
		UaStatus ret = OpcUa_Good;
		OpcUa_UInt32 timeout = m_iTimeout > 0 ? m_iTimeout : gl_Timeout;
		ret = waitOperation(timeout);
		return ret;
	}

	UaStatus CanOpenObject::releaseOperation()
	{
		OpcUa_StatusCode status = m_waitMessage.timedWait(0);
		return status;
	}

}
