#include "IOManagerSDO.h"
#include "OpcServer.h"
#include "IOTransactionContextCanOpen.h"
#include "srvtrace.h"
#include "uabase.h"
#include "UaControlDeviceItem.h"
#include "CanSDOItem.h"

//extern unique_ptr<OpcServer> pServer ;
extern OpcServer* pServer;
using namespace AddressSpace;

namespace CanOpen
{
	/** construction
	* @param pCanNode is a pointer to the UaCanNodeObjec which the manager is belonged to
	*/
	IOManagerSDO::IOManagerSDO(UaCanNodeObject * pCanNode)
	{
		m_pCanNode = pCanNode;

		OpcUa_Boolean nEnab,bBlOnAdd;
		OpcUa_Int32	iMinThr =1,iMaxThr=10,iMaxJobs=20;
		OpcUa_UInt32	nTimeout;
		pServer->getServerConfig()->getStackThreadPoolSettings(nEnab,iMinThr,iMaxThr,iMaxJobs,bBlOnAdd,nTimeout);
		//cout << "ThreadPollParameters " << nEnab << " " << iMinThr << " " << iMaxThr << " " << iMaxJobs << " " << bBlOnAdd << " " << nTimeout << endl;
		m_pThreadPool = new UaThreadPool(iMinThr,iMaxThr);
		
		m_pSamplingEngine = new SamplingEngine();
	}

	/** destruction */
	IOManagerSDO::~IOManagerSDO()
	{
		delete m_pSamplingEngine;
		delete m_pThreadPool;
	}

	/** Start a transaction.
	*  @param pCallback Callback interface used for the transaction. The IOManager must use this interface to finish the action for each passed node in the transaction.
	*  @param serviceContext General context for the service calls containing information like the session object, return diagnostic mask and timeout hint.
	*  @param hTransaction Handle for the transaction used by the SDK to identify the transaction in the callbacks. This handle was passed in to the IOManager with the beginTransaction method.
	*  @param totalItemCountHint A hint for the IOManager about the total number of nodes in the transaction. The IOManager may not be responsible for all nodes but he can use this hint if he wants to optimize memory allocation.
	*  @param maxAge Max age parameter used only in the Read service.
	*  @param timestampsToReturn Indicates which timestamps should be returned in a Read or a Publish response. 
	*  The possible enum values are:
	*      OpcUa_TimestampsToReturn_Source
	*      OpcUa_TimestampsToReturn_Server
	*      OpcUa_TimestampsToReturn_Both
	*      OpcUa_TimestampsToReturn_Neither
	*  @param transactionType Type of the transaction. The possible enum values are:
	*      READ
	*      WRITE
	*      MONITOR_BEGIN
	*      MONITOR_MODIFY
	*      MONITOR_STOP
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @return Error code
	*/
	UaStatus IOManagerSDO::beginTransaction ( 
		IOManagerCallback*       pCallback,
		const ServiceContext&    serviceContext, // ToDo handle timeout hint and diagnostic info
		OpcUa_UInt32             hTransaction,
		OpcUa_UInt32             totalItemCountHint,
		OpcUa_Double             maxAge,
		OpcUa_TimestampsToReturn timestampsToReturn,
		TransactionType          transactionType,
		OpcUa_Handle&            hIOManagerContext)
	{
		//cout << "Begin Transuction" << endl;
		TRACE1_INOUT(SERVER_CORE, UA_T"--> IOManagerSDO::beginTransaction %d",transactionType);
		UaStatus      ret = OpcUa_Good;

		IOTransactionContextSDO* pTransaction = new IOTransactionContextSDO;
		pTransaction->setSession(serviceContext.pSession());
		pTransaction->m_pCallback          = pCallback;
		pTransaction->m_hTransaction       = hTransaction;
		pTransaction->m_maxAge             = maxAge;
		pTransaction->m_timestampsToReturn = timestampsToReturn;
		pTransaction->m_totalItemCountHint = totalItemCountHint;
		pTransaction->m_transactionType    = transactionType;
		pTransaction->m_nAsyncCount			= 0;
		pTransaction->m_pIOManager    = this;

		if (transactionType == IOManager::TransactionWrite)
		{
			pTransaction->m_arrWriteValues.create(totalItemCountHint);
		}
		if ((transactionType == IOManager::TransactionMonitorBegin) ||
			(transactionType == IOManager::TransactionMonitorModify) ||
			(transactionType == IOManager::TransactionMonitorStop) )
		{
			pTransaction->m_arrSamplingHandles.create(totalItemCountHint);
		}
		if ( (transactionType == IOManager::TransactionRead) || 
			(transactionType == IOManager::TransactionWrite) ||
			(transactionType == IOManager::TransactionMonitorBegin))
		{
			
			pTransaction->m_arrUaVariableHandles.create(totalItemCountHint);
		}
		pTransaction->m_arrCallbackHandles.create(totalItemCountHint);

		hIOManagerContext = (OpcUa_Handle)pTransaction;
		TRACE1_INOUT(SERVER_CORE, UA_T"<-- IOManagerSDO::beginTransaction [ret=0x%lx]", ret.statusCode());
		return ret;
	}

	/** Finish a transaction.
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @return Error code
	*/
	UaStatus IOManagerSDO::finishTransaction(OpcUa_Handle hIOManagerContext)
	{
		TRACE0_INOUT(SERVER_CORE, UA_T"--> IOManagerSDO::finishTransaction");
		UaStatus      ret = OpcUa_Good;
//		OpcUa_UInt32  mMaxPoolSize, mMumberOfJobsInPool;
		IOTransactionContextSDO* pTransaction = (IOTransactionContextSDO*)hIOManagerContext;

		if(pTransaction)
		{
//			cout << "Finish Transaction" << endl;

				IOManagerSDOJob* pIOManagerSDOJob = new IOManagerSDOJob(pTransaction);
				if (pTransaction->m_totalItemCountHint > pTransaction->m_nAsyncCount)
				{
					pTransaction->m_arrCallbackHandles.resize(pTransaction->m_nAsyncCount);
					if (pTransaction->m_arrUaVariableHandles.length() > 0)
					{
						pTransaction->m_arrUaVariableHandles.resize(pTransaction->m_nAsyncCount);
					}
					if (pTransaction->m_arrWriteValues.length() > 0)
					{
						pTransaction->m_arrWriteValues.resize(pTransaction->m_nAsyncCount);
					}
					if (pTransaction->m_arrSamplingHandles.length() > 0)
					{
						pTransaction->m_arrSamplingHandles.resize(pTransaction->m_nAsyncCount);
					}
				}
//				ret = m_pThreadPool->addJob(pIOManagerSDOJob,false,true);
				ret = m_pThreadPool->addJob(pIOManagerSDOJob,true, true);
				/*
			switch ( pTransaction->m_transactionType )
			{
			case IOManager::READ:
				{
					executeRead(pTransaction);
					break;
				}
			case IOManager::WRITE:
				{
					executeWrite(pTransaction);
					break;
				}
			case IOManager::MONITOR_BEGIN:
				{
					break;
				}
			case IOManager::MONITOR_MODIFY:
				{
					break;
				}
			case IOManager::MONITOR_STOP:
				{
					break;
				}
			default:
				break;
			}
			*/

		}

		TRACE1_INOUT(SERVER_CORE, UA_T"<-- IOManagerSDO::finishTransaction [ret=0x%lx]", ret.statusCode());
		return ret;
	}

	void IOManagerSDOJob::execute()
	{
		IOManagerSDO* currentIoManager = (IOManagerSDO *)m_pTransactionContext->m_pIOManager;
		switch ( m_pTransactionContext->m_transactionType )
		{
		case IOManager::TransactionRead:
			{
//			cout << "Read" << endl;
				currentIoManager->executeRead(m_pTransactionContext);
				break;
			}
		case IOManager::TransactionWrite:
			{
//			cout << "Write" << endl;
			currentIoManager->executeWrite(m_pTransactionContext);
				break;
			}
		case IOManager::TransactionMonitorBegin:
			{
			//cout << "Monitor Begin" << endl;

				currentIoManager->executeMonitorBegin(m_pTransactionContext);
				break;
			}
			case IOManager::TransactionMonitorModify:
			{
//				cout << "Monitor Modify" << endl;
				currentIoManager->executeMonitorModify(m_pTransactionContext);
				break;
			}
			case IOManager::TransactionMonitorStop:
			{
				//cout << "Monitor Stop" << endl;

				currentIoManager->executeMonitorStop(m_pTransactionContext);
				break;
			}
		default:
			break;
		}
	}
	
	/** Read attribute value of a node.
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @param callbackHandle Handle for the node in the callback. This handle was passed in to the IOManager with the beginModifyMonitoring method.
	*  @param pVariableHandle Variable handle provided by the NodeManager::getVariableHandle. 
	*      This object contains the information needed by the IOManager to identify the node to read in its context.
	*  @param pReadValueId Context for the node to read. The context contains the IndexRange and the DataEncoding requested by the client. The other parameters of this context are already handled by the VariableHandle.
	*  @return Error code
	*/
	UaStatus IOManagerSDO::beginRead ( 
		OpcUa_Handle       hIOManagerContext,
		OpcUa_UInt32       callbackHandle,
		VariableHandle*    pVariableHandle,
		OpcUa_ReadValueId* pReadValueId) // ToDo handle index range
	{
		OpcUa_ReferenceParameter(pReadValueId);
		UaStatus    ret  = OpcUa_Good;
		UaDataValue   uaDataValue;

		IOTransactionContextSDO* pTransaction = (IOTransactionContextSDO*)hIOManagerContext;
		
		if(pTransaction)
		{
//			cout << "Begin Read" << endl;

			if ( pTransaction->m_transactionType == IOManager::TransactionRead )
			{
				pTransaction->m_arrCallbackHandles[pTransaction->m_nAsyncCount] = callbackHandle;
				pTransaction->m_arrUaVariableHandles[pTransaction->m_nAsyncCount] = (VariableHandleUaNode*)pVariableHandle;
				pVariableHandle->addReference();
				pTransaction->m_nAsyncCount++;
			}
			else
			{
				UA_ASSERT(false);
				ret = OpcUa_BadNotReadable;
			}
		}
		else
		{
			UA_ASSERT(false);
			ret = OpcUa_BadInvalidArgument;
		}

		return ret;
	}
	/** Write attribute value of a node.
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @param callbackHandle Handle for the node in the callback. This handle was passed in to the IOManager with the beginModifyMonitoring method.
	*  @param pVariableHandle Variable handle provided by the NodeManager::getVariableHandle. 
	*      This object contains the information needed by the IOManager to identify the node to read in its context.
	*  @param pWriteValue Context for the node to write. The context contains the IndexRange requested by the client. The NodeId and Attribute parameters of this context are already handled by the VariableHandle
	*  @return Error code
	*/
	UaStatus IOManagerSDO::beginWrite ( 
		OpcUa_Handle      hIOManagerContext,
		OpcUa_UInt32      callbackHandle,
		VariableHandle*   pVariableHandle,
		OpcUa_WriteValue* pWriteValue)
	{
		UaStatus      ret = OpcUa_Good;

		IOTransactionContextSDO* pTransaction = (IOTransactionContextSDO*)hIOManagerContext;

		if(pTransaction)
		{
//			cout << "Begin Write" << endl;

			if (pTransaction->m_transactionType == IOManager::TransactionWrite)
			{
				pTransaction->m_arrCallbackHandles[pTransaction->m_nAsyncCount] = callbackHandle;
				pTransaction->m_arrUaVariableHandles[pTransaction->m_nAsyncCount] = (VariableHandleUaNode*)pVariableHandle;
				pTransaction->m_arrWriteValues[pTransaction->m_nAsyncCount] = &pWriteValue->Value;
				pVariableHandle->addReference();
				pTransaction->m_nAsyncCount++;
			}
			else
			{
				UA_ASSERT(false);
				ret = OpcUa_BadNotWritable;
			}
		}
		else
		{
			assert(false);
			ret = OpcUa_BadInvalidArgument;
		}

		return ret;
	}
	/** Start monitoring of an item.
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @param callbackHandle Handle for the node in the callback. This handle was passed in to the IOManager with the beginModifyMonitoring method.
	*  @param pIOVariableCallback Callback interface used for data change callbacks to the MonitoredItem managed by the SDK.
	*  @param pVariableHandle Variable handle provided by the NodeManager::getVariableHandle. 
	*      This object contains the information needed by the IOManager to identify the node to read in its context.
	*  @param monitoringContext handle for the monitoring context.
	*  @return Error code
	*/
	UaStatus IOManagerSDO::beginStartMonitoring(
		OpcUa_Handle        hIOManagerContext,
		OpcUa_UInt32        callbackHandle,
		IOVariableCallback* pIOVariableCallback,
		VariableHandle*     pVariableHandle,
		MonitoringContext&  monitoringContext)
	{
	//	OpcUa_ReferenceParameter(hIOManagerContext);
	//	OpcUa_ReferenceParameter(callbackHandle);
	//	OpcUa_ReferenceParameter(pIOVariableCallback);
	//	OpcUa_ReferenceParameter(pVariableHandle);
	//	OpcUa_ReferenceParameter(monitoringContext);
	//	return OpcUa_BadNotSupported;
	//}

	

		UaStatus ret = OpcUa_Good;
		TRACE1_INOUT(SERVER_CORE, UA_T"--> IOManagerSDO::startMonitoring %d",(OpcUa_UInt32)monitoringContext.samplingInterval);

		IOTransactionContextSDO* pTransaction = (IOTransactionContextSDO*)hIOManagerContext;

		// OPC Data Access does not support absolute deadband
		if ( monitoringContext.isAbsoluteDeadband != OpcUa_False )
		{
			monitoringContext.sdkMustHandleAbsoluteDeadband = OpcUa_True;
		}

		if(pTransaction)
		{
	//		cout << "Begin Start Monitoring" << endl;

			if ( pTransaction->m_transactionType == IOManager::TransactionMonitorBegin )
			{

				pTransaction->m_arrCallbackHandles[pTransaction->m_nAsyncCount] = callbackHandle;
				pTransaction->m_arrUaVariableHandles[pTransaction->m_nAsyncCount] = (VariableHandleUaNode*)pVariableHandle;
				pVariableHandle->addReference();
				// Create the management object in this IOManager for the monitored item
				SamplingSDO* pSampleSDOItem = new SamplingSDO();
//				pSampleSDOItem->m_nSamplingInterval = (OpcUa_UInt32)monitoringContext.samplingInterval;

//				pSampleSDOItem->m_nSamplingInterval = (OpcUa_UInt32)monitoringContext.samplingInterval;
//				cout << "Sampling " << pSampleSDOItem->m_nSamplingInterval << endl;
				// Assign right subscription based on sampling interval

				pSampleSDOItem->m_pUaCanSDOItem = (UaControlDeviceItem *)((VariableHandleUaNode*)pVariableHandle)->pUaNode();
				pSampleSDOItem->m_pIOVariableCallback = pIOVariableCallback;
//				pSampleSDOItem->m = &m_mutexSDO;

				pTransaction->m_arrSamplingHandles[pTransaction->m_nAsyncCount] = m_handlesSampleItems.add(pSampleSDOItem);

				pTransaction->m_nAsyncCount++;
			}
			else
			{
				UA_ASSERT(false);
				ret = OpcUa_BadInternalError;
			}
		}
		else
		{
			assert(false);
			ret = OpcUa_BadInvalidArgument;
		}

		return ret;
	
	}
	
	/** Notify IOManager after modifying monitoring parameters of an item.
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @param callbackHandle Handle for the node in the callback. This handle was passed in to the IOManager with the beginModifyMonitoring method.
	*  @param hIOVariable The handle of the variable in the IOManager created with beginStartMonitoring. 
	*      The handle was passed to the SDK in the callback finishStartMonitoring.
	*  @param monitoringContext handle for the monitoring context.
	*  @return Error code
	*/
	UaStatus IOManagerSDO::beginModifyMonitoring(
		OpcUa_Handle       hIOManagerContext,
		OpcUa_UInt32       callbackHandle,
		OpcUa_UInt32       hIOVariable,
		MonitoringContext& monitoringContext)
	{
	//	OpcUa_ReferenceParameter(hIOManagerContext);
	//	OpcUa_ReferenceParameter(callbackHandle);
	//	OpcUa_ReferenceParameter(hIOVariable);
	//	OpcUa_ReferenceParameter(monitoringContext);
	//	return OpcUa_BadNotSupported;
	//}

	

		UaStatus ret = OpcUa_Good;

		IOTransactionContextSDO* pTransaction = (IOTransactionContextSDO*)hIOManagerContext;

		if ( monitoringContext.isAbsoluteDeadband != OpcUa_False )
		{
			monitoringContext.sdkMustHandleAbsoluteDeadband = OpcUa_True;
		}

		if(pTransaction)
		{
		//	cout << "Begin Modify Monitoring" << endl;

			if ( pTransaction->m_transactionType == IOManager::TransactionMonitorModify )
			{
				pTransaction->m_arrCallbackHandles[pTransaction->m_nAsyncCount] = callbackHandle;
				pTransaction->m_arrSamplingHandles[pTransaction->m_nAsyncCount] = hIOVariable;
				pTransaction->m_nAsyncCount++;
				SamplingSDO* pSampleSDOItem = m_handlesSampleItems.get(hIOVariable);
				// Assign right subscription based on sampling interval

				m_pSamplingEngine->removeItemFromSampling(pSampleSDOItem,pSampleSDOItem->m_nSamplingInterval);

//				pSampleSDOItem->m_nSamplingInterval = (OpcUa_UInt32)monitoringContext.samplingInterval;


			}
			else
			{
				UA_ASSERT(false);
				ret = OpcUa_BadInternalError;
			}
		}
		else
		{
			assert(false);
			ret = OpcUa_BadInvalidArgument;
		}

		return ret;

	}

	/** Notify IOManager after stopping monitoring of an item.
	*  @param hIOManagerContext IOManager handle for the transaction context. This handle is used to identify the transaction in the IOManager.
	*  @param callbackHandle Handle for the node in the callback. This handle was passed in to the IOManager with the beginModifyMonitoring method.
	*  @param hIOVariable The handle of the variable in the IOManager created with beginStartMonitoring. 
	*      The handle was passed to the SDK in the callback finishStartMonitoring.
	*  @return Error code
	*/

	UaStatus IOManagerSDO::beginStopMonitoring(
		OpcUa_Handle   hIOManagerContext,
		OpcUa_UInt32   callbackHandle,
		OpcUa_UInt32   hIOVariable)
	{
	//	OpcUa_ReferenceParameter(hIOManagerContext);
	//	OpcUa_ReferenceParameter(callbackHandle);
	//	OpcUa_ReferenceParameter(hIOVariable);
	//	return OpcUa_BadNotSupported;
	//}

		UaStatus ret = OpcUa_Good;

		IOTransactionContextSDO* pTransaction = (IOTransactionContextSDO*)hIOManagerContext;

		if(pTransaction)
		{
			//cout << "Begin sop monitoring" << endl;

			if ( pTransaction->m_transactionType == IOManager::TransactionMonitorStop )
			{
				pTransaction->m_arrCallbackHandles[pTransaction->m_nAsyncCount] = callbackHandle;
				pTransaction->m_arrSamplingHandles[pTransaction->m_nAsyncCount] = hIOVariable;
				pTransaction->m_nAsyncCount++;
			}
			else
			{
				UA_ASSERT(false);
				ret = OpcUa_BadInternalError;
			}
		}
		else
		{
			assert(false);
			ret = OpcUa_BadInvalidArgument;
		}

		return ret;

	}

	void IOManagerSDO::executeRead(IOTransactionContextSDO* m_pTransactionContext)
	{
		UaDataValue	dataValue;
		//cout << "Exicute read" << endl;

		for( OpcUa_UInt32 i = 0; i < m_pTransactionContext->m_arrUaVariableHandles.length(); i++)
		{
			UaStatus ret;
			VariableHandleUaNode *vhuanode = m_pTransactionContext->m_arrUaVariableHandles[i];
			UaControlDeviceItem *csdo = (UaControlDeviceItem *)vhuanode->pUaNode();

			if (csdo->accessLevel() & Ua_AccessLevel_CurrentRead) {
				ret  = csdo->read();
				if (ret.isGood()) {
					csdo->getAttributeValue(m_pTransactionContext->pSession(),vhuanode->m_AttributeID,NULL,dataValue);
					TRACE2_INOUT(SERVER_CORE, UA_T"<-- IOManagerSDO::Read %s [value=%s]",csdo->displayName(m_pTransactionContext->pSession()).toFullString().toUtf8() ,UaVariant(*dataValue.value()).toFullString().toUtf8() );
				}
				else {
					dataValue.setStatusCode(ret.statusCode());
					dataValue.setServerTimestamp(UaDateTime::now());
					dataValue.setSourceTimestamp(UaDateTime::now());
				}
			}
			else {
				dataValue.setStatusCode(OpcUa_BadNotReadable);
				dataValue.setServerTimestamp(UaDateTime::now());
				dataValue.setSourceTimestamp(UaDateTime::now());
			}
			m_pTransactionContext->m_pCallback->finishRead(
				m_pTransactionContext->m_hTransaction,
				m_pTransactionContext->m_arrCallbackHandles[i],
				dataValue,
				OpcUa_True,
				OpcUa_True);
		}
	}

	void IOManagerSDO::executeWrite(IOTransactionContextSDO* m_pTransactionContext)
	{
		UaDataValue	dataValue;
		//cout << "Exicute Write" << endl;

		for (OpcUa_UInt32 i = 0; i < m_pTransactionContext->m_arrUaVariableHandles.length(); i++)
		{
			UaStatus ret;
			VariableHandleUaNode *vhuanode = m_pTransactionContext->m_arrUaVariableHandles[i];
			UaControlDeviceItem *csdo = (UaControlDeviceItem *)vhuanode->pUaNode();;
			dataValue.setValue(m_pTransactionContext->m_arrWriteValues[i]->Value, OpcUa_False);

			if (csdo->accessLevel() & Ua_AccessLevel_CurrentWrite) {

				ret = csdo->write(dataValue);
				if (ret.isGood()) {
					TRACE2_INOUT(SERVER_CORE, UA_T"<-- IOManagerSDO::write successful %s [value=%s]", csdo->displayName(m_pTransactionContext->pSession()).toFullString().toUtf8(), UaVariant(*dataValue.value()).toFullString().toUtf8());
				}
				else {
					dataValue.setStatusCode(ret.statusCode());
					dataValue.setServerTimestamp(UaDateTime::now());
					dataValue.setSourceTimestamp(UaDateTime::now());
				}

			}
			else {
				dataValue.setStatusCode(OpcUa_BadNotWritable);
				dataValue.setServerTimestamp(UaDateTime::now());
				dataValue.setSourceTimestamp(UaDateTime::now());
			}

			m_pTransactionContext->m_pCallback->finishWrite(
				m_pTransactionContext->m_hTransaction,
				m_pTransactionContext->m_arrCallbackHandles[i],
				ret,
				OpcUa_True);

		}
	}
	
	void IOManagerSDO::executeMonitorBegin(IOTransactionContextSDO* pTransactionContext)
	{
		OpcUa_UInt32         count       = pTransactionContext->m_nAsyncCount;
		SamplingSDO*		pSampleItem;

		OpcUa_StatusCode	retCode =	OpcUa_BadInternalError;
		// Assign items to different subscriptions
		UaMutexLocker lock(&m_mutex);
		//cout << "Execute Monitoring Begin" << endl;

		for( OpcUa_UInt32 i = 0; i < count; i++)
		{
			// Get the sample item
			pSampleItem = m_handlesSampleItems.get(pTransactionContext->m_arrSamplingHandles[i]);
			OpcUa_UInt32 cSample = pSampleItem->m_nSamplingInterval;
			pSampleItem->m_nSamplingInterval = def_sample * cSample;

			if ( pSampleItem != NULL ) {
				m_pSamplingEngine->addItemToSampling(pSampleItem,pSampleItem->m_nSamplingInterval);
				retCode = OpcUa_Good;
			}
			pTransactionContext->m_pCallback->finishStartMonitoring(
					pTransactionContext->m_hTransaction,
					pTransactionContext->m_arrCallbackHandles[i],
					pTransactionContext->m_arrSamplingHandles[i],
					0,
					OpcUa_False,
					UaDataValue(),
					retCode);
		}
		lock.unlock();

	}

	void IOManagerSDO::executeMonitorModify(IOTransactionContextSDO* pTransactionContext)
	{
		OpcUa_UInt32        i;
		OpcUa_UInt32        count = pTransactionContext->m_nAsyncCount;
		SamplingSDO*		pSampleItem;
		OpcUa_StatusCode	retCode =	OpcUa_BadInternalError;

		// Assign items to different subscriptions
		UaMutexLocker lock(&m_mutex);
		//cout << "Execute Monitoring Modify" << endl;

		for ( i=0; i < count; i++ )
		{
			// Get the sample item
			pSampleItem = m_handlesSampleItems.get(pTransactionContext->m_arrSamplingHandles[i]);
			if ( pSampleItem != NULL )
			{
				pSampleItem = m_handlesSampleItems.get(pTransactionContext->m_arrSamplingHandles[i]);
				OpcUa_UInt32 cSample = pSampleItem->m_nSamplingInterval;
				pSampleItem->m_nSamplingInterval = def_sample * cSample;

				m_pSamplingEngine->addItemToSampling(pSampleItem,pSampleItem->m_nSamplingInterval);
				retCode  = OpcUa_Good;
			}
			pTransactionContext->m_pCallback->finishStopMonitoring(
					pTransactionContext->m_hTransaction,
					pTransactionContext->m_arrCallbackHandles[i],
					retCode);

		}
		lock.unlock();
	}

	void IOManagerSDO::executeMonitorStop(IOTransactionContextSDO* pTransactionContext)
	{
		OpcUa_UInt32        i;
		OpcUa_UInt32        count = pTransactionContext->m_nAsyncCount;
		SamplingSDO*		pSampleItem;
		OpcUa_StatusCode	retCode =	OpcUa_BadInternalError;
		//cout << "Execute monitorin Stop" << endl;

		// Assign items to different subscriptions
		UaMutexLocker lock(&m_mutex);
		for ( i=0; i < count; i++ )
		{
			// Get the sample item
			pSampleItem = m_handlesSampleItems.get(pTransactionContext->m_arrSamplingHandles[i]);
			if ( pSampleItem != NULL )
			{
				pSampleItem = m_handlesSampleItems.get(pTransactionContext->m_arrSamplingHandles[i]);
				m_pSamplingEngine->removeItemFromSampling(pSampleItem,pSampleItem->m_nSamplingInterval);
				m_handlesSampleItems.remove(pTransactionContext->m_arrSamplingHandles[i]);
				retCode  = OpcUa_Good;
			}
			pTransactionContext->m_pCallback->finishStopMonitoring(
					pTransactionContext->m_hTransaction,
					pTransactionContext->m_arrCallbackHandles[i],
					retCode);
		}
		lock.unlock();
	}
	
}
