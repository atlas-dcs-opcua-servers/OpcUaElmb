/******************************************************************************
** UaCanTrace.cpp
**
** Project: C++ OPC SDK base module
**
** Portable trace functionality class.
**
******************************************************************************/
#include "UaCanTrace.h"
#include "CanBusAccess.h"
//#include <stdarg.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include "opcua_core.h"
//#include "opcua_thread.h"
//#include "LogIt/LogIt.h"

	//FILE           * CanOpen::UaCanTrace::s_pfCanTrace             = NULL;
	//UaString*        CanOpen::UaCanTrace::s_pCanAppName            = NULL;
	//UaString*        CanOpen::UaCanTrace::s_pCanTraceFile          = NULL;
	//UaMutex        * CanOpen::UaCanTrace::s_pCanLock               = NULL;
	//unsigned int     CanOpen::UaCanTrace::s_nCanMaxTraceEntries    = 100000;
	//unsigned int     CanOpen::UaCanTrace::s_nCanNumBackupFiles     = 5;
	//unsigned int     CanOpen::UaCanTrace::s_nCanCountTraceEntries  = 0;
	//bool             CanOpen::UaCanTrace::s_IsLocalTimeCanTrace    = true;
	//bool             CanOpen::UaCanTrace::s_PrintDateInCanTrace    = true;
	//OpcUa_UInt32     CanOpen::UaCanTrace::s_traceFunction			= 0;

namespace CanOpen
{
	std::map<int,Log::LogComponentHandle> UaCanTrace::m_CanTraceComponent;
	std::map<string, int > UaCanTrace::m_logLevel;

	/** construction */
	UaCanTrace::UaCanTrace (
		const UaNodeId&    nodeId,
		const UaString&    name,
		OpcUa_UInt16       browseNameNameSpaceIndex,
		const UaVariant&   initialValue,
		OpcUa_Byte         accessLevel,
		NodeManagerConfig* pNodeConfig
		) : OpcUa::BaseVariableType(nodeId, name, pNodeConfig->getNameSpaceIndex(),
		initialValue, Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite,		pNodeConfig)

	{
		setValueHandling(UaVariable_Value_Cache);
	}
	
	void UaCanTrace::initTraceComponent(CanOpenOpcServerConfig::StandardMetaData_optional inter)
	{
		m_logLevel["PdoMessage"] = lPdoMessage;
		m_logLevel["SdoMessage"] = lSdoMessage;
		m_logLevel["SegSdoMessage"] = lSegSdoMessage;
		m_logLevel["NGMessage"] = lNGMessage;
		m_logLevel["EmergMessage"] = lEmergMessage;
		m_logLevel["NMTMessage"] = lNMTMessage;
		m_logLevel["CanTrace"] = lCanTrace;
		m_logLevel["Pdo1Message"] = lPdo1Message;
		m_logLevel["Pdo2Message"] = lPdo2Message;
		m_logLevel["Pdo3Message"] = lPdo3Message;
		m_logLevel["Pdo4Message"] = lPdo4Message;

		Log::initializeLogging(Log::INF);
		const std::string dbgname = "NoTrace";

		m_CanTraceComponent[NoTrace] = Log::registerLoggingComponent(dbgname, Log::INF);
		m_CanTraceComponent[lPdoMessage] = Log::registerLoggingComponent("PDO Message Trace", Log::INF);
		m_CanTraceComponent[lSdoMessage] = Log::registerLoggingComponent("SDO Message Trace", Log::INF);
		m_CanTraceComponent[lSegSdoMessage] = Log::registerLoggingComponent("Segmented SDO Trace", Log::INF);
		m_CanTraceComponent[lNGMessage] = Log::registerLoggingComponent("Node guarding Trace", Log::INF);
		m_CanTraceComponent[lEmergMessage] = Log::registerLoggingComponent("Emergency message Trace", Log::INF);
		m_CanTraceComponent[lNMTMessage] = Log::registerLoggingComponent("NMT Trace", Log::INF);
		m_CanTraceComponent[lCanTrace] = Log::registerLoggingComponent("CanModule", Log::INF);
		m_CanTraceComponent[lPdo1Message] = Log::registerLoggingComponent("PDO1 Message Trace", Log::INF);
		m_CanTraceComponent[lPdo2Message] = Log::registerLoggingComponent("PDO2 Message Trace", Log::INF);
		m_CanTraceComponent[lPdo3Message] = Log::registerLoggingComponent("PDO3 Message Trace", Log::INF);
		m_CanTraceComponent[lPdo4Message] = Log::registerLoggingComponent("PDO4 Message Trace", Log::INF);
		if (inter) {
			for (auto &itl : inter->Log())
			{
				for (auto &it : itl.ComponentLogLevels())
				{
					for (auto &itt : it.ComponentLogLevel())
					{
						Log::LOG_LEVEL lll;
						Log::logLevelFromString(itt.logLevel().data(), lll);
						Log::LogComponentHandle lch = m_CanTraceComponent[m_logLevel[itt.componentName()]];
						Log::setComponentLogLevel(lch, lll);
					}
				}
			}
		}
	}
	
	/** destruction */
	UaCanTrace::~UaCanTrace()
	{
//		s_pCanLock = 0;
	}
/*
	UaStatus UaCanTrace::setValue(Session *pSession, const UaDataValue& dataValue, OpcUa_Boolean checkAccessLevel)
	{
		UaStatus ret;
		ret = OpcUa::BaseVariableType::setValue(pSession, dataValue, checkAccessLevel);
		putTraceFunctions(dataValue.value()->Value.Int32);
		return ret;
	}
	*/
	/** Initialize the trace.
	*  @param maxTraceEntries the maximum of trace entries.
	*  @param NumBackupFiles the number of all backup files.
	*  @param traceFile the file where the trace will be printed out.
	*  @param appName the application name.
	*  @return the initialized trace.
	*/
	//long UaCanTrace::initCanTrace(
	//	unsigned int    maxTraceEntries,
	//	unsigned int    NumBackupFiles,
	//	const UaString& traceFile,
	//	const UaString & appName)
	//{
	//	if(s_pfCanTrace)
	//	{
	//		return -1;
	//	}

	//	if(s_pCanLock == NULL)
	//	{
	//		s_pCanLock = new UaMutex;
	//	}
	//	UaMutexLocker lock(s_pCanLock);

	//	if ( s_pCanAppName )
	//	{
	//		*s_pCanAppName = appName;
	//	}
	//	else
	//	{
	//		s_pCanAppName = new UaString(appName);
	//	}
	//	if ( s_pCanTraceFile )
	//	{
	//		*s_pCanTraceFile = traceFile;
	//	}
	//	else
	//	{
	//		s_pCanTraceFile = new UaString(traceFile);
	//	}
	//	s_nCanMaxTraceEntries     = maxTraceEntries;
	//	s_nCanNumBackupFiles      = NumBackupFiles;
	//	s_nCanCountTraceEntries   = 2;
	//	s_traceFunction	= 0;
	//	backupTrace();

	//	if( s_pfCanTrace == NULL)
	//	{
	//		return -1;
	//	}

	//	return 0;
	//}

	/** Change the trace settings.
	*  @param maxTraceEntries the maximum number of trace entries.
	*  @param NumBackupFiles the number of all backup files.
	*  @param traceFile the file where the trace will be printed in.
	*  @return the trace settings.
	*/
	//long UaCanTrace::changeCanTrace( 
	//	unsigned int    maxTraceEntries,
	//	unsigned int    NumBackupFiles,
	//	const UaString& traceFile)
	//{
	//	if(s_pfCanTrace == NULL || s_pCanTraceFile == NULL)
	//	{
	//		return -1;
	//	}
	//	if(s_pCanLock == NULL)
	//	{
	//		s_pCanLock = new UaMutex;
	//	}
	//	UaMutexLocker lock(s_pCanLock);

	//	s_nCanMaxTraceEntries     = maxTraceEntries;

	//	s_nCanNumBackupFiles      = NumBackupFiles;

	//	if( traceFile != *s_pCanTraceFile )
	//	{
	//		fclose(s_pfCanTrace);
	//		s_pfCanTrace = NULL;

	//		*s_pCanTraceFile = traceFile;

	//		if( (s_pfCanTrace = fopen( traceFile.toUtf8(), "w")) == NULL)
	//		{
	//			return -1;
	//		}

	//		printHeader(s_pfCanTrace);
	//	}

	//	return 0;
	//}
	/** Get the trace level.
	*  @return the trace level.
	*/
	//OpcUa_UInt32 UaCanTrace::getTraceLevel()
	//{
	//	return s_traceFunction;
	//}

	/** Set the trace function.
	*  
	*/
	//void UaCanTrace::setTraceFunction(CanTraceFunction ctf)
	//{
	//	s_traceFunction = s_traceFunction | ctf;
	//}

	/** Set the trace function.
	*  
	*/
	void UaCanTrace::putTraceFunctions(OpcUa_UInt32 ctf)
	{
		for (auto& it : UaCanTrace::m_CanTraceComponent) {
			if (ctf & it.first)
				Log::setComponentLogLevel(it.second, Log::TRC);
			else
				Log::setComponentLogLevel(it.second, Log::INF);
		}
	}

	/** Set time output for trace to local time.
	*  @param isLocal true if local false if not.
	*/
	//void UaCanTrace::setLocalTimeOutput(bool isLocal)
	//{
	//	s_IsLocalTimeCanTrace = isLocal;
	//}

	/** Set time output for trace to print also the date.
	*  @param printDateInTrace true if print date in trace false if not.
	*/
	//void UaCanTrace::setPrintDateInTrace(bool printDateInTrace)
	//{
	//	s_PrintDateInCanTrace = printDateInTrace;
	//}

	/** Error trace output.
	*  @param fmt the message to be printed out
	*/
	//void UaCanTrace::tPdoMessage(const char * fmt,...)
	//{
	//	UaMutexLocker lock(s_pCanLock);
	//	if( s_traceFunction & PdoMessage ) {
	//		fprintf( s_pfCanTrace, "Pdo Message " /*,pTraceCanBus->browseName().toString().toUtf8()*/ );
	//		va_list arg_ptr;
	//		va_start(arg_ptr, fmt);
	//		trace(fmt, arg_ptr);
	//		va_end(arg_ptr);
	//	}
	//}

	/** Warning trace output.
	*  @param fmt the message to be printed out
	*/
	//void UaCanTrace::tSdoMessage(const char * fmt,...)
	//{
	//	UaMutexLocker lock(s_pCanLock);
	//	if( s_traceFunction & SdoMessage ) {
	//		fprintf( s_pfCanTrace, "Sdo Message "/*,pTraceCanBus->browseName().toString().toUtf8()*/ );
	//		va_list arg_ptr;
	//		va_start(arg_ptr, fmt);
	//		trace(fmt, arg_ptr);
	//	va_end(arg_ptr);
	//	}
	//}

	/** Info trace output.
	*  @param fmt the message to be printed out
	*/
	//void UaCanTrace::tSegSdoMessage(const char * fmt,...)
	//{
	//	if( s_traceFunction & SegSdoMessage ) {
	//		UaMutexLocker lock(s_pCanLock);
	//		fprintf( s_pfCanTrace, "SegSdo Message "/*,pTraceCanBus->browseName().toString().toUtf8() */);
	//		va_list arg_ptr;
	//		va_start(arg_ptr, fmt);
	//		trace( fmt, arg_ptr);
	//		va_end(arg_ptr);
	//	}
	//}

	/** External interface call trace output.
	*  @param fmt the message to be printed out
	*/
	//void UaCanTrace::tNGMessage(const char * fmt,...)
	//{
	//	if( s_traceFunction & NGMessage ) {
	//		fprintf( s_pfCanTrace, "NG Message "/*,pTraceCanBus->browseName().toString().toUtf8() */);
	//		UaMutexLocker lock(s_pCanLock);
	//		va_list arg_ptr;
	//		va_start(arg_ptr, fmt);
	//		trace( fmt, arg_ptr);
	//		va_end(arg_ptr);
	//	}
	//}

	/** Ctor and Dtor trace output.
	*  @param fmt the message to be printed out
	*/
	//void UaCanTrace::tEmergMessage(const char * fmt,...)
	//{
	//	if( s_traceFunction & EmergMessage ) {
	//		fprintf( s_pfCanTrace, "Emerg Message "/*,pTraceCanBus->browseName().toString().toUtf8()*/ );
	//		UaMutexLocker lock(s_pCanLock);
	//		va_list arg_ptr;
	//		va_start(arg_ptr, fmt);
	//		trace( fmt, arg_ptr);
	//		va_end(arg_ptr);
	//	}
	//}

	/** File trace output preparation.
	*  @param fmt the message to be printed out
	*  @param arg_ptr a pointer to the arguments.
	*/
	//void UaCanTrace::trace( const char  * fmt,
	//	va_list       arg_ptr)
	//{
	//	char    strTemp[1900];
	//	OpcUa_StringA_vsnprintf(strTemp, 1900, (const OpcUa_StringA)fmt, arg_ptr);
	//	strTemp[1899] = 0;
	//	traceOutput( strTemp);
	//}

	/** File trace output.
	* @param sContent the message to be printed out
	* @param nModule module nimber (not use)
	*/
	//void UaCanTrace::traceOutput(const char * sContent, int nModule)
	//{
	//	OpcUa_ReferenceParameter(nModule);
	//	char    strTemp[2000];

	//	// time information
	//	//
	//	UaDateTime time = UaDateTime::now();

	//	s_nCanCountTraceEntries++;
	//	if(s_IsLocalTimeCanTrace)
	//	{
	//		if(s_PrintDateInCanTrace)
	//		{
	//			OpcUa_StringA_snprintf(strTemp, 2000, (OpcUa_StringA)"%s %s|%04X* %s\n", time.toDateString().toUtf8(), time.toTimeString().toUtf8(),  OpcUa_Thread_GetCurrentThreadId(), sContent);
	//			//fprintf( s_pfTrace, "%s %s|%d|%04X* %s\n", time.toDateString().toUtf8(), time.toTimeString().toUtf8(), traceLevel, OpcUa_Thread_GetCurrentThreadId(), sContent);
	//		}
	//		else
	//		{
	//			OpcUa_StringA_snprintf(strTemp, 2000, (OpcUa_StringA)"%s|%04X* %s\n", time.toTimeString().toUtf8(),  OpcUa_Thread_GetCurrentThreadId(), sContent);
	//			//fprintf( s_pfTrace, "%s|%d|%04X* %s\n", time.toTimeString().toUtf8(), traceLevel, OpcUa_Thread_GetCurrentThreadId(), sContent);
	//		}
	//	}
	//	else
	//	{
	//		if(s_PrintDateInCanTrace)
	//		{
	//			OpcUa_StringA_snprintf(strTemp, 2000, (OpcUa_StringA)"%s %s|%04X* %s\n", time.toDateString().toUtf8(), time.toTimeString().toUtf8(),  OpcUa_Thread_GetCurrentThreadId(), sContent);
	//			//fprintf( s_pfTrace, "%s %s|%d|%04X* %s\n", time.toDateString().toUtf8(), time.toTimeString().toUtf8(), traceLevel, OpcUa_Thread_GetCurrentThreadId(), sContent);
	//		}
	//		else
	//		{
	//			OpcUa_StringA_snprintf(strTemp, 2000, (OpcUa_StringA)"%s|%04X* %s\n", time.toTimeString().toUtf8(),  OpcUa_Thread_GetCurrentThreadId(), sContent);
	//		}
	//	}
	//	strTemp[1999] = 0;
	//	fprintf( s_pfCanTrace, "%s", strTemp);
	//	fflush( s_pfCanTrace);

	//	if( s_nCanCountTraceEntries >= s_nCanMaxTraceEntries )
	//	{
	//		backupTrace();
	//	}
	//}

	/** Close trace file. */
	//void UaCanTrace::closeTrace()
	//{
	//	if(s_pCanLock == NULL)
	//	{
	//		s_pCanLock = new UaMutex;
	//	}

	//	s_pCanLock->lock();
	//	if (s_pfCanTrace)
	//	{
	//		fclose( s_pfCanTrace);
	//		s_pfCanTrace = NULL;
	//	}

	//	s_pCanLock->unlock();
	//	if(s_pCanLock)
	//	{
	//		delete s_pCanLock;
	//		s_pCanLock = NULL;
	//	}

	//	if ( s_pCanAppName )
	//	{
	//		delete s_pCanAppName;
	//		s_pCanAppName = NULL;
	//	}
	//	if ( s_pfCanTrace )
	//	{
	//		delete s_pfCanTrace;
	//		s_pfCanTrace = NULL;
	//	}
	//	if ( s_pCanTraceFile ) 
	//	{
	//		delete s_pCanTraceFile;
	//		s_pCanTraceFile = NULL;
	//	}
	//}

	/** Copy the trace file to a backup file.
	*  @param oldName the old name to overwrite.
	*  @param newName the new name.
	*/
	//void UaCanTrace::copyFile(const UaString& oldName, const UaString& newName)
	//{
	//	UA_unlink(newName.toUtf8());
	//	Ua_rename(oldName.toUtf8(), newName.toUtf8());
	//}

	/** Backup error trace file. */
	//void UaCanTrace::backupTrace()
	//{
	//	if(s_pfCanTrace)
	//	{
	//		fclose(s_pfCanTrace);
	//		s_pfCanTrace = NULL;
	//	}

	//	s_nCanCountTraceEntries = 2;

	//	UaString     sOldFile;
	//	UaString     sNewFile;
	//	unsigned int i;

	//	for ( i=s_nCanNumBackupFiles; i>1; i--)
	//	{
	//		buildBackupFileName(*s_pCanTraceFile, i-1, sOldFile);
	//		buildBackupFileName(*s_pCanTraceFile, i, sNewFile);
	//		copyFile(sOldFile, sNewFile);
	//	}

	//	buildBackupFileName(*s_pCanTraceFile, 1, sNewFile);
	//	copyFile(*s_pCanTraceFile, sNewFile);

	//	if( (s_pfCanTrace = fopen( s_pCanTraceFile->toUtf8(), "w")) == NULL)
	//	{
	//		s_pfCanTrace = NULL;
	//	}
	//	else
	//	{
	//		printHeader(s_pfCanTrace);
	//	}
	//}

	/** Print header of a new file.
	*  @param pFile a pointer to a file.
	*/
	//void UaCanTrace::printHeader(FILE * pFile)
	//{
	//	UaDateTime time = UaDateTime::now();
	//	fprintf( pFile, "** %s: start trace\n", s_pCanAppName->toUtf8());
	//	fprintf( pFile, "** Date:\t%s\n**\n", time.toDateString().toUtf8());
	//	fflush( pFile );
	//}

	/** Build backup file name based on index and original file name.
	*  @param originalName the original name.
	*  @param index the index where to write.
	*  @param backupFileName the file name of the backup.
	*/
	//void UaCanTrace::buildBackupFileName( const UaString& originalName, unsigned int index, UaString& backupFileName)
	//{
	//	char tmpFileName1[2000];
	//	char tmpFileName2[2000];

	//	const char* pLastDot = strrchr (originalName.toUtf8(), '.');

	//	if (pLastDot != NULL )
	//	{
	//		int nChars = (int)(pLastDot - originalName.toUtf8());
	//		if ( nChars > 1900 )
	//		{
	//			nChars = 1900;
	//		}
	//		strncpy (tmpFileName1, originalName.toUtf8(), nChars );
	//		tmpFileName1[nChars] = '\0';
	//		snprintf(tmpFileName2, 2000, "%s_%d%s", tmpFileName1, index, pLastDot);
	//		backupFileName = tmpFileName2;
	//	}
	//	else
	//	{
	//		UaVariant avTemp;
	//		backupFileName = originalName;
	//		avTemp.setInt32(index);
	//		backupFileName += "_";
	//		backupFileName += avTemp.toString();
	//		backupFileName += ".log";
	//	}
	//}

}
