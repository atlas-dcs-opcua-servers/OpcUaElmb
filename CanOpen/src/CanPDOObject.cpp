#include <opcua_basedatavariabletype.h>
#include "uabasenodes.h"
#include "CanBusObject.h"
#include "CanNodeObject.h"
#include "CanPDOObject.h"
#include "CanPDOItem.h"
#include <uadatetime.h>
#include <boost/lexical_cast.hpp>

namespace CanOpen
{
	using boost::lexical_cast;
	using boost::bad_lexical_cast;
	/**
	 * @brief CanPDOObject Constructor
	 * @param par a parent CanNode Object
	 * @param conf XML configuration information for this object
	 * @param blink UaNode which represent the PDO in Address space
	 */
	CanPDOObject::CanPDOObject(pUserDeviceStruct *par,
			PDO *conf, UaNode *blink,OpcUa_UInt32 code) :
			CanOpenObject(par,conf,blink,code)
	{
		char *p;
		m_cobId = (OpcUa_UInt16)strtol(conf->cobid().data(),&p,16);
		const char *rtrvalue = conf->rtr().c_str();


		m_bInitRTR = (strcmp(rtrvalue,"init") == 0);

		m_buffer.resize(8);
		for( int i = 0; i < 8; i++)
			m_buffer[i] = 0;
		OpcUa_UInt32 m_numch = conf->numch();
		m_pdoBuffer.resize(m_numch+1,m_buffer);
		CanOpenDataAddress::getDirection(conf->access().c_str(),m_direction);

		m_iCount = 0;
		m_iMaxCount = (m_numch == 0) ? 1 : m_numch; // maximum channel for PDO object (ELMB)
	};

	
	/**
	 * @brief Include Item to PDO object
	 * @param ch index for channel
	 * @param pdoi PDO item
	 */
	void CanPDOObject::setItem(OpcUa_UInt16 ch,CanPDOItem *pdoi)
    {
         m_cPDOs.insert(pair<OpcUa_UInt16,CanPDOItem *>(ch,pdoi));
		 pdoi->setIndex(ch); 
    }

	/**
	 * @brief Pass data to item objects
	 * @param cms CAN message from driver  
	 */
	void CanPDOObject::pass(const CanMsgStruct *cms)
	{
		OpcUa_Byte selByte;
		if ((m_iMaxCount > 1) && (m_iMaxCount < cms->c_data[0]))
			return;	

		for(int i = 0; i < cms->c_dlc; i++)				// copy to buffer
			m_buffer[i] = cms->c_data[i];

		selByte = cms->c_data[0];
		
		if (m_iMaxCount > 1)
			m_pdoBuffer[selByte] = m_buffer; // copy to channel cache
		m_udt = UaDateTime::fromTime_t(cms->c_time.tv_sec);
		m_udt.addMilliSecs(cms->c_time.tv_usec/1000);	// convert time stamp to ua format

		typedef multimap<OpcUa_UInt16,CanPDOItem *>::iterator m_iter;
		pair<m_iter,m_iter> selec;
		if (m_iMaxCount > 1)	{	// find all item for this PDO object
			selec = m_cPDOs.equal_range(selByte);
		}
		else
			selec = m_cPDOs.equal_range(0);

		m_iter it;

		for (it = selec.first;  it != selec.second; ++it)
		{
			CanPDOItem *ucpdo = ((*it).second);
			ucpdo->setItemValue();			// extract date from PDO object to item
		}
		
		messageCame();
	}

	void CanPDOObject::getInitRTRData() 
	{ 
		CCanAccess *b = getCanBus();
		if (b) {
			if (m_bInitRTR) {
				b->sendRemoteRequest(getCobId());
			}
		}
	}

	/**
	 * @brief Send data to CAN bus
	 */
	UaStatus CanPDOObject::sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value)
	{
		OpcUa_ReferenceParameter(value);
		CCanAccess *b = getCanBus();
		if (b) {
			lock();
			if (code == BA_PDOOBJECT_RTR_COMMAND || code == BA_PDOOBJECT_RTR)
			{
				sendRTR();
				unlock();
				LOG(Log::DBG, Pdo1Message) << "bus=" << getBusName() << " phase=RTR Command " << "node=" << hex << getCobId();
				return OpcUa_Good;
			}
			unsigned char mes[8];
			for (int i = 0; i < 8; i++)
				mes[i] = m_buffer[i];

			LOG(Log::DBG, PdoMessage) << " bus=" << getBusName() << " output " << "node=" << hex << getCobId() << " len=8 " << " output " <<
				hex << (int)mes[0] << (int)mes[1] << (int)mes[2] << (int)mes[3] << (int)mes[4] << (int)mes[5] << (int)mes[6] << (int)mes[7];

			b->sendMessage(this->getCobId(), 8, mes);

			unlock();
		}
		return OpcUa_Good;
	}

	void CanPDOObject::addNodeId(bool type)
	{ 
		CanNodeObject *cn = static_cast<CanNodeObject *>(getParentDevice());
		if (type)
			m_cobId = m_cobId + cn->getCanNodeId();
		static_cast<CanBusObject *>(cn->getParentDevice())->addPDO(this);
	}


	UaStatus CanPDOObject::connectCode(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf, UaNode *blink)
	{
		UaDataValue udt;
		UaVariant val;
		UaDateTime sdt = UaDateTime::now();
		PDO *pdob = (PDO *)conf;
		switch (code) {
		case BA_PDOOBJECT_RTR_COMMAND:
			if (this->getDirectionType() == CAN_IN) {
				((OpcUa::BaseDataVariableType *)blink)->setDataType(OpcUaType_UInt32);
				val.setUInt32(0);
				udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
				((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
				m_pRTRCommand = (OpcUa::BaseVariableType *)blink;
				((OpcUa::BaseDataVariableType *)blink)->setValueHandling(UaVariable_Value_Cache);
			}
			break;
		}
		return OpcUa_Good;
	}

	UaVariant CanPDOObject::getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf)
	{
		UaVariant val;
		val.clear();
		PDO *pdob = (PDO *)conf;
		if (code == BA_PDOOBJECT_COBID) 
		{
//			OpcUa_UInt16 cobid = boost::lexical_cast<short>(pdob->cobid().c_str());
			val.setString(UaString(pdob->cobid().c_str()));
//			val.setUInt16(cobid);
		}
		return val;
	}
}
