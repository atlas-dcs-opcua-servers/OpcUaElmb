#include "CANopen.h"
#include "CCanUACallback.h"
#include "CanNodeObject.h"
#include "CanPDOObject.h"
#include "CanOpenObject.h"
#include "UaCanTrace.h"
#include <time.h>

//using namespace AddressSpace;

namespace CanOpen
{
	CCanUACallback::CCanUACallback(CanBusObject *cB) 	
	{  
//		boost::arg<1> _1;
//		boost::arg<2> _2;
//		boost::arg<3> _3;
		cBus = cB;
//		cBus->getCanBusInterface()->canMessageCame.connect(boost::bind<void>(&CCanUACallback::FireOnChange,this,_1));
//		cBus->getCanBusInterface()->canMessageError.connect(boost::bind<void>(&CCanUACallback::FireOnError,this,_1,_2,_3));
	} 

	CCanUACallback::~CCanUACallback(void) 	
	{  
		cout << " Delete Callback";

//		cBus->getCanBusInterface()->canMessageCame.disconnect(boost::bind<void>(&CCanUACallback::FireOnChange));
//		cBus->getCanBusInterface()->canMessageError.disconnect(boost::bind<void>(&CCanUACallback::FireOnError));
		PDOs.clear();

//		for (map<OpcUa_UInt16, CanNodeObject *>::iterator itn = NODEs.begin(); itn != NODEs.end(); itn++)
//			delete (*itn).second;

		NODEs.clear();
	} 

	void CCanUACallback::ConnectCallback() 	
	{  
		boost::arg<1> _1;
		boost::arg<2> _2;
		boost::arg<3> _3;
		CCanAccess *cca = cBus->getCanBusInterface();
		
		for(map<OpcUa_UInt16,CanNodeObject *>::iterator itn= NODEs.begin(); itn !=NODEs.end(); itn++)
			(*itn).second->setCanBusInterface(cca);

		cca->canMessageCame.connect(boost::bind(&CCanUACallback::FireOnChange,this,_1));
		cca->canMessageError.connect(boost::bind(&CCanUACallback::FireOnError,this,_1,_2,_3));
	}

	void CCanUACallback::FireOnChange(const CanMsgStruct &cms)
	{
		bool rRet = true;
		busPdos::iterator it;
		busNodes::iterator itn;

		OpcUa_UInt32 canopen_obj    = cms.c_id & CANOPEN_OBJECT_MASK;
		OpcUa_UInt16 canopen_nodeid = cms.c_id & CANOPEN_NODEID_MASK;

		switch (canopen_obj) {
		case CANOPEN_EMERGENCY_COBID:
			// Emergency message
			itn = NODEs.find(canopen_nodeid);
			LOG(Log::DBG, EmergMessage) << " bus=" << cBus->getCanBusName().c_str() << " input " << UaCanTrace::pdomes(cms);
			if (itn != NODEs.end() )
				(*itn).second->setEmergencyEvent(&cms);
			break;
		case CANOPEN_TPDO1_COBID: // PDO message
			LOG(Log::DBG, Pdo1Message) << " bus=" << cBus->getCanBusName() << " phase=input " << UaCanTrace::pdomes(cms);
		case CANOPEN_RPDO1_COBID:
		case CANOPEN_TPDO2_COBID:
			LOG(Log::DBG, Pdo2Message) << " bus=" << cBus->getCanBusName() << " phase=input " << UaCanTrace::pdomes(cms);
		case CANOPEN_RPDO2_COBID:
		case CANOPEN_TPDO3_COBID:
			LOG(Log::DBG, Pdo3Message) << " bus=" << cBus->getCanBusName() << " phase=input " << UaCanTrace::pdomes(cms);
		case CANOPEN_RPDO3_COBID:
		case CANOPEN_TPDO4_COBID:
			LOG(Log::DBG, Pdo4Message) << " bus=" << cBus->getCanBusName() << " phase=input " << UaCanTrace::pdomes(cms);
		case CANOPEN_RPDO4_COBID:

			LOG(Log::DBG, PdoMessage) << " bus=" << cBus->getCanBusName() << " phase=input " << UaCanTrace::pdomes(cms);

			it = PDOs.find(cms.c_id);
			if (it != PDOs.end() ) {
				(*it).second->pass(&cms);
			}
			break;
		case CANOPEN_SDOSERVER_COBID: 
			{
//				UaCanTrace::tSdoMessage(" SDO message came  %d func %2x index=%2x%2x subindex=%x", cms.c_id, cms.c_data[0], cms.c_data[2], cms.c_data[1], cms.c_data[3]);
				LOG(Log::DBG, SdoMessage) << " bus=" << cBus->getCanBusName() << " phase=input " << std::hex << (int)cms.c_data[0] << " CobID=" << std::hex << cms.c_id <<
					" Index="<< std::hex << (int)cms.c_data[2] << std::hex <<(int)cms.c_data[1]<< " Subindex=" << std::hex << (int)cms.c_data[3];
				LOG(Log::DBG, SegSdoMessage) << " bus=" << cBus->getCanBusName() << " phase=input " << std::hex << (int)cms.c_data[0] << " CobID=" << std::hex << cms.c_id <<
					" Index="<< std::hex << (int)cms.c_data[2] << std::hex <<(int)cms.c_data[1]<< " Subindex=" << std::hex << (int)cms.c_data[3];

				itn = NODEs.find(canopen_nodeid);
				if (itn != NODEs.end()) {
					(*itn).second->passSDO(&cms);
				}
			}
			break;
		case CANOPEN_NODEGUARD_COBID:
			LOG(Log::DBG, NGMessage) << " bus=" << cBus->getCanBusName().c_str() << " node=" << std::hex << (int)cms.c_id << " direction=read phase=came input=" << std::hex << (int)cms.c_data[0];

//			tNGMessage("NG came %s cobId=%X %hhX",cBus->getCanBusName().c_str(),cms.c_id,cms.c_data[0]);
			itn = NODEs.find(canopen_nodeid);
			if (itn != NODEs.end() )
				(*itn).second->setState(&cms);
			break;
		default:
//			LOG(Log::INF) << " bus=" << cBus->getCanBusName().c_str() << " node=" << std::hex << (int)cms.c_id << " direction=read phase=came input=" << std::hex << (int)cms.c_data[0];
			rRet = false;
		}
	}

	void CCanUACallback::FireOnError(const int error, const char *errMsg, timeval &t)
	{
		LOG(Log::ERR) << " bus=" << cBus->getCanBusName() << " error=" << errMsg ;
		cBus->setPortError(error,errMsg,t);
	}

	void CCanUACallback::addPDO(CanPDOObject *pdoObject) {
			PDOs.insert(pair<OpcUa_UInt32,CanPDOObject *>(pdoObject->getCobId(),pdoObject) );
		}

	void CCanUACallback::addCanNode(CanNodeObject *cno) {
			NODEs.insert(pair<OpcUa_Byte,CanNodeObject *>(cno->getCanNodeId(),cno) );
		}
		
	void CCanUACallback::sendInitNodeNmt()
		{
			UaStatus ret;
			for(busNodes::iterator bpi = NODEs.begin(); bpi != NODEs.end(); bpi++) {
				(*bpi).second->sendInitNmt();
			}
		}

		void CCanUACallback::waitAllData()
		{
			UaDateTime udt;
			UaStatus ret;
			for(busPdos::iterator bpi = PDOs.begin(); bpi != PDOs.end(); bpi++) {
				if ((*bpi).second->getDirectionType() == CAN_IN && (*bpi).second->getCobId() > 0x180) {
					ret = (*bpi).second->waitOperation(0);
				}
			}
		}

		void CCanUACallback::waitRTRData()
		{
			for(busPdos::iterator bpi = PDOs.begin(); bpi != PDOs.end(); bpi++) {
				if ((*bpi).second->getDirectionType() == CAN_IN && (*bpi).second->getCobId() > 0x180) {
					(*bpi).second->waitRTRData();
				}
			}
		}

		void CCanUACallback::executeRTR()
		{
			UaStatus ret;
			for(busPdos::iterator bpi = PDOs.begin(); bpi != PDOs.end(); bpi++) {
				if ((*bpi).second->getDirectionType() == CAN_IN && (*bpi).second->getCobId() > 0x180) {
					(*bpi).second->getInitRTRData();
				}
			}
		}

}
