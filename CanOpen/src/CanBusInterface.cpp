//#include "BuildingAutomationTypeIds.h"
#include "Ids.h"
#include "CanBusInterface.h"
#include "AbstractHardware.h"
#include "CanNodeObject.h"
#include "CanPDOObject.h"
#include "CanPDO16Object.h"
#include "CanPDOItem.h"
#include "CanSDOObject.h"
#include "CanSDOItem.h"
#include "CanOpenItem.h"
#include "Emergency.h"
#include "UaCanBusObject.h"
#include "UaCanNodeObject.h"
#include "variablehandleuanode.h"
#include <thread>
#include <memory>
#include <boost/assign/std/vector.hpp>

using namespace boost::assign;
using namespace AddressSpace;


unique_ptr<UserDevice::ControlInterface> createControlInterface(void) { return unique_ptr<UserDevice::ControlInterface >(new CanOpen::CanBusInterface()); }

//#ifdef WIN32 
//	__declspec(dllexport) ControlInterface *createControlInterface() 
//#else
//	ControlInterface *createControlInterface()
//#endif
//	{
//		ControlInterface *cc;
//		cc = new CanOpen::CanBusInterface();
//
//		return cc;
//	}

namespace CanOpen {

	list<UaDeviceMethod<UaControlDeviceGeneric> *>		CanBusInterface::m_listInitMethod;
	list<struct CanBusInterface::InitSet>		CanBusInterface::m_listInitSet;
	vector< AddressSpace::UaCalcItem *> CanBusInterface::m_vCCI;  //! set of formulas
	unique_ptr<CanBusAccess> CanBusInterface::g_pCanaccess;

//	AddressSpace::UaCalcCompiler *CanBusInterface::g_pCompiler;
	/**
	* create Canbus interface
	*/
	UaStatus CanBusInterface::initialize()
	{
		UaStatus ret = OpcUa_Good;
	//	cout << "Initialize Interface" << endl;
		#ifdef WIN32
			SetErrorMode(
				SEM_FAILCRITICALERRORS & SEM_NOGPFAULTERRORBOX & SEM_NOOPENFILEERRORBOX & SEM_NOALIGNMENTFAULTEXCEPT
			);
		#endif

		g_pCanaccess = unique_ptr<CanBusAccess>(new CanBusAccess()); // CAN interface

//		CanOpen::UaCanTrace::m_CanTraceComponent[lCanTrace] = Log::getComponentHandle("CanModule");
//		Log::setComponentLogLevel(CanOpen::UaCanTrace::m_CanTraceComponent[lCanTrace], Log::INF);
		CanOpen::UaCanTrace::m_CanTraceComponent[lCanTrace] = Log::getComponentHandle("CanModule");
		Log::setComponentLogLevel(CanOpen::UaCanTrace::m_CanTraceComponent[lCanTrace], Log::INF);

		for(list<CanBusObject *>::iterator it = m_CanBusList.begin(); it != m_CanBusList.end(); it++)
		{
			try {
				(*it)->openCanBus();

			}
			catch (exception &e) {
				string bn = (*it)->getCanBusName();
				LOG(Log::ERR) << "bus=" << bn << " " << e.what() << " " << noExit; 
				if (!noExit)
					exit(-1);
			}
		}
		
//		if (!noExit) {
			for (list<CanBusObject *>::iterator it = m_CanBusList.begin(); it != m_CanBusList.end(); it++)
			{
				(*it)->sendInitNmt();
				(*it)->sendInitNodeNmt();
				(*it)->startSyncInterval();
				(*it)->startNodeGuardingInterval();

				(*it)->initialize();
			}

			ret = variableInitSet();

			std::this_thread::sleep_for(std::chrono::milliseconds(CanOpen::InitTimeout));

			for (list<CanBusObject *>::iterator it = m_CanBusList.begin(); it != m_CanBusList.end(); it++)
			{
				(*it)->waitData();
			}

			ret = executeInitMethod();
//		}
		return ret;
	}

	CanBusInterface::CanBusInterface()
	{
		g_pCanaccess = 0;
		factory[BA_CANBUS] = new DEVICE<CanBusObject,CANBUS>();
		factory[BA_CANNODETYPE] = new DEVICE<CanNodeObject,NODE>();
		factory[BA_SDOOBJECT] = new DEVICE<CanSDOObject,SDO>();
		factory[BA_SDOITEMTYPE1] = new DEVICE<CanSDOItem,SDOITEM_TYPE1>();
		factory[BA_SDOITEMTYPE2] = new DEVICE<CanSDOItem,SDOITEM_TYPE2>();
		factory[BA_SDOITEMTYPE3] = new DEVICE<CanSDOItem,SDOITEM_TYPE3>();
		factory[BA_PDOOBJECT] = new DEVICE<CanPDOObject,PDO>();
		factory[BA_PDO16OBJECT] = new DEVICE<CanPDO16Object, PDO16>();
		factory[BA_PDOITEM] = new DEVICE<CanPDOItem,PDOITEM>();
		factory[BA_EMERGENCY] = new DEVICE<Emergency, NODE>();
	}

	CanBusInterface::~CanBusInterface()
	{
//		cout << "Delete Interface" << endl;

//		for_each(m_CanBusList.begin(),m_CanBusList.end(),[](CanBusObject *cbo)
//		{
////			CanBusObject *cbo = (*it);
//			delete cbo;
//			cbo = NULL;
//		});

		for(list<CanBusObject *>::iterator it = m_CanBusList.begin(); it != m_CanBusList.end(); it++)
		{
			CanBusObject *cbo = (*it);
			delete cbo;
			cbo = NULL;
		}

		m_CanBusList.clear();
		cout << "Delete Interface" << endl;

//		delete g_pCanaccess;
	}

	void CanBusInterface::closeInterface()
	{
		for (list<CanBusObject *>::iterator it = m_CanBusList.begin(); it != m_CanBusList.end(); it++)
		{
			CanBusObject *cbo = (*it);
			cbo->closeCanBus();
		}
		cout << "Close Interface" << endl;
	}

	pUserDeviceStruct * CanBusInterface::createInterfaceEntry(pUserDeviceStruct *parent, int code , ::xml_schema::type *conf ,UaNode *pAS)
	{
		InterfaceUserDataBase* interf = 0;
		pUserDeviceStruct  *rPointer = 0;
		UaStatus ret = OpcUa_Good;

		if (parent == NULL) {
			if (code == BA_CANBUS) {
				interf = factory[code]->createHardwareEntry(parent,conf,pAS,code);
				m_CanBusList.push_back(static_cast<CanBusObject *>(interf));
				rPointer = new pUserDeviceStruct(interf);
			}
		}
		else {
			if (factory.find(code) != factory.end()  ) {
				interf = factory[code]->createHardwareEntry(parent,conf,pAS,code);
				rPointer = new pUserDeviceStruct(interf);
			}
			else
			{
				InterfaceUserDataBase *parentInterf = static_cast<InterfaceUserDataBase*>(parent->getDevice());
				UserDevice::AbstractHardware *newd = new DEVICE<CanOpenItem, ::xml_schema::type>();
				interf = newd->createHardwareEntry(parent,conf,pAS,code);
				ret = parentInterf->connectCode(code, conf, pAS);
				rPointer = new pUserDeviceStruct(interf);
				delete newd;
			}
		}
		return rPointer;
	}

	/** Load the configuration from the configure file and generate node managers.
	 *  First method called after creation of ServerConfig. Must create all NodeManagers
	 *  before method startUp is called.
	 *  @return         Error code.
	 */
	UaStatus CanBusInterface::CreateXmlParser(UaString &sXmlFileName/*,UaString &sLogFileName*/)
	{
		UaStatus ret = OpcUa_True;
		
		m_sXmlFileName = sXmlFileName;
//		m_sLogFileName = sLogFileName;

		try {
			cout << "Config file " << m_sXmlFileName.toUtf8() << endl;
			auto_ptr< ::CanOpenOpcServerConfig > vDoc( CanOpenOpcServerConfig_(m_sXmlFileName.toUtf8()));

			m_nodetypes = vDoc->NODETYPE();
			m_canbuses = vDoc->CANBUS();
			m_items = vDoc->ITEM();
			m_regexprisson = vDoc->REGEXPR();
			m_logs = vDoc->StandardMetaData();
			m_settings = vDoc->SETTINGS();
			m_execute = vDoc->atStartup();
			m_setvalue = vDoc->SET();
		}
		catch (const xml_schema::exception& e)
		{
			cout << "Parser error " << e << endl;

			cout << e.what() << " " << e << endl;
			return OpcUa_Bad;
		}
		CanOpen::UaCanTrace::initTraceComponent(m_logs);
		return ret;

	}
	
//	UaStatus CanBusInterface::CreateAddressSpace(NmBuildingAutomation *nManager)
	UaStatus CanBusInterface::CreateAddressSpace(AddressSpace::NmBuildingAutomation *nManager, NodeManagerNodeSetXml* pXmlNodeManager, NodeManagerConfig *pDefaultNodeManager)

	{
		UaStatus	ret;
		UaString	sName;
		UaStatus Status;
		OpcUa_UInt32 uMaxTraceEntries = 10000;
		OpcUa_UInt32 uMaxBackupFiles = 4;

		cout << "Create Address Space" << endl;

		m_pNodeManager = nManager;
		m_pTypeNodeManager = pXmlNodeManager;
		m_pDefaultNodeManager = pDefaultNodeManager;

//		g_pCompiler = new UaCalcCompiler(nManager);

		///
		/// Create Trace level variable 
		///
		OpcUa_UInt16 nIdx = m_pNodeManager->getNameSpaceIndex();
		UaVariant defaultValue;
		defaultValue.setUInt32(0);

		m_Trace = new UaCanTrace(
			UaNodeId("TraceLevel", nIdx), // NodeId of the Variable
			"TraceLevel",           // Name of the Variable
			nIdx,    // Namespace index of the browse name (same like NodeId)
			defaultValue,           // Initial value
			Ua_AccessLevel_CurrentRead | Ua_AccessLevel_CurrentWrite, // Access level
			m_pNodeManager);                  // Node manager for this variable

		ret = m_pNodeManager->addNodeAndReference(OpcUaId_ObjectsFolder, m_Trace, OpcUaId_HasComponent);
		UA_ASSERT(ret.isGood());

		ret = createNodeTypes();   /// create the can node type describing in NODETYPE section of XML file
		UA_ASSERT(ret.isGood());
		if (ret.isBad())
			return ret;

		ret = createCanbuses();		/// create the can-bus structure CANBUS section of XML file
		UA_ASSERT(ret.isGood());
		if (ret.isBad())
			return ret;
/*
		ret = g_pCompiler->CompileItems(m_items,m_regexprisson);   //create Calculation Item
		UA_ASSERT(ret.isGood());
		if (ret.isBad())
			return ret;

		ret = g_pCompiler->CreateItems();
		UA_ASSERT(ret.isGood());
		if (ret.isBad())
			return ret;
*/
		ret = CompileItems(NULL, m_items, m_regexprisson, nManager);   //create Calculation Item
		UA_ASSERT(ret.isGood());
		if (ret.isBad())
			return ret;


		ret = createListInitmethod(m_execute,NULL, nManager);
		UA_ASSERT(ret.isGood());
		if (ret.isBad())
			return ret;

		ret = createListInitSet(m_setvalue, NULL, nManager);
		UA_ASSERT(ret.isGood());
		if (ret.isBad())
			return ret;

		if (m_settings.present()) {
			CanOpenOpcServerConfig::SETTINGS_type settingst =  m_settings.get(); 
//			if (settingst. NGcounter.present()) {
			CanOpen::CanNodeObject::ngCounter = settingst.NGcounter();// NGcounter.get();
				cout << "NG repeating counts =  " << CanOpen::CanNodeObject::ngCounter << endl;
//			}

//			if (settingst.DisconectRecovery.present()) {
				CanOpen::CanNodeObject::DisconectRecovery = settingst.DisconectRecovery();  // .get();
				cout << "Recover after reconnection =  " << (bool)CanOpen::CanNodeObject::DisconectRecovery << endl;
//			}

//			if (settingst.TypeConversion.present()) {
				CanOpen::TypeConversion = settingst.TypeConversion(); // .get();
				cout << "Type Conversion " << (bool)CanOpen::TypeConversion << endl;
//			}
//			else
//				CanOpen::TypeConversion = true;

//			if (settingst.InitTimeout.present()) {
				CanOpen::InitTimeout = settingst.InitTimeout(); // .get();
				cout << "Init Timeout " << (long)CanOpen::InitTimeout << endl;

				CanOpen::InitCalc = settingst.InitCalc(); // .get();
				cout << "Init Calc " << (long)CanOpen::InitCalc << endl;
				//			}
//			else
//				CanOpen::InitTimeout = 10;
				CanOpen::noSendDisconnectedSDO = settingst.NoSendDisconnectedSDO(); // .get();
				cout << "Init NoSendDisconnectedSDO " << (long)CanOpen::noSendDisconnectedSDO << endl;

		}

		UaDataValue udt;
		UaVariant val;
				
		UaDateTime sdt = UaDateTime::now();

		initCalculation();
		UA_ASSERT(ret.isGood());
//		delete g_pCompiler;
//		g_pCompiler = NULL;

		return ret;
	}
	
	UaStatus  CanBusInterface::createNodeTypes()
	{
		UaStatus addStatus;

		UaDeviceNodeType*	pCanNodeType = NULL;

		for(CanOpenOpcServerConfig::NODETYPE_sequence::iterator itnt(m_nodetypes.begin()); itnt != m_nodetypes.end(); itnt++ ) {
			NODETYPE *nodTyp = &(*itnt);

			/**************************************************************
			* Create the CanNode Type
			**************************************************************/

			// Add ObjectType "CanNodeType"
			UaNodeId uTypeNode = UaNodeId(nodTyp->name().c_str(), m_pNodeManager->getNameSpaceIndex());
			pCanNodeType = new UaDeviceNodeType(
				nodTyp->name().c_str(),		// Used as string in browse name and display name
				uTypeNode,					//  NodeId for types
				m_sLocale,					// Default LocaleId for UaLocalizedText strings
				OpcUa_False);				// Abstract object type -> can not be instantiated

			// Add new node to address space by creating a reference from BaseObjectType to this new node
			addStatus = m_pNodeManager->addNodeAndReference(OpcUaId_BaseObjectType,pCanNodeType, OpcUaId_HasComponent);
			UA_ASSERT(addStatus.isGood());
			UaObjectType* uv = m_pNodeManager->getInstanceDeclarationObjectType(BA_CANNODETYPE);
			addStatus = m_pNodeManager->addUaReference(pCanNodeType->nodeId(), uv->nodeId(), OpcUaId_HasSubtype);
			UA_ASSERT(addStatus.isGood());
			pCanNodeType->setXsdData(nodTyp);

		}
		return addStatus;
	}
	

	UaStatus  CanBusInterface::createCanbuses()
	{
		UaStatus addStatus;
		UaVariant                    defaultValue;
		UaNodeId					uaId;
		UaString newName;

		OpcUa_UInt16				 indx = m_pNodeManager->getNameSpaceIndex();

		AddressSpace::NmBuildingAutomation *pNodeManager =   (AddressSpace::NmBuildingAutomation *)m_pNodeManager;

		UaString					sLocale = ((AddressSpace::NmBuildingAutomation*)m_pNodeManager)->getDefaultLocale();

		UaControlDeviceGeneric   *pcanbus = NULL;

		///
		/// CAN bus ua node connects to top level node 
		///
		for( CanOpenOpcServerConfig::CANBUS_iterator it(m_canbuses.begin()); it != m_canbuses.end(); it++) {
				CANBUS *canb = (CANBUS *)&(*it);

			predCanBus pred(canb->port());	
			list<CanBusObject *>::iterator o = find_if(m_CanBusList.begin(), m_CanBusList.end(), pred);

			if (o == m_CanBusList.end()) {
				UaObjectType *pInstanceDeclarationObject = m_pNodeManager->getInstanceDeclarationObjectType(BA_CANBUS);   // take the type
				pcanbus = new UaControlDeviceGeneric(canb->name().c_str(), UaNodeId(canb->name().c_str(), indx), pNodeManager, pInstanceDeclarationObject, canb, NULL);
				addStatus = m_pNodeManager->addNodeAndReference(OpcUaId_ObjectsFolder, pcanbus, OpcUaId_Organizes);
				UA_ASSERT(addStatus.isGood());
			}
			else
			{
				cerr << "Can Port " << canb->port() << "  is duplicated" << endl;
				exit(-1);
			}

			addStatus = createNodes(canb->NODE(),pcanbus);				// create CAN Nodes
			UA_ASSERT(addStatus.isGood());
				
			addStatus = CompileItems(pcanbus,canb->ITEM(),canb->REGEXPR(), m_pNodeManager);   //create Calculation Item
			UA_ASSERT(addStatus.isGood());
			if (addStatus.isBad()) {
				LOG(Log::ERR) << "Calc Item" << " failed to create Calc Item, for canbus= " << pcanbus->browseName().toString().toUtf8() << " " << addStatus.pDiagnosticInfo()->m_localizedText.toString().toUtf8();
				cerr << "Error Cacl Item " << pcanbus->browseName().toString().toUtf8() << " " << addStatus.pDiagnosticInfo()->m_localizedText.toString().toUtf8() << endl;

				exit(-1);
			}
	
			addStatus = createListInitmethod(canb->atStartup(), pcanbus,pNodeManager);
			UA_ASSERT(addStatus.isGood());
			addStatus = createListInitSet(canb->SET(), pcanbus, pNodeManager);
			UA_ASSERT(addStatus.isGood());
			if (addStatus.isBad())
				return addStatus;

		}

		return addStatus;
	}


	UaStatus CanBusInterface::createNodes(CANBUS::NODE_sequence &nd,UaControlDeviceGeneric *pcanbus)
	{
		UaStatus ret;

		UaCanNodeObject * pcannode;
		UaString nodeName, typeName;
		UaNodeId nodeId,nodeTypeId;
		OpcUa_UInt16 indx = m_pNodeManager->getNameSpaceIndex();

		AddressSpace::NmBuildingAutomation *pNodeManager = (AddressSpace::NmBuildingAutomation *)m_pNodeManager;

		for (CANBUS::NODE_iterator it=nd.begin(); it != nd.end(); ++it)
		{
			NODE *nodeb = (NODE *)&(*it);

			nodeName = it->name().c_str();
			nodeId = pNodeManager->getNewNodeId(pcanbus,nodeName);
			UaNode *node = pNodeManager->findNode(nodeId);
			pUserDeviceStruct* cb = static_cast<pUserDeviceStruct*>(pcanbus->getUserData());
			if (node == NULL) {
				if (!it->type().present()) {
//					UaObject *pInstanceDeclarationObject = getInstanceDeclarationObjectType(BA_CANNODETYPE);   // take the type

//					pcannode =  new UaCanNodeObject(nodeb, pInstanceDeclarationObject,nodeId,pNodeManager,cb);
					pcannode = new UaCanNodeObject(nodeb, NULL, nodeId, pNodeManager, cb);
				}
				else {
					// Find Node Type by name
					typeName = it->type().get().c_str();
					nodeTypeId = UaNodeId(typeName,indx);
					UaDeviceNodeType	*pNode = (UaDeviceNodeType *)pNodeManager->findNode(UaNodeId(typeName,indx));
					if (pNode == NULL) {
						cerr << "Node Type" << it->type().get().c_str() << " has not been defined" << endl;
						exit(-1);
					}
 					pcannode =  new UaCanNodeObject(nodeb,(UaObject *)pNode, nodeId,pNodeManager,cb);
				}
				ret = m_pNodeManager->addNodeAndReference( pcanbus,pcannode, OpcUaId_Organizes);
				UA_ASSERT(ret.isGood());
				ret = createListInitmethod(nodeb->atStartup(), pcannode, pNodeManager);
				UA_ASSERT(ret.isGood());
				ret = createListInitSet(nodeb->SET(), pcannode, pNodeManager);
				UA_ASSERT(ret.isGood());
				if (ret.isBad())
					return ret;
			}
			else {
				cerr << "Node " << nodeName.toUtf8() << " is duplicated" << endl;
				exit(-1);
			}
		}
		return ret;
	}

	VariableHandle* CanBusInterface::getVariableHandler(Session* session, VariableHandle::ServiceType serviceType, OpcUa_NodeId *nodeid, OpcUa_Int32 attributeId)
	{

		OpcUa_ReferenceParameter(session);
		OpcUa_ReferenceParameter(serviceType);

		UaNodeId	t_uanodeid;

		VariableHandle* pVariableHandle = NULL;

		if ( attributeId == OpcUa_Attributes_Value )
		{
			OpcUa_UInt32 idt;
			// The value attribute is requested - we are using the IOManager
			UaNode *pUaNode = getNodeManager()->findNode(*nodeid);	
			UaNodeId id = pUaNode->typeDefinitionId();
			if (id.identifierType() == OpcUa_IdentifierType_Numeric) {
				idt = id.identifierNumeric();
				if (idt == BA_SDOITEMTYPE ||
					idt == BA_SDOITEMTYPE1 ||
					idt == BA_SDOITEMTYPE2 ||
					idt == BA_SDOITEMTYPE3 
					)
				{
					pVariableHandle = new VariableHandleUaNode;
					t_uanodeid = getCanNodeNodeId(pUaNode);
					UaNode *pParNode = getNodeManager()->findNode(t_uanodeid);	
					pVariableHandle->m_pIOManager = ((UaCanNodeObject *)pParNode)->getSDOIOManager();
					pVariableHandle->m_AttributeID = attributeId;
					((VariableHandleUaNode *)pVariableHandle)->setUaNode(pUaNode);
				}
				/*
				else {
					if (idt == Ba_PDOType_Item)
					{
						if (((UaVariable *)pUaNode)->accessLevel() == Ua_AccessLevel_CurrentWrite) {
							pVariableHandle = new VariableHandleUaNode;
							t_uanodeid = getNodeManager()->getTopNodeId(pUaNode);
							UaNode *pTopNode = getNodeManager()->findNode(t_uanodeid);
							pVariableHandle->m_pIOManager = ((UaCanBusObject *)pTopNode)->getPDOIOManager();
							pVariableHandle->m_AttributeID = attributeId;
							((VariableHandleUaNode *)pVariableHandle)->setUaNode(pUaNode);
						}
					}
				}
				*/
			}
		}
		return pVariableHandle;
	}

	UaNodeId CanBusInterface::getCanNodeNodeId(UaNode *pNode)
	{
		UaNodeId uNodeId;
		UaNode * uNode;
		uNode = getNodeManager()->getUpNode(pNode);
		if (uNode->typeDefinitionId().identifierNumeric() ==  BA_CANNODETYPE)
			uNodeId = uNode->nodeId(); 
		else {
			uNode = getNodeManager()->getUpNode(uNode);
//			printf("NodeId %d\n",uNode->typeDefinitionId().identifierNumeric());
			if (uNode->typeDefinitionId().identifierNumeric() == BA_CANNODETYPE) {
				uNodeId = uNode->nodeId();
			}
			else uNodeId.clear();
		}
		return uNodeId;
	}

 /**
 * This function returns IOManager for mode and attribute  
 * @param pUaNode pointer to UaN asking the IO manager
 * @param attributeId id of attribute of node 
 */
	IOManager* CanBusInterface::getIOManager(UaNode* pUaNode,  OpcUa_Int32 attributeId) 
	{

		UaNodeId	t_uanodeid;
		if (attributeId == OpcUa_Attributes_Value) {
			UaNodeId id = pUaNode->typeDefinitionId();
			if (id.identifierType() == OpcUa_IdentifierType_Numeric) {
				OpcUa_UInt32 idt = id.identifierNumeric();
				/*
				if (idt == Ba_PDOType_Item)
				{
					t_uanodeid = m_pNodeManager->getTopNodeId(pUaNode);
					UaNode *pTopNode = m_pNodeManager->findNode(t_uanodeid);
					if (((UaControlDeviceItem *)pUaNode)->accessLevel() == Ua_AccessLevel_CurrentWrite)
					{
						return ((UaCanBusObject *)pTopNode)->getPDOIOManager(); //PDO thread per bus
					}
				}
				else {
				*/
					if (idt == BA_SDOITEMTYPE ||
						idt == BA_SDOITEMTYPE1 ||
						idt == BA_SDOITEMTYPE2 ||
						idt == BA_SDOITEMTYPE3
						)
					{
						t_uanodeid = getCanNodeNodeId(pUaNode);
						UaNode *pParNode = m_pNodeManager->findNode(t_uanodeid);	
						return ((UaCanNodeObject *)pParNode)->getSDOIOManager(); //SDO thread per node
					}
			}
		}

		return NULL;
	}

	UaStatus CanBusInterface::createListInitmethod(CanOpenOpcServerConfig::atStartup_sequence &ex, UaControlDeviceGeneric *parent, NmBuildingAutomation *nManager)
	{
		
		for (CanOpenOpcServerConfig::atStartup_iterator exiter = ex.begin(); exiter != ex.end(); exiter++)
		{
			const UaString comm = (*exiter).command().get().c_str();
			UaNodeId uaMethodId = nManager->getNewNodeId(parent, comm);

			UaDeviceMethod<UaControlDeviceGeneric> *dm = (UaDeviceMethod<UaControlDeviceGeneric> *)nManager->findNode(uaMethodId);
			if (dm != NULL)
				m_listInitMethod.push_back(dm);
			else
			{
				cout << "The command " << comm.toUtf8() << " does not define!" << endl;
				return OpcUa_Bad;
			}
		}
		
		return OpcUa_Good;
	}

	UaStatus CanBusInterface::createListInitSet(CanOpenOpcServerConfig::SET_sequence &ex, UaControlDeviceGeneric *parent, NmBuildingAutomation *nManager)
	{
		for (CanOpenOpcServerConfig::SET_iterator exiter = ex.begin(); exiter != ex.end(); exiter++)
		{
			const UaString comm = exiter->variable().get().c_str();
			const UaString vval = exiter->value().get().c_str();
			UaNodeId uaVarId = nManager->getNewNodeId(parent, comm);

			OpcUa::BaseDataVariableType *dm = (OpcUa::BaseDataVariableType *)nManager->findNode(uaVarId);
			if (dm != NULL) {
				struct InitSet inset;
				inset.setVar = dm;
				inset.value = vval;
				m_listInitSet.push_back(inset);
			}
			else
			{
				cout << "The variable " << comm.toUtf8() << " does not define!" << endl;
				return OpcUa_Bad;
			}
		}
		return OpcUa_Good;
	}

	UaStatus CanBusInterface::executeInitMethod()
	{
		UaStatus ret = OpcUa_Good;

		for (list<UaDeviceMethod<UaControlDeviceGeneric> *>::iterator it = m_listInitMethod.begin(); it != m_listInitMethod.end(); it++)
		{
			UaStatus retexec = (*it)->execute();
			if (retexec.isBad())
				ret = retexec;
		}
		m_listInitMethod.clear();
		return ret;
	}

	UaStatus CanBusInterface::variableInitSet()
	{
		UaStatus ret = OpcUa_Good;

		for (list<struct InitSet> ::iterator it = m_listInitSet.begin(); it != m_listInitSet.end(); it++)
		{
			OpcUa::BaseDataVariableType *bvar = it->setVar;
			OpcUa_Byte acc = bvar->accessLevel();
			UaString uas = it->value;
	
			if (acc & Ua_AccessLevel_CurrentWrite)
			{
				UaNodeId uaid = bvar->dataType();
				OpcUa_Int32 tid = uaid.identifierNumeric();
				if (tid < 256)
				{
					OpcUa_BuiltInType binty = (OpcUa_BuiltInType)tid;
					UaDataValue udv;
					UaVariant uav = uas;
					uav.changeType(binty, false);
					udv.setValue(uav, false);
					((UaControlVariable *)bvar)->write(udv);
					bvar->setValue(0, udv, true);
				}
			}
		}
		m_listInitSet.clear();
		return ret;
	}
	UaStatus CanBusInterface::CompileItems(OpcUa::BaseObjectType *pParent, CanOpenOpcServerConfig::ITEM_sequence &itnd, CanOpenOpcServerConfig::REGEXPR_sequence &regex, NmBuildingAutomation *nManager)
	{
		UaStatus ret = OpcUa_Good;
		UaString sName, sFullName, tName;
		std::string stdName;
		UaVariant   defaultValue;
		UaDataValue dataValue;
		UaDateTime udt;
		UaNodeId uaId;
		UaVariant val;
		UaCalcItem* pUaCalcItem;
		
		OpcUa_BuiltInType typeName;

		for (CanOpenOpcServerConfig::ITEM_iterator sit = itnd.begin(); sit != itnd.end(); sit++)
		{
			mapDataVariable pVariables;			//! temporary set of pointer to variables created 
			sName = UaString(sit->name().c_str());
			OpcUa_Int32 minVar = 0xEFFFFFF;

			ret = createRegVariable((*sit),regex, pVariables, pParent, minVar, nManager);

			if (ret.isGood()) {
				if (minVar != INT_MAX) {
					for (OpcUa_Int32 j = 0; j < minVar; j++)
					{
						UaControlVariableMap ucVariables;
//						UaControlVariableSet ucs;
			 			sFullName = UaString("%1_%2").arg(sName).arg(j);
						uaId = nManager->getNewNodeId(pParent, sFullName);
						for (mapDataVariable::iterator it = pVariables.begin(); it != pVariables.end(); it++)
						{
							string nn = (*it).first;
							nn.erase(std::remove(nn.begin(), nn.end(), '$'), nn.end());
							ucVariables[nn] = (*it).second[j];
						}
						CanOpenDataAddress::getOpcUaType("double", typeName);
						UaVariant dv;
						dv.changeType(typeName, false);
						pUaCalcItem = new UaCalcItem(sFullName, uaId, pParent, nManager, dv, (*sit), ucVariables, (pParent) ? pParent->getSharedMutex() : NULL);
						if (pUaCalcItem->fExists()) {
							pUaCalcItem->setDataType(UaNodeId(typeName));
							dataValue.setDataValue(dv, OpcUa_False, OpcUa_BadWaitingForInitialData, UaDateTime::now(), UaDateTime::now());
							pUaCalcItem->setValue(0, dataValue, OpcUa_False);
						}

							//						ucs.push_back(pUaCalcItem);
							//						pVariables.insert(pair<string, UaControlVariableSet>(sFullName.toUtf8(), ucs));
							/*
													bool l = pUaCalcItem->compile();
													if (!l) {
														LOG(Log::INF) << "The expression of Item" << sName.toUtf8() << " is wrong!";
														//				cout << "The command " << comm << " does not define!" << endl;
														return OpcUa_Bad;
													}
													*/
							if (pParent == NULL) {
								ret = nManager->addNodeAndReference(OpcUaId_ObjectsFolder, pUaCalcItem, OpcUaId_HasComponent);
							}
							else
								ret = nManager->addNodeAndReference(pParent, pUaCalcItem, OpcUaId_HasComponent);

							UA_ASSERT(ret.isGood());

							addCalcCompilerItem(pUaCalcItem);
						}
				}
				else {
					UaControlVariableMap ucVariables;
					//				UaControlVariableSet ucs;
					uaId = nManager->getNewNodeId(pParent, sName);
					CanOpenDataAddress::getOpcUaType("double", typeName);
					UaVariant dv;
					dv.changeType(typeName, false);
					pUaCalcItem = new UaCalcItem(sName, uaId, pParent, nManager, dv, (*sit), ucVariables, (pParent) ? pParent->getSharedMutex() : NULL);
					if (pUaCalcItem->fExists()) {
						pUaCalcItem->setDataType(UaNodeId(typeName));
						dataValue.setDataValue(dv, OpcUa_False, OpcUa_BadWaitingForInitialData, UaDateTime::now(), UaDateTime::now());
						pUaCalcItem->setValue(0, dataValue, OpcUa_False);
					}

						//					ucs.push_back(pUaCalcItem);
					//					pVariables.insert(pair<string, UaControlVariableSet>(sit->name(), ucs));
										//bool l = pUaCalcItem->compile();

										//if (!l) {
										//	LOG(Log::INF) << "The expression of Item " << sName.toUtf8() << " is wrong!";
										//	//				cout << "The command " << comm << " does not define!" << endl;
										//	return OpcUa_Bad;
										//}
						if (pParent == NULL) {
							ret = nManager->addNodeAndReference(OpcUaId_ObjectsFolder, pUaCalcItem, OpcUaId_HasComponent);
						}
						else
							ret = nManager->addNodeAndReference(pParent, pUaCalcItem, OpcUaId_HasComponent);

						UA_ASSERT(ret.isGood());
						addCalcCompilerItem(pUaCalcItem);
				}
			}

		}
		return ret;

	}

	UaStatus CanBusInterface::createRegVariable(ITEM &itemvar, REGEXPR_sequence &regex, mapDataVariable& pvariables, OpcUa::BaseObjectType *pcn, OpcUa_Int32 &minVar, NmBuildingAutomation *nManager)

	{
		OpcUa_Int32 nVar = 0;
		minVar = INT_MAX;
		// scan throw the formula items to find the pattern
		for (REGEXPR_sequence::const_iterator it = regex.begin(); it != regex.end(); it++) {
			UaControlVariableSet ucVariables;
//			cout << itemvar.value() << " " << (*it).name() << endl;
			if (itemvar.value().find((*it).name()) != string::npos) {
				string t = (*it).value();
				nVar = nManager->findUaVariables(pcn, t, ucVariables);
				if (nVar == 0) {
//					LOG(Log::INF) << "Variable " << t << " is not defined!";
					cout << "Variable " << t << " is not defined!" << endl;
					return OpcUa_Bad;
				}
				if (minVar > nVar) minVar = nVar;		// minimum pattern result
													//				if (pvariables.find((*it)) == pvariables.end())
				pvariables.insert(pair<string, UaControlVariableSet>((*it).name(), ucVariables));
			}
		}

		if (itemvar.when().present()) {

			for (REGEXPR_sequence::const_iterator it = regex.begin(); it != regex.end(); it++) {
				UaControlVariableSet ucVariables;
				string t = itemvar.when().get();
				if (t.find((*it).name()) != string::npos) {
					string t = (*it).value();
					nVar = nManager->findUaVariables(pcn, t, ucVariables);

					if (nVar == 0) {
//						LOG(Log::INF) << "Variable " << t << " is not defined!";
						cout << "Variable " << t << " is not defined!" << endl;
						return OpcUa_Bad;
					}
					if (minVar > nVar) minVar = nVar;		// minimum pattern result
																//				if (pvariables.find((*it)) == pvariables.end())
					pvariables.insert(pair<string, UaControlVariableSet>((*it).name(), ucVariables));
				}
			}
		}
		if (itemvar.status().present()) {

			for (REGEXPR_sequence::const_iterator it = regex.begin(); it != regex.end(); it++) {
				UaControlVariableSet ucVariables;
				if (itemvar.status().get().find((*it).name()) != string::npos) {
					string t = (*it).value();
					nVar = nManager->findUaVariables(pcn, t, ucVariables);
					if (nVar == 0) {
//						LOG(Log::INF) << "Variable " << t << " is not defined!";
						cout << "Variable " << t << " is not defined!" << endl;
						return OpcUa_Bad;
					}
					if (minVar > nVar) minVar = nVar;		// minimum pattern result
															//				if (pvariables.find((*it)) == pvariables.end())
					pvariables.insert(pair<string, UaControlVariableSet>((*it).name(), ucVariables));
				}
			}
		}
		
		return OpcUa_Good;
	}


	void CanBusInterface::initCalculation()
	{
		for (unsigned int i = 0; i < m_vCCI.size(); i++)
		{
			AddressSpace::UaCalcItem *uci = m_vCCI[i];
			uci->compile();
		}
		if (CanOpen::InitCalc) {
			for (unsigned int i = 0; i < m_vCCI.size(); i++)
			{
				AddressSpace::UaCalcItem *uci = m_vCCI[i];
				uci->initCalculation();
			}
		}
		m_vCCI.clear();
	}

	void CanBusInterface::addCalcCompilerItem(AddressSpace::UaCalcItem *ucci)
	{
		m_vCCI.push_back(ucci);
	}
}
