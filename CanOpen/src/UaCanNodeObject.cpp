#include "CanBusInterface.h"
#include "UaCanNodeObject.h"
#include "NmBuildingAutomation.h"

#include "Ids.h"

#include "UaCanBusObject.h"
#include "UaControlDeviceGeneric.h"
#include "UaControlDeviceItem.h"
#include "IOManagerSDO.h"
#include "InterfaceUserDataBase.h"
#include "UaProgramItem.h"
#include "CanPDOItem.h"

using namespace UserDevice;
namespace CanOpen
{
	/**
	* Constructor UanNodeObject class
	* @param nd pointer to XSD node description
	* @param newNodeId node id of this new object
	* @param pNodeManager node manager of this node
	* @param pcanbus access to canbus interface
	*/
/*	
	UaCanNodeObject::UaCanNodeObject(NODE *nd, const UaNodeId& newNodeId, NmBuildingAutomation *pNodeManager,UserDataBase* pcanbus)
		:  UaControlDeviceGeneric(UaString(nd->name().c_str()), newNodeId,pNodeManager,BA_CANNODETYPE,nd,pcanbus)
	{

		UaStatus	ret ;
		
		ret = createSDOITEMs(nd,pNodeManager);		// create SDO items
		UA_ASSERT(ret.isGood());

		ret = createSDOs(nd,pNodeManager);			// create the SDO object - the set of SDO items variable
		UA_ASSERT(ret.isGood());

		ret = createAllTypesPDOs(nd,pNodeManager);
		UA_ASSERT(ret.isGood());

		ret = createItems<NODE>(nd);		// create the PDO object - the set of PDO item variable
		UA_ASSERT(ret.isGood());

		ret = createPROGRAMs(nd, pNodeManager);	 // create the Program objects - the set of sequence of methods
		UA_ASSERT(ret.isGood());

//		m_pIOManagerSDO = new IOManagerSDO(this);		// Input/Output manager for SDO operation
	}
	*/
	/**
	* Constructor UanNodeObject class from the type node
	* @param nd point to XSD node description
	* @param pNodeType the node of the type CAN node
	* @param newNodeId node id of this new object
	* @param pNodeManager node manager of this node
	* @param pcanbus pointer to canbus interface
	*/
	UaCanNodeObject::UaCanNodeObject(NODE *nd,UaObject *pNodeType , const UaNodeId& newNodeId, NmBuildingAutomation *pNodeManager, pUserDeviceStruct* pcanbus)
		: UaControlDeviceGeneric(UaString(nd->name().c_str()), newNodeId,pNodeManager, BA_CANNODETYPE,nd,pcanbus)
	{

		UaStatus		ret ;

		UaDataValue		attrValue;

		NODETYPE		*xsdObject=0;
		if (pNodeType)
			xsdObject = ((UaDeviceNodeType *)pNodeType)->getXsdData();

		if (xsdObject) {
			ret = createSDOITEMs(xsdObject,pNodeManager);		// create SDO items
			UA_ASSERT(ret.isGood());

			ret = createSDOs(xsdObject,pNodeManager);			// create the SDO object - the set of SDO items variable
			UA_ASSERT(ret.isGood());

			ret = createAllTypesPDOs(xsdObject,pNodeManager); // create the PDO object - the set of PDO item variable
			UA_ASSERT(ret.isGood());

			ret = createItems<NODETYPE>(xsdObject, pNodeManager);
			UA_ASSERT(ret.isGood());
		}

		ret = createSDOITEMs(nd,pNodeManager);		// create SDO items
		UA_ASSERT(ret.isGood());

		ret = createSDOs(nd,pNodeManager);			// create the SDO object - the set of SDO items variable
		UA_ASSERT(ret.isGood());

		ret = createAllTypesPDOs(nd,pNodeManager);
		UA_ASSERT(ret.isGood());

		ret = createItems<NODE>(nd, pNodeManager);	// create the PDO object - the set of PDO item variable
		UA_ASSERT(ret.isGood());

		ret = createPROGRAMs(nd,pNodeManager);	 // create the Program objects - the set of sequence of methods
		UA_ASSERT(ret.isGood());
	}

	UaStatus UaCanNodeObject::createAllTypesPDOs(NODETYPE *xsdObject,	 NmBuildingAutomation *pNodeManager)
	{
		UaStatus ret = OpcUa_Good;
		for (NODETYPE::NPDO_iterator pit = xsdObject->NPDO().begin(); pit != xsdObject->NPDO().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}

		for (NODETYPE::PDO16_iterator pit = xsdObject->PDO16().begin(); pit != xsdObject->PDO16().end(); pit++)
		{
			ret = createPDO16s((*pit), pNodeManager, true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}

		for (NODETYPE::RPDO1_iterator pit = xsdObject->RPDO1().begin(); pit != xsdObject->RPDO1().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODETYPE::RPDO2_iterator pit=xsdObject->RPDO2().begin(); pit != xsdObject->RPDO2().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODETYPE::RPDO3_iterator pit=xsdObject->RPDO3().begin(); pit != xsdObject->RPDO3().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODETYPE::RPDO4_iterator pit=xsdObject->RPDO4().begin(); pit != xsdObject->RPDO4().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODETYPE::TPDO1_iterator pit=xsdObject->TPDO1().begin(); pit != xsdObject->TPDO1().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODETYPE::TPDO2_iterator pit=xsdObject->TPDO2().begin(); pit != xsdObject->TPDO2().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODETYPE::TPDO3_iterator pit=xsdObject->TPDO3().begin(); pit != xsdObject->TPDO3().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}

		for (NODETYPE::TPDO4_iterator pit=xsdObject->TPDO4().begin(); pit != xsdObject->TPDO4().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		return ret;
	}

	UaStatus UaCanNodeObject::createAllTypesPDOs(NODE *xsdObject,	 NmBuildingAutomation *pNodeManager)
	{
		UaStatus ret;

		for (NODE::NPDO_iterator pit = xsdObject->NPDO().begin(); pit != xsdObject->NPDO().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}

		for (NODE::PDO16_iterator pit = xsdObject->PDO16().begin(); pit != xsdObject->PDO16().end(); pit++)
		{
			ret = createPDO16s((*pit), pNodeManager, true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}

		for (NODE::RPDO1_iterator pit = xsdObject->RPDO1().begin(); pit != xsdObject->RPDO1().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODE::RPDO2_iterator pit=xsdObject->RPDO2().begin(); pit != xsdObject->RPDO2().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODE::RPDO3_iterator pit=xsdObject->RPDO3().begin(); pit != xsdObject->RPDO3().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODE::RPDO4_iterator pit=xsdObject->RPDO4().begin(); pit != xsdObject->RPDO4().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODE::TPDO1_iterator pit=xsdObject->TPDO1().begin(); pit != xsdObject->TPDO1().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODE::TPDO2_iterator pit=xsdObject->TPDO2().begin(); pit != xsdObject->TPDO2().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		for (NODE::TPDO3_iterator pit=xsdObject->TPDO3().begin(); pit != xsdObject->TPDO3().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}

		for (NODE::TPDO4_iterator pit=xsdObject->TPDO4().begin(); pit != xsdObject->TPDO4().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,true);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}

		for (NODE::PDO_iterator pit=xsdObject->PDO().begin(); pit != xsdObject->PDO().end(); pit++)
		{
			ret = createPDOs((*pit),pNodeManager,false);		// create the PDO object - the set of PDO item variable
			if (ret.isBad()) {
				return ret;
			}
		}
		return ret;
	}

	UaStatus UaCanNodeObject::createPDOs(PDO &pit,  NmBuildingAutomation *pNodeManager,bool type)
	{
		UaStatus ret;
		UaString name;
		UaNodeId uaId;
		UaControlDeviceGeneric *pUaPdo;
		OpcUa_BuiltInType typeName; 
		UaControlDeviceItem *pUaPdoItem;
		UaVariant	defaultValue;
		UaDataValue uadef;
		CanPDOItem* cPDOItem;
		
		name = pit.name().c_str();
		OpcUa_Byte nch = (OpcUa_Byte)pit.numch();

		uaId = pNodeManager->getNewNodeId(this,name);

	
		pUserDeviceStruct* interf = static_cast<pUserDeviceStruct*>(getUserData());

		UaObjectType* prType = pNodeManager->getInstanceDeclarationObjectType(BA_PDOOBJECT);   //! get an object type based on numerical id ( if not return null)

		pUaPdo	= new UaControlDeviceGeneric (name,uaId,pNodeManager, prType,
			(::xml_schema::type *)&pit,
			interf);

		pUserDeviceStruct * udb = static_cast<pUserDeviceStruct*>(pUaPdo->getUserData());

		CanPDOObject *cPDO = static_cast<CanPDOObject *>(udb->getDevice());
		cPDO->addNodeId(type);

		ret =  pNodeManager->addNodeAndReference( this,pUaPdo, OpcUaId_HasComponent);
		UA_ASSERT(ret.isGood());

		::PDO::PDOITEM_sequence pitem = pit.PDOITEM();

		if (nch > 0) {
			for (OpcUa_Byte i = 0; i < nch; i++)
			{
				for (::PDO::PDOITEM_iterator pt = pitem.begin(); pt != pitem.end(); pt++)
				{
					name = pt->name().c_str();
					name += UaString("_%1").arg(i);
					CanOpenDataAddress::getOpcUaType(pt->type().c_str(),typeName);

					uaId = pNodeManager->getNewNodeId(pUaPdo,name);
//					defaultValue.setInt32(0);
					defaultValue.changeType(typeName, false);
					UaVariableType* prType = pNodeManager->getInstanceDeclarationVariableType(BA_PDOITEM);   //! get an object type based on numerical id ( if not return null)

					pUaPdoItem = new UaControlDeviceItem(name,uaId,pNodeManager,defaultValue,prType,
						(::xml_schema::type *)&(*pt),
						udb,pUaPdo->getSharedMutex());
					ret =  pNodeManager->addNodeAndReference( pUaPdo,pUaPdoItem, OpcUaId_HasComponent);

					pUaPdoItem->setDataType(UaNodeId(typeName));
					uadef.setDataValue(defaultValue, OpcUa_False, OpcUa_BadWaitingForInitialData, UaDateTime::now(), UaDateTime::now());
					pUaPdoItem->setValue(0, uadef, OpcUa_False);

					pUserDeviceStruct * udbi = static_cast<pUserDeviceStruct*>(pUaPdoItem->getUserData());

					cPDOItem = static_cast<CanPDOItem *>(udbi->getDevice());

					cPDO->setItem(i,cPDOItem);
					pUaPdoItem->setAccess(cPDO->getDirectionType());
				}
			}
		}
		else {
			for (::PDO::PDOITEM_iterator pit1 = pitem.begin(); pit1 != pitem.end(); pit1++)
			{
				name = pit1->name().c_str();
				CanOpenDataAddress::getOpcUaType(pit1->type().c_str(),typeName);
				uaId = pNodeManager->getNewNodeId(pUaPdo,name);
				
				defaultValue.changeType(typeName, false);
				UaVariableType* prType = pNodeManager->getInstanceDeclarationVariableType(BA_PDOITEM);   //! get an object type based on numerical id ( if not return null)

				pUaPdoItem = new UaControlDeviceItem(name,uaId,pNodeManager,defaultValue,prType,
					(::xml_schema::type *)&(*pit1),
					udb,pUaPdo->getSharedMutex());
				ret = pNodeManager->addNodeAndReference( pUaPdo,pUaPdoItem, OpcUaId_HasComponent);
				pUaPdoItem->setDataType(UaNodeId(typeName));
				pUserDeviceStruct * udbi = static_cast<pUserDeviceStruct *>(pUaPdoItem->getUserData());
				uadef.setDataValue(defaultValue, OpcUa_False, OpcUa_BadWaitingForInitialData, UaDateTime::now(), UaDateTime::now());
				pUaPdoItem->setValue(0, uadef, OpcUa_False);

				cPDOItem = static_cast<CanPDOItem *>(udbi->getDevice());

				cPDO->setItem((OpcUa_Byte)0,cPDOItem);
				pUaPdoItem->setAccess(cPDO->getDirectionType());

			}
		}

		return ret;
	}

	UaStatus UaCanNodeObject::createPDO16s(PDO16 &pit, NmBuildingAutomation *pNodeManager, bool type)
	{
		UaStatus ret;
		UaString name;
		UaNodeId uaId;
		UaControlDeviceGeneric *pUaPdo;
		OpcUa_BuiltInType typeName;
		UaControlDeviceItem *pUaPdoItem;
		UaVariant	defaultValue;
		UaDataValue uadef;
		CanPDOItem* cPDOItem;

		name = pit.name().c_str();
		OpcUa_UInt16 nch = pit.numch();

		uaId = pNodeManager->getNewNodeId(this, name);


		pUserDeviceStruct* interf = static_cast<pUserDeviceStruct*>(getUserData());

		UaObjectType* prType = pNodeManager->getInstanceDeclarationObjectType(BA_PDOOBJECT);   //! get an object type based on numerical id ( if not return null)

		pUaPdo = new UaControlDeviceGeneric(name, uaId, pNodeManager, prType,
			(::xml_schema::type *)&pit,
			interf);

		pUserDeviceStruct * udb = static_cast<pUserDeviceStruct*>(pUaPdo->getUserData());

		CanPDO16Object *cPDO = static_cast<CanPDO16Object *>(udb->getDevice());
		cPDO->addNodeId(type);

		ret = pNodeManager->addNodeAndReference(this, pUaPdo, OpcUaId_HasComponent);
		UA_ASSERT(ret.isGood());

		::PDO16::PDOITEM_sequence pitem = pit.PDOITEM();

		if (nch > 0) {
			for (OpcUa_UInt16 i = 0; i < nch; i++)
			{
				for (::PDO16::PDOITEM_iterator pt = pitem.begin(); pt != pitem.end(); pt++)
				{
					name = pt->name().c_str();
					name += UaString("_%1").arg(i);
					CanOpenDataAddress::getOpcUaType(pt->type().c_str(), typeName);

					uaId = pNodeManager->getNewNodeId(pUaPdo, name);
					//					defaultValue.setInt32(0);
					defaultValue.changeType(typeName, false);
					UaVariableType* prType = pNodeManager->getInstanceDeclarationVariableType(BA_PDOITEM);   //! get an object type based on numerical id ( if not return null)

					pUaPdoItem = new UaControlDeviceItem(name, uaId, pNodeManager, defaultValue, prType,
						(::xml_schema::type *)&(*pt),
						udb, pUaPdo->getSharedMutex());
					ret = pNodeManager->addNodeAndReference(pUaPdo, pUaPdoItem, OpcUaId_HasComponent);

					pUaPdoItem->setDataType(UaNodeId(typeName));
					uadef.setDataValue(defaultValue, OpcUa_False, OpcUa_BadWaitingForInitialData, UaDateTime::now(), UaDateTime::now());
					pUaPdoItem->setValue(0, uadef, OpcUa_False);

					pUserDeviceStruct * udbi = static_cast<pUserDeviceStruct*>(pUaPdoItem->getUserData());

					cPDOItem = static_cast<CanPDOItem *>(udbi->getDevice());

					cPDO->setItem(i, cPDOItem);
					pUaPdoItem->setAccess(cPDO->getDirectionType());
				}
			}
		}
		else {
			for (::PDO16::PDOITEM_iterator pit1 = pitem.begin(); pit1 != pitem.end(); pit1++)
			{
				name = pit1->name().c_str();
				CanOpenDataAddress::getOpcUaType(pit1->type().c_str(), typeName);
				uaId = pNodeManager->getNewNodeId(pUaPdo, name);

				defaultValue.changeType(typeName, false);
				UaVariableType* prType = pNodeManager->getInstanceDeclarationVariableType(BA_PDOITEM);   //! get an object type based on numerical id ( if not return null)

				pUaPdoItem = new UaControlDeviceItem(name, uaId, pNodeManager, defaultValue, prType,
					(::xml_schema::type *)&(*pit1),
					udb, pUaPdo->getSharedMutex());
				ret = pNodeManager->addNodeAndReference(pUaPdo, pUaPdoItem, OpcUaId_HasComponent);
				pUaPdoItem->setDataType(UaNodeId(typeName));
				pUserDeviceStruct * udbi = static_cast<pUserDeviceStruct *>(pUaPdoItem->getUserData());
				uadef.setDataValue(defaultValue, OpcUa_False, OpcUa_BadWaitingForInitialData, UaDateTime::now(), UaDateTime::now());
				pUaPdoItem->setValue(0, uadef, OpcUa_False);

				cPDOItem = static_cast<CanPDOItem *>(udbi->getDevice());

				cPDO->setItem((OpcUa_UInt16)0, cPDOItem);
				pUaPdoItem->setAccess(cPDO->getDirectionType());

			}
		}

		return ret;
	}

	UaStatus UaCanNodeObject::createSDOs(NODETYPE *sn,NmBuildingAutomation *pNodeManager)
	{
		UaStatus ret;
		UaString name;
		UaNodeId	uaId,uaIdsdo;
		UaControlDeviceGeneric *pUasdo;
		OpcUa_BuiltInType typeName; 
		UaControlDeviceItem *pUaSdoItem;
		DirectionType dir;
		UaVariant	defaultValue;
		SDO::SDOITEM_sequence sitem1;

		pUserDeviceStruct* interf = (pUserDeviceStruct *)getUserData();
		NODETYPE::SDO_sequence sit1 = sn->SDO();
		if (! sit1.empty() ) {
//			for (NODETYPE::SDO_iterator sit = sit1.begin(); sit != sit1.end(); sit++)
			OpcUa_Int32 len = sit1.size();
			for (OpcUa_Int32 l = 0; l < len ; l++)
			{
				name = sit1[l].name().c_str();

				uaIdsdo = pNodeManager->getNewNodeId(this,name);
				UaObjectType* prType = pNodeManager->getInstanceDeclarationObjectType(BA_SDOOBJECT);   //! get an object type based on numerical id ( if not return null)

				pUasdo	= new UaControlDeviceGeneric (name,uaIdsdo,pNodeManager,prType,
//				pUasdo = new UaControlDeviceGeneric(name, pNodeManager, prType,
					(::xml_schema::type *)&(sit1[l]),
					interf);
				ret = pNodeManager->addNodeAndReference( this,pUasdo, OpcUaId_HasComponent);
				UA_ASSERT(ret.isGood());

				sitem1 = sit1[l].SDOITEM();

				if (!sitem1.empty()) {
					for (SDO::SDOITEM_iterator sitem = sitem1.begin(); sitem != sitem1.end(); sitem++)
					{
						CanOpenDataAddress::getOpcUaType(sitem->type().c_str(),typeName); 
						CanOpenDataAddress::getDirection(sitem->access().c_str(),dir);

						name = sitem->name().c_str();	// name of sdo item

						UaString nTmp = UaString("%1.%2").arg(uaIdsdo.toString()).arg(name);
						uaId = UaNodeId(nTmp,pNodeManager->getNameSpaceIndex());				//create node id

						defaultValue.setInt32(0);
						defaultValue.changeType(typeName, false);
						UaVariableType* prType = pNodeManager->getInstanceDeclarationVariableType(BA_SDOITEMTYPE1);   //! get an object type based on numerical id ( if not return null)

						pUaSdoItem = new UaControlDeviceItem(name,uaId,pNodeManager,defaultValue,prType,
							&(*sitem),
//							(::xml_schema::type *)&(*sitem),
							static_cast<pUserDeviceStruct*>(pUasdo->getUserData()),pUasdo->getSharedMutex());
						pUaSdoItem->setAccess(dir);
						pUaSdoItem->setValueHandling(UaVariable_Value_Cache);

						pUaSdoItem->setDataType(UaNodeId(typeName));

						ret = pNodeManager->addNodeAndReference(pUasdo,pUaSdoItem, OpcUaId_HasComponent);
					}
				}
				SDO::SDOITEMARRAY_optional sdoi3 = sit1[l].SDOITEMARRAY();
				if (sdoi3.present()) {
					name = sdoi3.get().name().c_str();
					for (int n = 0; n < sdoi3.get().number(); n++)
					{
						name += UaString("_%1").arg(n);
						CanOpenDataAddress::getOpcUaType(sdoi3.get().type().c_str(),typeName);
						CanOpenDataAddress::getDirection(sdoi3.get().access().c_str(),dir);
						uaId = pNodeManager->getNewNodeId(pUasdo,name);
						defaultValue.setInt32(0);
						defaultValue.changeType(typeName, false);
						UaVariableType* prType = pNodeManager->getInstanceDeclarationVariableType(BA_SDOITEMTYPE3);   //! get an object type based on numerical id ( if not return null)

						pUaSdoItem = new UaControlDeviceItem(name,uaId,pNodeManager,defaultValue,prType,
							(::xml_schema::type *)&(sit1[l]),
							(pUserDeviceStruct*)pUasdo->getUserData(),pUasdo->getSharedMutex());
						pUaSdoItem->setAccess(dir);
						pUaSdoItem->setValueHandling(UaVariable_Value_Cache);

						pUaSdoItem->setDataType(UaNodeId(typeName));

						ret = pNodeManager->addNodeAndReference(pUasdo,pUaSdoItem, OpcUaId_HasComponent);
					}
				}
			}
		}
		return ret;
	}

	UaStatus UaCanNodeObject::createSDOs(NODE *sn,NmBuildingAutomation *pNodeManager)
	{
		UaStatus ret;
		UaString name;
		UaNodeId	uaId;
		UaControlDeviceGeneric *pUasdo;
		OpcUa_BuiltInType typeName; 
		UaControlDeviceItem *pUaSdoItem;
		DirectionType dir;
		UaVariant	defaultValue;
		pUserDeviceStruct* interf = (pUserDeviceStruct*)getUserData();
		for (NODE::SDO_iterator sit=sn->SDO().begin(); sit != sn->SDO().end(); sit++)
		{
			name = sit->name().c_str();
			uaId = pNodeManager->getNewNodeId(this,name);
			UaObjectType* prType = pNodeManager->getInstanceDeclarationObjectType(BA_SDOOBJECT);   //! get an object type based on numerical id ( if not return null)

			pUasdo	= new UaControlDeviceGeneric (name,uaId,pNodeManager,prType,
//			pUasdo = new UaControlDeviceGeneric(name, pNodeManager, prType,
				(::xml_schema::type *)&(*sit),
				interf);
			ret = pNodeManager->addNodeAndReference( this,pUasdo, OpcUaId_Organizes);
			UA_ASSERT(ret.isGood());
			SDO::SDOITEM_sequence sitem1 = sit->SDOITEM();
			for (SDO::SDOITEM_iterator sitem = sitem1.begin(); sitem != sitem1.end(); sitem++)
			{
				name = sitem->name().c_str();
				CanOpenDataAddress::getOpcUaType(sitem->type().c_str(),typeName);
				CanOpenDataAddress::getDirection(sitem->access().c_str(),dir);
				uaId = pNodeManager->getNewNodeId(pUasdo,name);

				defaultValue.setInt32(0);
				defaultValue.changeType(typeName, false);
				UaVariableType* prType = pNodeManager->getInstanceDeclarationVariableType(BA_SDOITEMTYPE1);   //! get an object type based on numerical id ( if not return null)

				pUaSdoItem = new UaControlDeviceItem(name,uaId,pNodeManager,defaultValue,prType,
					(::xml_schema::type *)&(*sitem),
					(pUserDeviceStruct*)pUasdo->getUserData(),pUasdo->getSharedMutex());
				pUaSdoItem->setAccess(dir);
				pUaSdoItem->setDataType(UaNodeId(typeName));

				ret = pNodeManager->addNodeAndReference(pUasdo,pUaSdoItem, OpcUaId_HasComponent);
			}
			SDO::SDOITEMARRAY_optional sdoi3 = sit->SDOITEMARRAY();
			if (sdoi3.present()) {
				name = sdoi3.get().name().c_str();
				for (int n = 0; n < sdoi3.get().number(); n++)
				{
					name += UaString("_%1").arg(n);
					CanOpenDataAddress::getOpcUaType(sdoi3.get().type().c_str(),typeName);
					CanOpenDataAddress::getDirection(sdoi3.get().access().c_str(),dir);
					uaId = pNodeManager->getNewNodeId(pUasdo,name);
					defaultValue.setInt32(0);
					defaultValue.changeType(typeName, false);
					UaVariableType* prType = pNodeManager->getInstanceDeclarationVariableType(BA_SDOITEMTYPE3);   //! get an object type based on numerical id ( if not return null)

					pUaSdoItem = new UaControlDeviceItem(name,uaId,pNodeManager,defaultValue,prType,
						(::xml_schema::type *)&(*sit),
						(pUserDeviceStruct*)pUasdo->getUserData(),pUasdo->getSharedMutex());
						pUaSdoItem->setAccess(dir);

						pUaSdoItem->setDataType(UaNodeId(typeName));

					ret = pNodeManager->addNodeAndReference(pUasdo,pUaSdoItem, OpcUaId_HasComponent);
				}
			}
		}

		return ret;
	}

	UaStatus UaCanNodeObject::createPROGRAMs(NODE *sn, NmBuildingAutomation *pNodeManager)
	{
		UaStatus ret;
		UaString name;
		UaNodeId	uaId;
		UaProgramItem *pProg;
//		OpcUa_BuiltInType typeName;
//		UaDeviceMethod *pUaMethod;
//		DirectionType dir;
//		UaVariant	defaultValue;

		for (NODE::PROGRAM_iterator pit = sn->PROGRAM().begin(); pit != sn->PROGRAM().end(); pit++)
		{
			name = pit->name().c_str();
			uaId = pNodeManager->getNewNodeId(this, name);
			PROGRAM *pt = &(*pit);
			UaObjectType* prType = pNodeManager->getInstanceDeclarationObjectType(BA_PROGRAM);   //! get an object type based on numerical id ( if not return null)
			pProg = new UaProgramItem(pt, prType, uaId, pNodeManager, this, static_cast<pUserDeviceStruct*>(getUserData()));
//			pProg = new UaProgramItem(pt, prType, name, pNodeManager, this, getUserData());
			ret = pNodeManager->addNodeAndReference(this, pProg, OpcUaId_Organizes);
			UA_ASSERT(ret.isGood());
		}

		return ret;
	}

//	template<typename NODEDEF>
	UaStatus UaCanNodeObject::createSDOITEMs(NODE *sdoi,NmBuildingAutomation *pNodeManager)
	{
		UaStatus ret;
		UaString name;
		UaNodeId	uaId;
		UaControlDeviceItem *pUaSdoItem;
		OpcUa_BuiltInType typeName; 
		DirectionType dir;
		UaVariant	defaultValue;

		pUserDeviceStruct* interf = (pUserDeviceStruct*)getUserData();
		for (NODE::SDOITEM_iterator sit=sdoi->SDOITEM().begin(); sit != sdoi->SDOITEM().end(); sit++)
		{
			name = sit->name().c_str();
			CanOpenDataAddress::getOpcUaType(sit->type().c_str(),typeName);
			CanOpenDataAddress::getDirection(sit->access().c_str(),dir);
			uaId = pNodeManager->getNewNodeId(this,name);
			defaultValue.setInt32(0);
			defaultValue.changeType(typeName, false);

			pUserDeviceStruct* pCanInter = pNodeManager->m_pControlInterface->createInterfaceEntry(interf,BA_SDOOBJECT,&(*sit),this);

			UaVariableType* prType = pNodeManager->getInstanceDeclarationVariableType(BA_SDOITEMTYPE2);   //! get an object type based on numerical id ( if not return null)

			pUaSdoItem = new UaControlDeviceItem(name,uaId,pNodeManager,defaultValue,prType,&(*sit),pCanInter,getSharedMutex());
			pUaSdoItem->setAccess(dir);
			pUaSdoItem->setValueHandling(UaVariable_Value_Cache);
			pUaSdoItem->setDataType(UaNodeId(typeName));
			ret = pNodeManager->addNodeAndReference(this, pUaSdoItem, OpcUaId_HasComponent);
		}

		return ret;
	}

	UaStatus UaCanNodeObject::createSDOITEMs(NODETYPE *sdoi,NmBuildingAutomation *pNodeManager)
	{
		UaStatus ret;
		UaString name;
		UaNodeId	uaId;
		UaControlDeviceItem *pUaSdoItem;
		OpcUa_BuiltInType typeName; 
		DirectionType dir;
		UaVariant	defaultValue;

		pUserDeviceStruct* interf = static_cast<pUserDeviceStruct*>(getUserData());

		for (NODETYPE::SDOITEM_iterator sit=sdoi->SDOITEM().begin(); sit != sdoi->SDOITEM().end(); sit++)
		{
			name = sit->name().c_str();
			CanOpenDataAddress::getOpcUaType(sit->type().c_str(),typeName);
			CanOpenDataAddress::getDirection(sit->access().c_str(),dir);
			uaId = pNodeManager->getNewNodeId(this,name);
			defaultValue.setInt32(0);
			defaultValue.changeType(typeName, false);

			pUserDeviceStruct* pCanInter = pNodeManager->m_pControlInterface->createInterfaceEntry(interf,BA_SDOOBJECT,&(*sit),this);
			UaVariableType* prType = pNodeManager->getInstanceDeclarationVariableType(BA_SDOITEMTYPE2);   //! get an object type based on numerical id ( if not return null)

			pUaSdoItem = new UaControlDeviceItem(name,uaId,pNodeManager,defaultValue,prType,&(*sit),pCanInter,getSharedMutex());
			pUaSdoItem->setAccess(dir);
			pUaSdoItem->setValueHandling(UaVariable_Value_Cache);

			pUaSdoItem->setDataType(UaNodeId(typeName));

			ret = pNodeManager->addNodeAndReference(this, pUaSdoItem, OpcUaId_HasComponent);
		}

		return ret;
	}
	
	UaCanNodeObject::~UaCanNodeObject(void)
	{
//		delete m_pIOManagerSDO;
//		m_pIOManagerSDO = NULL;
	}
	
	template<typename NODEDEF>
	UaStatus UaCanNodeObject::createItems(NODEDEF *nd, NmBuildingAutomation *pNodeManager)
	{
		UaStatus ret;
		ret = CanBusInterface::CompileItems(this,nd->ITEM(),nd->REGEXPR(), pNodeManager);
		if (ret.isBad()) {
			LOG(Log::ERR) << "Calc Item" << " failed to create Calc Item, for " << this->browseName().toString().toUtf8() << " " << ret.pDiagnosticInfo()->m_localizedText.toString().toUtf8();
			cerr << "Error Cacl Item " << this->browseName().toString().toUtf8() << " " << ret.pDiagnosticInfo()->m_localizedText.toString().toUtf8() << endl;
			exit(-1);
		}
	
		return ret;
	}
/*
	void UaCanNodeObject::setInitValue(UaVariant &uav,OpcUa_BuiltInType nType)
	{
		UaByteArray uba;
		switch (nType) {
		case OpcUaType_Boolean: uav.setBool(false); break;
		case OpcUaType_Byte: uav.setByte(0); break;
		case OpcUaType_Int16: uav.setInt16(0); break;
		case OpcUaType_UInt16: uav.setUInt16(0); break;
		case OpcUaType_Int32: uav.setInt32(0); break;
		case OpcUaType_UInt32: uav.setUInt32(0); break;
		case OpcUaType_Float: uav.setFloat(0.0); break;
		case OpcUaType_Double: uav.setDouble(0.0); break;
		case OpcUaType_ByteString: uav.setByteArray(uba); break;
		default: uav.setInt32(0); break;
		}
	}
*/
}
