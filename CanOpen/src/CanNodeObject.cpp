#include "CanNodeObject.h"
#include "CanSDOObject.h"
#include "CanBusObject.h"
#include "Ids.h"

OpcUa_Int16 CanOpen::CanNodeObject::ngCounter(3);
OpcUa_Boolean CanOpen::CanNodeObject::DisconectRecovery(true);


namespace CanOpen
{

	CanNodeObject::CanNodeObject(pUserDeviceStruct *interf,NODE *ca,UaNode *blink,OpcUa_UInt32 code)
		: CanObject(interf, ca, blink, code), m_state(0), m_iBootupCounter(0), m_EmergencyCounter(0)
	{
		m_requestedSDO = 0;
		CanBusObject * pcanbus = static_cast<CanBusObject *>(interf->getDevice());
		m_CanNodeId = ca->nodeid();
		setInitLive();
		setInitNmt((ca->nmt() == NULL) ? string() : ca->nmt().get());
		pcanbus->addNode(this);
		if (m_pCommIf) m_pCommIf = pcanbus->getCanBusInterface();

		m_pIOManager = new IOManagerSDO((UaCanNodeObject*)blink);		// Input/Output manager for SDO operation
	}

	CanNodeObject::~CanNodeObject(void)
	{

	}

	void CanNodeObject::passSDO( const CanMsgStruct *cms)
	{
		if (m_requestedSDO != NULL) {
			if (m_requestedSDO->getCobId() + CANOPEN_SDOSERVER_COBID == cms->c_id) {
				m_requestedSDO->pass(cms);
			}
		}
	}


	UaStatus CanNodeObject::sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value)
	{
		UaStatus ret = OpcUa_Good;
		string nameMethod;

		lock();
		switch (code) {
		case BA_CANOBJECT_START:
			sendNMT(start);
			nameMethod = "NMT Start";
			break;
		case BA_CANOBJECT_STOP:	
			sendNMT(stop);
			nameMethod = "NMT Stop";
			break;
		case BA_CANOBJECT_PREOPERATION:
			sendNMT(preOperational);
			nameMethod = "NMT PreOperation";
			break;
		case BA_CANOBJECT_RESET:
			sendNMT(reset);
			nameMethod = "NMT Reset";
			break;
		case BA_CANOBJECT_NMT:
			{
				OpcUa_Byte command = value->value()->Value.Byte;
				sendNMT((::NMTcommand)command);
				nameMethod = "NMT " + command;
			}
			break;

		default:
			ret = OpcUa_Bad;
		}
		unlock();

		LOG(Log::DBG, NMTMessage) << " bus=" << ((CanBusObject*)getParentDevice())->getCanBusName() << "node=" << getCanNodeId() << " phase=" << nameMethod ;
		
		return ret; 
	}
	
	void CanNodeObject::setState( const CanMsgStruct *cms)
	{
		UaDataValue dataValue;
		UaDateTime udt,sdt;
		UaVariant val;
		UaStatus Status;

		udt = UaDateTime::fromTime_t(cms->c_time.tv_sec);	// Time from can message
		udt.addMilliSecs(cms->c_time.tv_usec/1000);
		sdt = UaDateTime::now();					// server time
	
		OpcUa_Byte st = cms->c_data[0] & 0x7F;				//set state of the node from NG message
		
		NGComing(st);
		
		if (st > 0) {
			//  Write to state date variable
			if (m_state) {
				val.setByte(cms->c_data[0]);
				dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
				Status = m_state->setValue(NULL, dataValue, OpcUa_False);
			}
		}
		else {
			// increase Bootup item by 1
			OpcUa_UInt32 bcount;
			if (m_iBootupCounter) {
				dataValue = m_iBootupCounter->value(0);
				val = *dataValue.value();
				val.toUInt32(bcount);
				bcount +=1;
				val.setUInt32(bcount);
				dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
				Status = m_iBootupCounter->setValue(NULL, dataValue, OpcUa_False);

				// Boot up message
				// Create event
				/*
				BootupEvent eventData(browseName().namespaceIndex());
				// Create unique event id
				UaByteString eventId;
				EventManagerUaNode::buildEventId( UaByteString(), eventId);
				eventData.m_EventId.setByteString(eventId, OpcUa_True);

				// Fill all default event fields
				eventData.m_SourceNode.setNodeId(nodeId());
				eventData.m_SourceName.setString(browseName().toString());
				eventData.m_Time.setDateTime(udt);
				eventData.m_ReceiveTime.setDateTime(sdt);
				UaString messageText;
				messageText = UaString("The Bootup Message from Node %1 (%2)has come").arg(browseName().toString()).arg(getCanNodeId());
				eventData.m_Message.setLocalizedText(UaLocalizedText("en", messageText)); 
				eventData.m_Severity.setUInt16(500);

				// Fire the event
				m_pNodeConfig->getEventManagerUaNode()->fireEvent(&eventData);
				*/
			}
		}
	}

	/**
	* distribute emergency message can message to different items
	*/
	void CanNodeObject::setEmergencyEvent( const CanMsgStruct *cms)
	{
		UaDataValue dataValue;
		UaDateTime udt,sdt;
		UaVariant val;
		UaStatus Status;
		OpcUa_UInt32 st;

		udt = UaDateTime::fromTime_t(cms->c_time.tv_sec);	// Time from can message
		udt.addMilliSecs(cms->c_time.tv_usec/1000);
		sdt = UaDateTime::now();					// server time

		OpcUa_UInt16 serr = cms->c_data[1];
		serr = (serr << 8) + cms->c_data[0];
		val.setUInt16(serr);
		dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
		Status = m_EmergencyErrorCode->setValue(NULL, dataValue, OpcUa_False);

		dataValue = m_EmergencyCounter->value(0);
		val = *dataValue.value();
		val.toUInt32(st);
		st += 1;
		val.setUInt32(st);
		dataValue.setDataValue(val, OpcUa_False, OpcUa_Good, udt, sdt);
		Status = m_EmergencyCounter->setValue(NULL, dataValue, OpcUa_False);

		val.setByte(cms->c_data[2]);
		dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
		Status = m_Error->setValue(NULL, dataValue, OpcUa_False);

		val.setByte(cms->c_data[3]);
		dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
		Status = m_SpecificErrorCodeByte1->setValue(NULL, dataValue, OpcUa_False);

		val.setByte(cms->c_data[4]);
		dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
		Status = m_SpecificErrorCodeByte2->setValue(NULL, dataValue, OpcUa_False);

		val.setByte(cms->c_data[5]);
		dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
		Status = m_SpecificErrorCodeByte3->setValue(NULL, dataValue, OpcUa_False);

		val.setByte(cms->c_data[6]);
		dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
		Status = m_SpecificErrorCodeByte4->setValue(NULL, dataValue, OpcUa_False);

		val.setByte(cms->c_data[7]);
		dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
		Status = m_SpecificErrorCodeByte5->setValue(NULL, dataValue, OpcUa_False);

	}
	
	//! send NG message for this node
	void CanNodeObject::sendNG()			
		{ 
				if( m_pCommIf ) {
					LOG(Log::DBG, NGMessage) << "bus=" << ((CanBusObject*)getParentDevice())->getCanBusName() << " direction=send " << "node=" << std::hex << (int)getCanNodeId();

					//					tNGMessage("NG send nodeId = %X",getCanNodeId());
					m_pCommIf->sendRemoteRequest(CANOPEN_NODEGUARD_COBID+getCanNodeId());
				}
		}

	UaStatus CanNodeObject::connectCode(OpcUa_UInt32 code,::xsd::cxx::tree::type *conf,UaNode *blink)
	{
		UaDataValue udt;
		UaVariant val;
		UaDateTime sdt = UaDateTime::now();
		NODE *nodeb = (NODE *)conf;
		val.clear();

		switch (code) {
		case BA_CANOBJECT_NMT: 
			((OpcUa::BaseDataVariableType *)blink)->setDataType(OpcUaType_UInt32);
			val.setUInt32(0);
			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			((OpcUa::BaseDataVariableType *)blink)->setValueHandling(UaVariable_Value_Cache);
			break;
		case BA_CANNODETYPE_STATE: 
			udt.setDataValue(val, OpcUa_False, OpcUa_BadWaitingForInitialData, sdt, sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			m_state = (OpcUa::BaseVariableType *)blink;
			break;
		case BA_CANNODETYPE_BOOTUP: 
			val.setInt16(0);
			udt.setDataValue(val,OpcUa_False,OpcUa_Good,sdt,sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0,udt,OpcUa_False);
			m_iBootupCounter = (OpcUa::BaseVariableType *)blink;
			break;
		case BA_EMERGENCY_EMERGENCYERRORCODE:
			udt.setDataValue(val, OpcUa_False,OpcUa_BadWaitingForInitialData, sdt, sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			m_EmergencyErrorCode = (OpcUa::BaseVariableType *)blink;
			break;
		case BA_EMERGENCY_EMERGENCYCOUNTER:
			val.setUInt32(0);
			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			m_EmergencyCounter = (OpcUa::BaseVariableType *)blink;
			break;
		case BA_EMERGENCY_ERROR: 
			udt.setDataValue(val, OpcUa_False, OpcUa_BadWaitingForInitialData, sdt, sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			m_Error = (OpcUa::BaseVariableType *)blink;
			break;
		case BA_EMERGENCY_SPECIFICERRORCODEBYTE1:
			udt.setDataValue(val, OpcUa_False, OpcUa_BadWaitingForInitialData, sdt, sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			m_SpecificErrorCodeByte1 = (OpcUa::BaseVariableType *)blink;
			break;
		case BA_EMERGENCY_SPECIFICERRORCODEBYTE2: 
			udt.setDataValue(val, OpcUa_False, OpcUa_BadWaitingForInitialData, sdt, sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			m_SpecificErrorCodeByte2 = (OpcUa::BaseVariableType *)blink;
			break;
		case BA_EMERGENCY_SPECIFICERRORCODEBYTE3:
			udt.setDataValue(val, OpcUa_False, OpcUa_BadWaitingForInitialData, sdt, sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			m_SpecificErrorCodeByte3 = (OpcUa::BaseVariableType *)blink;
			break;
		case BA_EMERGENCY_SPECIFICERRORCODEBYTE4:
			udt.setDataValue(val, OpcUa_False, OpcUa_BadWaitingForInitialData, sdt, sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			m_SpecificErrorCodeByte4 = (OpcUa::BaseVariableType *)blink;
			break;
		case BA_EMERGENCY_SPECIFICERRORCODEBYTE5:
			udt.setDataValue(val, OpcUa_False, OpcUa_BadWaitingForInitialData, sdt, sdt);
			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
			m_SpecificErrorCodeByte5 = (OpcUa::BaseVariableType *)blink;
			break;
		//case BA_CANNODETYPE_TYPE:
		//	if (nodeb != NULL)
		//		if (nodeb->type().present()) {
		//			val.setString(UaString(nodeb->type().get().c_str()));
		//			udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
		//			((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
		//		}
		//	break;
		//case BA_CANNODETYPE_NODEID:
		//{
		//	OpcUa_UInt16 n = nodeb->nodeid();
		//	val.setUInt16(n);
		//	udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
		//	((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
		//}
		//	break;
		default: return OpcUa_Bad;
		}

		return OpcUa_Good;
	}

	UaVariant CanNodeObject::getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf)
	{
		UaVariant val;
		val.clear();
		NODE *nodeb = (NODE *)conf;

		switch (code) {
		case BA_CANNODETYPE_TYPE:
		if (nodeb != NULL)
			if (nodeb->type().present()) {
				val.setString(UaString(nodeb->type().get().c_str()));
			}
		break;
		case BA_CANNODETYPE_NODEID:
		{
			OpcUa_UInt16 n = nodeb->nodeid();
			val.setUInt16(n);
		}
		break;
		}
		return val;
	}


	void CanNodeObject::setLastState()
	{
		OpcUa_Byte st;
		if (isAlive()) 
			st = m_currentState;
		else st = m_lastState;
		UaThread::usleep(100);
		UaNode * discPDO = static_cast<UaNode *>(getAddressSpaceEntry());
		setStateRecursevely(discPDO, true);
		switch (st) 
		{
		case NMT_STOPPED: 
			sendNMT(stop);
			break;
		case NMT_OPERATIONAL: 
			sendNMT(start);
			break;

		case NMT_PREOPERATIONAL:
			sendNMT(preOperational);
			break;
		default: break;
		}
	}

	void CanNodeObject::setDisconnectedState( )
	{
		UaDataValue dataValue;
		UaDateTime udt,sdt;
		UaVariant val;
		UaStatus Status;
		
		m_lastState = m_currentState;
		m_currentState = NMT_DISCONNECTED;

		udt = UaDateTime::now();	// Time from can message
		sdt = UaDateTime::now();	// server time

		UaNode * discPDO = static_cast<UaNode *>(getAddressSpaceEntry());
		setStateRecursevely(discPDO, false);

		val.setByte(NMT_DISCONNECTED);
		dataValue.setDataValue(val,OpcUa_False,OpcUa_Good,udt,sdt);
		Status = m_state->setValue(0, dataValue, OpcUa_False);
//		

//	printf("Set Disconnect state %d %d\n",m_lastState , m_currentState);

	}

	void CanNodeObject::setStateRecursevely(UaNode *p, bool state)
	{
		UaReferenceLists *rl = p->getUaReferenceLists();
		for (UaReference *ur = const_cast<UaReference *>(rl->pTargetNodes()); ur != NULL; ur = ur->pNextForwardReference())
		{
			OpcUa_UInt32 refType = ur->referenceTypeId().identifierNumeric();
			if (refType == OpcUaId_HasComponent) {
				UaNode *pNode = ur->pTargetNode();
				int t = pNode->nodeClass();
				if (t == OpcUa_NodeClass_Object)
					setStateRecursevely(pNode, state);
				else
					if (t == OpcUa_NodeClass_Variable)
					{
						UaDataValue udv = (static_cast<UaVariable *>(pNode))->value(0);
						UaVariant v = *udv.value();
						if (!v.isEmpty()) {
//							std::cout << "Variant is empty" << std::endl;
							UaDataValue dv;

							if (state)
							{
								//							udv.setStatusCode(OpcUa_Good);
								dv.setDataValue(v, OpcUa_False, OpcUa_Good, UaDateTime::now(), UaDateTime::now());
							}
							else
							{
								dv.setDataValue(v, OpcUa_False, OpcUa_BadNoCommunication, UaDateTime::now(), UaDateTime::now());
								//dv.setStatusCode(OpcUa_BadNoCommunication);
							}
							//dv.setServerTimestamp(UaDateTime::now());
							//dv.setSourceTimestamp(UaDateTime::now());


							UaStatus us = (static_cast<UaVariable *>(pNode))->setValue(0, dv, OpcUa_False);
							//						cout << pNode->displayName(0).toString().toUtf8() << " " << state << " " << us.toString().toUtf8() <<  endl;
						}
					}
			}
		}
	}

}
