#include "SyncInterval.h"
#include "uadatetime.h"
#include "CanBusObject.h"
#include <thread>

namespace CanOpen
{
	/** ----------------------------------------------------------------------------
	 * Begin Class    SyncInterval
	 * send Sync message periodically
	 * constructors / destructors
	-----------------------------------------------------------------------------*/
	SyncInterval::SyncInterval(CanBusObject *pCI,OpcUa::BaseVariableType* pVar )
	{
		m_stop = OpcUa_False;
		m_iSyncIntervalVariable = pVar;		//sync interval
		m_pCanIn = pCI;						// can bus
//		start();
	}

	SyncInterval::~SyncInterval()
	{
		// Signal SyncInterval Thread to stop
		m_stop = OpcUa_True;
		// Wait until SyncInterval Thread stopped
		wait(1);
	}


	/** ----------------------------------------------------------------------------
	Class        syncInterval
	Method       run
	Description  Thread main function.
	-----------------------------------------------------------------------------*/
	void SyncInterval::run()
	{
		UaDataValue dSync;
		//    UaDateTime lastSimulation = UaDateTime::now();
		while( m_stop == OpcUa_False )
		{
			if (m_iSyncIntervalVariable != 0) {
	
				dSync = m_iSyncIntervalVariable->value(NULL);
				const OpcUa_Variant *vSync = dSync.value();
				OpcUa_Int32 tInterval = vSync->Value.Int32;
				if (tInterval != 0 ) {
					m_pCanIn->lock();
					m_pCanIn->sendSync();
					m_pCanIn->unlock();
					if (!m_stop) 
						std::this_thread::sleep_for(std::chrono::milliseconds(tInterval));

//					if (!m_stop) UaThread::msleep(tInterval);
				}
				else if (!m_stop) 
					std::this_thread::sleep_for(std::chrono::seconds(10));
//					UaThread::sleep(10);
			}
			else  
				if (!m_stop)
					std::this_thread::sleep_for(std::chrono::seconds(10));
//					UaThread::sleep(10);
		}
	}
}
