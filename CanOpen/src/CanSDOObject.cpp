#include <opcua_basedatavariabletype.h>
#include "uabasenodes.h"
#include "CanSDOObject.h"
#include "CanSDOItem.h"
#include "CanNodeObject.h"
#include "CanOpenData.h"
#include "CANopen.h"
#include <uadatetime.h>
#include "uasemaphore.h"
#include "UaCanTrace.h"
#include <boost/lexical_cast.hpp>


namespace CanOpen
{
	using boost::lexical_cast;
	using boost::bad_lexical_cast;
	/*
	* Constructor of CanSDOObject
	* @par CanObject using to pass message to bus
	* @conf SDO xml entry
	* @blink  
	*/
	CanSDOObject::CanSDOObject(pUserDeviceStruct *par,
			SDO *conf,UaNode *blink,int code) : CanOpenObject( par, conf,blink,code)
	{
		const char *p = conf->index().data();
		
		m_ind = (OpcUa_UInt16)strtol(p,NULL,16);
//		m_buffer.resize(8);
		CanNodeObject *cno = static_cast<CanNodeObject *>(par->getDevice());
		m_cobId = cno->getCanNodeId();
		
		m_iCount = 0;
		m_iMaxCount = 1;
	}
	
	OpcUa_BuiltInType CanSDOObject::getItemType()
	{ 
		return m_pSDOItem->getItemType();
	}

	OpcUa_Byte CanSDOObject::getSubIndex() 
	{ 
		return m_pSDOItem->getSubIndex();
	}

/*
	CanSDOObject::CanSDOObject(UserDataBase *par,
			SDOITEM_TYPE2 *conf,UaNode *blink,int code) : CanOpenObject( par, conf,blink,code)
	{
		char *p;
		m_ind = (OpcUa_UInt16)strtol(conf->index().c_data(),&p,16);
		m_buffer.resize(8);
//		CanNodeObject *cno = ((UserDataBase_ptr<CanNodeObject> *)par)->getDevice();
		CanNodeObject *cno = (CanNodeObject *)par;
		m_cobId = cno->getCanNodeId();

		m_iCount = 0;
		m_iMaxCount = 1;
	}
	*/
	/*
	* Constructor of CanSDOObject
	* @index of SDO message
	*/
	/*
	CanSDOObject::CanSDOObject(OpcUa_UInt16 index) : CanOpenObject(0,0,0,Ba_SDOType)
			, m_waitSdo(0,1)
	{
		m_ind = index;
		m_buffer.resize(8);
	}
	*/
	/**
	* SDO callback function to put sdo message to buffer
	*/
	void CanSDOObject::pass(const CanMsgStruct *cms)
	{

		m_udt = UaDateTime::fromTime_t(cms->c_time.tv_sec);		//  write time
		m_udt.addMilliSecs(cms->c_time.tv_usec/1000);			//	in millisecond
		for(int i = 0; i < cms->c_dlc; i++)
			m_buffer[i] = cms->c_data[i];						//  put data
		messageCame(); 											//	inform that message came
	}


	CanSDOObject::~CanSDOObject(void)
	{
	}

	///
	/// Check id this message for this object
	///
	bool CanSDOObject::isMsgForObject(const CanMsgStruct *cms) 
	{
		short index = cms->c_data[2];
		index = (index << 8) + cms->c_data[1];
		return ((cms->c_id == CANOPEN_SDOSERVER_COBID + getCobId()) && (m_ind == index) && (getSubIndex() == cms->c_data[3]));
	}

	UaStatus CanSDOObject::getDeviceMessage(OpcUa_UInt32 code)
	{
		OpcUa_ReferenceParameter(code);
		UaStatus ret = OpcUa_Good;
		CanNodeObject *cno = static_cast<CanNodeObject*>(getParentDevice());

		if (!cno->isAlive() && CanOpen::noSendDisconnectedSDO)
		{
			ret.setStatus(OpcUa_BadNoCommunication,"Device Disconnected");
			return ret;
		}

		lock();
		CCanAccess *cacc = getCanBus();
		if (cacc)
			ret = cacc->sendMessage(CANOPEN_SDOCLIENT_COBID + getCobId(), 8,(unsigned char *)(m_buffer.data()));
		else ret = OpcUa_Bad;
		unlock();
		if (ret.isGood()) {
			ret = waitOperation();
		}

		return ret;
	}
	UaStatus CanSDOObject::sendDeviceMessage(OpcUa_UInt32 code, UaDataValue *value)
	{
		OpcUa_ReferenceParameter(code);
		OpcUa_ReferenceParameter(value);
		UaStatus ret = OpcUa_Good;

		CanNodeObject *cno = static_cast<CanNodeObject*>(getParentDevice());
		if (!cno->isAlive() && CanOpen::noSendDisconnectedSDO)
		{
			ret.setStatus(OpcUa_BadNoCommunication, "Device Disconnected");
			return ret;
		}

		lock();
		CCanAccess *cacc = getCanBus();
		if (cacc)
			ret = cacc->sendMessage(CANOPEN_SDOCLIENT_COBID + getCobId(), 8, (unsigned char *)(m_buffer.data()));
		else ret = OpcUa_Bad;
		unlock();
		if (ret.isGood()) {
			ret = waitOperation();
		}

		return ret;
	}

	UaStatus CanSDOObject::connectCode(OpcUa_UInt32 code,::xsd::cxx::tree::type *conf,UaNode *blink)
	{
		OpcUa_ReferenceParameter(code);
		OpcUa_ReferenceParameter(conf);
		OpcUa_ReferenceParameter(blink);

		//UaDataValue udt;
		//UaVariant val;
		//UaDateTime sdt = UaDateTime::now();
		//char *end;

		//SDO *sdob = (SDO *)conf;

		//if (code == BA_SDOOBJECT_INDEX) {

		//	m_ind = strtoul(sdob->index().c_str(),&end,16);
		//	val.setString(UaString(sdob->index().c_str()));
		//
		//	udt.setDataValue(val, OpcUa_False, OpcUa_Good, sdt, sdt);
		//	((OpcUa::BaseDataVariableType *)blink)->setValue(0, udt, OpcUa_False);
		//	return OpcUa_Good;
		//}
		//else return OpcUa_Bad;
		return OpcUa_Good;
	}

	UaVariant CanSDOObject::getPropertyValue(OpcUa_UInt32 code, ::xsd::cxx::tree::type *conf)
	{
		UaVariant val;
		val.clear();
		SDO *sdob = (SDO *)conf;

		if (code == BA_SDOOBJECT_INDEX) {
//			m_ind = stoi(sdob->index());
//			m_ind = strtoul(sdob->index().c_str(), NULL, 16);
			val.setString(UaString(sdob->index().c_str()));

		}
		return val;
	}


	UaStatus CanSDOObject::readSdo(UaDataValue &udv)
	{
		UaStatus ret = OpcUa_Good;
		OpcUa_Boolean togleBit = false;

		/// create CAN message for initial request
		m_buffer[0] = SDO_INITIATE_UPLOAD_REQ;
		m_buffer[1] = 0xFF & m_ind;
		m_buffer[2] = 0xFF & (m_ind >> 8);
		m_buffer[3] = getSubIndex();
		m_buffer[4] = m_buffer[5] = m_buffer[6] = m_buffer[7] = 0;
		if (getItemType() == OpcUaType_ByteString)
			LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Read request " << "CobId=" << hex << getCobId() << " index = " << hex << getIndex() << " subindex = " << hex << (int)getSubIndex();
		else
			LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " Read request " << "CobId=" << hex << getCobId() << " index = " << hex << getIndex() << "  subindex= " << hex << std::hex << (int)getSubIndex();


		ret = getDeviceMessage(BA_SDOITEMTYPE);		// send read request 
		if (ret.isBad()) {
			LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Read request bad " << ret.toString().toUtf8() << " " << "CobId=" << hex << getCobId() << " index = " << hex <<
				getIndex() << " subindex = " << std::hex << (int)getSubIndex();
			LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " Read request bad " << ret.toString().toUtf8() << " " << "CobId=" << hex << getCobId() <<
				" index = " << hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();

			return ret;
		}
		OpcUa_UInt32 len;

		if (m_buffer[0] & SDO_INITIATE_UPLOAD_RESP) {
			if ((m_buffer[0] & SDO_EXPEDITED)) {		// expedited transfer 
				OpcUa_Byte nn;
				if (m_buffer[0] & SDO_DATA_SIZE_INDICATED) {
					nn = 4 - ((m_buffer[0] & SDO_DATA_SIZE_MASK) >> 2);
				}
				else nn = 0;
				if (getItemType() == OpcUaType_ByteString) {
					m_iLength = nn;
					m_segSdoBuffer.resize(nn);
					for (OpcUa_Int32 i = 0; i < nn; i++) {
						m_segSdoBuffer[i] = m_buffer[4 + i];
					}
				}
				UaDateTime sdt = UaDateTime::now();
				UaVariant val = getSDOItem()->UnPack();
				udv.setDataValue(val, OpcUa_False, OpcUa_Good, m_udt, sdt);
				LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " Read answer " << "CobId=" << std::hex << getCobId() << " " << std::hex << m_buffer.data()[0] <<
					" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex() << " "
					<< std::hex << (int)m_buffer.data()[4] << " " << std::hex << (int)m_buffer.data()[5] << " " << std::hex << (int)m_buffer.data()[6] << " " << std::hex << (int)m_buffer.data()[7];

			}
			else {
				len = (OpcUa_Byte)m_buffer[7];
				len = (((((len << 8) + (OpcUa_Byte)m_buffer[6]) << 8) + (OpcUa_Byte)m_buffer[5]) << 8) + (OpcUa_Byte)m_buffer[4];
				m_segSdoBuffer.resize(len);
				m_iLength = len;
				LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " initial read answer " << "CobId=" << std::hex << getCobId() << " " << std::hex << m_buffer.data()[0] <<
					" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex() << " len = " << std::hex << len;

				OpcUa_UInt32 nByte = 0;

				while (nByte < len) {
					m_buffer[0] = (togleBit ? (SDO_UPLOAD_SEGMENT_REQ | SDO_TOGGLE_BIT) : SDO_UPLOAD_SEGMENT_REQ);
					togleBit = !togleBit;
					m_buffer[1] = 0xFF & m_ind;
					m_buffer[2] = 0xFF & (m_ind >> 8);
					m_buffer[3] = getSubIndex();
					m_buffer[4] = m_buffer[5] = m_buffer[6] = m_buffer[7] = 0;
					ret = getDeviceMessage(BA_SDOITEMTYPE);		// send read request 
					if (ret.isBad()) {
						LOG(Log::DBG, SegSdoMessage) << " bus= " << getBusName() << " read " << ret.pDiagnosticInfo()->m_localizedText.toString().toUtf8() <<
							" " << ret.toString().toUtf8() << " CobId= " << std::hex << getCobId() << " index = " << std::hex << getIndex() << " subindex = " << std::hex << getSubIndex();

						(static_cast<CanNodeObject *>(getParentDevice()))->freeRequestSDO();
						return ret;
					}
					LOG(Log::DBG, SegSdoMessage) << " bus= " << getBusName() << " read " << "CobId=" << std::hex << getCobId() << " " << std::hex <<
						(int)m_buffer.data()[0] << " " << std::hex << (int)m_buffer.data()[1] << " " << std::hex << (int)m_buffer.data()[2] << " " << std::hex << (int)m_buffer.data()[3] << " " << std::hex <<
						(int)m_buffer.data()[4] << " " << std::hex << (int)m_buffer.data()[5] << " " << std::hex << (int)m_buffer.data()[6] << " " << std::hex << (int)m_buffer.data()[7];

					OpcUa_UInt32 header = m_buffer[0] & 0xE0;
					if (header == SDO_UPLOAD_SEGMENT_RESP) {
						OpcUa_Int32 lll = len - nByte;
						if (lll < 8) {
							if (m_buffer[0] & SDO_LAST_SEGMENT) {
								for (OpcUa_Int32 i = 1; i <= lll; i++)
									m_segSdoBuffer[(OpcUa_Int32)nByte++] = m_buffer[i];
								UaDateTime sdt = UaDateTime::now();
								UaVariant val;
								LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Read String " << "CobId=" << std::hex << getCobId() <<
									" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex() << " len = " << len << " " << m_segSdoBuffer.toHex().toUtf8();

								val = getSDOItem()->UnPack();
								udv.setDataValue(val, OpcUa_False, OpcUa_Good, m_udt, sdt);
								return ret;
							}
							else {
								LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " read length was incorrect " << "CobId=" << std::hex << getCobId() <<
									" index = " << std::hex << getIndex() << " subindex = " << std::hex << getSubIndex();

								ret.setStatus(OpcUa_UncertainNoCommunicationLastUsableValue, "Segmented SDO transfer was incorrect");
								return ret;
							}
						}
						else {
							for (OpcUa_Int32 i = 1; i < 8; i++)
								m_segSdoBuffer[(OpcUa_Int32)nByte++] = m_buffer[i];
						}
					}
					else {
						if (m_buffer[0] & SDO_ABORT_TRANSFER) {
							LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " read Aborted " << "CobId=" << std::hex << getCobId() <<
								" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();
							ret.setStatus(OpcUa_UncertainNoCommunicationLastUsableValue, "SDO transfer was aborted");
						}
						else {
							LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " read answer was incorrect " << "CobId=" << std::hex << getCobId() <<
								" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();
							ret.setStatus(OpcUa_UncertainNoCommunicationLastUsableValue, "Segmented SDO transfer was incorrect");
						}
						return ret;
					}
				}
			}
		}
		else
			if (m_buffer[0] & SDO_ABORT_TRANSFER) {
				LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " read Aborted " << "CobId=" << std::hex << getCobId() <<
					" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();
				ret.setStatus(OpcUa_UncertainNoCommunicationLastUsableValue, "SDO transfer was aborted");
			}
			else {
				LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Read String " << "CobId=" << std::hex << getCobId() <<
					" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex() << " len = " << m_iLength << " " << m_segSdoBuffer.toHex().toUtf8();
			}
			return ret;
	}



	UaStatus CanSDOObject::writeSdo(UaDataValue &udv)
	{
		UaStatus ret = OpcUa_Good;
		LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Write request " << "CobId=" << hex << getCobId() <<
			" index = " << hex << getIndex() << " subindex=" << hex << (int)getSubIndex() << " fun=" << hex << setw(2) << (int)m_buffer[0];
		LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " Write request " << "CobId=" << hex << getCobId() <<
			" index=" << hex << getIndex() << "  subindex=" << hex << std::hex << (int)getSubIndex() << " fun=" << hex << setw(2) << (int)m_buffer[0];

		ret = getSDOItem()->pack(udv.value());
		if (ret.isGood()) {
			OpcUa_Int32 len = m_iLength;
			OpcUa_Int32 index = 0;
			OpcUa_Byte nn;

			ret = sendDeviceMessage(BA_SDOITEMTYPE);
			if (ret.isBad()) {
				LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Write bad " << ret.toString().toUtf8() << " CobId=" << std::hex << getCobId() <<
					" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();
				LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " Write bad " << ret.toString().toUtf8() << " CobId=" << std::hex << getCobId() <<
					" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();

				return ret;
			}

			OpcUa_UInt32 header = m_buffer[0] & 0xF0;
			if (header == SDO_INITIATE_DOWNLOAD_RESP) {
				if (getItemType() == OpcUaType_ByteString) {
					LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " initial write answer " << "CobId=" << std::hex << getCobId() <<
						" " << std::hex << setw(2) << m_buffer.data()[0] << " index = " << std::hex << getIndex() << " subindex = " << std::hex << setw(2) << (int)getSubIndex() << " len = " << len;

					//					tSegSdoMessage(" bus %s initial write answer node=%X func %X index=%X subindex=%hhX len=%X",getBusName(), getCobId(), m_buffer.c_data()[0],getIndex(),getSubIndex(),len);

					if (len < 8) {
						nn = 7 - (OpcUa_Byte)len;
						m_buffer[0] = SDO_DOWNLOAD_SEGMENT_REQ | SDO_LAST_SEGMENT | (nn << 1);
						for (OpcUa_Int32 i = 1; i <= len; i++)
							m_buffer[i] = m_segSdoBuffer[index++];
						ret = sendDeviceMessage(BA_SDOITEMTYPE);
						if (ret.isBad()) {
							LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Write bad " << ret.toString().toUtf8() << " CobId=" << std::hex << getCobId() <<
								" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();

							return ret;
						}
					}
					else {
						m_buffer[0] = SDO_DOWNLOAD_SEGMENT_REQ;
						while (len > 0) {
							for (OpcUa_Int32 i = 1; i < 8; i++)
								m_buffer[i] = m_segSdoBuffer[index++];
							len -= 7;
							ret = sendDeviceMessage(BA_SDOITEMTYPE);
							if (ret.isBad()) {
								LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Write bad " << ret.toString().toUtf8() <<
									"CobId=" << std::hex << getCobId() << " index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();
								return ret;
							}
							header = m_buffer[0] & 0xE0;
							if (header == SDO_DOWNLOAD_SEGMENT_RESP) {
								LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Write answer " << "CobId=" << std::hex << getCobId() << " " << std::hex << m_buffer.data()[0] <<
									" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex() << " len = " << len;

								if (len < 8) {
									nn = 7 - (OpcUa_Byte)len;
									if (m_buffer[0] & SDO_TOGGLE_BIT)
										m_buffer[0] = SDO_DOWNLOAD_SEGMENT_REQ | SDO_LAST_SEGMENT | (nn << 1);
									else {
										m_buffer[0] = SDO_DOWNLOAD_SEGMENT_REQ | SDO_LAST_SEGMENT | (nn << 1);
										m_buffer[0] = m_buffer[0] | SDO_TOGGLE_BIT;
									}
									for (OpcUa_Int32 i = 1; i <= len; i++)
										m_buffer[i] = m_segSdoBuffer[index++];
									ret = sendDeviceMessage(BA_SDOITEMTYPE);
									if (ret.isBad()) {
										LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Write" << ret.toString().toUtf8() << " CobId=" << std::hex << getCobId() <<
											" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();

										return ret;
									}
									LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " End write answer " << "CobId=" << std::hex << getCobId() << " " << std::hex << m_buffer.data()[0] <<
										" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();

									len = 0;
								}
								else {
									if (m_buffer[0] & SDO_TOGGLE_BIT)
										m_buffer[0] = SDO_DOWNLOAD_SEGMENT_REQ;
									else
										m_buffer[0] = SDO_DOWNLOAD_SEGMENT_REQ | SDO_TOGGLE_BIT;
								}
							}
							else {
								if (m_buffer[0] & SDO_ABORT_TRANSFER) {
									LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " Write request Aborted " << "CobId=" << std::hex << getCobId() <<
										" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();

									ret.setStatus(OpcUa_UncertainNoCommunicationLastUsableValue, "SDO transfer was aborted");
								}
								else {
									LOG(Log::DBG, SegSdoMessage) << " bus " << getBusName() << " Write answer was incorrect " << "CobId=" << std::hex << getCobId() <<
										" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();

									ret.setStatus(OpcUa_UncertainNoCommunicationLastUsableValue, "Segmented SDO transfer was incorrect");
								}
								return ret;
							}
						}
					}
					m_iLength = 0;

				}
			}
			if (m_buffer[0] & SDO_ABORT_TRANSFER) {
				LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " Write request Aborted " << "CobId=" << std::hex << getCobId() <<
					" index = " << getIndex() << " subindex = " << std::hex << (int)getSubIndex();

				ret.setStatus(OpcUa_UncertainNoCommunicationLastUsableValue, "SDO transfer was aborted");
			}
			else {
				LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " Last respons Correct " << "CobId=" << std::hex << getCobId() << " index = " << getIndex() << " subindex = " << std::hex << (int)getSubIndex();

			}
		}
		else {
			LOG(Log::DBG, SdoMessage) << " bus " << getBusName() << " Write bad " << ret.toString().toUtf8() << " CobId=" << std::hex << getCobId() <<
				" index = " << std::hex << getIndex() << " subindex = " << std::hex << (int)getSubIndex();

		}
		return ret;
	}

	void CanSDOObject::setSDOItem(CanSDOItem *csdo)
	{
		static_cast<CanNodeObject *>(getParentDevice())->setRequestSDO(this);
		m_pSDOItem = csdo; 
	}
	void CanSDOObject::freeSDOItem() 
	{
		m_pSDOItem = 0; 
		static_cast<CanNodeObject *>(getParentDevice())->freeRequestSDO();
	}

}
