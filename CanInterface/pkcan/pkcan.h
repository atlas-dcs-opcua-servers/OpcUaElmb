/*
 *pkcan.h
 *
 *  Created on: JUL 2, 2012
 *      Author: vfilimon
 */

#ifndef CCANPKSCAN_H_
#define CCANPKSCAN_H_


#include <string>
using namespace std;

#include "windows.h"

#ifdef WIN32
#include "Winsock2.h"
#endif

#include <PCANBasic.h>
#include "CCanAccess.h"

//class CCanCallback;

class PKCanScan: public CCanAccess
{
public:
	PKCanScan();
	virtual ~PKCanScan();
	virtual bool createBUS(const char * ,const char *);
    virtual bool sendMessage(short cobID, unsigned char len, unsigned char *message);
	virtual bool sendRemoteRequest(short cobID);
	
	virtual bool getErrorMessage(long error, char **message);
private:
	TPCANHandle getHandle(const char *name);
	string getNamePort() { return canName; }
	bool sendErrorCode(long);

	TPCANStatus bus_status;
	bool run_can;
	int numChannel;
	unsigned int BaudRate;

	string canName;

#define Timeout  1000

   	TPCANHandle	canObjHandler;
	bool configureCanboard(const char *,const char *);

	HANDLE      m_hCanScanThread;
	HANDLE		m_ReadEvent;

    // Thread ID for the CAN update scan manager thread.
    DWORD           m_idCanScanThread;

	static DWORD WINAPI CanScanControlThread(LPVOID pCanScan);

};

#endif