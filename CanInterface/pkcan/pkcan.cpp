// This is the main DLL file.

#include "pkcan.h"

//////////////////////////////////////////////////////////////////////
// kvCann.cpp: implementation of the KVCanScan class.
//////////////////////////////////////////////////////////////////////
//#include "CCanCallback.h"
#include <time.h>
#include <stdio.h>
#include <string.h>

bool  initLibarary =  false;
#ifdef WIN32 
	extern "C" __declspec(dllexport) CCanAccess *getCanbusAccess()
#else
	extern "C" CCanAccess *getCanbusAccess()
#endif
	{
		CCanAccess *cc;
		cc = new PKCanScan;

		return cc;
	}

#include "gettimeofday.h"

PKCanScan::PKCanScan()
{
	return ;
}

PKCanScan::~PKCanScan()
{
	run_can = false;
	CloseHandle(m_ReadEvent);
	CAN_Uninitialize(canObjHandler);
}

DWORD WINAPI PKCanScan::CanScanControlThread(LPVOID pCanScan)
{
//	DWORD waitStatus;
	TPCANStatus Status;
	TPCANMsg msg;
	TPCANTimestamp time;
	TPCANHandle xHandler;
	CanMsgStruct cmt;
//	struct timezone tm;

	PKCanScan *ppcs = reinterpret_cast<PKCanScan *>(pCanScan);
	xHandler = ppcs->canObjHandler;
	while ( ppcs->run_can ) {
//		waitStatus = WaitForSingleObject(ppcs->m_ReadEvent,INFINITE);
//		if (waitStatus == WAIT_OBJECT_0) {
			Status = CAN_Read(xHandler,&msg,&time);
			if (Status == PCAN_ERROR_OK) {
				for(int i=0; i < 8; i++) cmt.c_data[i] = msg.DATA[i];
				cmt.c_dlc = msg.LEN;
				cmt.c_id = msg.ID;
				cmt.c_ff = msg.MSGTYPE;

				unsigned long long mmsec = 0xFFFFFFFFUL;
				mmsec = mmsec *  1000;
				mmsec = mmsec * time.millis_overflow;
				mmsec = mmsec + time.micros;
				mmsec = mmsec + 1000 * time.millis;

				cmt.c_time.tv_usec = mmsec % 1000000UL;
				cmt.c_time.tv_sec = mmsec / 1000000UL;
				ppcs->canMessageCame(cmt);
			}
			else {
				if (Status & PCAN_ERROR_QRCVEMPTY) {
					continue;
				}
				ppcs->sendErrorCode(Status);
				if (Status | PCAN_ERROR_ANYBUSERR) {
					CAN_Initialize(xHandler,ppcs->BaudRate);
					Sleep(100);
				}
			}
//		}
//		else break;
	}

	ExitThread(0);
	return 0;
}

bool PKCanScan::configureCanboard(const char *name,const char *parameters)
{
	TPCANStatus           Status = PCAN_ERROR_OK;

	unsigned int tseg1 = 0;
	unsigned int tseg2 = 0;
	unsigned int sjw = 0;
	unsigned int noSamp = 0;
	unsigned int syncmode = 0;
	BaudRate = PCAN_BAUD_125K;
	long br;
	int	numPar; 

	numPar = sscanf_s(parameters,"%d %d %d %d %d %d",&br,&tseg1,&tseg2,&sjw,&noSamp,&syncmode);

	/* Set baud rate to 125 Kbits/second.  */
	canObjHandler = getHandle(name);

	if (numPar == 1) {
		switch(br) {
		case 50000: BaudRate = PCAN_BAUD_50K; break;
		case 100000: BaudRate = PCAN_BAUD_100K; break;
		case 125000: BaudRate = PCAN_BAUD_125K; break;
		case 250000: BaudRate = PCAN_BAUD_250K; break;
		case 500000: BaudRate = PCAN_BAUD_500K; break;
		case 1000000: BaudRate = PCAN_BAUD_1M; break;
		default: BaudRate = br;
		}
	}
	else { if (numPar != 0) BaudRate = br; }
	Status = CAN_Initialize(canObjHandler, BaudRate,256,3);
	if (Status == PCAN_ERROR_OK) {
		CAN_FilterMessages(canObjHandler,0,0x7FF,PCAN_MESSAGE_STANDARD);

		m_hCanScanThread = CreateThread(NULL, 0, CanScanControlThread, 
			this, 0, &m_idCanScanThread);
		if (NULL == m_hCanScanThread) {
			DebugBreak();
			return false;
		}
		return true;
	}

	return false;
}


bool PKCanScan::sendErrorCode(long status)
{
	timeval ftTimeStamp; 
	if (status != bus_status) {
		gettimeofday(&ftTimeStamp,0);
		char *errMess;
		getErrorMessage(status,&errMess);
		canMessageError(status,errMess,ftTimeStamp );
		bus_status = (TPCANStatus)status;

	}
	if (status != PCAN_ERROR_OK) {
		status = CAN_Reset(canObjHandler);
		return false;
	}
	return true;
}

bool PKCanScan::sendMessage(short cobID, unsigned char len, unsigned char *message)
{
	TPCANStatus Status;
	unsigned char *buf = message;
	TPCANMsg    pkmsg;
	pkmsg.ID = cobID;
	pkmsg.MSGTYPE = PCAN_MESSAGE_STANDARD;

	int l, l1;
	l = len;
	do {
		if (l > 8) {
			l1 = 8; l = l - 8;
		}
		else l1 = l;
		pkmsg.LEN = l1;
		memcpy(pkmsg.DATA,buf,l1);
		Status = CAN_Write(canObjHandler, &pkmsg);
		if (Status != PCAN_ERROR_OK) {
			return sendErrorCode(Status);
		}
		buf = buf + l1;
	}
	while (l > 8);
	return sendErrorCode(Status);
}

bool PKCanScan::sendRemoteRequest(short cobID)
{
	TPCANStatus Status;

	TPCANMsg    pkmsg;
	pkmsg.ID = cobID;
	pkmsg.MSGTYPE = PCAN_MESSAGE_RTR;
	Status = CAN_Write(canObjHandler, &pkmsg);
	return sendErrorCode(Status);
}

bool PKCanScan::createBUS(const char *name ,const char *parameters )
{

	if (!configureCanboard(name,parameters))
		return false;
	// Initialization for Scan
	return true;
}

bool PKCanScan::getErrorMessage(long error, char **message)
{
  char tmp[300];

  CAN_GetErrorText((TPCANStatus)error,0, tmp);
  *message = new char[strlen(tmp)+1];	
  strcpy(*message,tmp);
  return true;
}

TPCANHandle PKCanScan::getHandle(const char *name)
{
	char tmpName[256];
	const char *chname[] = { "ISA1","ISA2","ISA3","ISA4","ISA5","ISA6","ISA7","ISA8",
		"DNG1",
		"PCI1","PCI2","PCI3","PCI4","PCI5","PCI6","PCI7","PCI8",
		"USB1","USB2","USB3","USB4","USB5","USB6","USB7","USB8",
		"PCC1","PCC2" };

	const TPCANHandle han[] = { 0x21,  // PCAN-ISA interface, channel 1
		0x22,  // PCAN-ISA interface, channel 2
		0x23,  // PCAN-ISA interface, channel 3
		0x24,  // PCAN-ISA interface, channel 4
		0x25,  // PCAN-ISA interface, channel 5
		0x26,  // PCAN-ISA interface, channel 6
		0x27,  // PCAN-ISA interface, channel 7
		0x28,  // PCAN-ISA interface, channel 8

		0x31,  // PCAN-Dongle/LPT interface, channel 1

		0x41,  // PCAN-PCI interface, channel 1
		0x42,  // PCAN-PCI interface, channel 2
		0x43,  // PCAN-PCI interface, channel 3
		0x44,  // PCAN-PCI interface, channel 4
		0x45,  // PCAN-PCI interface, channel 5
		0x46,  // PCAN-PCI interface, channel 6
		0x47,  // PCAN-PCI interface, channel 7
		0x48,  // PCAN-PCI interface, channel 8

		0x51,  // PCAN-USB interface, channel 1
		0x52,  // PCAN-USB interface, channel 2
		0x53,  // PCAN-USB interface, channel 3
		0x54,  // PCAN-USB interface, channel 4
		0x55,  // PCAN-USB interface, channel 5
		0x56,  // PCAN-USB interface, channel 6
		0x57,  // PCAN-USB interface, channel 7
		0x58,  // PCAN-USB interface, channel 8

		0x61,  // PCAN-PC Card interface, channel 1
		0x62 }; // PCAN-PC Card interface, channel 2 

	int chn = sizeof(chname)/sizeof(chname[0]);

	for (unsigned int j=0; j < strlen(name); j++)
		tmpName[j] = toupper(name[j]);
	
	for (int i = 0; i < chn; i++)
	{
		if (strcmp(chname[i],tmpName) >= 0)
			return han[i];
	}

	return 0;
}
