TEMPLATE = lib
TARGET   = pkcan
CONFIG  += shared thread debug_and_release

CONFIG(debug, debug|release) {
  DESTDIR = ../../bin
  OBJECTS_DIR = ./debug
}
CONFIG(release, debug|release) {
  DESTDIR = ../../bin
  OBJECTS_DIR = ./release
}

QT -= qt core gui

INCLUDEPATH += ./../../Can/CanBusAccess
INCLUDEPATH += "C:/Program Files/PCAN/PCAN-Basic API/include"

win32 {
  DEFINES += WIN32_LEAN_AND_MEAN
  DEFINES += _CRT_SECURE_NO_DEPRECATE
  INCLUDEPATH += $$(BOOST_ROOT)
  QMAKE_CXXFLAGS += /Zc:wchar_t
}

LIBS += -L"C:/Program Files/PCAN/PCAN-Basic API/Win32/VC_LIB"
LIBS += -lPCANBasic

HEADERS += ./pkcan.h
SOURCES += ./pkcan.cpp
