TEMPLATE = lib
TARGET   = stcan
CONFIG  += shared thread debug_and_release

CONFIG(debug, debug|release) {
  DESTDIR = ../Debug
  OBJECTS_DIR = ./debug
}
CONFIG(release, debug|release) {
  DESTDIR = ../Release
  OBJECTS_DIR = ./release
}

QT -= core gui

INCLUDEPATH += ./../../Can/CanBusAccess
INCLUDEPATH += "C:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Include"

win32 {
  DEFINES += WIN32_LEAN_AND_MEAN
  INCLUDEPATH += "C:/boost_1_50_0"
  QMAKE_CXXFLAGS += /Zc:wchar_t
}

LIBS += -L"C:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/Examples/Lib"
LIBS += -lusbcan32

HEADERS += ./stcan.h
SOURCES += ./stcan.cpp
