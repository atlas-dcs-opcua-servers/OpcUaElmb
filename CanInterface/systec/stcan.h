/*
 *S	Tcan.h
 *
 *  Created on: DEC 12, 2012
 *      Author: vfilimon
 */

#ifndef CCANSTCAN_H_
#define CCANSTCAN_H_

//#include "windows.h"

#ifdef WIN32
#include "Winsock2.h"
#endif

#define _TCHAR_DEFINED
#include <tchar.h>

#include "CCanAccess.h"
#include "Usbcan32.h"
#include <string>
using namespace std;

class STCanScan: public CCanAccess
{
public:
	STCanScan();
	virtual ~STCanScan();
	virtual bool createBUS(const char * ,const char *);
    virtual bool sendMessage(short cobID, unsigned char len, unsigned char *message);
	virtual bool sendRemoteRequest(short cobID);
	
	virtual bool getErrorMessage(long error, char **message);
	bool sendErrorCode(long);
private:
	bool run_can;
	int numModule;
	int numChannel;
	int numCanHandle;

	tUcanHandle m_UcanHandle;

	string getNamePort() { return canName; }

	

	long		 BaudRate;

	string canName;

#define Timeout  1000

	DWORD bus_status;
	bool configureCanboard(const char *,const char *);

};

#endif