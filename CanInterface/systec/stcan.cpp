// This is the main DLL file.

#include "stcan.h"

//////////////////////////////////////////////////////////////////////
// kvCann.cpp: implementation of the KVCanScan class.
//////////////////////////////////////////////////////////////////////
//#include "CCanCallback.h"
#include <time.h>
#include <stdio.h>
#include <string.h>


#ifdef WIN32 
	extern "C" __declspec(dllexport) CCanAccess *getCanbusAccess()
#else
	extern "C" CCanAccess *getCanbusAccess()
#endif
	{
		CCanAccess *cc;
		cc = new STCanScan;

		return cc;
	}

#include "gettimeofday.h"

void UnixTimeToFileTime(time_t t, LPFILETIME pft)
{
     // Note that LONGLONG is a 64-bit value
     LONGLONG ll;

     ll = Int32x32To64(t, 10000000) + 116444736000000000;
     pft->dwLowDateTime = (DWORD)ll;
     pft->dwHighDateTime = ll >> 32;
}

void PUBLIC UcanCallbackFktEx (tUcanHandle uhp,DWORD bEvent_p, BYTE bC, void* pCanScan)
{
	tCanMsgStruct frame;
	BYTE ch = bC ? USBCAN_CHANNEL_CH1 : USBCAN_CHANNEL_CH0;
	UCANRET Status;
	tStatusStruct pStatus_p;	
	STCanScan * ppcs = 	(STCanScan *)pCanScan;
	

    //----------------------------------------------------------------------------
    // NOTE:
    // Do not call functions of USBCAN32.DLL directly from this callback handler.
    // Use events or windows messages to notify the event to the application.
    //----------------------------------------------------------------------------

    // check event
    switch (bEvent_p)
    {
		case USBCAN_EVENT_RECIEVE:
		{
			Status = UcanReadCanMsgEx(uhp, &ch, &frame, NULL );
		
			if (USBCAN_CHECK_VALID_RXCANMSG (Status))
			{
				canMessage cmes;
				memcpy(&cmes.c_data,&frame.m_bData,8);
				cmes.c_dlc = frame.m_bDLC;
				cmes.c_ff = frame.m_bFF;
				cmes.c_id = frame.m_dwID;
				cmes.c_time.tv_sec = frame.m_dwTime/1000;
				cmes.c_time.tv_usec = frame.m_dwTime*1000;

				ppcs->canMessageCame(cmes);
				if (USBCAN_CHECK_WARNING (Status))
				{
					Status =  UcanGetStatusEx (uhp, ch, &pStatus_p);
					ppcs->sendErrorCode(pStatus_p.m_wCanStatus);
				}
			}
			// No warning? Print error
			else if (USBCAN_CHECK_WARNING (Status))
			{
				Status =  UcanGetStatusEx (uhp, ch, &pStatus_p);
				ppcs->sendErrorCode(pStatus_p.m_wCanStatus);
			}
		}
			break;
        // unknown event
        default:
            break;
    }
}

STCanScan::STCanScan()
{
	return ;
}

STCanScan::~STCanScan()
{
	run_can = false;
//	CloseHandle(m_ReadEvent);
//	CAN_Uninitialize(canObjHandler);
}


bool STCanScan::sendMessage(short cobID, unsigned char len, unsigned char *message)
{
	tCanMsgStruct frame;
	BYTE Status;
	unsigned char *buf = message; 
	frame.m_dwID = cobID;
	frame.m_bDLC = len;
	frame.m_bFF = 0;
	int l, l1;
	l = len;
	do {
		if (l > 8) {
			l1 = 8; l = l - 8;
		}
		else l1 = l;
		frame.m_bDLC = l1;
		memcpy(frame.m_bData,buf,l1);
		Status = UcanWriteCanMsgEx(m_UcanHandle,numChannel,&frame,NULL);
		if (Status != USBCAN_SUCCESSFUL) {
			return sendErrorCode(Status);
		}
		buf = buf + l1;
	}
	while (l > 8);
	return sendErrorCode(Status);
}

bool STCanScan::sendRemoteRequest(short cobID)
{
	tCanMsgStruct frame;
	BYTE Status;
	frame.m_dwID = cobID;
	frame.m_bDLC = 0;
	frame.m_bFF = USBCAN_MSG_FF_RTR;

	Status = UcanWriteCanMsgEx(m_UcanHandle,numChannel,&frame, NULL);
	return sendErrorCode(Status);
}

bool STCanScan::createBUS(const char *name ,const char *parameters )
{
	busName = strdup(name);
	if (!configureCanboard(name,parameters))
		return false;
	return true;
}

bool STCanScan::getErrorMessage(long error, char **message)
{
  char tmp[300];

  CAN_GetErrorText((TPCANStatus)error,0, tmp);
  *message = new char[strlen(tmp)+1];	
  strcpy(*message,tmp);
  return true;
}

bool STCanScan::configureCanboard(const char *name, const char *parameters)
{
	DWORD           Status = USBCAN_SUCCESSFUL;
	tUcanHandle			oh;
	char				*com,*strch;

    unsigned int tseg1 = 0;
    unsigned int tseg2 = 0;
    unsigned int sjw = 0;
    unsigned int noSamp = 0;
    unsigned int syncmode = 0;
    long		 BaudRate = USBCAN_BAUD_125kBit,br;
	int			 delta,numPar; 
	BYTE         bRet     = USBCAN_SUCCESSFUL;
	BOOL         fRet     = true;
	tUcanInitCanParam   InitParam;


	com = strchr(name,':');
	if (com == NULL) return NULL;
	delta = com - name;
	com++;

	strch = strchr(com,',');
	if (strch == NULL) {
		numCanHandle = atoi(com);
		numChannel = 0;
	}
	else {
		strch++;
		numModule = atoi(com);
		numChannel = atoi(strch);
	}
	numCanHandle = 2*numModule+numChannel;

	numPar = sscanf(parameters,"%d %d %d %d %d %d",&br,&tseg1,&tseg2,&sjw,&noSamp,&syncmode);

	if (numPar == 1) {
		switch(br) {
			case 50000: BaudRate = USBCAN_BAUD_50kBit; break;
			case 100000: BaudRate = USBCAN_BAUD_100kBit; break;
			case 125000: BaudRate = USBCAN_BAUD_125kBit; break;
			case 250000: BaudRate = USBCAN_BAUD_250kBit; break;
			case 500000: BaudRate = USBCAN_BAUD_500kBit; break;
			case 1000000: BaudRate = USBCAN_BAUD_1MBit; break;
			default: BaudRate = br;
		}
	}
	else { if (numPar != 0) BaudRate = br; }

    // check if USB-CANmodul already is initialized
        // initialize hardware
	if (parrent->isInUse(numCanHandle)) { 
		oh = parrent->getCanHandle(numCanHandle);
	}
	else
	{
		bRet = ::UcanInitHardwareEx (&oh, numModule, UcanCallbackFktEx,&numModule);
		if (bRet != USBCAN_SUCCESSFUL)   {
        // fill out initialisation struct
			::UcanDeinitHardware (oh);
			return false;
		}
	}
	InitParam.m_dwSize  = sizeof (InitParam);           // size of this struct
	InitParam.m_bMode   = kUcanModeNormal;              // normal operation mode
	InitParam.m_bBTR0   = HIBYTE (BaudRate);              // baudrate
	InitParam.m_bBTR1   = LOBYTE (BaudRate);
	InitParam.m_bOCR    = 0x1A;                         // standard output
	InitParam.m_dwAMR   = USBCAN_AMR_ALL;               // receive all CAN messages
	InitParam.m_dwACR   = USBCAN_ACR_ALL;
	InitParam.m_dwBaudrate = USBCAN_BAUDEX_USE_BTR01;
	InitParam.m_wNrOfRxBufferEntries = USBCAN_DEFAULT_BUFFER_ENTRIES;
	InitParam.m_wNrOfTxBufferEntries = USBCAN_DEFAULT_BUFFER_ENTRIES;
	parrent->setCanHandle(numModule,oh);
	parrent->setInUse(numModule,true);
        // initialize CAN interface
	bRet = ::UcanInitCanEx2 (oh,numChannel, &InitParam);
	if (bRet != USBCAN_SUCCESSFUL)
	{
		::UcanDeinitCanEx (oh,numChannel);
//		::UcanDeinitHardware (oh);
		return false;
	}
	parrent->setCanHandle(numCanHandle,oh);
	parrent->setInUse(numCanHandle,true);
	th = oh;
	return fRet;
}

