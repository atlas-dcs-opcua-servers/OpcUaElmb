TEMPLATE = lib
VERSION = 1.0.2
TARGET   = sockcan
CONFIG  += shared thread debug_and_release
CONFIG  -= qt core gui

CONFIG(debug, debug|release) {
  QMAKE_CXXFLAGS +=  -O0 -g3 -Wall -W -c -fmessage-length=0 -fPIC -fno-strict-aliasing -D_REENTRANT -Wno-deprecated
  DESTDIR = ../debug
  OBJECTS_DIR = ./debug
}
CONFIG(release, debug|release) {
  QMAKE_CXXFLAGS += -W -O3 -c -fmessage-length=0 -fPIC -fno-strict-aliasing -D_REENTRANT -Wno-deprecated
  DESTDIR = ../release
  OBJECTS_DIR = ./release
}

LIBS += -lpthread -L/usr/local/lib /usr/local/lib/libsocketcan.a

QT -= core gui

INCLUDEPATH += ./../../Can/CanBusAccess /usr/local/include

HEADERS += ./SockCanScan.h
SOURCES += ./SockCanScan.cpp
