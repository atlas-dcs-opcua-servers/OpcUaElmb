/*
 * SockCanScan.cpp
 *
 *  Created on: Jul 21, 2011
 *      Author: vfilimon
 */
#include "SockCanScan.h"
#include <errno.h>

#include <fstream>
#include <iostream>
#include <strstream>

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction

#include <time.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/uio.h>
#include <net/if.h>
#include "gettimeofday.h"

#include <linux/can.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>

#ifndef peak_can
#include <libsocketcan.h>
#endif

extern "C" CCanAccess *getCanbusAccess()
{

  cout << "getCanbusAccess called" << endl;
  CCanAccess *cc;
  cc = new CSockCanScan();
  return cc;
}

void* CSockCanScan::CanScanControlThread(void *pCanScan)
{
  canMessage cmsg;
  int timeErr;
  struct can_frame  sockmsg;
  CSockCanScan *ppcs = reinterpret_cast<CSockCanScan *>(pCanScan);
  ppcs->run_can = true;
  int sock = ppcs->sock;
  int nbytes;
  int errorFlag=0;
  int state = 0, ret;
  
  usleep(150); // wait to connection
  struct timeval tv;
  tv.tv_sec = 0;
  tv.tv_usec = 300000;
  setsockopt(sock,SOL_SOCKET,SO_RCVTIMEO,&tv ,sizeof(struct timeval));

  ppcs->clearErrorMessage(); // clear error flag 
  
  while (ppcs->run_can) {

/* these settings may be modified by recvmsg() */

    nbytes = read(sock, &sockmsg, sizeof(struct can_frame));
	if (!ppcs->run_can) 
		break;
//	printf("Read byte %s = %d\n",ppcs->channelName.c_str(),nbytes);
    if (nbytes < 0) {
		if (errno == EAGAIN)
			continue;
       perror("read error");
       printf ("Slowed down readout loop for %s ...\n", ppcs->channelName.c_str());

      // close socket, FIXME
      //
	  ret = can_get_state(ppcs->channelName.c_str(), &state);
      
printf("State socket for %s ret=%d state=%d\n", ppcs->channelName.c_str(),ret,state);

	  if ((ret != 0) || (state == CAN_STATE_STOPPED) || (state == CAN_STATE_SLEEPING)) {
    	  printf ("Closing socket for %s ...\n", ppcs->channelName.c_str());
    	  if (close(sock) < 0) {
    		  perror("Socket close error!");
    	  }
    	  printf ("Socket for %s closed.\n", ppcs->channelName.c_str());

 //     ppcs->waitCanDriver();
    	  usleep(15000000);

    	  printf ("Opening new socket for %s ...\n", ppcs->channelName.c_str());

      // FIXME: handle errors
      //
    	  sock = ppcs->openCanPort();
    	  ppcs->sock = sock;
    	  printf ("Socket for %s open.\n", ppcs->channelName.c_str());
      }
	
      continue;
    }

    if (nbytes <(int) sizeof(struct can_frame)) {
      cout << "read: incomplete CAN frame for " << ppcs->channelName.c_str() << endl;
      continue;
    }
    if (sockmsg.can_id & CAN_ERR_FLAG) {
      ppcs->sendErrorMessage(&sockmsg);
      errorFlag = sockmsg.can_id; 
      continue;
    }

    if (errorFlag) {
        ret = can_get_state(ppcs->channelName.c_str(),&state);
        if (state == 0) {
        	ppcs->clearErrorMessage();
        	errorFlag = 0;
        }
	}

    cmsg.c_id = sockmsg.can_id;
    timeErr = ioctl(sock,SIOCGSTAMP,&cmsg.c_time);
    cmsg.c_dlc = sockmsg.can_dlc;
    memcpy(&cmsg.c_data[0],&sockmsg.data[0],8);
    ppcs->canMessageCame(cmsg);
  }

  // loop finished, close open socket
  // These is a redundant code
  if (sock) {
    if (close(sock) < 0) {
      perror("Socket close error!");
    }
    else printf ("Socket for %s closed.\n", ppcs->channelName.c_str());
  }
  fflush(stdout);
  pthread_exit(NULL);
  return NULL;
}

CSockCanScan::~CSockCanScan()
{
  time_t t = time(NULL);
  struct tm *tm = localtime(&t);
 
  run_can = false;
  pthread_join(m_hCanScanThread,0);
  t = time(NULL);
  tm = localtime(&t);
  // socket will be closed in readout loop
  //
}

int CSockCanScan::configureCanboard(const char *name,const char *parameters)
{
  const char		*com;		// Channel name
  int		delta;
  com = strchr(name,':');
  if (com == NULL) {

    com = name;
  }
  else {
    delta = com - name;
    com++;
  }

  channelName = com;
  param = parameters;
  return openCanPort();
}

int CSockCanScan::openCanPort()
{
  unsigned int tseg1 = 0;
  unsigned int tseg2 = 0;
  unsigned int sjw = 0;
  unsigned int noSamp = 0;
  unsigned int syncmode = 0;
  bool	paramSpecified = true;
  struct sockaddr_can addr;

  memset ((char*)&addr, 0, sizeof addr);

  struct ifreq ifr;

  int numPar,err;
  unsigned int br;
  
  if (param.length() > 0) {
	if (param != "Unspecified") {
    	numPar = sscanf(param.c_str(),"%d %d %d %d %d %d",&br,
		    &tseg1,&tseg2,&sjw,&noSamp,&syncmode);
	}
	else paramSpecified = false;
  }

  /* Set baud rate to 125 Kbits/second.  */

  if (paramSpecified) {
  	err = can_do_stop(channelName.c_str());
  	if (err < 0) {
    	perror("Cannot stop channel");
    	return err;
  	}
  	err = can_set_bitrate(channelName.c_str(),br);
  	if (err < 0) {
    	perror("Cannot set bit rate");
    	return err;
  	}
  	err = can_do_start(channelName.c_str());
  	if (err < 0) {
    	perror("Cannot start channel");
    	return err;
  	}
  }

  sock = socket(PF_CAN, SOCK_RAW, CAN_RAW);
  if (sock < 0)   {
    // fill out initialisation struct
    perror("Cannot open the socket");
    return 0;
  }
  memset(&ifr.ifr_name, 0, sizeof(ifr.ifr_name));
  strncpy(ifr.ifr_name, channelName.c_str(), channelName.length());

  if (ioctl(sock, SIOCGIFINDEX, &ifr) < 0) {
    perror("SIOCGIFINDEX");
    return -1;
  }

  can_err_mask_t err_mask = 0x1ff;

  setsockopt(sock, SOL_CAN_RAW, CAN_RAW_ERR_FILTER,
	     &err_mask, sizeof(err_mask));

  addr.can_family = AF_CAN;
  addr.can_ifindex = ifr.ifr_ifindex;

  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    perror("bind");
    return 0;
  }
  // Fetch initial state of the port
  int ret;
  if (ret = can_get_state(channelName.c_str(), &err))
  {
	  cout << "can_get_state() failed with error code " << ret << ", it was not possible to obtain initial port state";
  }

  if (err == 0)
	  clearErrorMessage();
  else
  {
	  timeval now;
	  gettimeofday(&now, 0);
	  canMessageError(err, "Initial port state: error", now);
  }

  return sock;
}


bool CSockCanScan::sendMessage(short cobID, unsigned char len, unsigned char *message)
{
  struct can_frame frame;

  memset ((char*) &frame, 0, sizeof(frame));
  ssize_t nbytes;
  unsigned char *buf = message;
  frame.can_id = cobID;
  frame.can_dlc = len;
  int l, l1;
  l = len;
  
  do {
    if (l > 8) {
      l1 = 8; l = l - 8;
    }
    else l1 = l;
    frame.can_dlc = l1;
    memcpy(frame.data,buf,l1);

    nbytes = write(sock, &frame, sizeof(struct can_frame));
    if (nbytes < 0) 	
    {
	    perror("while write() on socket");
        if (errno == ENOBUFS) 
	    {	
		    printf ("ENOBUFS; waiting a jiffy ...\n");
		    // insert deleted systec buffer
		    /*
		     * struct ifreq ifr;
		     * strncpy(ifr.ifr_name, channelName.c_str(), sizeof(channelName.c_str()));
		     * ioctl(sock,SIODEVPRIVATE,&ifr); // stop sending and clear buffer
		     * ioctl(sock,SIODEVPRIVATE_1,&ifr); // start sebding
		     */
	    	usleep(100000);
            continue;
         }
    }
    if (nbytes < (int)sizeof(struct can_frame)) {
		return false; 
    }
    buf = buf + l1;
  }
  while (l > 8);

  return true; 
}

bool CSockCanScan::sendRemoteRequest(short cobID)
{
  struct can_frame frame;

  memset ((char*) &frame, 0, sizeof(frame));
  int nbytes;
  frame.can_id = cobID + CAN_RTR_FLAG	;
  frame.can_dlc = 0;
  do {
    nbytes = write(sock, &frame, sizeof(struct can_frame));
    if (nbytes < 0) 	
    {
       perror("while write() on socket");
       if (errno == ENOBUFS) 
       {	
		  	printf ("ENOBUFS; waiting a jiffy ...\n");
		    // insert deleted systec buffer
		  	usleep(100000);
    	    continue;
		}
    }
    else {
      if (nbytes < (int)sizeof(struct can_frame))
      {
        cout << "Warning: sendRemoteRequest() sent less bytes than expected" << endl;
        return false;
      }
    }
    break;
  }
  while(true);

  return true; 
}

bool CSockCanScan::createBUS(const char *name ,const char *parameters)
{
  name_of_port = string(name);
  cout << "Create bus " << name << " " << parameters << endl;
  if ((sock = configureCanboard(name,parameters)) < 0) {
    return false;
  }
  cout << "Create main loop" << endl;
  m_idCanScanThread =
    pthread_create(&m_hCanScanThread,NULL,&CanScanControlThread,
		   (void *)this);

  return (!m_idCanScanThread);
}

void CSockCanScan::sendErrorMessage(const struct can_frame *errFrame)
{
  int timeErr;
  timeval	c_time;
  char mesbuf[1024];
  string errorMessage;
	
  timeErr = ioctl(sock,SIOCGSTAMP,&c_time);
  unsigned int errFlag = errFrame->can_id & CAN_ERR_MASK;

  if (errFlag & CAN_ERR_TX_TIMEOUT)
    {
      errorMessage = "TX timeout (by netdevice driver)";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }

  if ( errFlag & CAN_ERR_LOSTARB )
    {
      sprintf(mesbuf,"%s %x","Lost arbitration",errFrame->data[0]);
      errorMessage = mesbuf;
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }

  if ((errFlag & CAN_ERR_CRTL) && (errFrame->data[1] & CAN_ERR_CRTL_RX_OVERFLOW) )
    {
      errorMessage = "RX buffer overflow";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }
  if ((errFlag & CAN_ERR_CRTL) && (errFrame->data[1] & CAN_ERR_CRTL_TX_OVERFLOW) )
    {
      errorMessage = "TX buffer overflow";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }
  if ((errFlag & CAN_ERR_CRTL) && (errFrame->data[1] & CAN_ERR_CRTL_RX_WARNING) )
    {
      errorMessage = "reached warning level for RX errors";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }
  if ((errFlag & CAN_ERR_CRTL) && (errFrame->data[1] & CAN_ERR_CRTL_TX_WARNING) )
    {
      errorMessage = "reached warning level for TX errors";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }
  if ((errFlag & CAN_ERR_CRTL) && (errFrame->data[1] & CAN_ERR_CRTL_RX_PASSIVE) )
    {
      errorMessage = "reached error passive status RX";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }
  if ((errFlag & CAN_ERR_CRTL) && (errFrame->data[1] & CAN_ERR_CRTL_TX_PASSIVE) )
    {
      errorMessage = "reached error passive status TX";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }

  if (errFlag & CAN_ERR_PROT)
    {
      sprintf(mesbuf,"%s %x %x","protocol violations",errFrame->data[2],errFrame->data[3]);
      errorMessage = mesbuf;
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }

  if (errFlag & CAN_ERR_TRX)
    {
      sprintf(mesbuf,"%s %x","transceiver status",errFrame->data[4]);
      errorMessage = mesbuf;
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }
  if (errFlag & CAN_ERR_ACK)
    {
      errorMessage = "received no ACK on transmission";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }
  if (errFlag & CAN_ERR_BUSOFF)
    {
      errorMessage = "bus off";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }
  if (errFlag & CAN_ERR_BUSERROR)
    {
      errorMessage = "bus error (may flood!)";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }
  if (errFlag & CAN_ERR_RESTARTED)
    {
      errorMessage = "controller restarted";
      canMessageError(errFlag,errorMessage.c_str(),c_time);
      return;
    }
}

void CSockCanScan::clearErrorMessage()
{
  string errorMessage = "";
  int timeErr;
  timeval	c_time;
	
  timeErr = ioctl(sock,SIOCGSTAMP,&c_time);

  canMessageError(0,errorMessage.c_str(),c_time);
  return;
}

void CSockCanScan::sendErrorMessage(const char *mess)
{
  int timeErr;
  timeval	c_time;

  timeErr = ioctl(sock,SIOCGSTAMP,&c_time);

  canMessageError(-1,mess,c_time);
  return;
}

void CSockCanScan::waitCanDriver()
{
  struct stat statBuf;
  int timeErr;
  string errorMessage;
  timeval	c_time;
  char procFile[] = "/proc/net/can/rcvlist_all";

  perror("Canbus Read error, Sockect close");

  timeErr = ioctl(sock,SIOCGSTAMP,&c_time);

  errorMessage = "Can Driver error";

  sendErrorMessage(errorMessage.c_str());

  string str;

  while (true) {

    usleep(200000);
	ifstream input(procFile);
	if (input) {

	    // read data as a block:
		string str((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());

	    int length = str.length();

	    cout << "Reading " << length << " characters... " << endl;

	    if (length > 0) {
	      cout << "all characters read successfully." << endl;
	      int sz = str.find(channelName.c_str());
	      if (sz > 0 ) {
	    	  input.close();
			   cout << "Network up..." << endl;
	    	  break;
	      }
          else {
    		  cout << "No Chennal..." << endl;
	      }
	    }
	    else
	      cout << "error: " << errno << endl;

	    input.close();
    }
    else
    	cout << "No CAN driver..." << endl;

  }

  clearErrorMessage();
  return;
}

