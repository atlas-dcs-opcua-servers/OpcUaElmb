TEMPLATE = lib
TARGET   = stcan
CONFIG  += shared thread debug_and_release
DESTDIR = ../../bin

CONFIG(debug, debug|release) {
  OBJECTS_DIR = ./debug
}
CONFIG(release, debug|release) {
  OBJECTS_DIR = ./release
}

QT -= core gui

INCLUDEPATH += ./../../Can/CanBusAccess

win32 {
  DEFINES += WIN32_LEAN_AND_MEAN
  DEFINES += _CRT_SECURE_NO_DEPRECATE
  INCLUDEPATH += $$(BOOST_ROOT)
  QMAKE_CXXFLAGS += /Zc:wchar_t
}

exists("C:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/Lib/usbcan64.lib") {
	INCLUDEPATH += "C:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/Include"
	LIBS += -L"C:/Program Files (x86)/SYSTEC-electronic/USB-CANmodul Utility Disk/lib"
LIBS += -lusbcan32

}
exists("C:/Program Files/SYSTEC-electronic/USB-CANmodul Utility Disk/Lib/usbcan32.lib") {
	INCLUDEPATH += "C:/Program Files/Kvaser/CANLIB/INC"
	LIBS += -L"C:/Program Files/Kvaser/CANLIB/Lib/MS"
	LIBS += -lusbcan32
}

HEADERS += ./stcan.h
SOURCES += ./stcan.cpp
