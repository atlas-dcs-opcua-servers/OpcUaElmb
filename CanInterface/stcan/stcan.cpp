#include "stcan.h"

//////////////////////////////////////////////////////////////////////
// strcan.cpp: implementation of the STCanScan class.
//////////////////////////////////////////////////////////////////////

#include <time.h>
#include <stdio.h>
#include <string.h>
#include "gettimeofday.h"

bool  initLibarary =  false;
bool STCanScan::inUse[256];
tUcanHandle STCanScan::canHandle[256];

#ifdef WIN32 
	extern "C" __declspec(dllexport) CCanAccess *getCanbusAccess()
#else
	extern "C" CCanAccess *getCanbusAccess()
#endif
	{
		CCanAccess *cc;
		cc = new STCanScan;

		return cc;
	}

void UnixTimeToFileTime(time_t t, LPFILETIME pft)
   {
     // Note that LONGLONG is a 64-bit value
     LONGLONG ll;

     ll = Int32x32To64(t, 10000000) + 116444736000000000;
     pft->dwLowDateTime = (DWORD)ll;
     pft->dwHighDateTime = ll >> 32;
   }


STCanScan::STCanScan()
{
	return ;
}

DWORD WINAPI STCanScan::CanScanControlThread(LPVOID pCanScan)
{
	BYTE Status;
//    unsigned long timeout = 250;
	tCanMsgStruct frame;

    STCanScan *ppcs = reinterpret_cast<STCanScan *>(pCanScan);

	while ( ppcs->run_can ) {
		Status = UcanReadCanMsgEx(ppcs->m_UcanHandle,(BYTE *)&ppcs->numChannel,&frame,0);
		if (Status == USBCAN_SUCCESSFUL) {
			canMessage cmt;
			cmt.c_id = frame.m_dwID;
			cmt.c_dlc = frame.m_bDLC;
			cmt.c_ff = frame.m_bFF;
			for (int i = 0; i < 8; i++)
				cmt.c_data[i] = frame.m_bData[i];
			gettimeofday(&(cmt.c_time));
//			cmt.c_time.tv_sec = frame.m_dwTime/1000;
//			cmt.c_time.tv_usec = (frame.m_dwTime & 1000) * 1000;
//	cout <<  ctime((const time_t *)&(cmt.c_time.tv_sec)) << endl;
			ppcs->canMessageCame(cmt);

		}
		else {
			if (Status == USBCAN_WARN_NODATA) {
				Sleep(100);
            }
			else 
				ppcs->sendErrorCode(Status);
		}
	}

    ExitThread(0);
    return 0;
}

STCanScan::~STCanScan()
{
	run_can = false;
	printf("Start Exit ST component\n");

	DWORD result = WaitForSingleObject(m_hCanScanThread, INFINITE);
     // deinitialize CAN interface
    ::UcanDeinitCanEx (m_UcanHandle,(BYTE)numChannel);
	printf("Finish Exit ST component\n");
}

bool STCanScan::createBUS(const char *name,const char *parameters)
{
	tUcanHandle		oh;

    unsigned int tseg1 = 0;
    unsigned int tseg2 = 0;
    unsigned int sjw = 0;
    unsigned int noSamp = 0;
    unsigned int syncmode = 0;
    long		 BaudRate = USBCAN_BAUD_125kBit,br;
	int			 numPar; 
	BYTE         bRet     = USBCAN_SUCCESSFUL;

	tUcanInitCanParam   InitParam;

	const char *na = strstr(name,":");
	numCanHandle = atoi(na+1);

	numModule = numCanHandle/2;
	numChannel = numCanHandle%2;

	numPar = sscanf(parameters,"%d %d %d %d %d %d",&br,&tseg1,&tseg2,&sjw,&noSamp,&syncmode);

	/* Set baud rate to 125 Kbits/second.  */

	if (numPar == 1) {
		switch(br) {
			case 50000: BaudRate = USBCAN_BAUD_50kBit; break;
			case 100000: BaudRate = USBCAN_BAUD_100kBit; break;
			case 125000: BaudRate = USBCAN_BAUD_125kBit; break;
			case 250000: BaudRate = USBCAN_BAUD_250kBit; break;
			case 500000: BaudRate = USBCAN_BAUD_500kBit; break;
			case 1000000: BaudRate = USBCAN_BAUD_1MBit; break;
			default: BaudRate = br;
		}
	}
	else { if (numPar != 0) BaudRate = br; }

    // check if USB-CANmodul already is initialized
        // initialize hardware
	if (isInUse(numModule)) { 
		oh = getCanHandle(numModule);
	}
	else
	{
		bRet = ::UcanInitHardwareEx (&oh, numModule, 0,0);
		if (bRet != USBCAN_SUCCESSFUL)   {
        // fill out initialisation struct
			::UcanDeinitHardware (oh);
			return false;
		}
	}
	InitParam.m_dwSize  = sizeof (InitParam);           // size of this struct
	InitParam.m_bMode   = kUcanModeNormal;              // normal operation mode
	InitParam.m_bBTR0   = HIBYTE (BaudRate);              // baudrate
	InitParam.m_bBTR1   = LOBYTE (BaudRate);
	InitParam.m_bOCR    = 0x1A;                         // standard output
	InitParam.m_dwAMR   = USBCAN_AMR_ALL;               // receive all CAN messages
	InitParam.m_dwACR   = USBCAN_ACR_ALL;
	InitParam.m_dwBaudrate = USBCAN_BAUDEX_USE_BTR01;
	InitParam.m_wNrOfRxBufferEntries = USBCAN_DEFAULT_BUFFER_ENTRIES;
	InitParam.m_wNrOfTxBufferEntries = USBCAN_DEFAULT_BUFFER_ENTRIES;
	setInUse(numModule,true);
        // initialize CAN interface
	bRet = ::UcanInitCanEx2 (oh,numChannel, &InitParam);
	if (bRet != USBCAN_SUCCESSFUL)
	{
		::UcanDeinitCanEx (oh,numChannel);
//		::UcanDeinitHardware (oh);
		return false;
	}
	setCanHandle(numModule,oh);
	m_UcanHandle = oh;

	m_hCanScanThread = CreateThread(NULL, 0, CanScanControlThread, 
			this, 0, &m_idCanScanThread);
	if (NULL == m_hCanScanThread) {
		DebugBreak();
		return false;
	}

	return true;
}

bool STCanScan::sendErrorCode(long status)
{
	char errMess[120];
	timeval ftTimeStamp; 
	if (status != bus_status) {
		gettimeofday(&ftTimeStamp,0);
		if (!getErrorMessage(status,errMess))
			canMessageError(status,errMess,ftTimeStamp );
		bus_status = status;
	}
	/*
	if (status != canOK) {
		status = canIoCtl(canObjHandler,canIOCTL_CLEAR_ERROR_COUNTERS,NULL,0);
		return false;
	}
	*/
	return true;
}

bool STCanScan::sendMessage(short cobID, unsigned char len, unsigned char *message)
{
	tCanMsgStruct frame;
	BYTE Status;
	unsigned char *buf = message; 
	frame.m_dwID = cobID;
	frame.m_bDLC = len;
	frame.m_bFF = 0;
	int l, l1;
	l = len;
	do {
		if (l > 8) {
			l1 = 8; l = l - 8;
		}
		else l1 = l;
		frame.m_bDLC = l1;
		memcpy(frame.m_bData,buf,l1);
		Status = UcanWriteCanMsgEx(m_UcanHandle,numChannel,&frame,NULL);
		if (Status != USBCAN_SUCCESSFUL) {
			break;
		}
		buf = buf + l1;
	}
	while (l > 8);
	return sendErrorCode(Status);
}

bool STCanScan::sendRemoteRequest(short cobID)
{
	tCanMsgStruct frame;
	BYTE Status;
	frame.m_dwID = cobID;
	frame.m_bDLC = 0;
	frame.m_bFF = USBCAN_MSG_FF_RTR;

	Status = UcanWriteCanMsgEx(m_UcanHandle,numChannel,&frame, NULL);
	return sendErrorCode(Status);
}

bool STCanScan::getErrorMessage(long error, char message[])
{
  char tmp[300];
// canGetErrorText((canStatus)error, tmp, sizeof(tmp));
// *message = new char[strlen(tmp)+1];	
    message[0] = 0;
// strcpy(*message,tmp);
  return true;
}
