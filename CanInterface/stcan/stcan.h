/*
 * stcan.h
 *
 *  Created on: Feb 22, 2012
 *      Author: vfilimon
 */

#ifndef CCANSTSCAN_H_
#define CCANSTVCAN_H_

#include "windows.h"
#include "tchar.h"

#ifdef WIN32
#include "Winsock2.h"
#endif

#include <string>
using namespace std;

#include "usbcan32.h"
#include "CCanAccess.h"

class STCanScan: public CCanAccess
{
public:
	STCanScan();
	virtual ~STCanScan();

	virtual bool createBUS(const char * ,const char *);
    virtual bool sendMessage(short cobID, unsigned char len, unsigned char *message);
	virtual bool sendRemoteRequest(short cobID);
	virtual bool getErrorMessage(long error, char message[]);

private:
	int numModule;
	int numChannel;
	int numCanHandle;
	tUcanHandle m_UcanHandle;

	string getNamePort() { return canName; }
	bool sendErrorCode(long);

	DWORD	bus_status;
	bool run_can;

	string canName;

#define Timeout  1000
	
    // The main control thread function for the CAN update scan manager.
    // Handle for the CAN update scan manager thread.

    HANDLE	m_hCanScanThread;

    // Thread ID for the CAN update scan manager thread.
    DWORD   m_idCanScanThread;

	static DWORD WINAPI CanScanControlThread(LPVOID pCanScan);

	bool configureCanboard(char *,char *,tUcanHandle&);
	static void setInUse(int n,bool t) { inUse[n] = t; }
	static bool isInUse(int n) { return inUse[n]; }
	static void setCanHandle(int n,tUcanHandle tU) { canHandle[n] = tU; }
	static tUcanHandle getCanHandle(int n) { return canHandle[n]; }

	static tUcanHandle canHandle[256];
	static bool inUse[256];


};

#endif