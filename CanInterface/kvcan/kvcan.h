/*
 * kvcan.h
 *
 *  Created on: Apr 5, 2011
 *      Author: vfilimon
 */

#ifndef CCANKVSCAN_H_
#define CCANSKVCAN_H_
#ifdef WIN32
#include "Winsock2.h"
#endif

#include <string>
using namespace std;

#include <canlib.h>
#include "CCanAccess.h"

class KVCanScan: public CCanAccess
{
public:
	KVCanScan();
	virtual ~KVCanScan();
	virtual bool createBUS(const char * ,const char *);
    virtual bool sendMessage(short cobID, unsigned char len, unsigned char *message);
	virtual bool sendRemoteRequest(short cobID);
	
	virtual bool getErrorMessage(long error, char **message);
private:

	string getNamePort() { return canName; }
	bool sendErrorCode(long);
//	virtual int getHandler();

	canStatus bus_status;
	bool run_can;
	unsigned int numChannel;

	string canName;

#define Timeout  1000

   	canHandle	canObjHandler;
	bool configureCanboard(const char *,const char *);
#ifdef WIN32
	static void CANLIBAPI CanScanControlThread(CanHandle hnd, void* context, unsigned int notifyEvent);
#else
	static void CANLIBAPI CanScanControlThread(canNotifyData *);
#endif


};

#endif