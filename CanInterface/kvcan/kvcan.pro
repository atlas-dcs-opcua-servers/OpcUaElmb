TEMPLATE = lib
TARGET   = kvcan
CONFIG  += shared thread debug_and_release
DESTDIR = ../../bin

CONFIG(debug, debug|release) {
#  DESTDIR = ../../bin
  OBJECTS_DIR = ./debug
}
CONFIG(release, debug|release) {
#  DESTDIR = ../../bin
  OBJECTS_DIR = ./release
}

QT -= qt core gui

INCLUDEPATH += ./../../Can/CanBusAccess

win32 {
  DEFINES += WIN32_LEAN_AND_MEAN
  DEFINES += _CRT_SECURE_NO_DEPRECATE
  INCLUDEPATH += $$(BOOST_ROOT)
  QMAKE_CXXFLAGS += /Zc:wchar_t
}

exists("C:/Program Files (x86)/Kvaser/CANLIB/Lib/MS/canlib32.lib") {
	INCLUDEPATH += "C:/Program Files (x86)/Kvaser/CANLIB/INC"
	LIBS += -L"C:/Program Files (x86)/Kvaser/CANLIB/Lib/MS"
}
exists("C:/Program Files/Kvaser/CANLIB/Lib/MS/canlib32.lib") {
	INCLUDEPATH += "C:/Program Files/Kvaser/CANLIB/INC"
	LIBS += -L"C:/Program Files/Kvaser/CANLIB/Lib/MS"
}
LIBS += -lcanlib32

HEADERS += ./kvcan.h
SOURCES += ./kvcan.cpp
