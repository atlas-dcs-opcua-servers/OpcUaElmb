// This is the main DLL file.

#include "kvcan.h"

//////////////////////////////////////////////////////////////////////
// kvCann.cpp: implementation of the KVCanScan class.
//////////////////////////////////////////////////////////////////////
//#include "CCanCallback.h"
#include <time.h>
#include <stdio.h>
#include <string.h>
#include "gettimeofday.h"

bool  initLibarary =  false;
#ifdef WIN32 
	extern "C" __declspec(dllexport) CCanAccess *getCanbusAccess()
#else
	extern "C" CCanAccess *getCanbusAccess()
#endif
	{
		CCanAccess *cc;
		cc = new KVCanScan;

		return cc;
	}

KVCanScan::KVCanScan()
{
	canInitializeLibrary();
	return ;
}

#ifdef WIN32
void CANLIBAPI KVCanScan::CanScanControlThread(int handle, void* pCanScan, unsigned int notifyEvent)
{
	canStatus Status;
	CanMsgStruct msg ;
   	unsigned long timeout = 1000;

   	KVCanScan *ppcs = reinterpret_cast<KVCanScan *>(pCanScan);
//	printf("Read Status %d\n",notifyEvent);
   	switch (notifyEvent) {
	case canNOTIFY_RX:
		unsigned int len,flag;
		unsigned long ttime;
		msg.c_id = 0;
		Status = canReadWait(handle,&msg.c_id,&msg.c_data,&len,&flag,&ttime,timeout);

		msg.c_time.tv_sec = ttime / 1000000;
		msg.c_time.tv_usec = ttime % 1000000;
		msg.c_dlc = len;
		msg.c_ff = flag;
		if (Status == canERR_NOMSG) 
		{
			break;
		}
		if (msg.c_ff & canMSG_ERROR_FRAME) {
			Status = canIoCtl(handle,canIOCTL_CLEAR_ERROR_COUNTERS,NULL,0);

			break;
		}
		if (msg.c_ff & canSTAT_ERROR_PASSIVE) {

			break;
		}

		if (Status == canOK) {
			ppcs->canMessageCame(msg);
//			printf("id = %d\n",msg.c_id);

		}
		else {
			ppcs->sendErrorCode(Status);
		}
	}

    return;
}
#else
void CANLIBAPI KVCanScan::CanScanControlThread(canNotifyData *cnd)
{
	canStatus Status;
	CanMsgStruct msg;
   	unsigned long timeout = 1000;
	unsigned int  dlc,ff;
   	KVCanScan *ppcs = reinterpret_cast<KVCanScan *>(cnd->tag);

   	switch (cnd->eventType ) {
	case canEVENT_RX:
		dlc = (unsigned int )msg.c_dlc;
		ff = (unsigned int )msg.c_ff;

		Status = canReadWait(ppcs->canObjHandler,&msg.c_id,&msg.c_data,&dlc,&ff,(unsigned long *)&msg.c_time,timeout);
		if (Status == canERR_NOMSG)
		{
			break;
		}
		if (msg.c_ff & canMSG_ERROR_FRAME) {
			Status = canIoCtl(ppcs->canObjHandler,canIOCTL_CLEAR_ERROR_COUNTERS,NULL,0);

			break;
		}
		if (msg.c_ff & canSTAT_ERROR_PASSIVE) {

			break;
		}

		if (Status == canOK) {
			ppcs->cb->FireOnChange(ppcs->canObjHandler,&msg);

		}
		else {
			ppcs->sendErrorCode(Status);
		}
	}

    return;
}
#endif

KVCanScan::~KVCanScan()
{
	run_can = false;
	canClose(canObjHandler);
}

bool KVCanScan::configureCanboard(const char *name,const char *parameters)
{
	canStatus   Status = canOK;

    unsigned int tseg1 = 0;
    unsigned int tseg2 = 0;
    unsigned int sjw = 0;
    unsigned int noSamp = 0;
    unsigned int syncmode = 0;
    unsigned int m_usedBaudRate = 125000;
//  unsigned long flag;
    
	int	 numPar; //check

	sscanf(name,"%d",&numChannel);
	if (parameters)
		numPar = sscanf(parameters,"%d %d %d %d %d %d",&m_usedBaudRate,&tseg1,&tseg2,&sjw,&noSamp,&syncmode);
	printf("Par %d %d %d %d %d %d\n",m_usedBaudRate,tseg1,tseg2,sjw,noSamp,syncmode);

	canObjHandler = canOpenChannel(numChannel,canOPEN_EXCLUSIVE | canOPEN_OVERRIDE_EXCLUSIVE | canOPEN_REQUIRE_INIT_ACCESS);
	if (canObjHandler >= 0) {
		printf("Handler = %d %d\n",canObjHandler,numChannel);
		switch(m_usedBaudRate) {
        case 1000000:
          m_usedBaudRate = BAUD_1M;
          break;
        case 500000:
          m_usedBaudRate = BAUD_500K;
          break;
        case 250000:
          m_usedBaudRate = BAUD_250K;
          break;
        case 125000:
          m_usedBaudRate = BAUD_125K;
          break;
        case 100000:
          m_usedBaudRate = BAUD_100K;
          break;
        case 62500:
          m_usedBaudRate = BAUD_62K;
          break;
        case 50000:
          m_usedBaudRate = BAUD_50K;
          break;
        default:
          printf("Baudrate set to 125 kbit/s. \n");
          m_usedBaudRate = BAUD_125K;
          break;
      }
 
		canSetBusParams(canObjHandler,m_usedBaudRate,tseg1,tseg2,sjw,noSamp,syncmode);
		Status = canIoCtl(canObjHandler,canIOCTL_FLUSH_RX_BUFFER,NULL,NULL);
		canBusOn(canObjHandler);
		
		bus_status = canOK;
#ifdef WIN32
		Status = kvSetNotifyCallback(canObjHandler,KVCanScan::CanScanControlThread,this,canNOTIFY_RX);
#else
		Status = canSetNotify(canObjHandler,KVCanScan::CanScanControlThread,canNOTIFY_RX,this);
#endif
		return true;
	}
	return false;
}

bool KVCanScan::sendErrorCode(long status)
{
	timeval ftTimeStamp; 
	if (status != bus_status) {
		gettimeofday(&ftTimeStamp,0);
		char *errMess;
		getErrorMessage(status,&errMess);
		canMessageError(status,errMess,ftTimeStamp );
		bus_status = (canStatus)status;

	}
	if (status != canOK) {
		status = canIoCtl(canObjHandler,canIOCTL_CLEAR_ERROR_COUNTERS,NULL,0);
		return false;
	}
	return true;
}

bool KVCanScan::sendMessage(short cobID, unsigned char len, unsigned char *message)
{
	canStatus Status;

	Status = canWrite(canObjHandler,cobID,message,len,0);
//	printf("Send %d Status %d\n",cobID,Status);
	if (Status != canOK) {
		if (Status == canERR_TXBUFOFL) {
			Status = canWriteSync(canObjHandler,Timeout); 
			if (Status == canOK) {
				Status = canWrite(canObjHandler,cobID,message,len,0);
			}
			else {
				Status = canFlushTransmitQueue(canObjHandler);  
			}
		}
	}
	
	return sendErrorCode(Status);
}

bool KVCanScan::sendRemoteRequest(short cobID)
{
	canStatus Status;
	char	msg[8];

	Status = canWrite(canObjHandler,cobID,msg,0,canMSG_RTR );
	if (Status != canOK) {
		if (Status == canERR_TXBUFOFL) {
			Status = canWriteSync(canObjHandler,Timeout); 
			if (Status == canOK) {
				Status = canWrite(canObjHandler,cobID,msg,0,canMSG_RTR);
			}
		}
	}
	return sendErrorCode(Status);
}

bool KVCanScan::createBUS(const char *name ,const char *parameters )
{
	if (!configureCanboard(name,parameters))
		return false;
	// Initialization for Scan
	return true;
}

bool KVCanScan::getErrorMessage(long error, char **message)
{
  char tmp[300];

  canGetErrorText((canStatus)error, tmp, sizeof(tmp));
  *message = new char[strlen(tmp)+1];	
  strcpy(*message,tmp);
  return true;
}
