#!/bin/bash

if [ "$1" == "" ]
then
  DEV="can0"
else
  DEV="$1"
fi

/sbin/ip link set $DEV type can bitrate 125000
/sbin/ifconfig $DEV up

