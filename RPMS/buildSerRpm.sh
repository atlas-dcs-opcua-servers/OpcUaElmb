#!/bin/bash

rm -rf checkout

mkdir checkout

EXPORT_GIT_PATH=ssh://git@gitlab.cern.ch:7999/atlas-dcs-opcua-servers/OpcUaElmb.git

if [ $# -eq 0 ]; then

	rev="$(git describe)"
	echo "Building from revision: $rev" 
	VERSION_STR="Built from revision: $rev"
	git clone --recurse-submodules $EXPORT_GIT_PATH checkout
	if [ $? -ne 0 ]; then
		exit -1
	fi
	TAG=$rev
	TAG_ARRAY=(${TAG//-/ })
	RPM_VERSION=${TAG_ARRAY[0]}
	RPM_RELEASE=${TAG_ARRAY[1]}
	cd checkout
elif [ $# -ne 2 ]; then
	echo "Usage: "
	echo "./buildRpm.sh --tag <tag name only numbers eg 1.0.3-9 >"
	echo "or"
	echo "./buildRpm.sh --revision <SVN revision number of trunk>"
	exit -1
elif [ $1 = "--tag" ]; then
	echo "Building from tag: $2"
	TAG=$2
	TAG_ARRAY=(${TAG//-/ })
	RPM_VERSION=${TAG_ARRAY[0]}
	RPM_RELEASE=${TAG_ARRAY[1]}
	VERSION_STR="Built from tag: $TAG"
	git clone --recurse-submodules $EXPORT_GIT_PATH --branch=$TAG checkout
	cd checkout
	if [ $? -ne 0 ]; then
		exit -1
	fi
elif [ $1 = "--fulltag" ]; then
	echo "Building from tag: $2"
	EXPORT_GIT_PATH_CI=https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/atlas-dcs-opcua-servers/OpcUaElmb.git
	TAG=$2
	TAG_ARRAY=(${TAG//-/ })
	RPM_VERSION=${TAG_ARRAY[0]}
	RPM_RELEASE=${TAG_ARRAY[1]}
	VERSION_STR="Built from tag: $TAG"
	git clone --recurse-submodules $EXPORT_GIT_PATH_CI --branch=$TAG checkout
	cd checkout

#	git checkout -b $TAG
	if [ $? -ne 0 ]; then
		exit -1
	fi
#	cd ..
else
	echo "--tag or --revision only allowed"
fi

./automated_build_linux.sh Release $TAG ..
git checkout master

