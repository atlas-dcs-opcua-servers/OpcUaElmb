#%define _binaries_in_noarch_packages_terminate_build   0
#%define _unpackaged_files_terminate_build 0
%define name OpcUaCanOpenServer
%define version 2.1.8
%define release 0 
%define place /opt/OpcUaCanOpenServer/bin
%define _topdir %(echo $PWD)
%define _tmpdir %{_topdir}/tmp

AutoReqProv: no 
Summary: OpcUaCanOpenServer
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}.tar.gz
License: zlib/libpng license
Group: Development/Application
BuildRoot: %{_tmpdir}/%{name}-%{version}-%{release}-buildroot
#BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildArch: x86_64
Prefix: %{_prefix}
Vendor: atlas-dcs@cern.ch


%description
OpcUaCanOpenServer

%prep
echo ">>> setup tag" 
echo %{name}

%setup -n %{name}

%build
#echo ">>> Build"

%install
echo "Installing ..." 
%{__rm} -rf %{buildroot}
echo  %{buildroot}
/bin/mkdir -p %{buildroot}%{place}
/bin/cp -a %{_topdir}/SOURCES/OpcUaCanOpenServer/* %{buildroot}%{place}

%pre
echo "preparing ..."

%post
echo "Post ..."
#execute commands
chmod u+s %{place}/%{name}

/bin/ln -f -s %{place}/libsockcan.so.1.0.2 %{place}/libsockcan.so.1.0
/bin/ln -f -s %{place}/libsockcan.so.1.0.2 %{place}/libsockcan.so.1
/bin/ln -f -s %{place}/libsockcan.so.1.0.2 %{place}/libsockcan.so
/bin/cp -a %{place}/OpcUaCanOpen.conf /etc/ld.so.conf.d

/sbin/ldconfig

%{place}/%{name} -cs

%preun
echo "Preun ..."

%postun
echo "Postun ..."

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%{place}

%changelog
