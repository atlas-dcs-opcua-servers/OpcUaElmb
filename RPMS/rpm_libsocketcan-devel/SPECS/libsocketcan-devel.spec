%define version 0.0.9 
# Author + Responsible:  Piotr Nikiel
%define name libsocketcan-devel 
%define release p0 
%define _topdir %(echo $PWD)
%define _tmpdir %{_topdir}/tmp
%define PREFIX /usr
%define buildroot %{_topdir}/BUILDROOT/


#AutoReqProv: no 
Summary: libsocketcan headers+libs 
Name: %{name}
Version: %{version}
Release: %{release}
Source0: libsocketcan-%{version}.tar.bz2
License: GPL 
Group: Development 
BuildArch: x86_64
BuildRoot: %{buildroot} 
Vendor: atlas-dcs@cern.ch
Prefix: %{PREFIX}

%description
libsocketcan devel package, that gives .a library that can be used to build shared objects (that is with PIC code).
The reason for a different build process from a classic configure-make is because .a generated wouldnt be PIC compiled.

%prep
echo %{_topdir}
%setup -n libsocketcan-%{version} 


%build
gcc -c -fPIC -O2 src/libsocketcan.c
ar cru libsocketcan.a libsocketcan.o

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/local/lib
cp libsocketcan.a $RPM_BUILD_ROOT/usr/local/lib
mkdir -p $RPM_BUILD_ROOT/usr/local/include
cd include
cp can_netlink.h libsocketcan.h $RPM_BUILD_ROOT/usr/local/include


%pre

%post

%preun

%postun

%clean

%files
%defattr(-,root,root)
%{PREFIX}

%changelog
