#!/bin/bash

rm -rf checkout

mkdir checkout
#read -s -p "Password: " PASSWORD
VERSION_STR=
#EXPORT_GIT_PATH=https://${USER}:${PASSWORD}@gitlab.cern.ch/atlas-dcs-opcua-servers/OpcUaElmb.git
EXPORT_GIT_PATH=ssh://git@gitlab.cern.ch:7999/atlas-dcs-opcua-servers/OpcUaElmb.git

if [ $# -eq 0 ]; then

	rev="$(git describe)"
	echo "Building from revision: $rev" 
	VERSION_STR="Built from revision: $rev"
	git clone --recurse-submodules $EXPORT_GIT_PATH checkout
	if [ $? -ne 0 ]; then
		exit -1
	fi
	TAG=$rev
	TAG_ARRAY=(${TAG//-/ })
	RPM_VERSION=${TAG_ARRAY[0]}
	RPM_RELEASE=${TAG_ARRAY[1]}
elif [ $# -ne 2 ]; then
	echo "Usage: "
	echo "./buildRpm.sh --tag <tag name only numbers eg 1.0.3-9 >"
	echo "or"
	echo "./buildRpm.sh --revision <SVN revision number of trunk>"
	exit -1
elif [ $1 = "--tag" ]; then
	echo "Building from tag: $2"
	TAG=$2
	TAG_ARRAY=(${TAG//-/ })
	RPM_VERSION=${TAG_ARRAY[0]}
	RPM_RELEASE=${TAG_ARRAY[1]}
	VERSION_STR="Built from tag: $TAG"
	git clone --recurse-submodules $EXPORT_GIT_PATH --branch=$TAG checkout
	cd checkout
#	git checkout -b OpcUaCanOpenServer-$TAG
#	git submodule update --recursive
	if [ $? -ne 0 ]; then
		exit -1
	fi
elif [ $1 = "--fulltag" ]; then
	echo "Building from tag: $2"
	EXPORT_GIT_PATH_CI=https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.cern.ch/atlas-dcs-opcua-servers/OpcUaElmb.git
	TAG=$2
	TAG_ARRAY=(${TAG//-/ })
	RPM_VERSION=${TAG_ARRAY[0]}
	RPM_RELEASE=${TAG_ARRAY[1]}
	VERSION_STR="Built from tag: $TAG"
	git clone --recurse-submodules $EXPORT_GIT_PATH_CI --branch=$TAG checkout
	cd checkout

#	git checkout -b $TAG
	if [ $? -ne 0 ]; then
		exit -1
	fi
#	cd ..
else
	echo "--tag or --revision only allowed"
fi
cd ..
VERSION_ADDITIONAL="\nBuilt by: $USER on `date` on $HOSTNAME arch=`arch` "
VERSION_STR="$VERSION_STR $VERSION_ADDITIONAL"

echo "#define VERSION_STR \"$VERSION_STR\"" > checkout/Server/src/Version.h
sed "s/<SoftwareVersion>[0-9.a-zA-Z]*<\/SoftwareVersion>/<SoftwareVersion>$VERSION_STR<\/SoftwareVersion>/ "  checkout/bin/ServerConfig.xml.linux > checkout/bin/ServerConfig.xml.linux.new
rm checkout/bin/ServerConfig.xml.linux
mv checkout/bin/ServerConfig.xml.linux.new checkout/bin/ServerConfig.xml.linux
rm -f rpmbuild/SOURCES/checkout.tar.gz
rm -f rpmbuild/SPECS/OpcUaCanOpenServer.spec
rm -Rf rpmbuild/BUILD/*
rm -Rf rpmbuild/BUILDROOT/*
tar cf rpmbuild/SOURCES/checkout.tar checkout/
gzip -1 rpmbuild/SOURCES/checkout.tar 
echo "%define version $RPM_VERSION" > rpmbuild/SPECS/OpcUaCanOpenServer.spec
echo "%define release $RPM_RELEASE" >> rpmbuild/SPECS/OpcUaCanOpenServer.spec
cat checkout/RPMS/build_from_svn_tag/OpcUaCanOpenServer.spec.template >> rpmbuild/SPECS/OpcUaCanOpenServer.spec
#rm -Rf checkout
cd rpmbuild
rpmbuild -bb SPECS/OpcUaCanOpenServer.spec
if [ $? -eq 0 ]; then
	echo "rpmbuild finished OK, your rpm should be here: `pwd`/rpmbuild/RPMS/x86_64/"
else
	echo "rpmbuild finished with error code, sorry, no RPM"
fi
git checkout master

